/*!
 * Bootstrap-select v1.12.2 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2017 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(root["jQuery"]);
  }
}(this, function (jQuery) {

(function ($) {
  $.fn.selectpicker.defaults = {
    noneSelectedText: '--------',
    noneResultsText: 'Sense resultats {0}',
    countSelectedText: 'Seleccionats {0} de {1}',
    maxOptionsText: ['Ha arribat al límit ({n} {var} max)', 'Ha arribat al límit del grup({n} {var} max)', ['elements', 'element']],
    multipleSeparator: ', ',
    selectAllText: 'Seleccionar Tots',
    deselectAllText: 'Desmarcar Tots'
  };
})(jQuery);


}));
