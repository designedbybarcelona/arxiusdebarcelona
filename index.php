<?php 
	
	ini_set('display_errors', 0);

	error_reporting(E_ALL ^ E_NOTICE);
	ini_set("display_errors", 1);
	
	require_once __DIR__.'/vendor/autoload.php';

	$app = new Silex\Application();

	// Detect environment (default: prod) by checking for the existence of $app_env
	if (isset($app_env) && in_array($app_env, array('prod','dev')))
	    $app['env'] = $app_env;
	else
    $app['env'] = 'prod';
	
	// WARNING: Disable this setting in production. Set it to false.
	$app['debug'] = true;

	require __DIR__.'/src/app.php';
	
	$app->run();