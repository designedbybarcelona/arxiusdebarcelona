<?php 
	
	// Variables Generals.
	require_once __DIR__.'/general.php';

	// Filtrar dades.

	if (isset($_REQUEST['o'])){
		$opcio = filter_var(htmlspecialchars(trim($_REQUEST['o'])), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($_REQUEST['id'])){
		$id = filter_var(htmlspecialchars(trim($_REQUEST['id'])), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($_REQUEST['t'])){
		$taula = filter_var(htmlspecialchars(trim($_REQUEST['t'])), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($_POST['p'])){
		$pagina = filter_var(htmlspecialchars(trim($_POST['p'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}
	if (isset($_REQUEST['c'])){
		$config = stripslashes_deep($_REQUEST['c']);
	}

	
	// Verificar dades.

	if(($opcio == "" || $id == "")) {
		exit();
	}

	/*
		Opció 2: Carregar Llistat.
		Opció 3: Valors a formulari (Modificar).
		Opció 4: Eliminar registre.
		Opció 7: Carregar form tipus d'actuació.
		Opció 8: Página de detall en modal.
		Opció 9: Carregar inputs pressupost i estat segons anys.
		Opció 10: Detalls actuacions.
		Opció 11: Consolidar pas.
		Opció 12: Projectes vinculació.
		Opció 14: Projectes districtes.
		Opció 15: Generar excel dels filtres.
		Opció 16: Generar PDF tipus.
		Opció 17: Generar informes tipus PDF.
		Opció 18: Guardar imatges a carpeta temporal.
		Opció 19: Modal per enviar mail als assistens / convocats.
		Opció 20: Enviar mails als assistens / convocats.
		Opció 21: Carregar formulari dels valors objectiu per N camps.
		Opció 22: Consulta, gràfics processos.
		Opció 23: Carregar formulari dels valors objectiu per N camps.
		Opció 30: Generar pdf pla de barris.
                Opció 32: MD5 Filtre.
                Opció 33: Gantts consultes.
                Opció 34: Linies estratègiques consultes.
                Opció 35: Modal última novetat.
                Opció 36: Modal detalls de les fites del projecte..
                Opció 37: Llistat projectes i actuacions, consultes.
                Opció 38: Actuacions cansultes.
                Opció 39: Inici Llistat projectes.
                Opció 40: Login IMI.
                Opció 41: Load operadors.
                Opció 42: Load arees.
                Opció 43: Load usuaris equip de treball a fases.
                Opció 44: Load noms responsables.
                Opció 45: Llistat d'actuacions i tasques.
                Opció 46: Àmbits al filtre.
                Opció 47: Gantt projecte individual.
                Opció 48: Recarregar calendari.
                Opció 49: Filtres serveis.
                Opció 50: Load usuaris equip de treball a accions.
                Opció 51: Generar informe calendari.
                Opció 52: Verificar les dates de les tasques que penjen de l'actuació.
	*/

	
	switch ($opcio) {

	case 2:
        	include("load/taules.php");
        break;

        case 3:
        	include("load/edicio.php");
        break;

        case 4:
        	include("load/eliminar.php");
        break;

        case 7:
        	include("load/7_actuacionstipusforms.php");
        break;

        case 8:
        	include("load/8_detallmodal.php");
        break;

        case 9:
        	include("load/9_dadesanys.php");
        break;

        case 10:
        	include("load/10_tascadetalls.php");
        break;

        case 11:
        	include("load/11_consolidarpas.php");
        break;

        case 12:
        	include("load/12_projectesvinculacio.php");
        break;

        case 14:
        	include("load/14_projectesdistrictes.php");
        break;

        case 15:
        	include("load/15_excelfiltres.php");
        break;

        case 16:
        	include("load/16_consultaindividualpdf.php");
        break;

        case 17:
        	include("load/17_generarinformetipus.php");
        break;

        case 18:
        	include("load/18_guardartmpfile.php");
        break;

        case 19:
        	include("load/19_modalenviarmailavis.php");
        break;

        case 20:
        	include("load/20_enviarmailavis.php");
        break;

        case 21:
        	include("load/21_valorsobjectiu.php");
        break;

        case 22:
        	include("load/22_consultagraficprocessos.php");
        break;

        case 23:
        	include("load/23_generarinformedintres.php");
        break;

        case 24:
        	include("load/24_consultagraficprocessosnou.php");
        break;

        case 25:
        	include("load/25_generarinformepbglobal.php");
        break;

        case 26:
        	include("load/26_actuacionsambits.php");
        break;

        case 27:
        	include("load/27_generarinformecalendari.php");
        break;

        case 28:
        	include("load/28_tasquescopiaragents.php");
        break;

        case 29:
        	include("load/29_tasquescopiaragents2.php");
        break;

        case 30:
                include("load/30_projectesaltres.php");
        break;

        case 31:
                include("load/31_projectesresponsables.php");
        break;

        case 32:
                include("load/32_md5filtre.php");
        break;

        case 33:
                include("load/33_consultagants.php");
        break;

        case 34:
                include("load/34_consultafiltrelinies.php");
        break;

        case 35:
                include("load/35_consultanovetatsproj.php");
        break;

        case 36:
                include("load/36_consultafitesproj.php");
        break;

        case 37:
                include("load/37_consultallistatproj.php");
        break;

        case 38:
                include("load/38_consultafiltreactuacions.php");
        break;

        case 39:
                include("load/39_inicillistatprojectes.php");
        break;

        case 40:
                include("load/40_loginimi.php");
        break;

        case 41:
                include("load/41_projectesoperadorsauto.php");
        break;

        case 42:
                include("load/42_projectesarees.php");
        break;

        case 43:
                include("load/43_accionsequipdetreball.php");
        break;

        case 44:
                include("load/44_nomsresponsables.php");
        break;

        case 45:
                include("load/45_projectesllistatactuacions.php");
        break;

        case 46:
                include("load/46_filtresambits.php");
        break;

        case 47:
                include("load/47_ganttprojecte.php");
        break;

        case 48:
                include("load/48_recarregarcalendari.php");
        break;

        case 49:
                include("load/49_filtresserveis.php");
        break;

        case 50:
                include("load/50_fasesequipdetreball.php");
        break;

        case 51:
                include("load/51_generarinformecalendari.php");
        break;

        case 52:
                include("load/52_actuacionsdates.php");
        break;

		
	}

	
	

	$dadesplantilla = array();
	return $dadesplantilla;

?>