<?php 

	// Busca pàgina per id.

	if (isset($idpagina) ) 
	{
		$idpagina = filter_var($idpagina, FILTER_SANITIZE_NUMBER_INT);
	}
	else{

		echo '
			<script>top.location.href = "'.$url.'/index.html";</script>
		';
		exit();

	} 

	// Variables Generals.
	require_once __DIR__.'/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	//Buscar Pàgina.

	$Pagines = $dbb->Pagines($idpagina);

	if ($Pagines != "" && !is_null($Pagines))
	{
		$srcdesti = $Pagines[file].".php";
		$urldesti = $Pagines[file].".html";

	}
	else
	{ 
		echo '
			<script>top.location.href = "'.$url.'/index.html";</script>
		';
		exit();
	}

	

