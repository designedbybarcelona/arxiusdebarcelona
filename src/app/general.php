<?php 

	if ($app['env'] == "dev") require_once __DIR__.'/../include/global.php';
	if ($app['env'] == "prod") require_once __DIR__.'/include/global.php';

	$url = $dbb["site"]["urlsite"];

	if (!$paginalogin)
	{
		/*
		echo '

			<script>
				top.location.href = "'.$url.'/login.html";
			</script>

		';
		header("Location: login.html");
		*/
		//exit();
	}

	$idiomes = array();
	$idiomes = explode(",", constant("DBB::idiomes"));
	$idioma = $app['session']->get('current_language');

	$dbb = new General($idioma, $app);

	// Pla a mostrar.
	if ($app['session']->get('idpla') == "" ) 
	{
		$app['session']->set('idpla', 1);
		$idpla = 1;
	}
	else
	{
		$idpla = $app['session']->get('idpla');
	}

	$quinaurl = $_SERVER['HTTP_HOST'];
	$host1 = explode('.', $quinaurl);
	$subdomain = $host1[0];

	
	// Menu.
	$Menu = $dbb->Menu();

	// Configuració del pla.
	$ConfigPla = $dbb->Llistats("plans"," AND t.id = :idpla",array("idpla"=>$idpla));
	
	// Nom usuari.
	$NomUsuari = $app['session']->get('current_user_nomcomplet');

	// General.
	$DadesGenerals = $dbb->Llistats("general","",array(),'id');

	// Any.
	$any = date('Y');

	// Paraules.
	$Paraules = $dbb->Llistats("paraules"," ","","clau");
	
	// Tocken per el formulari superior i lateral de cerca.
	if ($app['session']->get('tockenseguretatlogin') == "" ) {
		$app['session']->set('tockenseguretatlogin', makeToken());
	}

	// Cookies
	if(!isset($_COOKIE['eucookiepml']))
	{ 

		$cookieheader = '

			<script type="text/javascript">
				function SetCookie(c_name,value,expiredays)
				{
				var exdate=new Date()
					exdate.setDate(exdate.getDate()+expiredays)
					document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
				}
			</script>
			<script type="text/javascript">
				if( document.cookie.indexOf("eucookiepml") ===-1 ){
					$("#eucookielaw").show();
				}	
			    $("#removecookie").click(function () {
					SetCookie(\'eucookiepml\',\'eucookiepml\',365*10)
			      $("#eucookielaw").remove();
			    });

			</script>


		';

		$cookie = '
			
			 <style type="text/css">
		    <!--
		    #eucookielaw { display:none; position: fixed; bottom: 0px; width:100%; z-index:999;   }
		    -->
		    </style>
			<div id="eucookielaw" style="padding:10px; background:#3f3f3f; bottom:0px; color:#b5b5b5;">
			<p>'.$app['translator']->trans('base.alerts.txtcookies').'</p>
			<a id="removecookie" style="color:#e0e0e0; cursor:pointer;border: 0px solid; padding:4px; background:#858585;"><b>'.$app['translator']->trans('base.alerts.txtcookies2').'</b></a>
			</div>
			

		';
	
	} 

	// Array Permisos.
	$arraypermisos = $dbb->ArrayPermisos();


	$HeaderContent = $dbb->HeaderContent($idpagina);

	
	// Canvis
	$Canvis =  $dbb->FreeSql("SELECT distinct(t.id) as id, parent_id, clau, datai,
							   u.nom as nomresp, u.cognoms as cognomsresp, CASE tipus
																		        when 'a' then 'Alta'
																		        when 'm' then 'Modificació'
																		        when 'c' then 'Consolidar'
																		        else tipus
																		    end as tipus,
							  CASE t.titol_ca
							  	when 'dates' then t.descripcio_ca
							  END as detallcanvi
 
							  FROM pfx_canvis t
							  LEFT JOIN pfx_usuaris u ON t.clau_usuari = u.id
							  WHERE t.clau_pla = $idpla 
							  GROUP BY t.id
							  ORDER by datai desc");
	

	if (!$paginalogin)
	{
		$ConfigAlertes = $dbb->Llistats("confalertes"," ",array(),'parent_id asc',false);


		// Indicadors.
		$sessiopermissos = $app['session']->get(constant('General::nomsesiouser')."-permisos");
	
		if ( $sessiopermissos != 1 AND $app['session']->get(constant('General::nomsesiouser')) != "")
		{
			$condicio_indicadors = " WHERE t.responsable = '".$app['session']->get(constant('General::nomsesiouser'))."'";
			$condicio_indicadors2 = " AND t.responsable = '".$app['session']->get(constant('General::nomsesiouser'))."'";
			$condicio_indicadors3 = " AND (t.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))."
										OR
									 	 FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 
									 	)";
			$condicio_indicadors4 = " AND FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 ";
			
			$condicio_indicadors6 = " AND (p.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))."
										OR
									 	 FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 
									 	)";


		}

		/*
		$BustiesMenu =  $dbb->FreeSql("SELECT t.descripcio_ca as descripcio, t.id as id, DATE_FORMAT(inici, '%d/%m/%Y') as inici, prioritat, tipus,
									   CASE prioritat
									   		when '1' then
									            CASE
									            WHEN DATE(DATE_ADD(inici, INTERVAL 5 DAY)) < DATE(NOW())
									                THEN '1'
									                ELSE ''
									            END
									        when '2' then
									            CASE
									            WHEN DATE(DATE_ADD(inici, INTERVAL 10 DAY)) < DATE(NOW())
									                THEN '1'
									                ELSE ''
									            END
									        when '3' then
									            CASE
									            WHEN DATE(DATE_ADD(inici, INTERVAL 20 DAY)) < DATE(NOW())
									                THEN '1'
									                ELSE ''
									            END
									    END as colorpropera
									  FROM pfx_bustialocal t
									  WHERE t.status = 1 $condicio_indicadors2 
									  GROUP By t.id
									  ORDER BY t.titol_ca ",array());
									  */
		/*
		$ProjectesMenu =  $dbb->FreeSql("SELECT t.titol_ca as descripcio, t.id as id, DATE_FORMAT(t.final, '%d/%m/%Y') as final
										 FROM pfx_projectes t
										 WHERE 
										 	CASE
								            WHEN DATE(DATE_ADD(t.final, INTERVAL 1 DAY)) < DATE(NOW())
								                THEN '1'
								                ELSE ''
								            END
								            AND t.estat <> 4 $condicio_indicadors3 
										 GROUP By t.id
										 ORDER BY t.titol_ca ",array());

		$ActuacionsMenu =  $dbb->FreeSql("SELECT t.titol_ca as descripcio, t.id as id, DATE_FORMAT(t.final, '%d/%m/%Y') as final
										  FROM pfx_actuacions t
										  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
										  WHERE 
										 	CASE
								            WHEN DATE(DATE_ADD(t.final, INTERVAL 1 DAY)) < DATE(NOW())
								                THEN '1'
								                ELSE ''
								            END
								            $condicio_indicadors2 
										  GROUP By t.id
										  ORDER BY t.titol_ca ",array());

		$TasquesMenu =  $dbb->FreeSql("SELECT t.titol_ca as descripcio, t.id as id, DATE_FORMAT(t.final, '%d/%m/%Y') as final
									   FROM pfx_tasques t
									   INNER JOIN pfx_actuacions a ON a.id = t.clau_actuacio
									   INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
									   WHERE  
										 	CASE
								            WHEN DATE(DATE_ADD(t.final, INTERVAL 1 DAY)) < DATE(NOW())
								                THEN '1'
								                ELSE ''
								            END
								            $condicio_indicadors2
									   GROUP By t.id
									   ORDER BY t.titol_ca ",array());
		*/


		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 )
		{
			
			if ($app['session']->get(constant('General::nomsesiouser')."-permisos") == 3){
				$condicio_psensible = " AND p.sensible = 0 ";
				$condicio_psensible2 = " AND p2.sensible = 0 ";
			}

			$condiciousuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
		
			$condicio_permis_uvinc = $condicio_permis_idvinc = "";
			if (!empty($condiciousuarivinculacio) && $idpagina != 19){
				
				$primer = false; $lastElement = end($condiciousuarivinculacio);
				foreach ($condiciousuarivinculacio as $key_uv => $value_uv) 
				{
					if ($primer == false) { $condicio_permis_uvinc .= " AND ( "; $condicio_permis_idvinc .= " AND ( "; $primer = true; }
					$condicio_permis_uvinc .= " rp.id_act = '".$value_uv."'";
					$condicio_permis_idvinc .= " pv.id = '".$value_uv."'";
					if($value_uv == $lastElement) {  
						$condicio_permis_uvinc .= " ) "; $condicio_permis_idvinc .= " ) ";
					}else{ 
						$condicio_permis_uvinc .= " OR ";  $condicio_permis_idvinc .= " OR "; 
					}

				}

				
				
				
			}




			$condiciousuaricentres = $dbb->app['session']->get(constant('General::nomsesiouser')."-centres");
		
			$condicio_permis_centres = "";
			if (!empty($condiciousuaricentres) && $idpagina != 19){

				
				$primer = false; $lastElement = end($condiciousuaricentres);
				foreach ($condiciousuaricentres as $key_uv => $value_uv) 
				{
					if ($primer == false) { $condicio_permis_centres .= " AND ( ( "; $primer = true; }
					$condicio_permis_centres .= " FIND_IN_SET('".$value_uv."' ,REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"','')) > 0  ";
					if($value_uv == $lastElement) {  
						$condicio_permis_centres .= " )  OR p.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))."
														OR
									 	 				FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 
									 	)";
					}else{ 
						$condicio_permis_centres .= " OR "; 
					}

				}

				
				
				
			}

		}

		if (!empty($app['session']->get(constant('General::nomsesiouser')))){


			$ProjectesFitesMenu =  $dbb->FreeSql("SELECT left(t.observacionsfita,100) as descripcio, t.clau_projecte as id, 								DATE_FORMAT(t.datai, '%d/%m/%Y') as datai
											 FROM pfx_projectes_fites t
											 INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
											 WHERE 
											 	CASE
									            WHEN DATE(DATE_SUB(t.datai, INTERVAL ".$ConfigAlertes[4][diesi]." DAY)) <= DATE(NOW()) 
									                THEN '1'
									                ELSE ''
									            END
									            AND DATE(NOW()) <= DATE(t.datai)  

									             AND ( p.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))."
														OR
									 	 				FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 )

											 GROUP By t.id
											 ORDER BY t.observacionsfita ",array());


		}
		
								  
		
	}




	// Valors generals de l'array.
	$arraygeneral = array(

		'permisos' => $permisos,
		'idiomes' => $idiomes,
		'idioma' => $idioma,
		'pagina' => $idpagina,
		'headerTitle' => $HeaderContent["title"],
		'headerCodificacio' => $HeaderContent["codificacio"],
		'headerDescription' => $HeaderContent["description"],
		'headerKeywords' => $HeaderContent["keywords"],
		'headerAuthor' => $HeaderContent["author"],
		'headerOwner' => $HeaderContent["owner"],
		'headerRobots' => $HeaderContent["robots"],
		
		'cookieheader' => $cookieheader,
		'cookie' => $cookie,
		'subdomain' => $subdomain,

		'Menu' => $Menu,
		'ConfigPla' => $ConfigPla,
		'DadesGenerals' => $DadesGenerals,
		'NomUsuari' => $NomUsuari,
		'Paraules' => $Paraules,
		'arraypermisos' => $arraypermisos,
		'Canvis' => $Canvis,
		'ProjectesMenu' => $ProjectesMenu,
		'ActuacionsMenu' => $ActuacionsMenu,
		'TasquesMenu' => $TasquesMenu,
		'ProjectesFitesMenu' => $ProjectesFitesMenu,
		'UltimesTasquesMenu' => $UltimesTasquesMenu,
		'UltimesActuacionsMenu' => $UltimesActuacionsMenu,
		'UltimesFitesMarcMenu' => $UltimesFitesMarcMenu,
		'UltimesFitesProjmotorMenu' => $UltimesFitesProjmotorMenu,
		


	);
