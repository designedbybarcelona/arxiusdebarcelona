<?php

	if ($app['env'] == "dev") require_once __DIR__.'/../../../include/global.php';
	if ($app['env'] == "prod") require_once __DIR__.'/../../include/global.php';


	require('UploadHandler.php');

	// guardar a la base de dades.
	$db = new Db();
	$dbb = new General($idioma, $app);

	if($error_messages == null && !empty($_POST['t']) && !empty($_POST['ctf']) )
	{

		$coditaula = $dbb->Llistats("codis"," AND codi = :codi",array("codi"=>intval($_POST['t'])));
        if (!empty($coditaula))
        {
        	$nomtaula = $coditaula[1][nom];
        }

        if ($nomtaula == "portada"){
        	$upload_handler = new UploadHandler(); // per defecte hi ha posada la miniatura a images/logo.
        }else{
        	$upload_handler = new UploadHandler(array('image_versions' => array('')));
        }
        


		if ($id != 1 && !empty($id))
		{
			// Modificar.

			$db->query("UPDATE pfx_upfile 
						SET nomfile = :nomfile
						WHERE id = :id",
					array("id"=>intval($id),"nomfile" => $nomfile)
				);

		}
		else
		{
			// Alta.

			$data = array();
			//$data['clau_pla'] = $app['session']->get('idpla');
			$data['nomfile'] = $upload_handler->response[files][0]->name;
			$data['clau_id'] = intval($_POST['ctf']);
			$data['clau_prov'] = "";//;$id_prov;
			$data['taula'] = $nomtaula;
			$data['tipus'] = "";
			$data['estat'] = 1;

			$fields     =  array_keys($data);
			$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
			$sql 		= "INSERT INTO pfx_upfile (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")";
			
            $db->query("$sql",$data); 

		}
		
		
		

	}

	exit();
