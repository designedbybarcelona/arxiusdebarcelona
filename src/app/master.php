<?php

		
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Pàgines actives.

	$Dades .= "<h4>Pàgines actives</h4>";
	
	$Paginesactives =  $dbb->FreeSql("SELECT nom,file,codit,idpagina FROM pfx_pagines WHERE estat = 1 AND (file <> '' OR tipus = 1)",array());

	if (!empty($Paginesactives))
	{
		foreach ($Paginesactives as $key => $value) 
		{
			$Dades .= '<h5><u>'.$value[file].' ('.$value[nom].')</u></h5>';

			// Existeix fitxer?
			(file_exists(__DIR__.'/../../templates/'.$value[file].'.html')==1? $Dades .= '<span style="color:green;">OK HTML</span>': $Dades .='<span style="color:red;"><b>NO HTML</b></span>');

			$Dades .= ' <b>|</b> ';

			(file_exists(__DIR__.'/../pagines/'.$value[file].'.php')==1? $Dades .= '<span style="color:green;">OK PHP</span>': $Dades .='<span style="color:red;"><b>NO PHP</b></span>');

			$Dades .= ' <b>|</b> ';

			// Existeix registre a la BBDD de la taula (codis)?.
			$CodiT = $dbb->FreeSql("SELECT nom FROM pfx_codis WHERE codi = :codi",array('codi'=>$value[codit]));
			(!empty($CodiT)? $Dades .= '<span style="color:green;">OK CODIT</span>': $Dades .='<span style="color:red;"><b>NO CODIT</b></span>');			

			$Dades .= ' <b>|</b> ';

			// Existeix taula?.
			if (!empty($CodiT)) $TaulaID = $dbb->FreeSql("SELECT id FROM pfx_".$CodiT[1][nom]." LIMIT 1",array());
			(!empty($TaulaID)? $Dades .= '<span style="color:green;">OK TAULA</span>': $Dades .='<span style="color:red;"><b>NO TAULA</b></span>');
			$TaulaID = "";

			$Dades .= ' <b>|</b> ';

			// Existeix el load taula?
			$file = __DIR__.'/load/taules.php';
			$searchfor = '$taula == "'.$value[codit].'"';
			header('Content-Type: text/plain');
			$contents = file_get_contents($file);
			$pattern = preg_quote($searchfor, '/');
			$pattern = "/^.*$pattern.*\$/m";
			if(preg_match_all($pattern, $contents, $matches)){
			  $Dades .= '<span style="color:green;">OK LOAD TAULA</span>';
			}
			else{
			   $Dades .='<span style="color:red;"><b>NO LOAD TAULA</b></span>';
			}

			$Dades .= ' <b>|</b> ';

			// Existeix el load edició?
			$file = __DIR__.'/load/edicio.php';
			$searchfor = '$taula == "'.$value[codit].'"';
			header('Content-Type: text/plain');
			$contents = file_get_contents($file);
			$pattern = preg_quote($searchfor, '/');
			$pattern = "/^.*$pattern.*\$/m";
			if(preg_match_all($pattern, $contents, $matches)){
			  $Dades .= '<span style="color:green;">OK LOAD EDICIO</span>';
			}
			else{
			   $Dades .='<span style="color:red;"><b>NO LOAD EDICIO</b></span>';
			}

			$Dades .= ' <b>|</b> ';

			// Existeix el guardar?
			$file = __DIR__.'/guardar.php';
			$searchfor = '$idp == '.$value[idpagina].'';
			header('Content-Type: text/plain');
			$contents = file_get_contents($file);
			$pattern = preg_quote($searchfor, '/');
			$pattern = "/^.*$pattern.*\$/m";
			if(preg_match_all($pattern, $contents, $matches)){
			  $Dades .= '<span style="color:green;">OK GUARDAR</span>';
			}
			else{
			   $Dades .='<span style="color:red;"><b>NO GUARDAR</b></span>';
			}

			$Dades .= ' <b>|</b> ';

			// és correcte el codi de pàgina al form?
			if (!empty($value[file])){
				$file = __DIR__.'/../../templates/'.$value[file].'form.html';
				if (file_exists($file)){
					$searchfor =  'name="idp" value="'.$value[idpagina].'"'; //~ 8 %
					header('Content-Type: text/plain');
					$contents = file_get_contents($file);
					$pattern = preg_quote($searchfor, '/');
					$pattern = "/^.*$pattern.*\$/m";
					if(preg_match_all($pattern, $contents, $matches)){
					  $Dades .= '<span style="color:green;">OK CODI FORM</span>';
					}
					else{
					   $Dades .='<span style="color:red;"><b>NO CODI FORM</b></span>';
					}

					$Dades .= ' <b>|</b> ';
				}
			}
			 

			
		}
	}



	$dadesplantilla = array('Dades'=>$Dades);
	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;