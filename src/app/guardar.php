<?php
	
	$idpagina = 999;
	$paginaguardar = true;
	
	// Variables Generals.
	require_once __DIR__.'/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// TODO: mirar en aquest punt si l'usuari te permis d'escritura.


	// Recollir dades.
	$dades = array();
	
	if (isset($_POST['verif'])){
		$verif = filter_var(htmlspecialchars(trim($_POST['verif'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}
	if (isset($_POST['authToken'])){
		$authToken = htmlspecialchars(trim($_POST['authToken']));
	}
	if (isset($_POST['idp'])){
		$idp = filter_var(trim($_POST['idp']), FILTER_SANITIZE_NUMBER_INT);
	}
	

	// Verificar dades.

	/*
	if ($authToken != $app['session']->get('tockenseguretat') ) {
        echo $app['translator']->trans('base.alerts.introdades');  // Error.
		exit();
    }
    */
	if(!isset($verif) || $verif == ""){
		echo $app['translator']->trans('base.alerts.introdades');
		exit();
	}

	// Verificar ID's i Pàgina.
	$idok = $pagok = false;
	if (isset($_POST['id']) && isset($_POST['idx'])){
		if(empty($_POST['id']) || trim($_POST['idx']) == md5(constant('General::tockenid').trim($_POST['id']))){
			$idok = true;
		}
	}
	if (isset($_POST['idp']) && isset($_POST['idpx'])){
		if(trim($_POST['idpx']) == md5(constant('General::tockenid').trim($_POST['idp']))){
			$pagok = true;
		}
	}
	if ($idok == false || $pagok == false) { echo "<script>alert('Error al guardar!');</script>";exit();}


	/* 
		Dades Pla
	*/
	if ($idp == 11){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['tipus_pla'])){
			$tipus_pla = filter_var(htmlspecialchars(trim($_POST['tipus_pla'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["tipus_pla"] = $tipus_pla;
		}
		
		/*
		if (isset($_POST['relacionsuniques'])){
			$relacionsuniques = filter_var(htmlspecialchars(trim($_POST['relacionsuniques'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["relacionsuniques"] = $relacionsuniques;
		}
		else{
			$dades["relacionsuniques"] = 0;
		}*/
		if (isset($_POST['any'])){
			$any = $_POST['any'];
		    $dades['any_pla'] = $any;
		}
		

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'plans',
			'id'    =>  $id,
			'dades' =>  $dades,
			'tipus' =>  m,
		
		);

	}

	/* 
		Projectes
	*/
	elseif ($idp == 6){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_projectes ",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = "P".date(Y).$codi;

			// Abast territorial + EIX + LINIA + ACTUACIÓ + ID + 

			// Assignació automática de la vinculació desde el botó "nou projecte" de la home
			if (isset($_POST['idassignat'])){
				$idassignat = filter_var(htmlspecialchars(trim($_POST['idassignat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$idassignat = array($idassignat);
				$dades["vinculacio"] = json_encode($idassignat);
			}

			if (isset($_POST['vinculacio'])){
				$vinculacio = stripslashes_deep($_POST['vinculacio']);
				$dades["vinculacio"] = json_encode($vinculacio);
			}else{
				$dades['vinculacio'] = "";
			}

		}
		else
		{
			
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
			if (isset($_POST['vinculacio'])){
				$vinculacio = stripslashes_deep($_POST['vinculacio']);
				$dades["vinculacio"] = json_encode($vinculacio);
			}else{
				$dades['vinculacio'] = "";
			}
			
		}
		

		$dades["clau_pla"] = $idpla;
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if ($sessiopermissos == 1){
			if (isset($_POST['sensible'])){
				$sensible = filter_var(htmlspecialchars(trim($_POST['sensible'])), FILTER_SANITIZE_NUMBER_INT);
				$dades["sensible"] = $sensible;
			}else{
				$dades["sensible"] = "";
			}
		}
		if (isset($_POST['estrategic'])){
			$estrategic = filter_var(htmlspecialchars(trim($_POST['estrategic'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["estrategic"] = $estrategic;
		}else{
			$dades["estrategic"] = "";
		}
		if (isset($_POST['motivacio'])){
			$motivacio = (trim($_POST['motivacio']));
			$dades["motivacio"] = $motivacio;
		}
		if (isset($_POST['impactes'])){
			$impactes = (trim($_POST['impactes']));
			$dades["impactes"] = $impactes;
		}
		if (isset($_POST['recursos'])){
			$recursos = filter_var(htmlspecialchars(trim($_POST['recursos'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["recursos"] = $recursos;
		}
		if (isset($_POST['pesrelatiu'])){
			$pesrelatiu = filter_var(htmlspecialchars(trim($_POST['pesrelatiu'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pesrelatiu"] = $pesrelatiu;
		}
		if (isset($_POST['cap_projecte'])){
			$cap_projecte = filter_var(htmlspecialchars(trim($_POST['cap_projecte'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["cap_projecte"] = $cap_projecte;
		}
		if (isset($_POST['usuaris'])){
			$usuaris = stripslashes_deep($_POST['usuaris']);
			$dades["usuaris"] = json_encode($usuaris);
		}else{
			$dades['usuaris'] = "";
		}
		if (isset($_POST['ambits'])){
			$ambits = stripslashes_deep($_POST['ambits']);
			$dades["ambits"] = json_encode($ambits);
		}else{
			$dades['ambits'] = "";
		}
		if (isset($_POST['operadors'])){
			$operadors = stripslashes_deep($_POST['operadors']);
			$dades["operadors"] = json_encode($operadors);
		}else{
			$dades['operadors'] = "";
		}
		if (isset($_POST['arees'])){
			$arees = stripslashes_deep($_POST['arees']);
			$dades["arees"] = json_encode($arees);
		}else{
			$dades['arees'] = "";
		}
		if (isset($_POST['director'])){
			$director = filter_var(htmlspecialchars(trim($_POST['director'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["director"] = $director;
		}
		if (isset($_POST['adreca'])){
			$adreca = (trim($_POST['adreca']));
			$dades["adreca"] = $adreca;
		}
		if (isset($_POST['coordenades'])){
			$coordenades = filter_var(htmlspecialchars(trim($_POST['coordenades'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$coordenades = str_replace("(", "",str_replace(")", "", $coordenades));
			$coordenades = explode(",", $coordenades);
			$dades["lat"] = trim($coordenades[0]);
			$dades["lng"] = trim($coordenades[1]);
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0]");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['final'])){
			$final = filter_var(htmlspecialchars(trim($_POST['final'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			$dades["final"] = date("$final[2]-$final[1]-$final[0]");
		}
		else
		{
			$dades["final"] = "";
		}
		
		// Dades per anys.
		if (isset($_POST['totalagregat'])){
			$totalagregat = filter_var(htmlspecialchars(trim($_POST['totalagregat'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["totalagregat"] = $totalagregat;
		}
		else
		{
			$dades["totalagregat"] = "";
		}
		
		/*
		$pressupostos = array();
		//$totalpressupostcalc = 0;
		foreach ($_POST as $key => $value)
		{
			$findme   = 'pressupost_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$pressupostos[$key[1]] = $pres;
				//$totalpressupostcalc = $totalpressupostcalc + $pres;
			}
		}
		if (!empty($pressupostos))
		{
			$dades["pressupostos"] = json_encode($pressupostos);
		}
		$pressupostos1 = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'pressupost1_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$pressupostos1[$key[1]] = $pres;
			}
		}
		if (!empty($pressupostos1))
		{
			$dades["pressupostos1"] = json_encode($pressupostos1);
		}
		$pressupostos2 = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'pressupost2_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$pressupostos2[$key[1]] = $pres;
			}
		}
		if (!empty($pressupostos2))
		{
			$dades["pressupostos2"] = json_encode($pressupostos2);
		}
		/*
		if (isset($_POST['totalpressupost'])){
			$value = str_replace(".", "", $_POST['totalpressupost']);
			$totalpressupost = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			if (empty($totalpressupost) AND $totalpressupost != '0.00' AND $totalpressupost != '0') $totalpressupost = $totalpressupostcalc;
			$dades["totalpressupost"] = $totalpressupost;
		}*/
		// Dades per anys.
		/*
		$executats = array();
		//$totalexecutatcalc = 0;
		foreach ($_POST as $key => $value)
		{
			$findme   = 'executat_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$executats[$key[1]] = $pres;
				//$totalexecutatcalc = $totalexecutatcalc + $pres;
			}
		}
		if (!empty($executats))
		{
			$dades["executats"] = json_encode($executats);
		}
		$executats1 = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'executat1_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$executats1[$key[1]] = $pres;
			}
		}
		if (!empty($executats1))
		{
			$dades["executats1"] = json_encode($executats1);
		}
		$executats2 = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'executat2_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$executats2[$key[1]] = $pres;
			}
		}
		if (!empty($executats2))
		{
			$dades["executats2"] = json_encode($executats2);
		}
		/*
		if (isset($_POST['totalexecutat'])){
			$value = str_replace(".", "", $_POST['totalexecutat']);
			$totalexecutat = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			if (empty($totalexecutat) AND $totalexecutat != '0.00' AND $totalexecutat != '0') $totalexecutat = $totalexecutatcalc;
			$dades["totalexecutat"] = $totalexecutat;
		}*/
		/*
		$totals = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'total_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$totals[$key[1]] = $pres;
				//$totalexecutatcalc = $totalexecutatcalc + $pres;
			}
		}
		if (!empty($totals))
		{
			$dades["totals"] = json_encode($totals);
		}
		$totals2 = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'total2_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$totals2[$key[1]] = $pres;
				//$totalexecutatcalc = $totalexecutatcalc + $pres;
			}
		}
		if (!empty($totals2))
		{
			$dades["totals2"] = json_encode($totals2);
		}
		*/
		if (isset($_POST['districtes'])){
			$districtes = stripslashes_deep($_POST['districtes']);
			$dades["districtes"] = json_encode($districtes);
		}else{
			$dades['districtes'] = '["43"]'; // Per defecte posem el 43 (CIUTAT).
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else
		{
			$dades["estat"] = "2"; // No iniciat.
		}
		if (isset($_POST['tantpercent'])){
			$tantpercent = filter_var(htmlspecialchars(trim($_POST['tantpercent'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["tantpercent"] = $tantpercent;
		}
		if (isset($_POST['risc'])){
			$risc = (trim($_POST['risc']));
			$dades["risc"] = $risc;
		}
		if (isset($_POST['propostesrel'])){
			$propostesrel = (trim($_POST['propostesrel']));
			$dades["propostesrel"] = $propostesrel;
		}

		
		
		
		if (!empty($id))
		{
			// Guardar ID's anteriors de cap de projecte i usuaris per enviar mails si s'han canviat.
			/*
			$Projecte = $dbb->Llistats("projectes"," AND id = :id" ,array("id"=>$id), "id", false );
			if (!empty($Projecte))
			{
				$dades['avis'][cap_projecte] = $Projecte[1][cap_projecte];
				$dades['avis'][usuaris] = $Projecte[1][usuaris];

				// canvi % execució.
				if ($Projecte[1][tantpercent] != $tantpercent){
					$dbb->Canvis_proj(array('taula' => 1, 'tipus' => 'm', 'clau' => $id));
				}
				
			}
			*/

			// Guardar canvis en dates inici, final.
			$Projecte = $dbb->Llistats("projectes"," AND id = :id" ,array("id"=>$id), "id", false );
			if (!empty($Projecte))
			{
				if ($Projecte[1][inici] != $dades["inici"] || $Projecte[1]["final"] != $dades["final"])
				{
					$dadesdescripcio['previ'] = array($Projecte[1]["inici"], $Projecte[1]["final"]);
					$dadesdescripcio['modificat'] = array($dades["inici"],$dades["final"]);
					$dbb->Canvis(array('taula' => 1, 'tipus' => 'm', 'clau' => $id, 'titol_ca' => 'dates','descripcio_ca' => json_encode($dadesdescripcio) ));
				}

				if (1==2 and $Projecte[1][codi]=="")
				{
					// CODI PROJECTE
					if (!empty($vinculacio)){
						foreach ($vinculacio as $key_v => $value_v) {
							if (!empty($condeixos)) $condeixos .= " OR ";
							$valorpam = explode("_", $value_v);
							$condeixos .= " (id = ".intval($valorpam[1])." AND nivell = 4)";
						}
						$DadesVinc = $dbb->Llistats("projectes_vinculacio"," AND $condeixos " ,array(), "id", false );
						if (!empty($DadesVinc)){
							$clau_pam = $DadesVinc[1][clau_pam];
							$DadesPam = $dbb->Llistats("projectes_vinculacio"," AND id = '$clau_pam'  " ,array(), "id", false );
							$nompam = $DadesPam[1][titol_ca];
							$pos = strpos($nompam , '.');
							$part1 = substr($nompam , 0, $pos);

							/*
							$clau_eix = $DadesVinc[1][clau_eix];
							$DadesEix = $dbb->Llistats("projectes_vinculacio"," AND id = '$clau_eix'  " ,array(), "id", false );
							$nomeix = str_replace("EIX", "", $DadesEix[1][titol_ca]);
							$pos = strpos($nomeix , '.');
							$part2 = substr($nomeix , 0, $pos);
							*/

							$clau_linia = $DadesVinc[1][parent_id];
							$DadesLin = $dbb->Llistats("projectes_vinculacio"," AND id = '$clau_linia'  " ,array(), "id", false );
							$nomlinea = $DadesLin[1][titol_ca];
							$primers8 = substr($nomlinea, 0, 8);
							$part3 = substr($primers8, 0, strrpos($primers8, '.'));
							

							$nomvinc = $DadesVinc[1][titol_ca];
							$primers8 = substr($nomvinc, 0, 8);
							$part4 = substr($primers8, 0, strrpos($primers8, '.'));

							if ($part4==0){ // Sense vinculació directa posem el codi de la linea davant el 0 de l'actuació 0.
								$part4 = $part3.$part4;
							}
						}
					}

					// Si alguna part està en blanc, no guarda.
					if ($part1==""||$part4==""){
						/*
						echo '
							<script>
							    $("div.errorprojectes").show();
								$("div.errorprojectes").html("<div class=\"alert alert-dismissable alert-danger\">Atenció! Els canvis no es guardaran, selecciona tots els camps obligatoris.</div>");
							</script>
						';
						exit();
						*/
					}

					$nouid = sprintf("%05d", $id);
				    $dades["codi"] = $part1."".str_replace(".", "", trim($part4))."_".$nouid;	


				}
				

				
			}
		}
		

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

		

	}

	/* 
		Actuacions
	*/
	elseif ($idp == 7){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_actuacions WHERE clau_pla = $idpla");
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = "A".date(Y).$codi;
		}
		else
		{
			
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
			
		}

		$dades["clau_pla"] = $idpla;
		if (isset($_POST['clau_projecte'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['clau_projecte'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['final'])){
			$final = filter_var(htmlspecialchars(trim($_POST['final'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			$dades["final"] = date("$final[2]-$final[1]-$final[0] 00:00:00");
		}
		else
		{
			$dades["final"] = "";
		}
		/*
		// Dades per anys.
		$pressupostos = array();
		foreach ($_POST as $key => $value)
		{
			$findme   = 'pressupost_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$pressupostos[$key[1]] = $pres;
			}
		}
		if (!empty($pressupostos))
		{
			$dades["pressupostos"] = json_encode($pressupostos);
		}
		*/
		if (isset($_POST['usuaris'])){
			$usuaris = stripslashes_deep($_POST['usuaris']);
			$dades["usuaris"] = json_encode($usuaris);
		}else{
			$dades['usuaris'] = "";
		}
		if (isset($_POST['operadors'])){
			$operadors = stripslashes_deep($_POST['operadors']);
			$dades["operadors"] = json_encode($operadors);
		}else{
			$dades['operadors'] = "";
		}
		if (isset($_POST['districtes'])){
			$districtes = stripslashes_deep($_POST['districtes']);
			$dades["districtes"] = json_encode($districtes);
		}else{
			$dades['districtes'] = '["43"]'; // Per defecte posem el 43 (CIUTAT).
		}
		if (isset($_POST['adreca'])){
			$adreca = (trim($_POST['adreca']));
			$dades["adreca"] = $adreca;
		}
		if (isset($_POST['coordenades'])){
			$coordenades = filter_var(htmlspecialchars(trim($_POST['coordenades'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$coordenades = str_replace("(", "",str_replace(")", "", $coordenades));
			$coordenades = explode(",", $coordenades);
			$dades["lat"] = trim($coordenades[0]);
			$dades["lng"] = trim($coordenades[1]);
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}





		if (!empty($id))
		{
			// Guardar ID's anteriors de responsable per enviar mails si s'han canviat.
			/*
			$Actuacio = $dbb->Llistats("actuacions"," AND id = :id", array("id"=>$id), "id", false );
			if (!empty($Actuacio))
			{
				$dades['avis'][responsable] = $Actuacio[1][responsable];
			}
			*/


			// Verificar si hi ha tasques amb dates més petites o mes grans i modificar automàticament.

			$RegistreTasques = $dbb->Llistats("tasques"," AND t.clau_actuacio = :id ",array("id"=>$id), "id", false);	

			if (!empty($RegistreTasques) && !empty($dades['inici']) && !empty($dades['final']) )
			{

				$inicitime = strtotime($dades['inici']);
			    $finaltime = strtotime($dades['final']);

			    foreach ($RegistreTasques as $key => $value) {
			    	
			    	$dadesr = array();

			    	$inicitasca = strtotime($value['inici']);
			    	$finaltasca = strtotime($value['final']);

			    	if ($inicitasca < $inicitime){
			    		$dadesr['inici'] = $dades['inici'];
			    	}

			    	if ($finaltasca > $finaltime){
			    		$dadesr['final'] = $dades['final'];
			    	}

			    	if (!empty($dadesr))
			    	{
			    		$arrayguardarr = array('taula' =>  'tasques', 
												'id'  => $value['id'],
			 								  	'dades' =>  $dadesr,
											  	'tipus' =>  m,);

						$dbb->GuardarRegistre2($arrayguardarr);
			    	}
			    	

			    }

				

			}

		}
		

				
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'actuacions',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Tasques
	*/
	elseif ($idp == 8){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		if (isset($_POST['clau_projecte'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['clau_projecte'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		if (isset($_POST['clau_actuacio'])){
			$clau_actuacio = filter_var(htmlspecialchars(trim($_POST['clau_actuacio'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_actuacio"] = $clau_actuacio;
		}
		if (isset($_POST['clau_actuacio2'])){
			$clau_actuacio2 = filter_var(htmlspecialchars(trim($_POST['clau_actuacio2'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_actuacio2"] = $clau_actuacio2;
		}
		

		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_tasques WHERE clau_pla = $idpla");
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = "T".date(Y).$codi;//"$clau_projecte"."_"."$clau_actuacio"."_".$codi;
		}
		else
		{
			
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
			/*
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_tasques WHERE id=$id AND clau_pla = $idpla");
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = "T".date(Y).$codi[2];//"$clau_projecte"."_"."$clau_actuacio"."_".$codi[2];
			*/
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}

		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['final'])){
			$final = filter_var(htmlspecialchars(trim($_POST['final'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			$dades["final"] = date("$final[2]-$final[1]-$final[0] 00:00:00");
		}
		else
		{
			$dades["final"] = "";
		}
		if (isset($_POST['usuaris'])){
			$usuaris = stripslashes_deep($_POST['usuaris']);
			$dades["usuaris"] = json_encode($usuaris);
		}else{
			$dades['usuaris'] = "";
		}
		if (isset($_POST['operadors'])){
			$operadors = stripslashes_deep($_POST['operadors']);
			$dades["operadors"] = json_encode($operadors);
		}else{
			$dades['operadors'] = "";
		}

		if (isset($_POST['districtes'])){
			$districtes = stripslashes_deep($_POST['districtes']);
			$dades["districtes"] = json_encode($districtes);
		}else{
			$dades['districtes'] = '["43"]'; // Per defecte posem el 43 (CIUTAT).
		}
		if (isset($_POST['adreca'])){
			$adreca = (trim($_POST['adreca']));
			$dades["adreca"] = $adreca;
		}
		if (isset($_POST['coordenades'])){
			$coordenades = filter_var(htmlspecialchars(trim($_POST['coordenades'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$coordenades = str_replace("(", "",str_replace(")", "", $coordenades));
			$coordenades = explode(",", $coordenades);
			$dades["lat"] = trim($coordenades[0]);
			$dades["lng"] = trim($coordenades[1]);
		}

		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}
		/*
		// Dades per anys.
		$pressupostos = array();
		$costeconomiccalc = 0;
		foreach ($_POST as $key => $value)
		{
			$findme   = 'pressupost_';
			$pos = strpos($key, $findme);
			if ($pos !== false) 
			{
				$key = explode('_', $key);
				$value = str_replace(".", "", $value);
				$pres = filter_var(htmlspecialchars(trim($value)), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$pressupostos[$key[1]] = $pres;
				$costeconomiccalc = $costeconomiccalc + $pres;
			}
		}
		if (!empty($pressupostos))
		{
			$dades["pressupostos"] = json_encode($pressupostos);
		}
		*/
		


		if (!empty($id))
		{	
			/*
			// Guardar ID's anteriors de responsable per enviar mails si s'han canviat.
			$Tasca = $dbb->Llistats("tasques"," AND id = :id" , array("id"=>$id),"id", false );
			if (!empty($Tasca))
			{
				$dades['avis'][responsabletasca] = $Tasca[1][responsable];
			}
			*/
		}

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'tasques',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Usuaris.
	*/
	elseif ($idp == 3){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
				$condicioid = " AND id <> $id ";
			}else{
				$tipus = a;
				$condicioid = "";
			}
			
		}else{
			$tipus = a;
		}
		if (isset($_POST['clau_permisos'])){
			$clau_permisos = filter_var(htmlspecialchars(trim($_POST['clau_permisos'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_permisos"] = $clau_permisos;
		}
		/*
		if (isset($_POST['clau_pla'])){
			$clau_pla = filter_var(htmlspecialchars(trim($_POST['clau_pla'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_pla"] = $clau_pla;
		}
		*/
		/*
		if(isset($_POST['plans'])) 
		{
		    $plans = $_POST['plans'];
		    $dades['plans'] = array_unique($plans);
		}else{
			$dades['plans'] = "";
		}
		*/
		$dades['clau_pla'] = array("1");

		if (isset($_POST['nom'])){
			$nom = filter_var(htmlspecialchars(trim($_POST['nom'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["nom"] = $nom;
		}
		if (isset($_POST['cognoms'])){
			$cognoms = filter_var(htmlspecialchars(trim($_POST['cognoms'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["cognoms"] = $cognoms;
		}
		if (isset($_POST['telefon'])){
			$telefon = filter_var(htmlspecialchars(trim($_POST['telefon'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["telefon"] = $telefon;
		}
		if (isset($_POST['carrec'])){
			$carrec = filter_var(htmlspecialchars(trim($_POST['carrec'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["carrec"] = $carrec;
		}
		if (isset($_POST['email'])){
			$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["email"] = $email;
			$dades["usuari"] = $email;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		if (isset($_POST['organse'])){
			$organse = stripslashes_deep($_POST['organse']);
			$dades["organse"] = json_encode($organse);
		}else{
			$dades['organse'] = "";
		}
		/*
		if (isset($_POST['vinculacio'])){
			$vinculacio = stripslashes_deep($_POST['vinculacio']);
			$dades["vinculacio"] = json_encode($vinculacio);
		}else{
			$dades['vinculacio'] = "";
		}
		*/

		if ( isset($_POST['passw']) && isset($_POST['passw2']))
		{
			if ($_POST['passw'] == $_POST['passw2'] && $_POST['passw'] != "")
			{
				//Genero un salt aleatori
				/*
				$salt = substr(hash('whirlpool',microtime()),rand(0,105),22);
				$pass = trim($_POST['passw']);
				$i = rand(0,22);  //22 per la longitud del salt.
			    $encryptedpass = crypt($pass,'$2a$07$' . substr($salt, 0, $i));
			    */

				$dades['password'] = md5(trim($_POST['passw']));
				$dades['txtpass'] = base64_encode(trim($_POST['passw']));
			}
		}


		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}

		// Verificar repetits.
		if ($dbb->Llistats("usuaris"," AND usuari = :usuari $condicioid",array("usuari"=>$email,), "nom", false) != false)
		{
			echo '
				<script>
					$("#resultusuaris").html("<div class=\"alert alert-dismissable alert-danger\">Atenció! Existeix un usuari amb el mateix email. Els canvis no es guardaran.</div>");
				</script>
			';
			exit();
		}

		

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'usuaris',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Dimensions Projectes
	*/
	elseif ($idp == 22){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_dimensions',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Tipologies Fites Projectes
	*/
	elseif ($idp == 23){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_fites_tipologies',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Passos
	*/
		/*
	elseif ($idp == 24){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		if (isset($_POST['e'])){
			$e = filter_var(htmlspecialchars(trim($_POST['e'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $e;
		}
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_tasca"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'passos',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}
	*/

	/* 
		Objectius Projectes
	*/
	elseif ($idp == 25){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_objectius',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Indicadors Projectes
	*/
	elseif ($idp == 26){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['inicial'])){
			$inicial = filter_var(htmlspecialchars(trim($_POST['inicial'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["inicial"] = $inicial;
		}
		if (isset($_POST['actual'])){
			$actual = filter_var(htmlspecialchars(trim($_POST['actual'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["actual"] = $actual;
		}
		if (isset($_POST['objectiu'])){
			$objectiu = filter_var(htmlspecialchars(trim($_POST['objectiu'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["objectiu"] = $objectiu;
		}
		
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_indicadors',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Vinculació estratègica
	*/
	elseif ($idp == 24){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		$nivell = 1;
		$parent_id = 0;
		if (isset($_POST['nivell'])){
			$nivell = filter_var(htmlspecialchars(trim($_POST['nivell'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["nivell"] = $nivell;
		}
		if ($nivell != 1 && $nivell != 2 && $nivell != 4)
		{
			if (isset($_POST['parent_id'])){
				$parent_id = filter_var(htmlspecialchars(trim($_POST['parent_id'])), FILTER_SANITIZE_NUMBER_INT);
			}
		}
		if ($nivell == 2)
		{
			if (isset($_POST['parents_ids'])){
				$parents_ids = stripslashes_deep($_POST['parents_ids']);
				$dades["parents_ids"] = json_encode($parents_ids);
			}else{
				$dades['parents_ids'] = "";
			}
		}
		if ($nivell == 4)
		{
			if (isset($_POST['parent_pam'])){
				$dades["clau_pam"] = filter_var(htmlspecialchars(trim($_POST['parent_pam'])), FILTER_SANITIZE_NUMBER_INT);
			}else{
				$dades["clau_pam"] = "";
			}
			if (isset($_POST['parent_eix'])){
				$dades["clau_eix"] = filter_var(htmlspecialchars(trim($_POST['parent_eix'])), FILTER_SANITIZE_NUMBER_INT);
			}else{
				$dades["clau_eix"] = "";
			}
			if (isset($_POST['parent_id'])){
				$parent_id = filter_var(htmlspecialchars(trim($_POST['parent_id'])), FILTER_SANITIZE_NUMBER_INT);
			}
		} 
		$dades["parent_id"] = $parent_id;
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		if (isset($_POST['clau_cero'])){
			$clau_cero = filter_var(htmlspecialchars(trim($_POST['clau_cero'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["clau_cero"] = $clau_cero;
		}else{
			$dades["clau_cero"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		/*
			Control per evitar que canviin relacions si encara penjen actuacions d'elles.
			Si és un Eix, linia del que penjen actuacions no es pot modificar d'on penjen.
		*/
		if (!empty($id) && ($nivell == 2 || $nivell == 3) ){

			$Relacionspam = $dbb->Llistats("projectes_vinculacio"," AND id = :id" ,array("id"=>$id), "id", false );
			if ($nivell == 2) {
				// Si desmarquem algún PAM/PAD buscar actuacions en el "desmarcat"

				if (!empty($Relacionspam[1][parents_ids])){
					$idsrelacions = json_decode($Relacionspam[1][parents_ids],true);
					$buscaeliminat= array_diff($idsrelacions,$parents_ids);
					if (!empty($buscaeliminat)){
						$condicioclaupam = "";
						foreach ($buscaeliminat as $key_r => $value_r) {
							$condicioclaupam .= " AND clau_pam = '$value_r' ";
						}
						$Relacions = $dbb->Llistats("projectes_vinculacio"," $condicioclaupam AND nivell = 4 AND clau_eix = :id" ,array("id"=>$id), "id", false );
					}
				}
			}
			if ($nivell == 3) {
				if (!empty($Relacionspam[1][parent_id])){
					// si ha variat el parent_id busquem si te relacionats.
					if ($Relacionspam[1][parent_id] != $dades[parent_id]){
						$Relacions2 = $dbb->Llistats("projectes_vinculacio"," AND nivell = 4 AND parent_id = :id" ,array("id"=>$id), "id", false );
					}
				} 
			}

			if (!empty($Relacions) || !empty($Relacions2))
			{
				echo '
					<script>
						$("#resultprojectesvinc").html("<div class=\"alert alert-dismissable alert-danger\">Atenció! Existeixen PROGRAMES relacionats, canvia la vinculació d\'aquestes. Els canvis no es guardaran.</div>");
					</script>
				';
				exit();
			}
		}

		// Si és una linea creem automàticament una Actuació 0 que depengui d'ella.
		// Es fa desprès de la creació pq ja tenim l'id.


		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_vinculacio',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Organs de participació i seguiment AD HOC
	*/
	elseif ($idp == 28){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['identipart'])){
			$identipart = filter_var(htmlspecialchars(trim($_POST['identipart'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["identipart"] = $identipart;
		}
		if (isset($_POST['dataop'])){
			$dataop = filter_var(htmlspecialchars(trim($_POST['dataop'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dataop = explode("/",$dataop);
			$dades["dataop"] = date("$dataop[2]-$dataop[1]-$dataop[0] 00:00:00");
		}
		else
		{
			$dades["dataop"] = "";
		}
		if (isset($_POST['dataop2'])){
			$dataop2 = filter_var(htmlspecialchars(trim($_POST['dataop2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dataop2 = explode("/",$dataop2);
			$dades["dataop2"] = date("$dataop2[2]-$dataop2[1]-$dataop2[0] 00:00:00");
		}
		else
		{
			$dades["dataop2"] = "";
		}
		if (isset($_POST['observacionsop'])){
			$observacionsop = (trim($_POST['observacionsop']));
			$dades["observacionsop"] = $observacionsop;
		}
		
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_organs',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Districtes / Barris
	*/
	elseif ($idp == 29){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		$nivell = 1;
		$parent_id = 0;
		if (isset($_POST['nivell'])){
			$nivell = filter_var(htmlspecialchars(trim($_POST['nivell'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["nivell"] = $nivell;
		}
		if ($nivell != 1)
		{
			if (isset($_POST['parent_id'])){
				$parent_id = filter_var(htmlspecialchars(trim($_POST['parent_id'])), FILTER_SANITIZE_NUMBER_INT);
			}
		}
		$dades["parent_id"] = $parent_id;
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_districtes',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Òrgans executius (responsables de projecte)
	*/
	elseif ($idp == 30){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		$nivell = 1;
		$parent_id = 0;
		if (isset($_POST['nivell'])){
			$nivell = filter_var(htmlspecialchars(trim($_POST['nivell'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["nivell"] = $nivell;
		}
		if ($nivell != 1)
		{
			if (isset($_POST['parent_id'])){
				$parent_id = filter_var(htmlspecialchars(trim($_POST['parent_id'])), FILTER_SANITIZE_NUMBER_INT);
			}
		}else{
			if (isset($_POST['vinculacio'])){
				$vinculacio = stripslashes_deep($_POST['vinculacio']);
				$dades["vinculacio"] = json_encode($vinculacio);
			}else{
				$dades['vinculacio'] = "";
			}
		}
		$dades["parent_id"] = $parent_id;
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}

		if (isset($_POST['acronim'])){
			$acronim = filter_var(htmlspecialchars(trim($_POST['acronim'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["acronim"] = $acronim;
		}
		if (isset($_POST['adreca'])){
			$adreca = (trim($_POST['adreca']));
			$dades["adreca"] = $adreca;
		}
		if (isset($_POST['coordenades'])){
			$coordenades = filter_var(htmlspecialchars(trim($_POST['coordenades'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$coordenades = str_replace("(", "",str_replace(")", "", $coordenades));
			$coordenades = explode(",", $coordenades);
			$dades["lat"] = trim($coordenades[0]);
			$dades["lng"] = trim($coordenades[1]);
		}
		if (isset($_POST['telefon'])){
			$telefon = filter_var(htmlspecialchars(trim($_POST['telefon'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["telefon"] = ($telefon);
		}
		if (isset($_POST['email'])){
			$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["email"] = ($email);
		}
		if (isset($_POST['web'])){
			$web = filter_var(htmlspecialchars(trim($_POST['web'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["web"] = ($web);
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = ($descripcio_ca);
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		if (isset($_POST['vinculacio'])){
			$vinculacio = stripslashes_deep($_POST['vinculacio']);
			$dades["vinculacio"] = json_encode($vinculacio);
		}else{
			$dades['vinculacio'] = "";
		}

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_organs_e',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Agents
	*/
	elseif ($idp == 31){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_agents',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Rols.
	*/
	elseif ($idp == 32){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if(isset($_POST['permisosd'])) 
		{
		    $permisosd = $_POST['permisosd'];
		    $dades['permisosd'] = $permisosd;
		}else{
			$dades['permisosd'] = "";
		}
		

		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}



		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'permisos',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Plens
	*/
	elseif ($idp == 41){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_plens",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_plens WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipologia"] = $tipologia;
		}
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['convocats'])){
			$convocats = stripslashes_deep($_POST['convocats']);
			$dades["convocats"] = json_encode($convocats);
		}else{
			$dades['convocats'] = "";
		}
		if (isset($_POST['altresassistents'])){
			$altresassistents = (trim($_POST['altresassistents']));
			$dades["altresassistents"] = $altresassistents;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'plens',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Àrees d'actuació municipal.
	*/
	elseif ($idp == 42){

		$dbb->AreaPrivada($idp);

		//// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'plens_arees',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Decrets d'alcaldia
	*/
	elseif ($idp == 43){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_decrets",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_decrets WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'decrets',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Comissió permament
	*/
	elseif ($idp == 44){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_comissiopermanent",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_comissiopermanent WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['convocats'])){
			$convocats = stripslashes_deep($_POST['convocats']);
			$dades["convocats"] = json_encode($convocats);
		}else{
			$dades['convocats'] = "";
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'comissiopermanent',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Consell de participació
	*/
	elseif ($idp == 45){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_consellparticipacio",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_consellparticipacio WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['convocats'])){
			$convocats = stripslashes_deep($_POST['convocats']);
			$dades["convocats"] = json_encode($convocats);
		}else{
			$dades['convocats'] = "";
		}
		if (isset($_POST['altresassistents'])){
			$altresassistents = (trim($_POST['altresassistents']));
			$dades["altresassistents"] = $altresassistents;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'consellparticipacio',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Comissió informativa
	*/
	elseif ($idp == 46){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_comissioinformativa",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_comissioinformativa WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['convocats'])){
			$convocats = stripslashes_deep($_POST['convocats']);
			$dades["convocats"] = json_encode($convocats);
		}else{
			$dades['convocats'] = "";
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'comissioinformativa',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Òrgan deliberatiu
	*/
	elseif ($idp == 47){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_organdeliberatiu",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_organdeliberatiu WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['convocats'])){
			$convocats = stripslashes_deep($_POST['convocats']);
			$dades["convocats"] = json_encode($convocats);
		}else{
			$dades['convocats'] = "";
		}
		if (isset($_POST['altresassistents'])){
			$altresassistents = (trim($_POST['altresassistents']));
			$dades["altresassistents"] = $altresassistents;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'organdeliberatiu',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Processos Bàssics
	*/
	elseif ($idp == 48){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_processos",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_processos WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		*/
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['ambit'])){
			$ambit = filter_var(htmlspecialchars(trim($_POST['ambit'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["ambit"] = $ambit;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		
		if (isset($_POST['assistents'])){
			$assistents = stripslashes_deep($_POST['assistents']);
			$dades["assistents"] = json_encode($assistents);
		}else{
			$dades['assistents'] = "";
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['acords'])){
			$acords = (trim($_POST['acords']));
			$dades["acords"] = $acords;
		}
		if (isset($_POST['areaactuacio'])){
			$areaactuacio = stripslashes_deep($_POST['areaactuacio']);
			$dades["areaactuacio"] = json_encode($areaactuacio);
		}else{
			$dades['areaactuacio'] = "";
		}

		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'processos',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Indicadors Processos
	*/
	elseif ($idp == 49){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['responsable'])){
			$responsable = filter_var(htmlspecialchars(trim($_POST['responsable'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["responsable"] = $responsable;
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipologia"] = $tipologia;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['compromis'])){
			$compromis = (trim($_POST['compromis']));
			$dades["compromis"] = $compromis;
		}
		if (isset($_POST['formula'])){
			$formula = (trim($_POST['formula']));
			$dades["formula"] = $formula;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['periodicitat'])){
			$periodicitat = filter_var(htmlspecialchars(trim($_POST['periodicitat'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["periodicitat"] = $periodicitat;
		}
		if (isset($_POST['numvalors'])){
			$numvalors = filter_var(htmlspecialchars(trim($_POST['numvalors'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["numvalors"] = $numvalors;
		}
		if (isset($_POST['tipusindicador'])){
			$tipusindicador = filter_var(htmlspecialchars(trim($_POST['tipusindicador'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipusindicador"] = $tipusindicador;
		}
		if (isset($_POST['tipusgrafics'])){
			$tipusgrafics = stripslashes_deep($_POST['tipusgrafics']);
			$dades["tipusgrafics"] = json_encode($tipusgrafics);
		}else{
			$dades['tipusgrafics'] = "";
		}
		if (isset($_POST['objectiu2'])){
			$objectiu = filter_var(htmlspecialchars(trim($_POST['objectiu2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["objectiu"] = $objectiu;
		}else
		{
			if (isset($_POST['textobjectiu'])){
				$textobjectiu = stripslashes_deep($_POST['textobjectiu']);
			}
			if (isset($_POST['objectiu'])){
				$objectiu = stripslashes_deep($_POST['objectiu']);
			}
			$objectius = array();
			if (is_array($textobjectiu))
			{
				foreach ($textobjectiu as $key => $value) {
					$objectius[$key+1][text] = $value;
					$objectius[$key+1][valor] = $objectiu[$key];
				}
				$dades["objectiu"] = json_encode($objectius);
			}
			
		}
		

		
		if (isset($_POST['ctf'])){
			$clau_proces = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_proces"] = $clau_proces;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'processos_indicadors',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Valors Indicadors Processos
	*/
	elseif ($idp == 50){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $datai) || empty($datai) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}

		if ($tipus == a) // Guarda el valor_objectiu de l'indicador en el moment de l'alta.
		{
			if (isset($_POST['valor_objectiu'])){
				$valor_objectiu = json_decode((trim($_POST['valor_objectiu'])));
				$dades["valor_objectiu"] = $valor_objectiu;
			}
		}
		
		if (isset($_POST['textvalor'])){
			$textvalor = stripslashes_deep($_POST['textvalor']); // per mostrar als resultats, taules, etc.
		}
		if (isset($_POST['valors'])){
			$valors = stripslashes_deep($_POST['valors']);
		}
		if (isset($_POST['detalls'])){
			$detalls = stripslashes_deep($_POST['detalls']);
		}
		$valorsfinal = array();
		foreach ($valors as $key => $value) {
			$valorsfinal[$key+1][textvalor] = $textvalor[$key];
			$valorsfinal[$key+1][valor] = $value;
			$valorsfinal[$key+1][detalls] = $detalls[$key];
		}
		$dades["valor"] = json_encode($valorsfinal);
		
		

		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'processos_indicadors_valors',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Bústia local
	*/
	elseif ($idp == 51){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		$dades["clau_pla"] = $idpla;
		
		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_bustialocal",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi;
		}
		else
		{
			// Generar codi nou.
			$Codiactual = $dbb->FreeSql("SELECT codi FROM pfx_bustialocal WHERE id=:id",array("id"=>$id));
			$codi = $Codiactual[1][codi];
			$codi = explode("_", $codi);
			$dades["codi"] = $codi[2];
		}
		/*
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}*/
		if (isset($_POST['tipus'])){
			$tipusbustia = filter_var(htmlspecialchars(trim($_POST['tipus'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipus"] = $tipusbustia;
		}
		if (isset($_POST['ambit'])){
			$ambit = filter_var(htmlspecialchars(trim($_POST['ambit'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["ambit"] = $ambit;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipologia"] = $tipologia;
		}
		if (isset($_POST['emailorigen'])){
			$emailorigen = filter_var(htmlspecialchars(trim($_POST['emailorigen'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["emailorigen"] = $emailorigen;
		}
		if (isset($_POST['prioritat'])){
			$prioritat = filter_var(htmlspecialchars(trim($_POST['prioritat'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["prioritat"] = $prioritat;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = date("Y-m-d H:i:s");
		}
		if (isset($_POST['final'])){
			$final = filter_var(htmlspecialchars(trim($_POST['final'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			if ($final != "" && $final != "0000-00-00"){
				$final = explode("/",$final);
				$dades["final"] = date("$final[2]-$final[1]-$final[0] 00:00:00");
			}else{
				$dades["final"] = "";
			}
		}
		else
		{
			$dades["final"] = "";
		}
		if (isset($_POST['responsable'])){
			$responsable = filter_var(htmlspecialchars(trim($_POST['responsable'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["responsable"] = $responsable;
		}
		if (isset($_POST['areaagestio'])){
			$areaagestio = stripslashes_deep($_POST['areaagestio']);
			$dades["areaagestio"] = json_encode($areaagestio);
		}else{
			$dades['areaagestio'] = "";
		}
		if (isset($_POST['observacions'])){
			$observacions = (trim($_POST['observacions']));
			$dades["observacions"] = $observacions;
		}
		if (isset($_POST['status'])){
			$status = filter_var(htmlspecialchars(trim($_POST['status'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["status"] = $status;
		}
		if (isset($_POST['latFld'])){
			$lat = filter_var(htmlspecialchars(trim($_POST['latFld'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["lat"] = $lat;
		}
		if (isset($_POST['lngFld'])){
			$lng = filter_var(htmlspecialchars(trim($_POST['lngFld'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["lng"] = $lng;
		}
		
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'bustialocal',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}

	/* 
		Àmbits bústia
	*/
	elseif ($idp == 52){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'bustiaambits',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Categories tipus Indicadors Processos
	*/
	elseif ($idp == 53){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['nom'])){
			$descripcio_ca = (trim($_POST['nom']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'processos_indicadors_tipus',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Configuració consutles
	*/
	elseif ($idp == 54){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
	
		if (isset($_POST['c_1_1'])){$dades["c_1_1"] = filter_var(htmlspecialchars(trim($_POST['c_1_1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_1_1"] = 0;}
		if (isset($_POST['c_1_2'])){$dades["c_1_2"] = filter_var(htmlspecialchars(trim($_POST['c_1_2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_1_2"] = 0;}	
		if (isset($_POST['c_1_3'])){$dades["c_1_3"] = filter_var(htmlspecialchars(trim($_POST['c_1_3'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_1_3"] = 0;}

		if (isset($_POST['c_2_1'])){$dades["c_2_1"] = filter_var(htmlspecialchars(trim($_POST['c_2_1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_1"] = 0;}
		if (isset($_POST['c_2_2'])){$dades["c_2_2"] = filter_var(htmlspecialchars(trim($_POST['c_2_2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_2"] = 0;}
		if (isset($_POST['c_2_3'])){$dades["c_2_3"] = filter_var(htmlspecialchars(trim($_POST['c_2_3'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_3"] = 0;}
		if (isset($_POST['c_2_4'])){$dades["c_2_4"] = filter_var(htmlspecialchars(trim($_POST['c_2_4'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_4"] = 0;}
		if (isset($_POST['c_2_5'])){$dades["c_2_5"] = filter_var(htmlspecialchars(trim($_POST['c_2_5'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_5"] = 0;}
		if (isset($_POST['c_2_6'])){$dades["c_2_6"] = filter_var(htmlspecialchars(trim($_POST['c_2_6'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_6"] = 0;}
		if (isset($_POST['c_2_7'])){$dades["c_2_7"] = filter_var(htmlspecialchars(trim($_POST['c_2_7'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_7"] = 0;}	
		if (isset($_POST['c_2_8'])){$dades["c_2_8"] = filter_var(htmlspecialchars(trim($_POST['c_2_8'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_2_8"] = 0;}

		if (isset($_POST['c_3_1'])){$dades["c_3_1"] = filter_var(htmlspecialchars(trim($_POST['c_3_1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_3_1"] = 0;}
		if (isset($_POST['c_3_2'])){$dades["c_3_2"] = filter_var(htmlspecialchars(trim($_POST['c_3_2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_3_2"] = 0;}

		if (isset($_POST['c_4_1'])){$dades["c_4_1"] = filter_var(htmlspecialchars(trim($_POST['c_4_1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_4_1"] = 0;}
		if (isset($_POST['c_4_2'])){$dades["c_4_2"] = filter_var(htmlspecialchars(trim($_POST['c_4_2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_4_2"] = 0;}
		if (isset($_POST['c_4_3'])){$dades["c_4_3"] = filter_var(htmlspecialchars(trim($_POST['c_4_3'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_4_3"] = 0;}
		if (isset($_POST['c_4_4'])){$dades["c_4_4"] = filter_var(htmlspecialchars(trim($_POST['c_4_4'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_4_4"] = 0;}

		if (isset($_POST['c_5_1'])){$dades["c_5_1"] = filter_var(htmlspecialchars(trim($_POST['c_5_1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);}else{$dades["c_5_1"] = 0;}

		foreach ($dades as $key => $value) {
			
			$key2 = explode("_", $key);

			$arrayguardar = array('taula' =>  "configuracio", 
								  'dades' =>  array(	
											'parent_id' => $key2[1],
											'clau' => $key2[2],
											'estat' => $value, 
										 ),'tipus' =>  m);

		
			$fields     =  array_keys($arrayguardar[dades]);
			$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
			$sql 		= "INSERT INTO pfx_".$arrayguardar[taula]." (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")  ON DUPLICATE KEY UPDATE estat = '".$arrayguardar[dades][estat]."'";
			
            $dbb->db->query("$sql",$arrayguardar[dades]); 



		}
		$return = array("1","1");
		echo json_encode($return);
		exit();

		
		
		

	}

	/* 
		Objectius Actuacions
	*/
	elseif ($idp == 55){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_actuacio"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'actuacions_objectius',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Àmbits Actuacions.
	*/
	elseif ($idp == 56){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		$nivell = 1;
		$parent_id = 0;
		if (isset($_POST['nivell'])){
			$nivell = filter_var(htmlspecialchars(trim($_POST['nivell'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["nivell"] = $nivell;
		}
		if ($nivell != 1)
		{
			if (isset($_POST['parent_id'])){
				$parent_id = filter_var(htmlspecialchars(trim($_POST['parent_id'])), FILTER_SANITIZE_NUMBER_INT);
			}
		}
		$dades["parent_id"] = $parent_id;
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'actuacions_ambits',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Colectius Tasques.
	*/
	elseif ($idp == 57){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		$dades["estat"] = 1;
		
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'tasques_colectius',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Pla dintres.
	*/
	elseif ($idp == 58){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if ($tipus == a)
		{
			// Generar codi nou.
			$Codigran = $dbb->FreeSql("SELECT MAX(id) AS id FROM pfx_projectes ",array());
			$codi = $Codigran[1][id] + 1;
			$dades["codi"] = $codi.'/'.date(Y);
		}
		else
		{
			
			if (isset($_POST['codi'])){
				$codi = filter_var(htmlspecialchars(trim($_POST['codi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
				$dades["codi"] = $codi;
			}
			
		}
		

		$dades["clau_pla"] = $idpla;
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['adreca'])){
			$adreca = (trim($_POST['adreca']));
			$dades["adreca"] = $adreca;
		}
		if (isset($_POST['tipuspropietat'])){
			$tipuspropietat = filter_var(htmlspecialchars(trim($_POST['tipuspropietat'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipuspropietat"] = $tipuspropietat;
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["estat"] = $estat;
		}
		if (isset($_POST['barri'])){
			$barri = filter_var(htmlspecialchars(trim($_POST['barri'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["barri"] = $barri;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['usuaris'])){
			$usuaris = stripslashes_deep($_POST['usuaris']);
			$dades["usuaris"] = json_encode($usuaris);
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['origen'])){
			$origen = (trim($_POST['origen']));
			$dades["origen"] = $origen;
		}
		if (isset($_POST['finalitzat'])){
			$finalitzat = filter_var(htmlspecialchars(trim($_POST['finalitzat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$finalitzat = explode("/",$finalitzat);
			$dades["finalitzat"] = date("$finalitzat[2]-$finalitzat[1]-$finalitzat[0] 00:00:00");
		}
		else
		{
			$dades["finalitzat"] = "";
		}
		if (isset($_POST['retirat'])){
			$retirat = filter_var(htmlspecialchars(trim($_POST['retirat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$retirat = explode("/",$retirat);
			$dades["retirat"] = date("$retirat[2]-$retirat[1]-$retirat[0] 00:00:00");
		}
		else
		{
			$dades["retirat"] = "";
		}
		if (isset($_POST['inftecnic'])){
			$inftecnic = filter_var(htmlspecialchars(trim($_POST['inftecnic'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inftecnic = explode("/",$inftecnic);
			$dades["inftecnic"] = date("$inftecnic[2]-$inftecnic[1]-$inftecnic[0] 00:00:00");
		}
		else
		{
			$dades["inftecnic"] = "";
		}
		if (isset($_POST['valoracio'])){
			$valoracio = filter_var(htmlspecialchars(trim($_POST['valoracio'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["valoracio"] = $valoracio;
		}
		if (isset($_POST['taudiencia'])){
			$taudiencia = filter_var(htmlspecialchars(trim($_POST['taudiencia'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$taudiencia = explode("/",$taudiencia);
			$dades["taudiencia"] = date("$taudiencia[2]-$taudiencia[1]-$taudiencia[0] 00:00:00");
		}
		else
		{
			$dades["taudiencia"] = "";
		}
		if (isset($_POST['oconservacio'])){
			$oconservacio = filter_var(htmlspecialchars(trim($_POST['oconservacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$oconservacio = explode("/",$oconservacio);
			$dades["oconservacio"] = date("$oconservacio[2]-$oconservacio[1]-$oconservacio[0] 00:00:00");
		}
		else
		{
			$dades["oconservacio"] = "";
		}
		if (isset($_POST['conveni'])){
			$conveni = filter_var(htmlspecialchars(trim($_POST['conveni'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$conveni = explode("/",$conveni);
			$dades["conveni"] = date("$conveni[2]-$conveni[1]-$conveni[0] 00:00:00");
		}
		else
		{
			$dades["conveni"] = "";
		}
		if (isset($_POST['dinhabilitat'])){
			$dinhabilitat = filter_var(htmlspecialchars(trim($_POST['dinhabilitat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dinhabilitat = explode("/",$dinhabilitat);
			$dades["dinhabilitat"] = date("$dinhabilitat[2]-$dinhabilitat[1]-$dinhabilitat[0] 00:00:00");
		}
		else
		{
			$dades["dinhabilitat"] = "";
		}
		if (isset($_POST['coordenades'])){
			$coordenades = filter_var(htmlspecialchars(trim($_POST['coordenades'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$coordenades = str_replace("(", "",str_replace(")", "", $coordenades));
			$coordenades = explode(",", $coordenades);
			$dades["lat"] = trim($coordenades[0]);
			$dades["lng"] = trim($coordenades[1]);
		}

		// Sansionador.

		if (isset($_POST['tipussubsidiaria'])){
			$tipussubsidiaria = filter_var(htmlspecialchars(trim($_POST['tipussubsidiaria'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipussubsidiaria"] = $tipussubsidiaria;
		}
		if (isset($_POST['dataconveni'])){
			$dataconveni = filter_var(htmlspecialchars(trim($_POST['dataconveni'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dataconveni = explode("/",$dataconveni);
			$dades["dataconveni"] = date("$dataconveni[2]-$dataconveni[1]-$dataconveni[0] 00:00:00");
		}
		else
		{
			$dades["dataconveni"] = "";
		}
		if (isset($_POST['pressupost'])){
			$pressupost = filter_var(htmlspecialchars(trim($_POST['pressupost'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressupost"] = $pressupost;
		}
		if (isset($_POST['definitiu'])){
			$definitiu = filter_var(htmlspecialchars(trim($_POST['definitiu'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["definitiu"] = $definitiu;
		}
		if (isset($_POST['observacions'])){
			$observacions = (trim($_POST['observacions']));
			$dades["observacions"] = $observacions;
		}
		


		
		if (!empty($id))
		{
			// Guardar ID's anteriors de cap de projecte i usuaris per enviar mails si s'han canviat.
			//$Projecte = $dbb->Llistats("projectes"," AND id = :id" ,array("id"=>$id), "id", false );
			//if (!empty($Projecte))
			//{
			//	$dades['avis'][cap_projecte] = $Projecte[1][cap_projecte];
			//	$dades['avis'][usuaris] = $Projecte[1][usuaris];
			//}
		}
		

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

		

	}

	/* 
		Contactes Dintres
	*/
	elseif ($idp == 59){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['tipuscontacte'])){
			$tipuscontacte = filter_var(htmlspecialchars(trim($_POST['tipuscontacte'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipuscontacte"] = $tipuscontacte;
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}
		if (isset($_POST['cp'])){
			$cp = filter_var(htmlspecialchars(trim($_POST['cp'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["cp"] = encryptRSA($cp);
		}
		if (isset($_POST['ciutat'])){
			$ciutat = filter_var(htmlspecialchars(trim($_POST['ciutat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["ciutat"] = encryptRSA($ciutat);
		}
		if (isset($_POST['telefon'])){
			$telefon = filter_var(htmlspecialchars(trim($_POST['telefon'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["telefon"] = encryptRSA($telefon);
		}
		if (isset($_POST['email'])){
			$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["email"] = encryptRSA($email);
		}
		

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_contactes',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Observacions Dintres
	*/
	elseif ($idp == 60){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_observacions',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Projectes Dintres
	*/
	elseif ($idp == 61){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['tipuscontacte'])){
			$tipuscontacte = filter_var(htmlspecialchars(trim($_POST['tipuscontacte'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipuscontacte"] = $tipuscontacte;
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['termini'])){
			$termini = filter_var(htmlspecialchars(trim($_POST['termini'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$termini = explode("/",$termini);
			$dades["termini"] = date("$termini[2]-$termini[1]-$termini[0] 00:00:00");
		}
		else
		{
			$dades["termini"] = "";
		}
		if (isset($_POST['presentacio'])){
			$presentacio = filter_var(htmlspecialchars(trim($_POST['presentacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$presentacio = explode("/",$presentacio);
			$dades["presentacio"] = date("$presentacio[2]-$presentacio[1]-$presentacio[0] 00:00:00");
		}
		else
		{
			$dades["presentacio"] = "";
		}
		if (isset($_POST['validacio'])){
			$validacio = filter_var(htmlspecialchars(trim($_POST['validacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$validacio = explode("/",$validacio);
			$dades["validacio"] = date("$validacio[2]-$validacio[1]-$validacio[0] 00:00:00");
		}
		else
		{
			$dades["validacio"] = "";
		}
		if (isset($_POST['pressupost'])){
			$pressupost = filter_var(htmlspecialchars(trim($_POST['pressupost'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressupost"] = $pressupost;
		}
		if (isset($_POST['tipusllicencia'])){
			$tipusllicencia = filter_var(htmlspecialchars(trim($_POST['tipusllicencia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipusllicencia"] = $tipusllicencia;
		}
		if (isset($_POST['llicencia'])){
			$llicencia = filter_var(htmlspecialchars(trim($_POST['llicencia'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$llicencia = explode("/",$llicencia);
			$dades["llicencia"] = date("$llicencia[2]-$llicencia[1]-$llicencia[0] 00:00:00");
		}
		else
		{
			$dades["llicencia"] = "";
		}
		if (isset($_POST['iniciobres'])){
			$iniciobres = filter_var(htmlspecialchars(trim($_POST['iniciobres'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$iniciobres = explode("/",$iniciobres);
			$dades["iniciobres"] = date("$iniciobres[2]-$iniciobres[1]-$iniciobres[0] 00:00:00");
		}
		else
		{
			$dades["iniciobres"] = "";
		}
		if (isset($_POST['finalobres'])){
			$finalobres = filter_var(htmlspecialchars(trim($_POST['finalobres'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$finalobres = explode("/",$finalobres);
			$dades["finalobres"] = date("$finalobres[2]-$finalobres[1]-$finalobres[0] 00:00:00");
		}
		else
		{
			$dades["finalobres"] = "";
		}


		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_projectes',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Tècnics Projectes Dintres
	*/
	elseif ($idp == 62){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['cp'])){
			$cp = filter_var(htmlspecialchars(trim($_POST['cp'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["cp"] = $cp;
		}
		if (isset($_POST['ciutat'])){
			$ciutat = filter_var(htmlspecialchars(trim($_POST['ciutat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["ciutat"] = $ciutat;
		}
		if (isset($_POST['telefon'])){
			$telefon = filter_var(htmlspecialchars(trim($_POST['telefon'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["telefon"] = $telefon;
		}
		if (isset($_POST['email'])){
			$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["email"] = $email;
		}
		

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_tasca;
		}
		if (isset($_POST['ctf2'])){
			$ctf2 = filter_var(htmlspecialchars(trim($_POST['ctf2'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $ctf2;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_projectes_tecnics',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Observacions técniques Dintres
	*/
	elseif ($idp == 63){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_observacions_tecniques',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Sancionador Dintres
	*/
	elseif ($idp == 64){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		/*
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		*/
		if (isset($_POST['tipussancionador'])){
			$tipussancionador = filter_var(htmlspecialchars(trim($_POST['tipussancionador'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipussancionador"] = $tipussancionador;
		}
		if (isset($_POST['incoat'])){
			$incoat = filter_var(htmlspecialchars(trim($_POST['incoat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$incoat = explode("/",$incoat);
			$dades["incoat"] = date("$incoat[2]-$incoat[1]-$incoat[0] 00:00:00");
		}
		else
		{
			$dades["incoat"] = "";
		}
		if (isset($_POST['caducitat'])){
			$caducitat = filter_var(htmlspecialchars(trim($_POST['caducitat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$caducitat = explode("/",$caducitat);
			$dades["caducitat"] = date("$caducitat[2]-$caducitat[1]-$caducitat[0] 00:00:00");
		}
		else
		{
			$dades["caducitat"] = "";
		}
		if (isset($_POST['tram1'])){
			$tram1 = filter_var(htmlspecialchars(trim($_POST['tram1'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$tram1 = explode("/",$tram1);
			$dades["tram1"] = date("$tram1[2]-$tram1[1]-$tram1[0] 00:00:00");
		}
		else
		{
			$dades["tram1"] = "";
		}
		if (isset($_POST['tram2'])){
			$tram2 = filter_var(htmlspecialchars(trim($_POST['tram2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$tram2 = explode("/",$tram2);
			$dades["tram2"] = date("$tram2[2]-$tram2[1]-$tram2[0] 00:00:00");
		}
		else
		{
			$dades["tram2"] = "";
		}
		if (isset($_POST['tram3'])){
			$tram3 = filter_var(htmlspecialchars(trim($_POST['tram3'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$tram3 = explode("/",$tram3);
			$dades["tram3"] = date("$tram3[2]-$tram3[1]-$tram3[0] 00:00:00");
		}
		else
		{
			$dades["tram3"] = "";
		}
		

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_sancionador',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Propietaris Sancionador Dintres
	*/
	elseif ($idp == 65){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}

		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = encryptRSA($descripcio_ca);
		}
		if (isset($_POST['cp'])){
			$cp = filter_var(htmlspecialchars(trim($_POST['cp'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["cp"] = encryptRSA($cp);
		}
		if (isset($_POST['ciutat'])){
			$ciutat = filter_var(htmlspecialchars(trim($_POST['ciutat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["ciutat"] = encryptRSA($ciutat);
		}
		if (isset($_POST['telefon'])){
			$telefon = filter_var(htmlspecialchars(trim($_POST['telefon'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["telefon"] = encryptRSA($telefon);
		}
		if (isset($_POST['email'])){
			$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["email"] = encryptRSA($email);
		}
		if (isset($_POST['coeficient'])){
			$coeficient = filter_var(htmlspecialchars(trim($_POST['coeficient'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["coeficient"] = $coeficient;
		}
		if (isset($_POST['reallotjament'])){
			$reallotjament = (trim($_POST['reallotjament']));
			$dades["reallotjament"] = $reallotjament;
		}
		if (isset($_POST['liquidacio'])){
			$liquidacio = filter_var(htmlspecialchars(trim($_POST['liquidacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$liquidacio = explode("/",$liquidacio);
			$dades["liquidacio"] = date("$liquidacio[2]-$liquidacio[1]-$liquidacio[0] 00:00:00");
		}
		else
		{
			$dades["liquidacio"] = "";
		}
		if (isset($_POST['importparticular'])){
			$importparticular = filter_var(htmlspecialchars(trim($_POST['importparticular'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["importparticular"] = $importparticular;
		}
		if (isset($_POST['pagat'])){
			$pagat = filter_var(htmlspecialchars(trim($_POST['pagat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$pagat = explode("/",$pagat);
			$dades["pagat"] = date("$pagat[2]-$pagat[1]-$pagat[0] 00:00:00");
		}
		else
		{
			$dades["pagat"] = "";
		}
		if (isset($_POST['presuadjudicat'])){
			$presuadjudicat = filter_var(htmlspecialchars(trim($_POST['presuadjudicat'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["presuadjudicat"] = $presuadjudicat;
		}
		if (isset($_POST['importdefinitiu'])){
			$importdefinitiu = filter_var(htmlspecialchars(trim($_POST['importdefinitiu'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["importdefinitiu"] = $importdefinitiu;
		}
		if (isset($_POST['observacions'])){
			$observacions = (trim($_POST['observacions']));
			$dades["observacions"] = encryptRSA($observacions);
		}
		

		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_sancionador"] = $clau_tasca;
		}
		if (isset($_POST['ctf2'])){
			$ctf2 = filter_var(htmlspecialchars(trim($_POST['ctf2'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_dintres"] = $ctf2;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'dintres_sancionador_propietaris',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites projectes
	*/
	elseif ($idp == 66){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipologia"] = $tipologia;
		}
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_fites',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites tasques
	*/
	elseif ($idp == 67){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		
		if (isset($_POST['ctf'])){
			$clau_tasca = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_tasca"] = $clau_tasca;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'tasques_fites',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Operadors projectes
	*/
	elseif ($idp == 68){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['persones'])){
			$persones = (trim($_POST['persones']));
			$dades["persones"] = $persones;
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["tipologia"] = $tipologia;
		}else{
			$dades["tipologia"] = 0;
		}
		/*
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}*/
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_operadors',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites projectes Comunicació
	*/
	elseif ($idp == 69){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		if (isset($_POST['datafi'])){
			$datafi = filter_var(htmlspecialchars(trim($_POST['datafi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datafi = explode("/",$datafi);
			$dades["datafi"] = date("$datafi[2]-$datafi[1]-$datafi[0] 00:00:00");
		}
		else
		{
			$dades["datafi"] = "";
		}
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['tasques'])){
			$tasques = stripslashes_deep($_POST['tasques']);
			$dades["tasques"] = json_encode($tasques);
		}
		if (isset($_POST['pobjectiu'])){
			$pobjectiu = stripslashes_deep($_POST['pobjectiu']);
			$dades["pobjectiu"] = json_encode($pobjectiu);
		}
		if (isset($_POST['tipologiafitescom'])){
			$tipologiafitescom = stripslashes_deep($_POST['tipologiafitescom']);
			$dades["tipologiafitescom"] = json_encode($tipologiafitescom);
		}
		if (isset($_POST['funciocom'])){
			$funciocom = filter_var(htmlspecialchars(trim($_POST['funciocom'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["funciocom"] = $funciocom;
		}else{
			$dades["funciocom"] = 0;
		}
		if (isset($_POST['puntualcom'])){
			$puntualcom = filter_var(htmlspecialchars(trim($_POST['puntualcom'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["puntualcom"] = $puntualcom;
		}else{
			$dades["puntualcom"] = 0;
		}
		if (isset($_POST['pressuposte'])){
			$pressuposte = filter_var(htmlspecialchars(trim($_POST['pressuposte'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressuposte"] = $pressuposte;
		}
		if (isset($_POST['estatfita'])){
			$estatfita = filter_var(htmlspecialchars(trim($_POST['estatfita'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estatfita"] = $estatfita;
		}else{
			$dades["estatfita"] = 0;
		}
		

		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_fites_com',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites projectes Participacio
	*/
	elseif ($idp == 70){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		if (isset($_POST['datafi'])){
			$datafi = filter_var(htmlspecialchars(trim($_POST['datafi'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datafi = explode("/",$datafi);
			$dades["datafi"] = date("$datafi[2]-$datafi[1]-$datafi[0] 00:00:00");
		}
		else
		{
			$dades["datafi"] = "";
		}
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['tasques'])){
			$tasques = stripslashes_deep($_POST['tasques']);
			$dades["tasques"] = json_encode($tasques);
		}
		if (isset($_POST['destinatarispart'])){
			$destinatarispart = filter_var(htmlspecialchars(trim($_POST['destinatarispart'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["destinatarispart"] = $destinatarispart;
		}else{
			$dades["destinatarispart"] = 0;
		}
		if (isset($_POST['assistents'])){
			$assistents = filter_var(htmlspecialchars(trim($_POST['assistents'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["assistents"] = $assistents;
		}
		if (isset($_POST['tipologiapart'])){
			$tipologiapart = filter_var(htmlspecialchars(trim($_POST['tipologiapart'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["tipologiapart"] = $tipologiapart;
		}else{
			$dades["tipologiapart"] = 0;
		}
		if (isset($_POST['perspectiva'])){
			$perspectiva = filter_var(htmlspecialchars(trim($_POST['perspectiva'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["perspectiva"] = $perspectiva;
		}else{
			$dades["perspectiva"] = 0;
		}
		if (isset($_POST['diversitat'])){
			$diversitat = filter_var(htmlspecialchars(trim($_POST['diversitat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["diversitat"] = $diversitat;
		}else{
			$dades["diversitat"] = 0;
		}
		if (isset($_POST['materials'])){
			$materials = filter_var(htmlspecialchars(trim($_POST['materials'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["materials"] = $materials;
		}else{
			$dades["materials"] = 0;
		}
		if (isset($_POST['pressuposte'])){
			$pressuposte = filter_var(htmlspecialchars(trim($_POST['pressuposte'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressuposte"] = $pressuposte;
		}
		if (isset($_POST['estatfita'])){
			$estatfita = filter_var(htmlspecialchars(trim($_POST['estatfita'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estatfita"] = $estatfita;
		}else{
			$dades["estatfita"] = 0;
		}

		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_fites_part',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Projectes Projectes motor.
	*/
	elseif ($idp == 71){

		$dbb->AreaPrivada($idp);

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		if (isset($_POST['tantpercent'])){
			$tantpercent = filter_var(htmlspecialchars(trim($_POST['tantpercent'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["tantpercent"] = $tantpercent;
		}
		if (isset($_POST['riscos'])){
			$riscos = (trim($_POST['riscos']));
			$dades["riscos"] = $riscos;
		}
		$dades["clau_pla"] = $idpla;
	
	

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_projmotor',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Valors Indicadors Projectes
	*/
	elseif ($idp == 72){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $datai) || empty($datai) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}

		if ($tipus == a) // Guarda el valor_objectiu de l'indicador en el moment de l'alta.
		{
			if (isset($_POST['valor_objectiu'])){
				$valor_objectiu = json_decode((trim($_POST['valor_objectiu'])));
				$dades["valor_objectiu"] = $valor_objectiu;
			}
		}
		
		if (isset($_POST['textvalor'])){
			$textvalor = stripslashes_deep($_POST['textvalor']); // per mostrar als resultats, taules, etc.
		}
		if (isset($_POST['valors'])){
			$valors = stripslashes_deep($_POST['valors']);
		}
		if (isset($_POST['detalls'])){
			$detalls = stripslashes_deep($_POST['detalls']);
		}
		$valorsfinal = array();
		foreach ($valors as $key => $value) {
			$valorsfinal[$key+1][textvalor] = $textvalor[$key];
			$valorsfinal[$key+1][valor] = $value;
			$valorsfinal[$key+1][detalls] = $detalls[$key];
		}
		$dades["valor"] = json_encode($valorsfinal);
		
		

		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_indicadors_valors',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Indicadors Projectes nou
	*/
	elseif ($idp == 73){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['responsable'])){
			$responsable = filter_var(htmlspecialchars(trim($_POST['responsable'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["responsable"] = $responsable;
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipologia"] = $tipologia;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['compromis'])){
			$compromis = (trim($_POST['compromis']));
			$dades["compromis"] = $compromis;
		}
		if (isset($_POST['formula'])){
			$formula = (trim($_POST['formula']));
			$dades["formula"] = $formula;
		}
		if (isset($_POST['inici'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

			$myregex = '~^\d{2}/\d{2}/\d{4}$~';
			if (!DateTime::createFromFormat('d/m/Y', $inici) || empty($inici) )
			{
				echo "Format de data incorrecte.";exit();
			}

			$inici = explode("/",$inici);
			$dades["inici"] = date("$inici[2]-$inici[1]-$inici[0] 00:00:00");
		}
		else
		{
			$dades["inici"] = "";
		}
		if (isset($_POST['periodicitat'])){
			$periodicitat = filter_var(htmlspecialchars(trim($_POST['periodicitat'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["periodicitat"] = $periodicitat;
		}
		if (isset($_POST['numvalors'])){
			$numvalors = filter_var(htmlspecialchars(trim($_POST['numvalors'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["numvalors"] = $numvalors;
		}
		if (isset($_POST['tipusindicador'])){
			$tipusindicador = filter_var(htmlspecialchars(trim($_POST['tipusindicador'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["tipusindicador"] = $tipusindicador;
		}
		if (isset($_POST['tipusgrafics'])){
			$tipusgrafics = stripslashes_deep($_POST['tipusgrafics']);
			$dades["tipusgrafics"] = json_encode($tipusgrafics);
		}else{
			$dades['tipusgrafics'] = "";
		}
		if (isset($_POST['objectiu2'])){
			$objectiu = filter_var(htmlspecialchars(trim($_POST['objectiu2'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["objectiu"] = $objectiu;
		}else
		{
			if (isset($_POST['textobjectiu'])){
				$textobjectiu = stripslashes_deep($_POST['textobjectiu']);
			}
			if (isset($_POST['objectiu'])){
				$objectiu = stripslashes_deep($_POST['objectiu']);
			}
			$objectius = array();
			if (is_array($textobjectiu))
			{
				foreach ($textobjectiu as $key => $value) {
					$objectius[$key+1][text] = $value;
					$objectius[$key+1][valor] = $objectiu[$key];
				}
				$dades["objectiu"] = json_encode($objectius);
			}
			
		}
		

		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_indicadors_nou',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Categories tipus Indicadors PRojectes
	*/
	elseif ($idp == 74){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['nom'])){
			$descripcio_ca = (trim($_POST['nom']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_indicadors_tipus',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Projectes urls
	*/
	elseif ($idp == 75){

		$dbb->AreaPrivada($idp);


		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_urls',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}

	/* 
		Actuacions urls
	*/
	elseif ($idp == 76){

		$dbb->AreaPrivada($idp);


		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'actuacions_urls',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}

	/* 
		Tasques urls
	*/
	elseif ($idp == 77){

		$dbb->AreaPrivada($idp);


		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['ctf'])){
			$parent_id = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["parent_id"] = $parent_id;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'tasques_urls',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}


	/* 
		General projectes
	*/
	elseif ($idp == 78){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_general',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}


	/* 
		Novetats projectes
	*/
	elseif ($idp == 79){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_novetats',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites marc
	*/
	elseif ($idp == 80){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		/*
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		*/
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'fites_marc',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Novetats projectes Motor
	*/
	elseif ($idp == 81){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_projmotor_nov',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Fites projectes Motor
	*/
	elseif ($idp == 82){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		if (isset($_POST['avis'])){
			$avis = filter_var(htmlspecialchars(trim($_POST['avis'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["avis"] = $avis;
		}else{
			$dades["avis"] = 0;
		}
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_projmotor_fites',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Novetats actuacions
	*/
	elseif ($idp == 83){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$clau_actuacio = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_actuacio"] = $clau_actuacio;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'actuacions_novetats',
			'id'    =>  $id,
			'idp'   =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}

	/* 
		Novetats projectes
	*/
	elseif ($idp == 85){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$datai = explode("/",$datai);
			$dades["datai"] = date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}

		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_riscos',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	/* 
		Configuració Alertes
	*/
	elseif ($idp == 86){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
	
		if (isset($_POST['c_1_datai'])){$dades["c_1_datai"] = filter_var(htmlspecialchars(trim($_POST['c_1_datai'])), FILTER_SANITIZE_NUMBER_INT);}else{$dades["c_1_datai"] = 0;}

		if (isset($_POST['c_2_datai'])){$dades["c_2_datai"] = filter_var(htmlspecialchars(trim($_POST['c_2_datai'])), FILTER_SANITIZE_NUMBER_INT);}else{$dades["c_2_datai"] = 0;}
		if (isset($_POST['c_2_dataf'])){$valordataf["c_2_dataf"] = filter_var(htmlspecialchars(trim($_POST['c_2_dataf'])), FILTER_SANITIZE_NUMBER_INT);}else{$valordataf["c_2_dataf"] = 0;}

		if (isset($_POST['c_3_datai'])){$dades["c_3_datai"] = filter_var(htmlspecialchars(trim($_POST['c_3_datai'])), FILTER_SANITIZE_NUMBER_INT);}else{$dades["c_3_datai"] = 0;}		

		if (isset($_POST['c_4_datai'])){$dades["c_4_datai"] = filter_var(htmlspecialchars(trim($_POST['c_4_datai'])), FILTER_SANITIZE_NUMBER_INT);}else{$dades["c_4_datai"] = 0;}


		foreach ($dades as $key => $value) {
			
			$key2 = explode("_", $key);

			$arrayguardar = array('taula' =>  "confalertes", 
								  'dades' =>  array(	
											'parent_id' => $key2[1],
											'diesi' => $value,
											'diesf' => ($key2[1] == 2?$valordataf["c_2_dataf"]:0), 
										 ),'tipus' =>  m);

		
			$fields     =  array_keys($arrayguardar[dades]);
			$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
			$sql 		= "INSERT INTO pfx_".$arrayguardar[taula]." (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")  ON DUPLICATE KEY UPDATE diesi = '".$arrayguardar[dades][diesi]."', diesf = '".$arrayguardar[dades][diesf]."'";
			
            $dbb->db->query("$sql",$arrayguardar[dades]); 



		}
		$return = array("1","1");
		echo json_encode($return);
		exit();

		
		
		

	}


	/* 
		Conceptes projectes
	*/
	elseif ($idp == 87){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['any'])){
			$any = filter_var(htmlspecialchars(trim($_POST['any'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["any"] = $any;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}

		if (isset($_POST['pressupost1'])){
			$pressupost1 = filter_var(htmlspecialchars(trim($_POST['pressupost1'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressupost1"] = $pressupost1;
		}
		if (isset($_POST['pressupost2'])){
			$pressupost2 = filter_var(htmlspecialchars(trim($_POST['pressupost2'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressupost2"] = $pressupost2;
		}
		if (isset($_POST['pressupost3'])){
			$pressupost3 = filter_var(htmlspecialchars(trim($_POST['pressupost3'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$dades["pressupost3"] = $pressupost3;
		}


		
		if (isset($_POST['ctf'])){
			$clau_actuacio = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_actuacio;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_conceptes',
			'id'    =>  $id,
			'idp'   =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);


	}

	/*
		Documentació per consultar.
	*/
	elseif ($idp == 88){

	}

	/* 
		Documentació (gestió).
	*/
	elseif ($idp == 89){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}

		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = filter_var(htmlspecialchars(trim($_POST['descripcio_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'documentacio',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Àrees impulsores
	*/
	elseif ($idp == 90){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['persones'])){
			$persones = (trim($_POST['persones']));
			$dades["persones"] = $persones;
		}
		if (isset($_POST['tipologia'])){
			$tipologia = filter_var(htmlspecialchars(trim($_POST['tipologia'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["tipologia"] = $tipologia;
		}else{
			$dades["tipologia"] = 0;
		}
		/*
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}*/
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_area',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}
	
	/* 
		Responsabilitats projectes
	*/
	elseif ($idp == 91){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		

		if (isset($_POST['responsable'])){
			$responsable = (trim($_POST['responsable']));
			$dades["responsable"] = $responsable;
		}
			
		if (isset($_POST['titol_ca'])){
			$titol_ca = (trim($_POST['titol_ca']));
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['observacionsfita'])){
			$observacionsfita = (trim($_POST['observacionsfita']));
			$dades["observacionsfita"] = $observacionsfita;
		}
		
		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_responsabilitats',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Indicadors projectes
	*/
	elseif ($idp == 92){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['datai'])){
			$datai = filter_var(htmlspecialchars(trim($_POST['datai'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			//$datai = explode("/",$datai);
			$dades["datai"] = date("$datai-01-01 00:00:00");//date("$datai[2]-$datai[1]-$datai[0] 00:00:00");
		}
		else
		{
			$dades["datai"] = "";
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = (trim($_POST['titol_ca']));
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = (trim($_POST['descripcio_ca']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['objectiu'])){
			$objectiu = (trim($_POST['objectiu']));
			$dades["objectiu"] = $objectiu;
		}
		if (isset($_POST['assolit'])){
			$assolit = filter_var(htmlspecialchars(trim($_POST['assolit'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["assolit"] = $assolit;
		}else{
			$dades["assolit"] = 0;
		}

		
		if (isset($_POST['ctf'])){
			$clau_projecte = filter_var(htmlspecialchars(trim($_POST['ctf'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["clau_projecte"] = $clau_projecte;
		}
		$dades["clau_pla"] = $idpla;
		$dades["estat"] = 1;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'projectes_indicadors_arxius',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Portada (gestió).
	*/
	elseif ($idp == 93){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		/*
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = filter_var(htmlspecialchars(trim($_POST['descripcio_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		*/

		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["clau_pla"] = $idpla;

		
		
		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'portada',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}


	//----------------------------------------------


	

	

	/* 
		Plans
	*/
	elseif ($idp == 20){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		/*
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		*/
		$dades["estat"] = 1;


		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'plans',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Dades Login
	*/
	elseif ($idp == 21){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();
		
		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			{
				$tipus = m;
			}else{
				$tipus = a;
			}
			
		}else{
			$tipus = a;
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = filter_var(htmlspecialchars(trim($_POST['descripcio_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['ordre'])){
			$ordre = filter_var(htmlspecialchars(trim($_POST['ordre'])), FILTER_SANITIZE_NUMBER_INT);
			$dades["ordre"] = $ordre;
		}
		if (isset($_POST['estat'])){
			$estat = filter_var(htmlspecialchars(trim($_POST['estat'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["estat"] = $estat;
		}else{
			$dades["estat"] = 0;
		}
		$dades["tipus"] = "1";
		$dades["taula"] = "login";


		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'upfile',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

		$clau_prov = $app['session']->get('clau_prov');
		$Upfiles = $dbb->Llistats("upfile"," AND clau_prov = :clau_prov ", array("clau_prov"=>$clau_prov), "id", false);


		if (!empty($Upfiles))
		{
			$arrayguardar[tipus] = m;
			$arrayguardar[id] = $Upfiles[1][id];
			$dades["clau_prov"] = "";
			$arrayguardar[dades] = $dades;
			$app['session']->set('clau_prov', uniqid());

		}


	}


	/*
		Dades Upgiles
	*/
	elseif ($idp == 998){

		//$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();
		
		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
			if (!empty($id))
			
			$tipus = m;
			
			
		}else{
			exit();
		}
		if (isset($_POST['descripcio_ca'])){
			$descripcio_ca = filter_var(htmlspecialchars(trim($_POST['descripcio_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["descripcio_ca"] = $descripcio_ca;
		}
		if (isset($_POST['principal'])){
			$principal = filter_var(htmlspecialchars(trim($_POST['principal'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["principal"] = $principal;
		}else{
			$dades["principal"] = 0;
		}

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'upfile',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	/* 
		Dades Pla, Usuaris.
	*/
		/*
	elseif ($idp == 22){

		$dbb->AreaPrivada($idp);

		// $dbb->AccesPla();

		// Dades.
		if (isset($_POST['id'])){
			$id = filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT);
		}
		if (isset($_POST['titol_ca'])){
			$titol_ca = filter_var(htmlspecialchars(trim($_POST['titol_ca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["titol_ca"] = $titol_ca;
		}
		if (isset($_POST['tipus_pla'])){
			$tipus_pla = filter_var(htmlspecialchars(trim($_POST['tipus_pla'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["tipus_pla"] = $tipus_pla;
		}
		if (isset($_POST['ambit_pla'])){
			$ambit_pla = filter_var(htmlspecialchars(trim($_POST['ambit_pla'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$dades["ambit_pla"] = $ambit_pla;
		}
		
		
		
		if (isset($_POST['any'])){
			$any = $_POST['any'];
		    $dades['any_pla'] = $any;
		}
		

		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'plans',
			'id'    =>  $id,
			'dades' =>  $dades,
			'tipus' =>  m,
		
		);

	}
	*/

	




	/*
		Dades login (text)
	
	elseif ($idp == 998){

		// Dades.
		$id = 1;
		$tipus = m;
			
		if (isset($_POST['descripcio_ca2'])){
			$descripcio_ca = (trim($_POST['descripcio_ca2']));
			$dades["descripcio_ca"] = $descripcio_ca;
		}



		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'general',
			'id'    =>  $id,
			'idp'    =>  $idp,
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}*/

	/* 
		Dades Usuaris.
	*/
	elseif ($idp == 997){

		$id = $dbb->app['session']->get(constant('General::nomsesiouser'));
		$tipus = m;

		if ( isset($_POST['passw']) && isset($_POST['passw2']))
		{
			if ($_POST['passw'] == $_POST['passw2'] && $_POST['passw'] != "")
			{
				//Genero un salt aleatori
				/*
				$salt = substr(hash('whirlpool',microtime()),rand(0,105),22);
				$pass = trim($_POST['passw']);
				$i = rand(0,22);  //22 per la longitud del salt.
			    $encryptedpass = crypt($pass,'$2a$07$' . substr($salt, 0, $i));
			    */

				$dades['password'] = md5(trim($_POST['passw']));
				$dades['txtpass'] = base64_encode(trim($_POST['passw']));
			}
		}

		// Verificar repetits.
		if ($dbb->PassActual(trim($_POST['passwa']),$id) != true)
		{
			echo '
				<script>
					$("#resultusuaris").html("<div class=\"alert alert-dismissable alert-danger\">Atenció! El password introduit no es correcte. Els canvis no es guardaran.</div>");
				</script>
			';
			exit();
		}


		// Configurar guardar.
		$arrayguardar = array(
		
			'taula' =>  'usuaris',
			'id'    =>  $id,
			'idp'   =>  "",
			'dades' =>  $dades,
			'tipus' =>  $tipus,
		
		);

	}

	

	// Guardar.
	$GuardarRegistre = $dbb->GuardarRegistre($arrayguardar);


	// Enviar mail a l'usuari amb les dades.
	if ($idp == 3){

		$envi = filter_var(trim($_POST['env']), FILTER_SANITIZE_NUMBER_INT);

		if ($envi == '1' ){

			$dadesenv = array();
			
			$dadesenv["email"] = $email;
			$dadesenv["password"] = trim($_POST['passw']);
			$dadesenv["tipus"] = "recuperar";
			$dadesenv["novesdades"] = "1";
			
			$dbb->EnviarMail($dadesenv);

			
		}
	}

	// Creació de l'actuació Zero al crear una linea o al afegir un PAM a un eix.
	/*
	if ($idp == 24){


		if ($GuardarRegistre[0] == 1){

			// Nova linea:
			if ($dades[nivell] == 3 && $tipus == a)
			{

				$dades[nivell] = 4;
				$dades[clau_eix] = $dades[parent_id];
				$parent_id = $dades[parent_id];
				$dades[parent_id] = $GuardarRegistre[1];
				$dades[titol_ca] = "0.Sense vinculació directa.";
				$dades[clau_cero] = 1;

				// Els eixos pertanyen a X PAM per tant s'han de crear actuacions 0 per tots ells.
				$Relacions = $dbb->Llistats("projectes_vinculacio"," AND nivell = 2 AND id = :id" ,array("id"=>$parent_id), "id", false );
				if (!empty($Relacions[1][parents_ids])){
					$parents_ids = json_decode($Relacions[1][parents_ids]);
					foreach ($parents_ids as $key => $value) 
					{
						$dades[clau_pam] = $value;
				
						$arrayguardar = array('taula' =>  'projectes_vinculacio', 
											  'dades' =>  $dades,
											  'tipus' =>  a,);
						$dbb->GuardarRegistre2($arrayguardar); 
					}
				}
			}

			echo json_encode($GuardarRegistre); // retornem el registre inicial per que continui el procés.

		}
		else{
			echo json_encode($GuardarRegistre); // retornem el registre inicial per que continui el procés.
		}
	}
	*/

	// Creació de les relacions a la taula auxiliar.
	if ($idp == 6){

		if ($GuardarRegistre[0] == 1){

			if (!empty($vinculacio)){

				// Eliminar relacions previes.
				$db = new Db();
				$db->query("DELETE FROM pfx_projectes_relacionsvinc
					     	WHERE id_proj = '".$GuardarRegistre[1]."' ");

				$arrayvinc=array();
				foreach ($vinculacio as $key_v => $value_v) {

					$valors = explode("_", $value_v);

					foreach ($valors as $key_v2 => $valor) 
					{	
						if (!in_array($valor, $arrayvinc))
						{
							$NivellVinc = $dbb->Llistats("projectes_vinculacio"," AND id = :id" ,array("id"=>$valor), "id", false );
							$nivell = $NivellVinc[1][nivell];
							if ($nivell==1)
								$camp = "id_pam";
							elseif ($nivell==2)
								$camp = "id_eix";
							elseif ($nivell==3)
								$camp = "id_linia";
							elseif ($nivell==4)
								$camp = "id_act";
							
							$dadesr[$camp] = $valor;
							array_push($arrayvinc, $valor);
						}
					}
					
				}

				$dadesr[id_proj] = $GuardarRegistre[1];
					
				$arrayguardarr = array('taula' =>  'projectes_relacionsvinc', 
									  'dades' =>  $dadesr,
									  'tipus' =>  a,);
				$dbb->GuardarRegistre2($arrayguardarr);
			}

			echo json_encode($GuardarRegistre); // retornem el registre inicial per que continui el procés.

		}

	}

	// Creació de les relacions a la taula auxiliar 2.
	if ($idp == 30){

		if ($GuardarRegistre[0] == 1){

			if (!empty($vinculacio)){


				// Eliminar relacions previes.
				$db = new Db();
				$db->query("DELETE FROM pfx_projectes_relacionscentres
					     	WHERE id_centre = '".$GuardarRegistre[1]."' ");

				$arrayvinc=array();
				foreach ($vinculacio as $key_v => $value_v) {

					$valors = explode("_", $value_v);

					foreach ($valors as $key_v2 => $valor) 
					{	
						if (!in_array($valor, $arrayvinc))
						{
							$NivellVinc = $dbb->Llistats("projectes_vinculacio"," AND id = :id" ,array("id"=>$valor), "id", false );
							$nivell = $NivellVinc[1][nivell];
							if ($nivell==1)
								$camp = "id_pam";
							elseif ($nivell==2)
								$camp = "id_eix";
							elseif ($nivell==3)
								$camp = "id_linia";
							elseif ($nivell==4)
								$camp = "id_act";

							// Per sinó trien tot l'arbre...
							$dadesr = array();
							if ($nivell==4){
								$dadesr[id_pam] = $NivellVinc[1][clau_pam];
								$dadesr[id_eix] = $NivellVinc[1][clau_eix];
								$dadesr[id_linia] = $NivellVinc[1][parent_id];
								$dadesr[id_act] = $valor;
							}else{
								$dadesr[$camp] = $valor;
								array_push($arrayvinc, $valor);
							}

							// només guardem quan son nivell 4. Sinó intentarà guardar per duplicat els mateixos ID quan son nivells superiors.
							if ($nivell == 4){

								$dadesr[id_centre] = $GuardarRegistre[1];
					
								$arrayguardarr = array('taula' =>  'projectes_relacionscentres', 
													  'dades' =>  $dadesr,
													  'tipus' =>  a,);

								$dbb->GuardarRegistre2($arrayguardarr);

							}
							
						}
					}
					
				}
				
				
			}

			echo json_encode($GuardarRegistre); // retornem el registre inicial per que continui el procés.

		}

	}
	

	$dadesplantilla = array();
	return $dadesplantilla;
