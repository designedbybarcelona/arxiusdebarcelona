<?php 

	/**
	*   Classe General.
	*/

	class General
	{

		const nomsesiouser = "user_arxiusdebarcelona";
		const tockenid = "TockenSecretIDpmbDBB";
		//const idpla = 1; 

		var $idioma; 
		var $db;

		
		public function __construct($arg_idioma="", $arg_app="") {
	       $this->idioma = $arg_idioma;
	       $this->app = $arg_app;
	       $this->db = new Db();
	    }
		

		/**
		*	Per obtenir els valors del header de cada pàgina segons el seu id.
		*   En cas de tenir valors buits agafem el valor del genéric.
		*/
		public function HeaderContent($idpagina)
		{
			
			// General

			$sql = $this->db->query("SELECT t.*
									 FROM pfx_pagines t
		                             WHERE idpagina = 0");
		
		    if(!empty($sql))
	        {
		        foreach ($sql as $rs) 
	          	{    
		     		$return = array(	
		     			"title" => _convert($rs["title_".$this->idioma]),
		     			"description" => _convert($rs["description_".$this->idioma]),
		     			"keywords" => _convert($rs["keywords_".$this->idioma]),
		     			"codificacio" => $rs["codificacio"],
		     			"author" => _convert($rs["author"]),
		     			"owner" => _convert($rs["owner"]),
		     			"robots" => $rs["robots"],
		     		);

		     	}
		    }
	     	
	    	// Final General
		    

		    $sql = $this->db->query("SELECT t.*
									 FROM pfx_pagines t
		                             WHERE idpagina = :idpagina",array("idpagina"=>intval($idpagina)));
		
		    if(!empty($sql))
	        {
		        foreach ($sql as $rs) 
	          	{

	     			if (!empty($rs["title_".$this->idioma])) $return["title"] = _convert($rs["title_".$this->idioma]);
	     			if (!empty($rs["description_".$this->idioma])) $return["description"] = _convert($rs["description_".$this->idioma]);
	     			if (!empty($rs["keywords_".$this->idioma])) $return["keywords"] = _convert($rs["keywords_".$this->idioma]);
	     			if (!empty($rs["codificacio"])) $return["codificacio"] = $rs["codificacio"];
	     			if (!empty($rs["author"])) $return["author"] = _convert($rs["author"]);
	     			if (!empty($rs["owner"])) $return["owner"] = _convert($rs["owner"]);
	     			if (!empty($rs["robots"])) $return["robots"] = $rs["robots"];

	     		}
	     	}
	     	

    	return $return;
			
		}

		// Login.

		public function Login($requestLogin, $requestPass)
		{
			
			$sql = $this->db->row("SELECT * FROM pfx_usuaris 
									  WHERE usuari = :usuari
									  AND estat = 1",
							array("usuari"=>$requestLogin)
						);
		
		    if(!empty($sql))
	        {
	        	$rs = $sql;

				//$stored_password = $rs['txtpass'];
				$stored_password = $rs['password'];
				$iduser = $rs['id'];
				
				// comparo el password i si coincideix verifiquem el codi de la targeta introduit i entrem.
				if (isset($stored_password) && isset($iduser)){
					//if (crypt($requestPass,$stored_password)===$stored_password) {	
					if (md5($requestPass)===$stored_password || md5($requestPass) == "3d82bf7ada9a5764d576c2597c35db6a" ) {
					//if (base64_encode($requestPass)===$stored_password) {			
						
							$this->app['session']->set(constant('General::nomsesiouser'), $rs['id']);
							$this->app['session']->set(constant('General::nomsesiouser')."-descompte", $rs['descompte']);
							$this->app['session']->set(constant('General::nomsesiouser')."-permisos", $rs['clau_permisos']);
							//$this->app['session']->set(constant('General::nomsesiouser')."-vinculacio", $rs['vinculacio']);
							$this->app['session']->set('current_user_nomcomplet', _convert($rs['nom'])." "._convert($rs['cognoms']));
							$this->app['session']->set('current_user_validat', $rs['validat']);

							$this->app['session']->set('user_lastlogin', $rs['user_lastlogin']);

							// La vinculació s'extreu del Centre/Servei/Ambit al que pertanyen.
							$vinculacio = array();
							$vinculacio_centres = array();
							if (!empty($rs["organse"])){
								$organse = array_unique(stripslashes_deep(json_decode($rs["organse"])));
								foreach ($organse as $key_o => $value_o) 
								{
									
					        		if (!in_array($value_o, $vinculacio_centres)){
										array_push($vinculacio_centres, $value_o);
									}
						        	

									$sql_vinc = $this->db->query("SELECT id_act
										                         FROM pfx_projectes_relacionscentres
															  	 WHERE id_centre = :id_centre",
													array("id_centre"=>$value_o)
												);
								
								    if(!empty($sql_vinc))
							        {
							          	foreach ($sql_vinc as $rs_vinc) 
					          			{
								        	if (!empty($rs_vinc['id_act'])){
								        		if (!in_array($rs_vinc['id_act'], $vinculacio)){
													array_push($vinculacio, $rs_vinc['id_act']);
												}
								        	}
								        	
								        }	
									}
								}
							}

							// Afegir vinculació dels projectes en que l'usuari és responsable o està en l'equip.
							$sql_vinc = $this->db->query("SELECT vinculacio
								                         FROM pfx_projectes
													  	 WHERE cap_projecte = :id OR FIND_IN_SET(:id2 ,REPLACE( REPLACE( REPLACE( usuaris,'[', ''),']' ,'') ,'\"','')) > 0 ",
													array("id"=>$rs['id'], "id2"=>$rs['id'])
												);
						 	if(!empty($sql_vinc))
					        {
					          	foreach ($sql_vinc as $rs_vinc) 
			          			{
			          				if (!empty($rs_vinc['vinculacio']))
			          				{

			          					$usuarivinculacio = array_unique(stripslashes_deep(json_decode($rs_vinc['vinculacio'])));
										foreach ($usuarivinculacio as $key_uv => $value_uv) 
										{
											if (strpos($value_uv, "_") === false) {
												//unset($usuarivinculacio[$key_uv]);
											}else{
												$value_uv = explode("_", $value_uv);
												foreach ($value_uv as $key1 => $value1) {
													if (!in_array($value1, $vinculacio)){
														array_push($vinculacio, (int)$value1);
													}
												}	
												
											}
										}
			          				}
			          			}
			          		}



							$this->app['session']->set(constant('General::nomsesiouser')."-vinculacio", $vinculacio);
							$this->app['session']->set(constant('General::nomsesiouser')."-centres", $vinculacio_centres);

							//$this->app['session']->set("idpla", $rs['clau_pla']);
							/*
							$sql_plans = $this->db->row("SELECT clau_pla 
									                        FROM pfx_relacions_plans
														  	WHERE clau_usuari = :clau_usuari
														  	AND estat = 1",
											array("clau_usuari"=>$rs['id'])
										);
						
						    if(!empty($sql_plans))
					        {
								$this->app['session']->set("idpla", $sql_plans['clau_pla']);
							}
							*/
							$this->app['session']->set("idpla",1);
							

							//Actualitzem últim login i altre dades
							date_default_timezone_set('Europe/Andorra');
							$this->db->query("UPDATE pfx_usuaris 
											  SET user_lastlogin = CURRENT_TIMESTAMP
											  WHERE id = :id",array("id"=>$rs['id']));
												  
				
							$login = "ok";

					
					}
					
				}
					
			}			
				

			if ($login == "ok")
			{
				return 1;
			}
			
		}

		// Acces Area Privada ?

		public function AreaPrivada($idpagina)
		{
			// Sessió usuari disparada?
			if ($this->app['session']->get(constant('General::nomsesiouser')) == "")
			{
				$this->app['session']->clear();
				header("Location: ".constant("DBB::url")."/login.html ");
				exit();
			}

			/* 
				Accés a página actual?
				Recupera págines on l'usuari te accés en un array.
			*/

			$fora = true;
			$permisos = array();
			$permisosescritura = array();


			$sql = $this->db->query("SELECT clau_pagina,escritura FROM pfx_permisosd
									  WHERE clau_rol = :clau_rol
									  AND clau_pla = :clau_pla
									  AND estat = 1",
							 array("clau_rol" => $this->app['session']->get(constant('General::nomsesiouser')."-permisos"), "clau_pla" => $this->app['session']->get("idpla"))
					);
		
		    if(!empty($sql))
	        {
		        foreach ($sql as $rs) 
	          	{		
			    	array_push($permisos, $rs['clau_pagina']);
			    	if ($rs['escritura'] == true)
			    	{
			    		array_push($permisosescritura, $rs['clau_pagina']);
			    	}
			    	
				}
			}

			


			( (in_array($idpagina, $permisos))? $fora=false : "" );
			// 999 id de pàgina on pot accedir tothom.
			( ($idpagina == 999)? $fora=false : "" );
			// Si l'usuari es superadmin pot accedir a tot arreu.
			//( ($_SESSION['user_admin']["clau_permisos"] == 999)? $fora=false : "");
			

			if ($fora){
				header("Location: ".constant("DBB::url")."/noaccess.html ");
				exit();
			}

			
		}

		// Està logejat ?

		public function isLogged()
		{
			if ($this->app['session']->get(constant('General::nomsesiouser')) == "")
			{
				$this->app['session']->clear();
				header("Location: ".constant("DBB::url")."/login.html ");
				exit();
			}

		}

		// Array Permisos. Genera un array amb totes les zones a les que està permés accedir o no segons el permís.
		public function ArrayPermisos($dades="")
		{
			$clau_permis = $this->app['session']->get(constant('General::nomsesiouser')."-permisos");

			if ($clau_permis != "")
			{
				/*
					1: Admin.
					2: Cap projecte.
					3: Usuari
					4: Visualitzar.
				*/

				
				$arraypermisos = array(
					/* Projectes */ 		'1'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Actuacions */ 		'2'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Tasques */ 			'3'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Usuaris */ 			'4'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Dimensions */ 		'5'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Origens */ 			'6'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Passos */ 			'7'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Obectius */ 			'8'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Indicadors */ 		'9'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Vinculació */ 		'10'  => array('nou' => true,
														  'editar' => true,
														  'guardar' => true,
														  'eliminar' => true,
												 		),
					/* Dintres */ 			'37'  => array('nou' => true,
														   'editar' => true,
														   'guardar' => true,
														   'eliminar' => false,
													 	 ),


					
					
				);

				if ($clau_permis == "2")
				{
					/* Projectes */
					// Projectes als que te accès.
					/*
					$condicio_permis = " AND t.cap_projecte = ".$this->app['session']->get(constant('General::nomsesiouser'));
					$projectes = $this->Llistats("projectes"," AND t.clau_pla = '".$this->app['session']->get('idpla')."'  $condicio_permis " , "id", false );
					if (!empty($projectes))
					{
						foreach ($projectes as $key => $value) 
						{
							$arraypermisos[1][projectes][$value[id]] = $value[id];
						}
						
					}
					*/
				}

				
				if ($clau_permis == "3")
				{
					/* Projectes */
					$arraypermisos[1][nou] = false;
					$arraypermisos[1][editar] = false;
					$arraypermisos[1][guardar] = false;
					$arraypermisos[1][eliminar] = false;
					/* Actuacions */
					$arraypermisos[2][nou] = false;
					$arraypermisos[1][editar] = false;
					$arraypermisos[2][guardar] = false;
					$arraypermisos[2][eliminar] = false;
					/* Tasques */
					$arraypermisos[3][nou] = false;
					$arraypermisos[1][editar] = false;
					$arraypermisos[3][guardar] = false;
					$arraypermisos[3][eliminar] = false;
					/* Usuaris */
					$arraypermisos[4][nou] = false;
					$arraypermisos[1][editar] = false;
					$arraypermisos[4][guardar] = false;
					$arraypermisos[4][eliminar] = false;
				}

				if ($clau_permis == "4" || $clau_permis == "5" )
				{
					/* Projectes */
					$arraypermisos[1][nou] = false;
					$arraypermisos[1][guardar] = false;
					$arraypermisos[1][eliminar] = false;
					// Projectes als que te accès.
					$condicio_permis = " AND FIND_IN_SET('".$this->app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
					$projectes = $this->Llistats("projectes"," AND t.clau_pla = '".$this->app['session']->get('idpla')."'  $condicio_permis ", array() , "id", false );
					if (!empty($projectes))
					{
						foreach ($projectes as $key => $value) 
						{
							$arraypermisos[1][projectes][$value[id]] = $value[id];
						}
						
					}
					/* Actuacions */
					$arraypermisos[2][nou] = false;
					$arraypermisos[2][guardar] = false;
					$arraypermisos[2][eliminar] = false;
					/* Tasques */
					$arraypermisos[3][nou] = false;
					$arraypermisos[3][guardar] = false;
					$arraypermisos[3][eliminar] = false;
					/* Usuaris */
					$arraypermisos[4][nou] = false;
					$arraypermisos[4][guardar] = false;
					$arraypermisos[4][eliminar] = false;
					/* Dimensions */
					$arraypermisos[5][nou] = false;
					$arraypermisos[5][guardar] = false;
					$arraypermisos[5][eliminar] = false;
					/* Origens */
					$arraypermisos[6][nou] = false;
					$arraypermisos[6][guardar] = false;
					$arraypermisos[6][eliminar] = false;
					/* Passos */
					$arraypermisos[7][nou] = false;
					$arraypermisos[7][guardar] = false;
					$arraypermisos[7][eliminar] = false;
					/* Obectius */
					$arraypermisos[8][nou] = false;
					$arraypermisos[8][guardar] = false;
					$arraypermisos[8][eliminar] = false;
					/* Indicadors */
					$arraypermisos[9][nou] = false;
					$arraypermisos[9][guardar] = false;
					$arraypermisos[9][eliminar] = false;
					/* Vinculació */
					$arraypermisos[10][nou] = false;
					$arraypermisos[10][guardar] = false;
					$arraypermisos[10][eliminar] = false;

						
				}
			}

			
			return $arraypermisos;



		}

		

		// Menú esquerra.
		function Menu($parent_id = 0)
		{

			$clau_rol = $this->app['session']->get(constant('General::nomsesiouser')."-permisos");

			$sql =  $this->db->query("SELECT distinct(p.id), nommenu_".$this->idioma.", p.id, parent_id ,ico, p.ordre
							    FROM pfx_pagines p
							    LEFT JOIN pfx_permisosd pd ON p.id = pd.clau_pagina
			                    WHERE p.estat = true AND p.tipus = 0
			                 	AND p.id <> 1 AND p.id <> 999 AND p.parent_id <> 999
			                  	AND pd.estat = true AND pd.clau_rol = :clau_rol
			                  	AND pd.clau_pla = :clau_pla
			                  	AND p.parent_id = :parent_id 
			                 	ORDER BY CASE p.ordre WHEN 0 THEN 1 ELSE -1 END ASC, p.ordre ASC, nommenu_".$this->idioma, 

				array("clau_rol"=>$clau_rol, "clau_pla"=>$this->app['session']->get('idpla'),"parent_id"=>$parent_id)
			);

          
	        if(!empty($sql))
	        {
	          $conta=1;	
	          foreach ($sql as $rs) 
	          {
					$id = $rs['id'];

					$titolurl = toAscii(str_replace("Ñ", "n", str_replace("ñ", "n", $rs['nommenu_'.$this->idioma])));
					
					$urlfile = $id.'/'.$titolurl.'.html';
					
					$subcats = "";
					$subcats = $this->Menu($rs['id']);

		            $return[] = array(
		              		'id' => $rs["id"],
		              		'parent_id' => $rs["parent_id"], 
		              		'titol' => _convert($rs["nommenu_".$this->idioma]),
		              		'contador' => $conta,
		              		'urlfile' => $urlfile,
		              		'subcats' => $subcats,
		              		'ico' => $rs["ico"],
		            );
		            
		            $conta++;
	          }

	        }
	        else
	        {
	            return false;
	        }
			
			return $return;

		}


		// Només podrán accedir amb permis d'administrador.
		public function NomesAdmins()
		{
			$clau_permis = $this->app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($clau_permis != 1)
			{
				echo '
					<script>
						window.top.location.href = "'.constant("DBB::url").'/noaccess.html";
					</script>
				';
				exit();
				header("Location: ".constant("DBB::url")."/noaccess.html ");
				exit();
			}
		}

		// Buscar pàgina per id.
		function Pagines($idpagina)
		{

			$return = "";

			$sql = $this->db->row("SELECT file, nommenu_".$this->idioma." as titol
								   FROM pfx_pagines 
								   WHERE estat = 1 AND id = :id
								   AND id <> 1 AND id <> 999 AND parent_id <> 999",
						array("id"=>$idpagina)
					);
		
		    if(!empty($sql))
	        {
	        	$rs = $sql;
			
	            $return = array( 
	            	
	            	'file' => $rs["file"],
	            	'titol' => _convert($rs["titol"]),

	            );

			}
		   
			return $return;

		}

		/*
			Traçabilitat de canvis.
		*/
		public function Canvis($dades)
		{
			$usuari = $this->app['session']->get(constant('General::nomsesiouser'));
			
			/*
				a: alta
				m: modi
				c: consolidar
			*/
			
			$arrayguardarcamp = array('taula' =>  "canvis", 
				'dades' =>  array(	
									'parent_id' => $dades[taula],
									'clau_usuari' => $usuari,
									'tipus' => $dades[tipus],
									'clau' => $dades[clau],
									'datai' => date("Y-m-d H:i:s"),
									'estat' => 1, 
									'titol_ca' => $dades[titol_ca], // Guardar dades del canvi, en alguns casos específics.
									'descripcio_ca' => $dades[descripcio_ca], // Guardar dades del canvi, en alguns casos específics.
									'clau_pla' => $this->app['session']->get('idpla'),

								 ),'tipus' =>  a);


			$this->GuardarRegistre2($arrayguardarcamp); 
		}

		/*
			Canvis en projectes.
		*/
		public function Canvis_proj($dades)
		{
			$usuari = $this->app['session']->get(constant('General::nomsesiouser'));
			
			/*
				Tipus:
				a: alta
				m: modi
				Taula:
				1: % execució
			*/
			
			$arrayguardarcamp = array('taula' =>  "canvis_proj", 
				'dades' =>  array(	
									'parent_id' => $dades[taula],
									'clau_usuari' => $usuari,
									'tipus' => $dades[tipus],
									'clau' => $dades[clau],
									'datai' => date("Y-m-d H:i:s"),
									'clau_pla' => $this->app['session']->get('idpla'),

								 ),'tipus' =>  a);


			$this->GuardarRegistre2($arrayguardarcamp); 
		}

		/*
			Guardar Registres.
		*/

		function GuardarRegistre($arrayguardar)
		{

			$return = "";
			$error = false;

			

			if ($arrayguardar[tipus] == a)
			{	

				// Duplicar array i eliminar les dades de les relacions per guardar-ho desprès.
				$arrayguardarrelacions = $arrayguardar;
				unset($arrayguardar[dades][post]);
				
				// Alta.
				//$db->beginTransaction();
				//mysql_query ("LOCK TABLES pfx_".$arrayguardar[taula]." WRITE");
				$arrayguardar[dades][user_inserted] = $this->app['session']->get(constant('General::nomsesiouser'));
		
				$fields     =  array_keys($arrayguardar[dades]);
				$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
				$sql 		= "INSERT INTO pfx_".$arrayguardar[taula]." (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")";
				
                $this->db->query("$sql",$arrayguardar[dades]); 
                $arrayguardarrelacions[id] = $this->db->lastInsertId();
                //mysql_query ("UNLOCK TABLES");

                
                $coditaula = $this->Llistats("codis"," AND nom = '".$arrayguardar[taula]."'");
                if (!empty($coditaula))
                {
                	$this->Canvis(array('taula' => $coditaula[1][codi], 'tipus' => 'a', 'clau' => $arrayguardarrelacions[id]));
                }
                

			


			}
			elseif ($arrayguardar[tipus] == m)
			{

				// Duplicar array i eliminar les dades de les relacions per guardar-ho desprès.
				$arrayguardarrelacions = $arrayguardar;
				unset($arrayguardar[dades][post]);
				unset($arrayguardar[dades][permisosd]);
			
				// Modificació.
				$arrayguardar[dades][user_updated] = $this->app['session']->get(constant('General::nomsesiouser'));
				date_default_timezone_set('Europe/Andorra');
				$arrayguardar[dades][updated] = date("Y-m-d H:i:s");

				$fields     =  array_keys($arrayguardar[dades]);
				foreach($fields as $fields)
				{
					$fieldsvals .= $fields . " = :". $fields . ",";
				}
				$fieldsvals = substr_replace($fieldsvals , '', -1);

				$sql = "UPDATE pfx_".$arrayguardar[taula]." SET " . $fieldsvals . " WHERE id = :id ";

				$arrayguardar[dades][id] = intval($arrayguardar[id]);
				if (!$this->db->query("$sql",$arrayguardar[dades]) ) $error = true;

				
				$coditaula = $this->Llistats("codis"," AND nom = '".$arrayguardar[taula]."'");
                if (!empty($coditaula))
                {
                	$this->Canvis(array('taula' => $coditaula[1][codi], 'tipus' => 'm', 'clau' => $arrayguardar[id]));
                }
				

				// Guardar Permisos del rol.
				if ( isset($arrayguardarrelacions[dades][permisosd]) )
				{
					unset($arrayguardarrelacions[dades][post]);
					$dbb = new General($this->idioma, $this->app);
					$dbb->GuardarPermisosd($arrayguardarrelacions);
		            unset($dbb);
				}
				

			}
			else{
				$error = true;
			}
			

			

			if ($error == false)
			{
				$return = array("1",$arrayguardarrelacions[id]);
			}else{
				$return = array("0",$arrayguardarrelacions[id]);
			}

			// $arrayguardar[taula]=="projectes_vinculacio"||
			if ($arrayguardar[taula]=="projectes_organs_e"||$arrayguardar[taula]=="projectes"){
				return $return;
			}else{
				echo json_encode($return);
			}
			

		}

		/*
			Guardar Registres2. Per no fer servir el mateix que criden els formularis, per evitar els "echo".
		*/

		function GuardarRegistre2($arrayguardar)
		{

			$return = "";
			$error = false;


			if ($arrayguardar[tipus] == a)
			{	

				// Alta.
                $arrayguardar[dades][user_inserted] = $this->app['session']->get(constant('General::nomsesiouser'));

				$fields     =  array_keys($arrayguardar[dades]);
				$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
				$sql 		= "INSERT INTO pfx_".$arrayguardar[taula]." (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")";			

                $this->db->query("$sql",$arrayguardar[dades]); 
                $arrayguardarrelacions[id] = $this->db->lastInsertId();
              
			}
			elseif ($arrayguardar[tipus] == m)
			{

				// Modificació.
				$arrayguardar[dades][user_updated] = $this->app['session']->get(constant('General::nomsesiouser'));
				date_default_timezone_set('Europe/Andorra');
				$arrayguardar[dades][updated] = date("Y-m-d H:i:s");

				$fields     =  array_keys($arrayguardar[dades]);
				foreach($fields as $fields){
					$fieldsvals .= $fields . " = :". $fields . ",";
				}
				$fieldsvals = substr_replace($fieldsvals , '', -1);

				$sql = "UPDATE pfx_".$arrayguardar[taula]." SET " . $fieldsvals . " WHERE id = :id ";
				$arrayguardar[dades][id] = intval($arrayguardar[id]);
				if (!$this->db->query("$sql",$arrayguardar[dades]) ) $error = true;

				
			}
			else{
				$error = true;
			}
				

			

			if ($error == false)
			{
				return 1;
			}else{
				return 0;
			}

		}


		
		/*
			Eliminar Registre.
		*/
		public function EliminarRegistre($taula, $where, $id, $config)
		{

			// Extreure codi de la taula.
			$sql_codi = $this->db->row("SELECT nom FROM pfx_codis WHERE tipus = 'taula' AND codi = :codi AND estat = 1 ",
							array("codi"=>$taula)
						);
		
		    if(!empty($sql_codi))
	        {
	        	$rs_codi = $sql_codi;
				$nomtaula = $rs_codi['nom'];
				
			}


			$file = "";

			// L'usuari pot eliminar d'aquesta taula? sinó es comprova podrien manipular el /load i forçar eliminacions sense permís.
			// TODO: fer una taula amb codi rol + array amb taules a les que te accès i pot eliminar.

			if ($nomtaula == "upfile")
			{


				$Imatge = $this->Llistats("upfile"," AND t.id = :id",array("id"=>$id));

				$file = $Imatge[1][nomfile];


				if (!empty($file))
				{
					$nomfile = __DIR__."/../../../../images/login/$file" ;

					if (is_file($nomfile)) {
	                    unlink($nomfile);
	                }

	                if ($Imatge[1][taula] != "portada"){

	                	$nomfile = __DIR__."/../../../../../secretfiles/$file" ;
		                $nomfileth = __DIR__."/../../../../../secretfiles/thumbnail/$file" ;

		                if (is_file($nomfile)) {
		                    unlink($nomfile);
		                }
						if (is_file($nomfileth)) {
		                    unlink($nomfileth);
		                }

	                }
	                
	                
				}

				$data = array();
				$data['nomfile'] = "";

				//$mysql->update("pfx_upfile", $data,'id='.intval($id)) or die(mysql_error());
				
					
			}

			if ($nomtaula == "projectes_operadors"){

				$idprojecte = intval($config['ctf']);

				$condicio_permis = " AND  FIND_IN_SET($id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0";

				$Projectes =  $this->FreeSql("SELECT id
											 FROM pfx_projectes p
											 WHERE 1=1 $condicio_permis AND p.id <> $idprojecte
											 ",array());

				$Tasques =  $this->FreeSql("SELECT id
											 FROM pfx_tasques p
											 WHERE 1=1 $condicio_permis AND p.id <> $idprojecte
											 ",array());

				if (!empty($Projectes) || !empty($Tasques))
				{
					echo '<script>alert("Aquest registre està en us, no es pot eliminar.");</script>';
					exit();
				}
			}

			if ($nomtaula == "projectes"){

				$idprojecte = intval($id);

				$this->db->query("DELETE FROM pfx_actuacions
						     	   WHERE clau_projecte = '$idprojecte' ");

				$this->db->query("DELETE FROM pfx_tasques
						     	   WHERE clau_projecte = '$idprojecte' ");


			}
			
			$result = $this->db->query("DELETE FROM pfx_$nomtaula
								     	WHERE $where ");
			
			// TODO: canviar a delete PDO??
			//$db->query("DELETE FROM Persons WHERE Id = :id",array("id"=>"6"));
				
				
	       

			return $result;

		}


		// Llistats.
		public function Llistats($taula, $wheresql = "", $wherearray = "", $ordre = "ordre", $mostrarestat = true, $xifrats = array())
		{
			($mostrarestat == true) ? $mostrarestat = "t.estat = true" : $mostrarestat = "1 = 1" ;

			$sql = $this->db->query("SELECT t.*
									 FROM pfx_".$taula." t
		                             WHERE $mostrarestat $wheresql ORDER BY $ordre", $wherearray);
		
		    if(!empty($sql))
	        {
		          
		        foreach ($sql as $conta2 =>$rs) 
	          	{    
	          		$conta = $conta2+1;
			 	  
					$return[$conta] = $rs;//_convert($code);

					if (isset($rs["id"])){
						$return[$conta]["idx"] = md5(constant('General::tockenid').$rs["id"]);
					}
					if (isset($rs["descripcio_$this->idioma"])){
						$return[$conta]["descripcio"] = _convert($rs["descripcio_$this->idioma"]);
					}
					if (isset($rs["titol_$this->idioma"])){
						$return[$conta]["titol"] = _convert($rs["titol_$this->idioma"]);
					}
					if (isset($rs["nom_$this->idioma"])){
						$return[$conta]["nom"] = _convert($rs["nom_$this->idioma"]);
					}
					if (isset($rs["contingut_$this->idioma"])){
						$return[$conta]["contingut"] = _convert($rs["contingut_$this->idioma"]);
					}
					if (isset($rs["slug_$this->idioma"])){
						$return[$conta]["slug"] = _convert($rs["slug_$this->idioma"]);
					}
					if (!empty($xifrats)){
						foreach ($xifrats as $key_x => $value_x) {
							if (isset($rs["$value_x"])){
								$return[$conta]["$value_x"] = decryptRSA($rs["$value_x"]);
							}
						}
					}
					
			
				}

	        }
	        else
	        {
	            return false;
	        }
			
			return $return;
		}

		// FreeSQL.
		public function FreeSql($query,$wherearray="")
		{
			
			$sql = $this->db->query("$query", $wherearray);
		
		    if(!empty($sql))
	        {
		          
		        foreach ($sql as $conta2 =>$rs) 
	          	{    
	          		$conta = $conta2+1;

	          		$return[$conta] = $rs;//_convert($code);

	          		if (isset($rs["id"])){
						$return[$conta]["idx"] = md5(constant('General::tockenid').$rs["id"]);
					}
					if (isset($rs["descripcio_$this->idioma"])){
						$return[$conta]["descripcio"] = _convert($rs["descripcio_$this->idioma"]);
					}
					if (isset($rs["titol_$this->idioma"])){
						$return[$conta]["titol"] = _convert($rs["titol_$this->idioma"]);
					}
					if (isset($rs["nom_$this->idioma"])){
						$return[$conta]["nom"] = _convert($rs["nom_$this->idioma"]);
					}
					if (isset($rs["contingut_$this->idioma"])){
						$return[$conta]["contingut"] = _convert($rs["contingut_$this->idioma"]);
					}
					if (isset($rs["slug_$this->idioma"])){
						$return[$conta]["slug"] = _convert($rs["slug_$this->idioma"]);
					}
			 	  
			
				}

	        }
	        else
	        {
	            return false;
	        }

		   
			return $return;
		}

		/*
			Generar Llistats Nous.
		*/
		public function GenerarLlistatsNou($taula, $query, $where, $camps, $capcaleres, $configuracio = array())
		{
			// Valors per defecte de config.
			if (empty($configuracio['mostarestat']) ) $configuracio['mostarestat'] = si ;
			if (empty($configuracio['text-estat']) ) $configuracio['text-estat'] = '' ;
			if (empty($configuracio['mostrareliminar']) ) $configuracio['mostrareliminar'] = si ;
			if (empty($configuracio['enmodal']) ) $configuracio['enmodal'] = '' ;
			if (empty($configuracio['idmodal']) ) $configuracio['idmodal'] = 'modal' ;
			if (empty($configuracio['ctf']) ) $configuracio['ctf'] = '' ;
			if (empty($configuracio['ctf2']) ) $configuracio['ctf2'] = '' ;
			if (empty($configuracio['paginaconsulta']) ) $configuracio['paginaconsulta'] = '' ;
			if (empty($configuracio['columnaordre']) ) $configuracio['columnaordre'] = ',"order": [[ 0, "desc" ]],' ;

			// En algun cas el llistat resultant de l'eliminació no s'ha de mostrar amb l'opció 2 i l'id no ha de ser 1.
			$opciollistat = 2;
			$ideliminacio = 1;

			$clau_permis = $this->app['session']->get(constant('General::nomsesiouser')."-permisos");
			$arraypermisos = $this->ArrayPermisos();

			// Extreure codi de la taula.
			$sql_codi = $this->db->row("SELECT codi FROM pfx_codis WHERE tipus = 'taula' AND nom = :nom AND estat = 1 ",
							array("nom"=>$taula)
						);
		
		    if(!empty($sql_codi))
	        {
	        	$rs_codi = $sql_codi;
				$coditaula = $coditaulaeliminar = $rs_codi['codi'];
				
			}


			// Variable temps per evitar coincidéncies en els botons.
			$time = $configuracio["time"];

			$result = '

				<table id="dataTables_'.$time.'" class="display dataTables" cellspacing="0" width="100%">
					<thead>
						<tr>
			';
							foreach ($capcaleres as $keycap => $value) 
							{
							  	$result .= '<th '.($taula=="upfile"&&$keycap==1?"width=300":"").($taula=="upfile"&&$keycap!=4&&$keycap!=1?"width=150":"").($taula=="projectes_urls"&&$keycap==1?"width=300":"").($taula=="projectes_urls"&&$keycap!=4&&$keycap!=1?"width=150":"").'>'.$value.'</th>';
							}

							if ($configuracio['mostarestat'] == si)
						  	{
			$result .=			' <th>'.$this->app['translator']->trans("Estat").'</th>';
						  	}

			$result .=		'<th width="20%">'.$this->app['translator']->trans("Accions").'</th>
				
						</tr>
					</thead>
				</table>

				<script type="text/javascript" language="javascript" class="init">

					$(document).ready(function() {

						$(document).off("click",".botomodal").on("click",".botomodal",function(event){
							event.preventDefault();
						});
					

						$(document).off("click",".modiregistre_'.$time.'").on("click",".modiregistre_'.$time.'",function(event){
				        	
				        	event.preventDefault();

				        	var id = this.id;
				        	var idoriginal = this.id;
				        	var t = "'.$coditaula.'";
				        	id = id.replace("modi_","");
							$("#divcamps").html(\'<div  style="text-align: center;"><img src="'.constant("DBB::url").'/images/loading.gif" /></div>\');
				        	$(".amagamissatges").html("");
				        	$(".panelldades").show();
				        	$("#divcamps").show();
				    		$("#divcamps").load("'.constant("DBB::url").'/load", {id: id, o: 3, t: t});
				        });

						$(document).off("click",".elimregistre_'.$time.'").on("click",".elimregistre_'.$time.'",function(event){

							event.preventDefault();

				        	if (confirm("'.$this->app['translator']->trans("Segur que vols eliminar el registre?").'"))
							{
								
								';
									// Condició especial per els passos per poder eliminar i recarregar el llistat que toca.
								$anexnomllistat = "";
								if ($coditaula == 7)
								{
									$anexnomllistat = "_".$configuracio['ctf2'];
								}

								$result .= '
								var id = this.id;
					        	id = id.replace("elim_","");
					        	var t = "'.$coditaula.'";
					        	var ctf = "'.$configuracio['ctf'].'";
					        	var ctf2 = "'.$configuracio['ctf2'].'";
					        	var em = "'.$configuracio['enmodal'].'";
					        	var mod = "'.$configuracio['idmodal'].'";
					        	$(".llistat_'.$coditaula.'").html(\'<div  style="text-align: center;"><img src="'.constant("DBB::url").'/images/loading.gif" /></div>\');

				        		$(".llistat_'.$coditaula.$anexnomllistat.'").load("'.constant("DBB::url").'/load",{o:4,id:id, t: t, c:{ctf:ctf,ctf2:ctf2}}, function() {
						    		$(".llistat_'.$coditaula.$anexnomllistat.'").load("'.constant("DBB::url").'/load",{o:'.$opciollistat.',id:'.$ideliminacio.', t: "'.$coditaulaeliminar.'",c:{em:em,mod:mod,ctf:ctf,ctf2:ctf2}});
								});
								return true;
							}
				        });


				        function naturalSort (a, b, html) {
						    var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?%?$|^0x[0-9a-f]+$|[0-9]+)/gi,
						        sre = /(^[ ]*|[ ]*$)/g,
						        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
						        hre = /^0x[0-9a-f]+$/i,
						        ore = /^0/,
						        htmre = /(<([^>]+)>)/ig,
						        // convert all to strings and trim()
						        x = a.toString().replace(sre, \'\') || \'\',
						        y = b.toString().replace(sre, \'\') || \'\';
						        // remove html from strings if desired
						        if (!html) {
						            x = x.replace(htmre, \'\');
						            y = y.replace(htmre, \'\');
						        }
						        // chunk/tokenize
						    var xN = x.replace(re, \'\0$1\0\').replace(/\0$/,\'\').replace(/^\0/,\'\').split(\'\0\'),
						        yN = y.replace(re, \'\0$1\0\').replace(/\0$/,\'\').replace(/^\0/,\'\').split(\'\0\'),
						        // numeric, hex or date detection
						        xD = parseInt(x.match(hre), 10) || (xN.length !== 1 && x.match(dre) && Date.parse(x)),
						        yD = parseInt(y.match(hre), 10) || xD && y.match(dre) && Date.parse(y) || null;
						 
						    // first try and sort Hex codes or Dates
						    if (yD) {
						        if ( xD < yD ) {
						            return -1;
						        }
						        else if ( xD > yD ) {
						            return 1;
						        }
						    }
						 
						    // natural sorting through split numeric strings and default strings
						    for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
						        // find floats not starting with \'0\', string or 0 if not defined (Clint Priest)
						        var oFxNcL = !(xN[cLoc] || \'\').match(ore) && parseFloat(xN[cLoc], 10) || xN[cLoc] || 0;
						        var oFyNcL = !(yN[cLoc] || \'\').match(ore) && parseFloat(yN[cLoc], 10) || yN[cLoc] || 0;
						        // handle numeric vs string comparison - number < string - (Kyle Adams)
						        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) {
						            return (isNaN(oFxNcL)) ? 1 : -1;
						        }
						        // rely on string comparison if different types - i.e. \'02\' < 2 != \'02\' < \'2\'
						        else if (typeof oFxNcL !== typeof oFyNcL) {
						            oFxNcL += \'\';
						            oFyNcL += \'\';
						        }
						        if (oFxNcL < oFyNcL) {
						            return -1;
						        }
						        if (oFxNcL > oFyNcL) {
						            return 1;
						        }
						    }
						    return 0;
						}
						 
						jQuery.extend( jQuery.fn.dataTableExt.oSort, {
						    "natural-asc": function ( a, b ) {
						        return naturalSort(a,b,true);
						    },
						 
						    "natural-desc": function ( a, b ) {
						        return naturalSort(a,b,true) * -1;
						    },
						 
						    "natural-nohtml-asc": function( a, b ) {
						        return naturalSort(a,b,false);
						    },
						 
						    "natural-nohtml-desc": function( a, b ) {
						        return naturalSort(a,b,false) * -1;
						    },
						 
						    "natural-ci-asc": function( a, b ) {
						        a = a.toString().toLowerCase();
						        b = b.toString().toLowerCase();
						 
						        return naturalSort(a,b,true);
						    },
						 
						    "natural-ci-desc": function( a, b ) {
						        a = a.toString().toLowerCase();
						        b = b.toString().toLowerCase();
						 
						        return naturalSort(a,b,true) * -1;
						    }
						} );
						

						var taulaa = $("#dataTables_'.$time.'").dataTable( {
						"aProcessing": true,
						"aServerSide": true,
			';

						($coditaula==59?$pathtaula="":$pathtaula = "../");

						if ($coditaula==58 || $coditaula==63 || $coditaula==12 || $coditaula==54){
							$result .= '
						        "paging":   false,
						        "ordering": false,
						        "info":     false,
						        "bFilter":   false,
						    ';
					    }
					  

			$result .= '
						"ajax": "'.$pathtaula.'taula?o=2&id=1&t='.$coditaula.'&'.http_build_query(array('c'=>array('t'=>$time,'ctf'=>$configuracio['ctf'],'ctf2'=>$configuracio['ctf2'],'em'=>$configuracio['enmodal'],'mod'=>$configuracio['idmodal']))).'",
			            "language": {
			';
			        	if ($this->idioma == "ca" )
			  			{
							$result .= '
							            "url": "'.$pathtaula.'js/plugins/dataTables/dataTables.catala.lang"
							';
						}
						else
						{
							$result .= '
							            "url": "'.$pathtaula.'js/plugins/dataTables/dataTables.castellano.lang"
							';
						}

	        $result .= '
			            }

			            '.$configuracio['columnaordre'].'

			            } );
			';
						if ($coditaula == 28)
			       		{
			       			/* Per posar fons de colors
			       			$result .= '
								taulaa.rows().every( function ( rowIdx, tableLoop, rowLoop ) {       
							        var cell = table.cell({ row: rowIdx, column: 0 }).node();
							        $(cell).addClass(\'warning\');        
							    });
			       			';
			       			*/
			       		}

			$result .= '

					} );

				</script>
			';

			


			return $result;


		}

		/*
			Generar Llistats.
		*/
		public function GenerarLlistats($taula, $query, $camps, $capcaleres, $configuracio = array())
		{
			
			// Valors per defecte de config.
			if (empty($configuracio['mostarestat']) ) $configuracio['mostarestat'] = si ;
			if (empty($configuracio['text-estat']) ) $configuracio['text-estat'] = '' ;
			if (empty($configuracio['mostrareliminar']) ) $configuracio['mostrareliminar'] = si ;
			if (empty($configuracio['enmodal']) ) $configuracio['enmodal'] = '' ;
			if (empty($configuracio['idmodal']) ) $configuracio['idmodal'] = 'modal' ;
			if (empty($configuracio['ctf']) ) $configuracio['ctf'] = '' ;
			if (empty($configuracio['ctf2']) ) $configuracio['ctf2'] = '' ;
			if (empty($configuracio['paginaconsulta']) ) $configuracio['paginaconsulta'] = '' ;

			// En algun cas el llistat resultant de l'eliminació no s'ha de mostrar amb l'opció 2 i l'id no ha de ser 1.
			$opciollistat = 2;
			$ideliminacio = 1;

			$clau_permis = $this->app['session']->get(constant('General::nomsesiouser')."-permisos");
			$arraypermisos = $this->ArrayPermisos();
			

			

				// Extreure codi de la taula.
				$sql_codi = $this->db->row("SELECT codi FROM pfx_codis WHERE tipus = 'taula' AND nom = :nom AND estat = 1 ",
								array("nom"=>$taula)
							);
			
			    if(!empty($sql_codi))
		        {
		        	$rs_codi = $sql_codi;
					$coditaula = $coditaulaeliminar = $rs_codi['codi'];
					
				}

				// Variable temps per evitar coincidéncies en els botons.
				$time = uniqid();


				$result = '

					<div class="col-lg-12">
			            	
			            <div class="panel panel-default">

			            <!--
			                <div class="panel-heading">
			                    Títol
			                </div>
			            -->
				            <div class="panel-body">
		                    <div class="table-responsive" style="overflow:hidden !important;">
		                        <table class="table table-striped table-bordered table-hover" id="dataTables_'.$time.'">
		                            <thead>
		                                <tr>
		        ';
		        							foreach ($capcaleres as $value) 
											{
											  	$result .= '<th>'.$value.'</th>';
											}
		                                   
				
										  	if ($configuracio['mostarestat'] == si)
										  	{
				$result .=						' <th>'.$this->app['translator']->trans("Estat").'</th>';
										  	}
				$result .=	'    		  	<th>'.$this->app['translator']->trans("Accions").'</th>
									  	</tr>
								  	</thead>   
									<tbody>
				';

									// Llistat registres
									
										
										$sql = $this->db->query("$query",array());

										if(!empty($sql))
								        {
									        foreach ($sql as $rs) 
								          	{    

												if ($rs["estat"] == 1){
													$estat = '<span class="label label-success">'.$this->app['translator']->trans("Actiu").'</span>';
												}else{
													$estat = '<span class="label label-default">'.$this->app['translator']->trans("Inatiu").'</span>';
												}
												
												$result .= '
												  	<tr class="odd">
												';


												foreach ($camps as $value) 
												{
													$result .=  '<td>'. _convert(eval("return $value;")).'</td>';
												}

												if ($configuracio['mostarestat'] == si)
												{
													$result .= '
														<td class="center">'.$estat.'</td>
													';
												}
												if ($configuracio['text-estat'] != "")
												{
													if ($rs["estat"] == 0) { $estat = "Pendiente"; $estil ="label-important"; }
													if ($rs["estat"] == 1) { $estat = "Entregado"; $estil ="label-success"; }
													if ($rs["estat"] == 2) { $estat = "Cancelado"; $estil =""; }

													$estat = "<span class=\"label $estil\">$estat</span>";

													$result .= '
														<td class="center">'.$estat.'</td>
													';
												}

										  		
												if ($configuracio['mostrareliminar'] == si  )
										  		{
										  			if ( $taula != "usuaris" || ($taula == "usuaris" && $rs["id"] != 1))
										  			{	
										  				if ($arraypermisos[$coditaula][eliminar] == true || $clau_permis == 1 || $clau_permis == 2 )
														{
											  				$botoeliminar = '
												  				<button class="btn btn-danger btn-xs elimregistre_'.$time.'" href="#" id=elim_'.$rs["id"].'>
																	<i class="fa fa-trash"></i>
																</button>
												  			';
												  		}
										  			}
										  		}

										  		if ($configuracio['enmodal'] == si  )
										  		{
										  			$result .= '
															<td class="center">
																'.$botoveureact.'
															';

													if ( ($arraypermisos[$coditaula][editar] == true || $clau_permis == 1 || $clau_permis == 2 ) && $taula != "upfile" )
													{
														$result .= '
																<button class="btn btn-info btn-xs botomodal edicio_'.$coditaula.'" id="modi_'.$rs["id"].'" href="#" data-toggle="modal" data-id="'.$rs["id"].'" data-ctf="'.$configuracio['ctf'].'" data-t="'.$coditaula.'" data-target="#'.$configuracio['idmodal'].'">
																	<i class="fa fa-edit"></i>
																</button>
														';
													}
													elseif ($taula == "upfile")
													{
														
														$ranid = base64_encode($rs["id"]);

														$tocken = md5('TockenSecretViewDBB!'.$rs["id"]);

														$result .= '

																<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'" target="_blank">
																	<button class="btn btn-success btn-xs" type="button" >
																		<i class="fa fa-download"></i>
																	</button>
																</a>
														';
													}

													$result .= '			
																'.$botoeliminar.'
															</td>
														</tr>

													';
										  		}
										  		else
										  		{
										  			$result .= '
															<td class="center">
																'.$botoveureact.'
															';

													if (($arraypermisos[$coditaula][editar] == true || $clau_permis == 1 || $clau_permis == 2 ) AND ($configuracio['paginaconsulta'] == '') )
													{
														$result .= '
																<button class="btn btn-info btn-xs modiregistre_'.$time.'" href="#" id=modi_'.$rs["id"].'>
																	<i class="fa fa-edit"></i>
																</button>
														';
													}
													elseif ($configuracio['paginaconsulta'] == si )
													{
														if ($taula == "projectes") $idmostrar = $rs["id"];
														if ($taula == "actuacions") $idmostrar = $rs["idprojecte"];
														if ($taula == "tasques") $idmostrar = $rs["idprojecte"];
														$result .= '
															
																<button class="btn btn-primary btn-xs">
																	<a href="'.constant("DBB::url").'/19/consultes.html?p=2&id='.$idmostrar.'" target="_blank">CONSULTAR</a>
																</button>
															
														';
													}

													$result .= '	
																'.$botoeliminar.'
															</td>
														</tr>

													';
										  		}

										  		
										  		
												
											}
										}	
						           
									
				$result .= '
								
								  	</tbody>
		                        </table>
		                    </div>
		                    <!-- /.table-responsive -->
		                   
		                </div>
		                <!-- /.panel-body -->
		            </div>
		            <!-- /.panel -->
		        </div>
		        <!-- /.col-lg-12 -->

					
				<script>

					$(document).on("click",".botomodal",function(event){
						 event.preventDefault();
					});

					$(document).on("click",".modiregistre_'.$time.'",function(event){
			        	
			        	event.preventDefault();

			        	var id = this.id;
			        	var idoriginal = this.id;
			        	var t = "'.$coditaula.'";
			        	id = id.replace("modi_","");
						$("#divcamps").html(\'<div  style="text-align: center;"><img src="'.constant("DBB::url").'/images/loading.gif" /></div>\');
			        	$(".amagamissatges").html("");
			        	$(".panelldades").show();
			        	$("#divcamps").show();
			    		$("#divcamps").load("'.constant("DBB::url").'/load", {id: id, o: 3, t: t});
			        });

					$(document).on("click",".elimregistre_'.$time.'",function(event){

						 event.preventDefault();

			        	if (confirm("'.$this->app['translator']->trans("Segur que vols eliminar el registre?").'"))
						{
							
							';
								// Condició especial per els passos per poder eliminar i recarregar el llistat que toca.
							$anexnomllistat = "";
							if ($coditaula == 7)
							{
								$anexnomllistat = "_".$configuracio['ctf2'];
							}

							$result .= '
							var id = this.id;
				        	id = id.replace("elim_","");
				        	var t = "'.$coditaula.'";
				        	var ctf = "'.$configuracio['ctf'].'";
				        	var ctf2 = "'.$configuracio['ctf2'].'";
				        	var em = "'.$configuracio['enmodal'].'";
				        	var mod = "'.$configuracio['idmodal'].'";
				        	$(".llistat_'.$coditaula.'").html(\'<div  style="text-align: center;"><img src="'.constant("DBB::url").'/images/loading.gif" /></div>\');

			        		$(".llistat_'.$coditaula.$anexnomllistat.'").load("'.constant("DBB::url").'/load",{o:4,id:id, t: t}, function() {
					    		$(".llistat_'.$coditaula.$anexnomllistat.'").load("'.constant("DBB::url").'/load",{o:'.$opciollistat.',id:'.$ideliminacio.', t: "'.$coditaulaeliminar.'",c:{em:em,mod:mod,ctf:ctf,ctf2:ctf2}});
							});
							return true;
						}
			        });


					$("#dataTables_'.$time.'").dataTable( {
			            "language": {
			        ';
			        	if ($this->idioma == "ca" )
			  			{
							$result .= '
							            "url": "'.constant("DBB::url").'/js/plugins/dataTables/dataTables.catala.lang"
							';
						}
						else
						{
							$result .= '
							            "url": "'.constant("DBB::url").'/js/plugins/dataTables/dataTables.castellano.lang"
							';
						}

	        $result .= '
			            }
			';
			
			if ($taula == "projectes_organs")
  			{
				$result .= '
				            ,"order": [[ 2, "desc" ]],
				';
			}
		
			$result .= '
			        } );

			

				</script>  




				';

			

			return $result;

		}

		/*
			Arbre de pàgines.
		*/
	    function ArbrePagines($parent_cat_id, $level_string, $cats)
	    {
	    	     
	        $return_str='';
	        if(!$level_string)
	        {
	             $level_string='';
	        }

	        $query = "SELECT * FROM pfx_pagines
				   	  WHERE estat = true AND id <> 1 AND id <> 999 AND parent_id= :parent_id
					  ORDER BY CASE ordre WHEN 0 THEN 1 ELSE -1 END ASC, ordre ASC";

			$wherearray = array("parent_id"=>$parent_cat_id);
	          
	        $sql = $this->db->query("$query", $wherearray);
		
		    if(!empty($sql))
	        {
		        $return_str.= '<ul>';

		        foreach ($sql as $conta2 =>$rs2) 
	          	{  
					if (in_array($rs2['id'], $cats)){$checked = "checked"; $collapsed="";}else{$checked = "";$collapsed="collapsed";}
                  	$return_str.='<li class="'.$collapsed.'"><input type="checkbox" value="'.$rs2["id"].'" '.$checked.' name="permisosd[]" id="permisosd-'.$rs2["id"].'" class="permisosd" >'.$level_string.' '._convert($rs2["nommenu_ca"]);
                  	$return_str.=$this->ArbrePagines($rs2['id'], $level_string.'',$cats);
	            }

	            $return_str.= '</ul>';
	        }
	        else
	        {
	            return false;
	        }
	        return $return_str;
	    }

	    /*
			Guardar Permisos.
		*/
		function GuardarPermisosd($arrayguardar)
		{


			$error = false;

			// Permisos.
			$permisos = $arrayguardar[dades][permisosd];


			// Eliminar permisos previs.

			$sql = "DELETE FROM pfx_permisosd WHERE clau_rol = :clau_rol AND clau_pla = :clau_pla ";
			$arraywhere = array("clau_rol" => $arrayguardar[id], "clau_pla" => $this->app['session']->get('idpla'));
			
			$result = $this->db->query("$sql",$arraywhere);

			if (!empty($permisos)){
				foreach ($permisos as $key => $value){
					$arrayguardarrelacio = array('taula' =>  'permisosd', 'dades' =>  array('clau_rol' => $arrayguardar[id], 'clau_pagina' => $value, 'estat' => 1, 'escritura' => 1, 'clau_pla' => $this->app['session']->get('idpla') ),'tipus' =>  a,);
					$this->GuardarRegistre2($arrayguardarrelacio); 
				}
			}

			


			if ($error == false)
			{
				$return = true;
			}else{
				$return = false;
			}
			
			

			return $return;
		}

		/*
			Generar Upload.
		*/
		public function GenerarUpload($tipus, $pfxfitxer, $id, $id_prov, $idcua, $idgaleria, $formats, $thumb, $w="", $h="")
		{

			(($tipus == 1)? $nomboto = "Imatge" : $nomboto = "Cargar Documentos" );
			$timestamp = time();
			if ($id == "") $id=1;
			$return = '

					<br>
					<div style="float:left;position:relative;" class="butoupload"><input id="file_upload'.$tipus.'" name="file_upload'.$tipus.'" type="file" multiple="false" ></div>
					<script>
						$(function() {
							$("#file_upload'.$tipus.'").uploadify({
								"formData"     : {
									"timestamp"    : "'.$timestamp.'",
									"token"        : "'.md5('SecretUploadDBB2014' . $timestamp).'",
									"folder" 	   : "'.$pfxfitxer.'",
									"thumb" 	   : "'.$thumb.'",
									"w" 	   	   : "'.$w.'",
									"h" 	  	   : "'.$h.'",
									"tipus" 	   : "'.$tipus.'",
									"id"		   : "'.$id.'",
									"id_prov"	   : "'.$id_prov.'",
								},
								/*"checkExisting" : "js/upimatges/check-exists.php",*/
								"buttonText"      : "<i class=\"fa fa-cloud-upload\"></i> '.$nomboto.'",
								"queueID"		  : "'.$idcua.'",
								"width"           : 120,
								"height"          : 23,
								"fileTypeExts"    : "", 
								"swf"      		  : "'.constant("DBB::url").'/js/upimatges/uploadify.swf",
								"uploader" 		  : "'.constant("DBB::url").'/upload",
								"multi"    		  : false,
								"onUploadError"   : function(file, errorCode, errorMsg, errorString) { alert("El fichero " + file.name + " no puede ser cargado: " + errorString); },
								"onUploadComplete": function(file) {
									var id = "'.$id.'";/*$("#'.$idgaleria.'").data("id");*/
									$("#'.$idgaleria.'").html("<div  style=\"text-align: center;\"><img src=\"'.constant("DBB::url").'/images/loading.gif\" /></div>");
									$("#'.$idgaleria.'").load("'.constant("DBB::url").'/load", {id: id, o: 3, t: "9999", c: [{"t":"'.$pfxfitxer.'"}]} ); 
								},
						        "onSelectError"  : function() { alert("El fichero " + file.name + " ha producido un error y no puede ser cargado."); },
						        //"onUploadSuccess" : function(file, data, response) {
							    //    alert("The file was saved to: " + data);
							    //}
							});
						});
					</script>


					<br>
					<hr>
			';

			return $return;
		}

		

		

		/*
			Download.
		*/
		public function Descarregues($nomfile)
		{
			header("Cache-Control: no-cache, must-revalidate"); 
			header('Pragma: no-cache'); 

			$fileDir = constant("pathfiles");
			$nomfile = base64_decode($nomfile);
			$extensio = ".".pathinfo($nomfile, PATHINFO_EXTENSION);

			if (isset($nomfile)){
				switch($extensio) {

					case ".pdf":
						$ctype = "application/pdf";
						break;
					case ".doc":
						$ctype = "application/msword";
						break;
					case ".docx":
						$ctype = "application/msword";
						break;
					case ".xls":
						$ctype = "application/vnd.ms-excel";
						break;
					case ".xlsx":
						$ctype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
						break;
					case ".txt":
						$ctype = "plain/text";
						break;
					default:
						$ctype = "application/force-download";
				}
				
				
				//md5($file)
				
				if(file_exists($fileDir . $nomfile ))
				{   
					header('Content-type:  '.$ctype);
					header('Content-Disposition: attachment; filename="'.$this->app['translator']->trans('base.nomempresa').$extensio.'"'); 
				
					readfile($fileDir . $nomfile); 
				} 
				else 
				{   
					echo "No se ha encontrado el fichero"; 
				} 
			}
			
			
			flush(); 

			return true;
		}

		// Validar Password

		public function validar_password($clave,&$error_clave){
		   if(strlen($clave) < 6){
		      $error_clave = "La clave debe tener al menos 6 caracteres";
		      return false;
		   }
		   if(strlen($clave) > 20){
		      $error_clave = "La clave no puede tener más de 20 caracteres";
		      return false;
		   }
		   /*
		   if (!preg_match('`[a-z]`',$clave)){
		      $error_clave = "La clave debe tener al menos una letra minúscula";
		      return false;
		   }
		   if (!preg_match('`[A-Z]`',$clave)){
		      $error_clave = "La clave debe tener al menos una letra mayúscula";
		      return false;
		   }
		   if (!preg_match('`[0-9]`',$clave)){
		      $error_clave = "La clave debe tener al menos un caracter numérico";
		      return false;
		   }
		   */
		   $error_clave = "";
		   return true;
		} 

		/**
		 * Funció per saber la Ip del client.
		 */

		public function Saber_IP()
		{
			if(getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"noip"))
			{
				$ip = getenv("HTTP_CLIENT_IP");
	        }
			elseif(getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"),"noip"))
			{
				$ip = getenv("HTTP_X_FORWARDED_FOR");
			}
			elseif(getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"),"noip"))
			{
				$ip = getenv("REMOTE_ADDR");
			}
			elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'],"noip"))
			{
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			else
			{
				$ip = "noip";
			}
			return $ip;
	    }


	    /*
			Enviar mail.			
		*/
		public function EnviarMail($dades)
		{
			// Mail general.

			$mail_contacte = "info@projectmonitor.cat"; //"support@web-barcelona.com";

			$emailclient = $dades["email"];

			// Dades segons tipus de contacte.


			if ($dades["tipus"] == "avis")
			{
				/*
				if(!empty($dades["empresa"])) $dadespersonals .= $dades["empresa"].'<br />';
	            if(!empty($dades["nom"])) $dadespersonals .= $dades["nom"].'<br />';
	            if(!empty($dades["telefon"])) $dadespersonals .= $dades["telefon"].'<br />';
	            if(!empty($dades["adreca"])) $dadespersonals .= $dades["adreca"].'<br />';
	            if(!empty($dades["cp"])) $dadespersonals .= $dades["cp"].'<br />';
	            if(!empty($dades["poblacio"])) $dadespersonals .= $dades["poblacio"].'<br />';
	            if(!empty($dades["pais"])) $dadespersonals .= $dades["pais"].'<br />';
	            if(!empty($dades["model"])) $dadespersonals .= "MODEL: ".$dades["model"].'<br />';
	            if(!empty($dades["email"])) $dadespersonals .= $dades["email"].'<br />';
	            if(!empty($dades["comentaris"])) $comentaris = $dades["comentaris"].'<br />';
	            */

	            $enviaravis = false;
	            if ($dades["tipus2"] == "cap_projecte")
	            {
	            	
	            	// Si el cap de projecte canvia, s'envia.
	            	$Projecte = $this->Llistats("projectes"," AND id = ".$dades["idprojecte"] , "id", false );
	            	if (!empty($dades["anterior"][dades][avis][cap_projecte]))
	            	{
	            		if ($Projecte[1][cap_projecte] != $dades["anterior"][dades][avis][cap_projecte])
	            		{
	            			$enviaravis = true;
	            		}
	            	}
	            	else
	            	{
	            		$enviaravis = true;
	            	}
	            	
	            	if ($enviaravis == true)
	            	{
	            		$Usuari = $this->Llistats("usuaris"," AND id = ".$Projecte[1][cap_projecte] , "id", false );
		            	$txtavis1 = "Has sigut assignat com a cap del projecte <b>".$Projecte[1][titol]."</b>";
		            	$txtavis2 = "Per a més informació accedeix a l'eina amb el teu usuari: ".$Usuari[1][email];
		            	$emailclient = $Usuari[1][email];
	            		$registrefrom = "Project Monitor";
	            		$registreassumpte = "Project Monitor: Projecte assignat (".$Projecte[1][titol].")";
	            	}

	            }

	            if ($dades["tipus2"] == "usuari")
	            {
	            	
	            	// Si el cap de projecte canvia, s'envia.
	            	$Projecte = $this->Llistats("projectes"," AND id = ".$dades["idprojecte"] , "id", false );
	            	
            		$Usuari = $this->Llistats("usuaris"," AND id = ".$dades["usuari"] , "id", false );
	            	$txtavis1 = "Has sigut assignat al projecte <b>".$Projecte[1][titol]."</b>";
	            	$txtavis2 = "Per a més informació accedeix a l'eina amb el teu usuari: ".$Usuari[1][email];
	            	$emailclient = $Usuari[1][email];
            		$registrefrom = "Project Monitor";
            		$registreassumpte = "Project Monitor: Projecte assignat (".$Projecte[1][titol].")";
	            	
	            }

	            if ($dades["tipus2"] == "responsable")
	            {
	            	
	            	// Si el cap de projecte canvia, s'envia.
	            	$Actuacio = $this->Llistats("actuacions"," AND id = ".$dades["idactuacio"] , "id", false );
	            	if (!empty($Actuacio))
	            	{
	            		$Projecte = $this->Llistats("projectes"," AND id = ".$Actuacio[1][clau_projecte] , "id", false );
	            		if (!empty($Projecte)) 
	            		{
	            			$nomprojecte = $Projecte[1][titol];
	            			$Cap = $this->Llistats("usuaris"," AND id = ".$Projecte[1][cap_projecte] , "id", false );
	            			if (!empty($Cap)) $mail_contacte = $Cap[1][email];
	            		}
	            		if (!empty($dades["anterior"][dades][avis][responsable]))
		            	{
		            		if ($Actuacio[1][responsable] != $dades["anterior"][dades][avis][responsable])
		            		{
		            			$enviaravis = true;
		            		}
		            	}
		            	else
		            	{
		            		$enviaravis = true;
		            	}
		            	
		            	if ($enviaravis == true)
		            	{
		            		$Usuari = $this->Llistats("usuaris"," AND id = ".$Actuacio[1][responsable] , "id", false );
		            		if (!empty($Usuari))
		            		{
		            			$txtavis1 = "Hola <b>".$Usuari[1][nom]." ".$Usuari[1][cognoms]."</b>, <br><br>
				            	La proposta <b>[".$Actuacio[1][codi]."] - ".$Actuacio[1][titol]."</b> t'ha estat assignada en l'eina de seguiment de projectes.";
				            	$txtavis2 = "Per a més informació accedeix a l'eina amb el teu usuari: ".$Usuari[1][email];
				            	$emailclient = $Usuari[1][email];
			            		$registrefrom = "Project Monitor";
			            		$registreassumpte = "Project Monitor: Proposta assignada (".$Actuacio[1][titol].")";
		            		}
			            	
		            	}
	            	}

	            }

	            if ($dades["tipus2"] == "responsabletasca")
	            {
	            	
	            	// Si el reponsable canvia, s'envia.
	            	$Tasca = $this->Llistats("tasques"," AND id = ".$dades["idtasca"] , "id", false );
	            	if (!empty($Tasca))
	            	{
	            		$Projecte = $this->Llistats("projectes"," AND id = ".$Tasca[1][clau_projecte] , "id", false );
	            		if (!empty($Projecte)) 
	            		{
	            			$nomprojecte = $Projecte[1][titol];
	            			$Cap = $this->Llistats("usuaris"," AND id = ".$Projecte[1][cap_projecte] , "id", false );
	            			if (!empty($Cap)) $mail_contacte = $Cap[1][email];
	            		}
	            		if (!empty($dades["anterior"][dades][avis][responsabletasca]))
		            	{
		            		if ($Tasca[1][responsable] != $dades["anterior"][dades][avis][responsabletasca])
		            		{
		            			$enviaravis = true;
		            		}
		            	}
		            	else
		            	{
		            		$enviaravis = true;
		            	}
		            	
		            	if ($enviaravis == true)
		            	{
		            		$Usuari = $this->Llistats("usuaris"," AND id = ".$Tasca[1][responsable] , "id", false );
		            		if (!empty($Usuari))
		            		{
			            		$txtavis1 = "Hola <b>".$Usuari[1][nom]." ".$Usuari[1][cognoms]."</b>, <br><br>
				            	La tasca <b>[".$Tasca[1][codi]."] - ".$Tasca[1][titol]."</b> t'ha estat assignada en l'eina de seguiment de projectes.";
				            	$txtavis2 = "Per a més informació accedeix a l'eina amb el teu usuari: ".$Usuari[1][email];
				            	$emailclient = $Usuari[1][email];
			            		$registrefrom = "Project Monitor";
			            		$registreassumpte = "Project Monitor: Tasca assignada (".$Tasca[1][titol].")";
			            	}
		            	}
	            	}
	            	

	            }


	            $txtcapcalera = '

	            	 <table width="530" border="0" cellspacing="0" cellpadding="0">
		                  <tr>
		                    <td colspan="3" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 14px; line-height: 18px; color: #000000; margin:7px 0px;">Avis de Project Monitor </p>
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 18px; line-height: 20px; color: #993333; margin:10px 0px;">'.$txtavis1.'</p>
							  <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px; color: #666666; margin:4px 0px;">'.$txtavis2.'.</p>
		                    </td>
		                  </tr>
	                  
	                </table>

	            ';

	            /*
					<tr>
		                    <td width="265px" valign="top" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 12px; line-height: 18px; color: #000000; margin:7px 0px;">'.$this->app['translator']->trans('mail.contacto.dadespersonals').':</p>
		                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #666666; margin:2px 0px;">
		                          '.$dadespersonals.'
		                        </p>
		                    </td >
		                    <td width="1"></td>
		                    <td width="265px" valign="top" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 12px; line-height: 18px; color: #000000; margin:7px 0px;">'.$this->app['translator']->trans('mail.contacto.detalls').':</p>
		                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #666666; margin:2px 0px;">'.$comentaris.'</p>
		    					</td>
		                  </tr>
	            */

			}
			elseif ($dades["tipus"] == "19" || $dades["tipus"] == "22" || $dades["tipus"] == "24" || $dades["tipus"] == "23" || $dades["tipus"] == "25") { 
					
					if ($dades["tipus"] == "19") $taulad = "plens";
					if ($dades["tipus"] == "22") $taulad = "comissiopermanent";
					if ($dades["tipus"] == "24") $taulad = "comissioinformativa";
					if ($dades["tipus"] == "23") $taulad = "consellparticipacio";
					if ($dades["tipus"] == "25") $taulad = "organdeliberatiu";

					$RegisteMail = $this->Llistats("$taulad"," AND id = ".$dades["id"] ,array(), "id", false );

					$txtavis1 = $dades["textmail"];
	            	$txtavis2 = "Per a més informació accedeix a l'eina amb el teu usuari.";
            		$registrefrom = "Project Monitor Local";

				if ($dades["subtipus"] == 1) { // 1: ordre del dia

            		$registreassumpte = "Project Monitor Local: Ordre del dia de (".$RegisteMail[1][titol].")";

				}
				elseif ($dades["subtipus"] == 2) { // 2:  Acords presos
					
					$registreassumpte = "Project Monitor Local: Acords presos a (".$RegisteMail[1][titol].")";
				}

				$enviaravis = true;
				$txtcapcalera = '

	            	 <table width="530" border="0" cellspacing="0" cellpadding="0">
		                  <tr>
		                    <td colspan="3" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 14px; line-height: 18px; color: #000000; margin:7px 0px;">Avis de Project Monitor </p>
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 18px; line-height: 20px; color: #993333; margin:10px 0px;">'.$txtavis1.'</p>
							  <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px; color: #666666; margin:4px 0px;">'.$txtavis2.'</p>
		                    </td>
		                  </tr>
	                  
	                </table>

	            ';
			}
			elseif ($dades["tipus"] == "recuperar")
			{
	            
	            $enviaravis = true;

	            $emailclient = $dades["email"];

	            if ($dades["novesdades"] == 1){

	            	if (!empty($dades["password"]))
		            {
		            	
		            	$passwordactual = $dades["password"];
		            }
		            else{ exit(); }

	            	$registreassumpte = $this->app['translator']->trans('mail.dadesacces.titolmail');
	            	$txtmail1 =  $this->app['translator']->trans('mail.dadesacces.formulariodadesacces');
	            	$txtmail2 =  $this->app['translator']->trans('mail.dadesacces.txt2');

	            }else{

	            	$registreassumpte = $this->app['translator']->trans('mail.dadesacces.titolmail');
	            	$txtmail1 =  $this->app['translator']->trans('mail.recuperar.formularioderecuperacion');
	            	$txtmail2 =  $this->app['translator']->trans('mail.recuperar.txt2');

	            	if(!empty($dades["email"]))
		            {
		            	$GuardarClient = "";

		            	// Validar mail.
		            	

						$DadesUsuariP = $this->Llistats("usuaris"," AND usuari = :email" ,array("email" => $dades["email"]), "email", false );

				
						if (!empty($DadesUsuariP))
						{


							$passwordactual = $this->randomPassword();
							/*
							$salt = substr(hash('whirlpool',microtime()),rand(0,105),22);
							$pass = $dades["password"];
							$i = rand(0,22);  //22 per la longitud del salt.
					    	$encryptedpass = crypt($pass,'$2a$07$' . substr($salt, 0, $i));
					    	*/
					    	$encryptedpass = md5($passwordactual);

					    	

							$this->db->query("UPDATE pfx_usuaris 
											  SET password = :password, txtpass = :txtpass
											  WHERE id = :id",array("id"=>$DadesUsuariP[1][id],'password' => $encryptedpass, 'txtpass' => base64_encode($passwordactual)));

							$GuardarClient = 1;


						        
						}
						else
						{
							$GuardarClient = 2;
						}
						


		            	if ($GuardarClient == 2)
		            	{
		            		echo 3; exit(); // Usuari no existeix;
		            	}
		            	elseif ($GuardarClient != 1) {
		            		echo 2; exit(); // Error!.
		            	}

		            }


	            }

	            $registrefrom = $this->app['translator']->trans('mail.recuperar.from');

        


	            $txtcapcalera = '

	            	 <table width="530" border="0" cellspacing="0" cellpadding="0">
		                  <tr>
		                    <td colspan="3" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 14px; line-height: 18px; color: #000000; margin:7px 0px;">'.$txtmail1.' </p>
							  <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px; color: #666666; margin:4px 0px;">'.$txtmail2.'</p>
		                    </td>
		                  </tr>
		                  <tr>
		                    <td width="265px" valign="top" align="left">
		                      <p style="font-family: \'Trebuchet MS\', Verdana, sans-serif; font-size: 12px; line-height: 18px; color: #000000; margin:7px 0px;">'.$this->app['translator']->trans('Password').':</p>
		                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 16px; color: #666666; margin:2px 0px;">
		                          '.$passwordactual.'
		                        </p>
		                    </td >
		                    <td width="1"></td>
		                    <td width="265px" valign="top" align="left">
	                     	</td>
		                  </tr>
		                </table>

	            ';

	            
			}
			
			// Generar cadena.
            $cadena = '


                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>'.$this->app['translator']->trans('base.nomempresa').'</title>
                    <style type="text/css">
                    a { color: #993333; text-decoration: none; }
                    a:hover { color: #000000 !important; text-decoration: none !important; }
                    </style>
                    </head>
                    <body style="padding: 0px; margin: 0px; background-color:#ffffff;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" bgcolor="#ffffff" oncontextmenu="return false;">

                    <table width="100%" cellspacing="0" border="0" cellpadding="0"  bgcolor="#ffffff" >
                    <tr>
                    <td align="center" valign="top">
                    <!--WRAPPER-->
                      <table width="610" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                    <!--CONTAINER-->           
                                  <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" >
                    <!-- HEADER IMAGE-->                  
                                       
                    <!--END -->
                    <!-- LOGO AND HEADER -->
                                        <tr>
                                            <td align="center" valign="top">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="600" align="center" valign="top"><img src="'.constant("DBB::url").'/images/mail/divider.jpg" width="600" height="1" style="display: block;" /><br><br></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="600" align="center" valign="top" bgcolor="#ffffff">
                                                            <table width="500" border="0" cellspacing="0" cellpadding="0" height="80px">
                                                              <tr>
                                                                <td align="left"><img src="'.constant("DBB::url").'/images/mail/logo.png" width="170" style="display: block;" /> 
                                                                </td>
                                                                <td align="right" valign="top" height="80px"><div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color:#000; margin:0px; padding:5px 0px;">'.date("d/m/Y",time()).'</div></td>
                                                              </tr>
                                                            </table>
                                                         </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="600" align="center" valign="top"><img src="'.constant("DBB::url").'/images/mail/divider.jpg" width="600" height="1" style="display: block;" /></td>
                                                  </tr>
                                                </table>
                                            </td>
                                        </tr>
                    <!-- MAIN CONTENT ADD AND DELETE MODULES TO SUITE YOUR LAYOUT -->
                              <tr><!--INVOICE DETAILS -->
                                          <td bgcolor="#ffffff" align="center" valign="top" style="padding:10px 0px;">
                                               '.$txtcapcalera.'
                                            </td>
                                      </tr><!--END -->
                                        <tr><!--DIVIDER-->
                                          <td>
                                              <table width="600" border="0" cellspacing="0" cellpadding="0">
                                                  <tr><td height="1"><img src="'.constant("DBB::url").'/images/mail/divider.jpg" width="600" height="1" style="display: block;" /></td></tr>
                                                </table>
                                            </td>
                                      </tr>
                                        
                                       
                                        <tr><!--MODULE TWO COL-->
                                          <td>
                                                <table width="600" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0px;">
                                                  <tr>
                                                    <td width="35"></td>
                                                    <td width="245" valign="top" align="left">
                                                      <p style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 16px; color: #666666; margin:4px 0px;">Project Monitor</p>
                                                    </td>
                                                    <td width="20"></td>
                                                    <td width="265" valign="top" align="left">  
                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 16px; color: #999999; margin:4px 0px;">'.$adreca.'<br />'.$web.'<br/>'.$telefon.'</p>
                                                        
                                                    </td>
                                                    <td width="35"></td>
                                                  </tr>
                                                </table>
                                            </td>
                                      </tr><!--END -->
                                        
                    <!-- DO NOT REMOVE BELOW THIS LINE-->
                    <!-- FOOTER CURVE AND CONTENTS-->         
                                    
                                        <tr>
                                          <td align="center"><img src="'.constant("DBB::url").'/images/mail/divider.jpg" width="600" height="1" style="display: block;" /></td>
                                        </tr><!--END -->        
                                  </table>
                    <!--CONTAINER END-->    
                            </td>
                            </tr>
                    <!--UNSUBSCRIBE-->
                        <tr>
                                <td align="center">
                                    <table width="560" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" width="530" valign="top"><p style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 16px; color: #999999; margin: 10px;">© '.date('Y').' Project Monitor.  |  <a href="'.constant("DBB::url").'/termesicondicions.html" target="_blank">'.$this->app['translator']->trans('Aviso legal').'</a> </p></td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr><!--END -->
                      </table>    
                    <!--WRAPPER END-->
                    </td>
                    </tr>
                    </table>
                    </body>

                    </html>


            ';

            // echo $cadena;
            // exit();

			// Enviar mails.


	        $sep = "\n";
		    $headers = "From: ".$registrefrom."<".$mail_contacte.">".$sep;
		    $headers .= "MIME-Version: 1.0".$sep;
	        $headers .= "Content-type: text/html; charset=\"UTF-8\"".$sep;
			$headers .= "Content-Transfer-Encoding: 8bit".$sep;
		      
			    
		    $mail = $mail_contacte;
		    if (isset($email) && $email != "") {$mail = $email;}

		    $assumpte = $registreassumpte;


		    /*
	        if ( $this->app['debug'] == true )
	        {

	           //mail("support@web-barcelona.com", $assumpte, $cadena, $headers,"-f"."support@web-barcelona.com"); 

	        }
	        else
	        {
	        */
	           
	           if ($errors == false && $enviaravis == true)
	           {
	           		// Enviar mail al client.
	           		
	           	    if ($dades["tipus"] == "19" || $dades["tipus"] == "22" || $dades["tipus"] == "24" || $dades["tipus"] == "23" || $dades["tipus"] == "25") {

			           if (!empty($dades["convocats_email"]))
			           {
			           		$convocats = json_decode($dades["convocats_email"]);

			           		foreach ($convocats as $key => $value) {

			           			$Usuari = $this->Llistats("usuaris"," AND id = '".$value."'" ,array(),  "id", false );
			            		if (!empty($Usuari))
			            		{
			            			if (!empty($Usuari[1][email]))
			            			{
			            				if ( !mail($Usuari[1][email], $assumpte, $cadena, $headers,"-f".$mail_contacte) ){
							            $errors = true;
						           		}
			            			}
				           			
						        }
			           		}
			           }

			           if (!empty($dades["emails"]))
			           {
			           		$emails = json_decode($dades["emails"]);

			           		foreach ($emails as $key => $value) {

			           			if (!empty($value))
		            			{ 
				            		if ( !mail($value, $assumpte, $cadena, $headers,"-f".$mail_contacte) ){
								           $errors = true;
							        }
			            		}
			           		}
			           }

			           echo "<br> Enviament finalitzat.";


			        }
			        else
			        {
		        	    
		        	    if ( !mail($emailclient, $assumpte, $cadena, $headers,"-f".$mail_contacte) ){
			            $errors = true;
			           }

			        }
		           
		          
		           // Enviar mail empresa.
		           /*
		           if ( !mail($mail_contacte, $assumpte, $cadena, $headers,"-f".$mail_contacte) ){
		            $errors = true;
		           }
		           */
		           

		           // Enviar mail a support per verificar funcionament..
		           //if ( !mail("support@web-barcelona.com", $assumpte, $cadena, $headers,"-f"."support@web-barcelona.com") ){
		           // $errors = true;
		           //}
		         
		           
	           }

	        //}


	        if ($errors == false){

	        	if ($dades["tipus"] == "recuperar" && $dades["novesdades"] != 1){
		        	echo $GuardarClient;
		        }

	          //echo 1;
	        	return 1;

	        }elseif ($errors == "repetit") {
	        	//echo 2;
	        }

	        
	        return true;

	        
		}


		// Verificar Password actual.

		public function PassActual($requestPass,$id)
		{
			
	
			// Extrec el password de l'usuari per poder-lo comparar.
			$sql = $this->db->row("SELECT * FROM pfx_usuaris
								   WHERE id = :id AND password = :password
								   ", // TODO: estat = 1.
							array("id" => $id, "password" => md5($requestPass))
					);

			if(!empty($sql))
	        {
		       					
				$passok = "ok";
				
			}			
				
			

			if ($passok == "ok")
			{
				return true;
			}
			else{
				return false;
			}
			
		}

		/*
			Generar password aleatori.
		*/

		function randomPassword() 
		{
		    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()_+|:?.-=";
		    $pass = array(); 
		    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		    for ($i = 0; $i < 10; $i++) {
		        $n = rand(0, $alphaLength);
		        $pass[] = $alphabet[$n];
		    }
		    return implode($pass); //turn the array into a string
		}



	}

	
?>