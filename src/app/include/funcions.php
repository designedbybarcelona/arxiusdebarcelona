<?php
	/**
	 * DesignedByBarcelona 
	*/

	/**
	 * Retorna l'extensió del fitxer.
	 * @param string $sFileName.
	 * return string.
	 */

    function StripFileExt($sFileName)
    {
		if ($sFileName && strpos($sFileName,".") !== false)
		{
			 return substr($sFileName,strrpos($sFileName,"."));
		}
	}

	
	/**
	 * Converteix a majúscules la cadena.
	 * @param string $Text.
	 * return string.
	 */

	function UpperText($Text)
	{
		return strtr(strtoupper($Text),"àèìòùáéíóúçñäëïöü","ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
	}
	
	
	/**
	 * Funció per saber la Ip del client.
	 */

	function Saber_IP()
	{
		if(getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"),"noip"))
		{
			$ip = getenv("HTTP_CLIENT_IP");
        }
		elseif(getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"),"noip"))
		{
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}
		elseif(getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"),"noip"))
		{
			$ip = getenv("REMOTE_ADDR");
		}
		elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'],"noip"))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		else
		{
			$ip = "noip";
		}
		return $ip;
    }
	

	/**
	*Funció per saber el SO del client.
	*/
	
	function SaberSO()
	{
		$Agente = $_SERVER['HTTP_USER_AGENT'];
		if      (ereg("Windows NT 5.1",  $Agente)) $sistemaOperativo = "Windows XP";
		elseif  (ereg("Windows NT 5.0",  $Agente)) $sistemaOperativo = "Windows 2000";
		elseif  (ereg("Win98 ",      $Agente)) $sistemaOperativo = "Windows 98";
		elseif  (ereg("Win",             $Agente)) $sistemaOperativo = "Windows ??";
		elseif  ( (ereg("Mac",           $Agente)) ||
			  (ereg("PPC", $Agente))) $sistemaOperativo = "Macintosh";
		elseif  (ereg("Debian",          $Agente)) $sistemaOperativo = "Debian";
		elseif  (ereg("Linux",           $Agente)) $sistemaOperativo = "Linux";
		elseif  (ereg("FreeBSD",         $Agente)) $sistemaOperativo = "FreeBSD";
		elseif  (ereg("SunOS",           $Agente)) $sistemaOperativo = "SunOS";
		elseif  (ereg("IRIX",            $Agente)) $sistemaOperativo = "IRIX";
		elseif  (ereg("BeOS",            $Agente)) $sistemaOperativo = "BeOS";
		elseif  (ereg("OS/2",            $Agente)) $sistemaOperativo = "OS/2";
		elseif  (ereg("AIX",             $Agente)) $sistemaOperativo = "AIX";
		else   $sistemaOperativo = "Desconocido"; 
	 
		return $sistemaOperativo;
	} 


	/**
	*Tallar cadena de text
	*/

	function myTruncate($string, $limit, $break=".", $pad="…") {
 
		if(strlen($string) <= $limit)
		 
		return $string;
			 
		if(false !== ($breakpoint = strpos($string, $break, $limit))){
		 
			if($breakpoint < strlen($string)-1) {
			 
				$string = substr($string, 0, $breakpoint) . $pad;
			 
			}
		 
		}
		 
		return $string;
	 
	}


	/*
	*  Dies laborables desde una data inicial a una final.
	*/

	function DiasHabiles($fecha_inicial,$fecha_final)
	{
		list($dia,$mes,$year) = explode("-",$fecha_inicial);
		$ini = mktime(0, 0, 0, $mes , $dia, $year);
		list($diaf,$mesf,$yearf) = explode("-",$fecha_final);
		$fin = mktime(0, 0, 0, $mesf , $diaf, $yearf);

		$r = 0;
		while($ini != $fin)
		{
			$ini = mktime(0, 0, 0, $mes , $dia+$r, $year);
			$newArray[] .=$ini; 
			$r++;
		}
		return $newArray;
	}

	function Evalua($arreglo)
	{
		$feriados        = array(
		'1-1', 		 //  Any nou
		'1-5',  	 //  Dia del treballador
		'15-8', 	 //  Santa Maria
		'1-11', 	 //  Día de Todos los Santos
		'8-12', 	 //  Inmaculada Concepció
		'25-12', 	 //  Nadal
		'25-12', 	 //  Sant Esteve
		);

		$j= count($arreglo);

		for($i=0;$i<=$j;$i++)
		{
			$dia = $arreglo[$i];

	        $fecha = getdate($dia);
            $feriado = $fecha['mday']."-".$fecha['mon'];
            if($fecha["wday"]==0 or $fecha["wday"]==6)
            {
                $dia_++;
            }
            elseif(in_array($feriado,$feriados))
            {   
                $dia_++;
            }
		}
		$rlt = $j - $dia_;
		return $rlt;
	}

	
	/*
	*  Saber URL del navegador.
	*/
	
	function Saber_URL()
	{
		$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		return $url;
	}

	/*
	*  Saber URL del navegador.
	*/
	
	function Saber_URL2()
	{
		$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		return $url;
	}


	/*
		Convertir cadenes a UTF-8 en cas de que no ho siguin.
	*/

	function _convert($content) {
	    if(!mb_check_encoding($content, 'UTF-8')
	        OR !($content === mb_convert_encoding(mb_convert_encoding($content, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) {

	        $content = mb_convert_encoding($content, 'UTF-8');

	        if (mb_check_encoding($content, 'UTF-8')) {
	            // log('Converted to UTF-8');
	        } else {
	            // log('Could not converted to UTF-8');
	        }
	    }
	    return $content;
	} 

	// Netejar URL.
	
	setlocale(LC_ALL, 'en_US.UTF8');
	function toAscii($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}
		//$str = strtr($str, "àèìòùáéíóúçñäëïöüÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ", "aeiouaeioucnaeiouAEIOUAEIOUCNAEIOU");
	    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
	    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
	    $str = str_replace($a, $b, $str);
		// Es menja els accents...
		//$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = iconv('ISO-8859-1', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	/*
		Generar Token per els formularis d'accès.
	*/
	function makeToken()
	{
	    return sha1(microtime() . "Web-Barcelona.com");
	}

	/*
		Netejar array multidimensional o no.
	*/
	function stripslashes_deep($value)
	{
	    $value = is_array($value) ?
	                array_map('stripslashes_deep', $value) :
	                stripslashes($value);

	    return $value;
	}

	function neteja_e ($string)
	{
		$string = _convert(str_replace("&#39;", "'", str_replace("‟", "'", $string)));

		return $string;
	}

	function encryptRSA($plaintext){

		$plaintext = gzcompress($plaintext);

		// Get the public Key
		$publicKey = openssl_pkey_get_public(file_get_contents(__DIR__.'/../../private/pml_public.key'));

		$a_key = openssl_pkey_get_details($publicKey);

		// Encrypt the data in small chunks and then combine and send it.
		$chunkSize = ceil($a_key['bits'] / 8) - 11;
		$output = '';

		while ($plaintext)
		{
		    $chunk = substr($plaintext, 0, $chunkSize);
		    $plaintext = substr($plaintext, $chunkSize);
		    $encrypted = '';
		    if (!openssl_public_encrypt($chunk, $encrypted, $publicKey))
		    {
		        //die('Failed to encrypt data');
		        return "";
		    }
		    $output .= $encrypted;
		}
		openssl_free_key($publicKey);
		 
		// This is the final encrypted data to be sent to the recipient
		return base64_encode($output); // sense el base 64 a la base de dades ha de tenir un Tipus de dades BLOB.

	}

	function decryptRSA($cripencryptedtext){

		$encrypted = base64_decode($cripencryptedtext);

		// Get the private Key
		if (!$privateKey = openssl_pkey_get_private(file_get_contents(__DIR__.'/../../private/pml_private.key')))
		{
		    die('Private Key failed');
		}
		$a_key = openssl_pkey_get_details($privateKey);
		 
		// Decrypt the data in the small chunks
		$chunkSize = ceil($a_key['bits'] / 8);
		$output = '';
		 
		while ($encrypted)
		{
		    $chunk = substr($encrypted, 0, $chunkSize);
		    $encrypted = substr($encrypted, $chunkSize);
		    $decrypted = '';
		    if (!openssl_private_decrypt($chunk, $decrypted, $privateKey))
		    {
		        //die('Failed to decrypt data');
		        return "";
		    }
		    $output .= $decrypted;
		}
		openssl_free_key($privateKey);
		 
		// Uncompress the unencrypted data.
		if (!empty($output)) return(gzuncompress($output));
		else return "";
	}

	function encrypt_decrypt($action, $string) {
	    $output = false;

	    $encrypt_method = "AES-256-CBC";
	    $secret_key = '4e40aa32a3fbcb892281cd5c7520071a';
	    $secret_iv = '6ce30eee259c17af2f8a8148c610d24a';

	    // hash
	    $key = hash('sha256', $secret_key);
	    
	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);

	    if( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    }
	    else if( $action == 'decrypt' ){
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }

	    return $output;
	}

	
?>