<?php

	// Variables Generals.
	require_once __DIR__.'/general.php';

	// Guardar dades del formulari.

	$dades = array();

	
	if (isset($_POST['verif'])){
		$verif = filter_var(htmlspecialchars(trim($_POST['verif'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}
	
	if (isset($_POST['tipus'])){
		$tipus = filter_var(htmlspecialchars(trim($_POST['tipus'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		$dades["tipus"] = $tipus;
	}
	
	
	
	if (isset($_POST['email'])){
		$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_VALIDATE_EMAIL);
		$dades["email"] = $email;
	}
	

	
	if (isset($_POST['condicions'])){
		$condicions = filter_var(trim($_POST['condicions']), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($_POST['tockenseguretatlogin'])){
		$authToken = htmlspecialchars(trim($_POST['tockenseguretatlogin']));
	}
	
	  
                             


	// Verificar dades.
	/*
	if ($authToken != $app['session']->get('tockenseguretatlogin') ) {
        echo $app['translator']->trans('base.alerts.introdades');  echo 1;
		exit();
    }
    */
	if(!isset($verif) || $verif == ""){
		echo $app['translator']->trans('base.alerts.introdades'); echo 2;
		exit();
	}
	if($email == "") {
		echo $app['translator']->trans('base.alerts.introdades'); echo 3;
		exit();
	}
	

	// Enviar mail.

	//$dbb->logger(array("enviar_mail",$dades));

	$dbb->EnviarMail($dades);

	$dadesplantilla = array();

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}

	return $dadesplantilla;

	   
