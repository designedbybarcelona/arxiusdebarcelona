<?php
	
	ini_set('display_errors', 0);
	

	function addImage($image, $x, $y, $new_width, $new_height)
    {

    	/*
        if (!fileExistsInPath($image))
        {
            echo $image . " not found<br>";
            return;
        }
        */

        list($width, $height) = getimagesize($image);

        /**
          get width/height witdh imagemagick

          $image_absolute_path = realpath($image);
          $im = new Imagick();
          $im->pingImage($image_absolute_path);
          //image info
          $width = $im->getImageWidth();
          $height= $im->getImageHeight();
         */
        if ($width - 50 >= $height)
        {
            $new_height = 0;
        }
        else
        {
            $new_width = 0;
        }


        //$pdf->Image($image, $x, $y, $new_width, $new_height);
        return array($new_width, $new_height);
    }

	// Generar PDF.

	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Path secret.
    $imagePATHsecret=pathfiles."/../../../secretfiles/tmp/";
	

	if (ob_get_level() == 0) ob_start();

	//echo "Generant PDF...<br>";

	

		// Inici creació PDF.

		// Include the main TCPDF library (search for installation path).
	    require_once( __DIR__.'/../../../js/tcpdf/tcpdf.php');

	 	class MYPDF extends TCPDF {

		    //Page header
		    public function Header() {

		    	if ($this->tocpage) {
		            // *** replace the following parent::Header() with your code for TOC page
		            //parent::Header();

		        } else { 
		        	// *** replace the following parent::Header() with your code for normal pages
		        }
		            
	            // Logo
		        $image_file = pathfiles."/../images/".'logopdf.png';
		        $this->Image($image_file, 10, 5, 30, '');
		        // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

		        $dbb = new General($idioma, $app);
		        $DadesGenerals = $dbb->Llistats("general","",array(),'id');

		        $this->SetFont('helvetica', 'B', 7);
		        $this->SetTextColor(255, 0, 0);
		        $this->SetXY(158, 10);
		        $this->Cell(60, 0, $DadesGenerals[1][descripcio_ca], 0, false, 'C', 0, '', 0, false, 'M', 'M');
		        $this->SetTextColor(0, 0, 0);
		        $this->SetXY(107, 15);
		        $this->Cell(100, 0, 'Calendari global fites', 0, false, 'C', 0, '', 0, false, 'M', 'M');

				$this->SetXY(7, 20);
				$this->setCellHeightRatio(0.2);
				$this->SetDrawColor(0,0,0);
				$this->Cell(195, 0,'', '', 1, 'C', 1, '', 0, false, 'T', 'C');
	       
		        
		    }

		    // Page footer
		    
		    public function Footer() {
		    	
		        if ($this->tocpage) {
		            // *** replace the following parent::Footer() with your code for TOC page
		            //parent::Footer();
		        } else {
		            // *** replace the following parent::Footer() with your code for normal pages
		            // Position at 15 mm from bottom
	        		$this->SetY(-15);
			        // Set font
			        $this->SetFont('helvetica', 'I', 8);
			        // Page number
			        $this->Cell(0, 20, 'Pàgina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	        	
			        
		        }
		    }
		    
		}
	  

	    // create new PDF document
	    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	    // Path imatges.
	    $imagePATH=pathfiles."/../images/";

	    // set document information
	    $pdf->SetCreator(PDF_CREATOR);
	    $pdf->SetAuthor($app['translator']->trans('Calendari'));
	    $pdf->SetTitle($app['translator']->trans('Calendari'));
	    $pdf->SetSubject($app['translator']->trans('Calendari'));
	    $pdf->SetKeywords($app['translator']->trans('Calendari'));

	    //$pdf->startPage( $orientation = '',$format = '', true );
	   
	    //$pdf->SetHeaderData("../../images/logo.jpg", '50', '', ""); 
	    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Seguiment de projectes");
	    $pdf->SetHeaderData('','','','');

	    $pdf->setPrintHeader(false);
	    //$pdf->setPrintHeader();
	    // set header and footer fonts
	    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
	    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	    $pdf->setPrintFooter(false);

	    // set default monospaced font
	    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	    // set margins
	    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	    $pdf->SetMargins(7, 7, 7);
	    $pdf->SetHeaderMargin(0);
	    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	    $pdf->SetFooterMargin(0); // PDF_MARGIN_FOOTER

	    // set auto page breaks
	    $pdf->SetAutoPageBreak(TRUE, 5);

	    // set image scale factor
	    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	    // set some language-dependent strings (optional)
	    //if (@file_exists(dirname(__FILE__).'/lang/esp.php')) {
	    //  require_once(dirname(__FILE__).'/lang/esp.php');
	    //  $pdf->setLanguageArray($l);
	    //}

	    $tbl = "";


			 // ---------------------------------------------------------

	    if (isset($getenc['filtre1'])) if (($getenc['filtre1']==1)) $_POST['filtre1'] = 1;
	    if (isset($getenc['filtre2'])) if (($getenc['filtre2']==1)) $_POST['filtre2'] = 1;
	    if (isset($getenc['filtre3'])) if (($getenc['filtre3']==1)) $_POST['filtre3'] = 1;
	    if (isset($getenc['filtre4'])) if (($getenc['filtre4']==1)) $_POST['filtre4'] = 1;
	    if (isset($getenc['filtre5'])) if (($getenc['filtre5']==1)) $_POST['filtre5'] = 1;

	    if (isset($getenc['inifit'])) if (($getenc['inifit']!="")) $_POST['inifit'] = $getenc['inifit'];
	    if (isset($getenc['finfit'])) if (($getenc['finfit']!="")) $_POST['finfit'] = $getenc['finfit'];


	    

	    // Filtres marcats.
	    if (isset($_POST['filtre1'])){ $filtre1 = true; $nomsfiltres .= " Fites ".(!empty($nomsfiltres)?" - ":""); }
	    if (isset($_POST['filtre2'])){ $filtre2 = true; $nomsfiltres .= " Inici/Final Projectes ".(!empty($nomsfiltres)?" - ":""); }
	    if (isset($_POST['filtre3'])){ $filtre3 = true; $nomsfiltres .= " Inici/Final Fases ".(!empty($nomsfiltres)?" - ":""); }
	    if (isset($_POST['filtre4'])){ $filtre4 = true; $nomsfiltres .= " Inici/Final Accions ".(!empty($nomsfiltres)?" - ":""); }
	    if (isset($_POST['filtre5'])){ $filtre5 = true; $nomsfiltres .= " Els meus projectes ".(!empty($nomsfiltres)?" - ":""); }

	    // -- Portada.

	    $pdf->AddPage();
		$pdf->SetFont('helvetica', 'B', 12);
		//$pdf->SetFontSize(20);

		$pdf->SetXY(7, 3);
		$pdf->Write(0, date('d/m/Y'), '', 0, 'L', true, 0, false, false, 0);
		$pdf->Ln(2);
		$pdf->SetXY(7, 8);
		$pdf->Write(0, 'Calendari de dates relatives a: '.$nomsfiltres, '', 0, 'L', true, 0, false, false, 0);
		
		$pdf->SetFont('helvetica', '', 7);

		$pdf->Ln(2);
		/*
		if (!empty($ConfigPla)){

			$pdf->SetXY(7, 20);
			$pdf->Write(0, str_replace("<p>", "", str_replace("</p>", "\n", $ConfigPla[1][descripcio_ca])), '', 0, 'L', true, 0, false, false, 0);

		}

		$pdf->Ln(2);*/

		$pdf->SetFillColor(255, 255, 255);
	    $pdf->SetTextColor(0,0,0);
	    $pdf->SetLineWidth(1.2);
	    $pdf->SetDrawColor(0,0,0);
		$pdf->setCellHeightRatio(2);

	

	if ($id != 1){
		$condicioprojecte = " AND t.clau_projecte = $id";
		$condicioprojecte2 = " AND ta.clau_projecte = $id";
		$condicioprojecte3 = " AND ta.parent_id = $id";
	}
	
	


	$conddates = "";

	if (isset($_POST['inifit'])){
		$inici = filter_var(htmlspecialchars(trim($_POST['inifit'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		$inici = explode("/",$inici);
		if (!empty($inici[2]))
		//$conddates .= " AND p.inici >= '$inici[2]-$inici[1]-$inici[0]' ";
		$inicifiltre = "$inici[2]-$inici[1]-$inici[0]";
	}
	if (isset($_POST['finfit'])){
		$final = filter_var(htmlspecialchars(trim($_POST['finfit'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		$final = explode("/",$final);
		if (!empty($final[2]))
		//$conddates .= " AND p.inici <= '$final[2]-$final[1]-$final[0]' ";
		$finalfiltre = "$final[2]-$final[1]-$final[0]";
	}
	if (isset($_POST['paginici'])){
		$paginici = true;
	}

	// Amb filtre buit 30 dies.
	
	if ( $paginici && empty($inicifiltre) && empty($finalfiltre)) {
		$inicifiltre = date("Y-m-d", time() - (60*60*24*10));
		$finalfiltre = date("Y-m-d", time() + (60*60*24*30));
	}

	// Events.
	$events = json_decode(stripslashes($_POST["eventsenviar"]));

	$events = (array)$events;

	$nouarray = array();

	// Esborrar segons filtre.
	foreach ($events as $key => $value) {
		$nouvalue = (array)$value;
		if ( ($nouvalue['className'] == "calfites" && $filtre1 == true ) || 
			 ($nouvalue['className'] == "calprojectes" && $filtre2 == true ) || 
			 ($nouvalue['className'] == "calactuacions" && $filtre3 == true ) || 
			 ($nouvalue['className'] == "caltasques" && $filtre4 == true ) ||
			 ($nouvalue['className'] == "calmeus" && $filtre5 == true ) ){

			// Filtre per data.
			if ( ( strtotime($nouvalue['start']) >= strtotime($inicifiltre) && 
				   strtotime($nouvalue['start']) <= strtotime($finalfiltre) 
				 ) ){
				array_push($nouarray, $nouvalue);
			}
			
		}
	}

	
	//if (empty($conddates)) $conddates = "AND p.inici <= DATE_ADD(now(),INTERVAL  30 DAY) AND p.inici >= DATE(now()) ";

	$arrayDates = array();

	if (!$paginici) $condicio_permis_campana = "";

	// Preparar array per imprimir.


	foreach ($nouarray as $key => $value) {

		array_push($arrayDates,array(
				'datai' => date("d/m/Y", strtotime($value[start])),
				'datastr' => strtotime($value[start]),
				'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value[nomprojecte])))),
				'tipologia' => $value[tipologia],
				'descripcio' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value[title])))),
				'tipus' => $value[tipus],
				
		));
	}
		

	$arrayordre = array();
	foreach ($arrayDates as $key => $value) {
		
  		$arrayordre[$key] = $value['datastr'];  

	}

	array_multisort($arrayordre, SORT_ASC, $arrayDates );

	// Array de tipus de dades.
	$arraytipus = array(
		'fita' => "Fita",
		'projecte' => "Final projecte",
		'projecteinici' => "Inici projecte",
		'actuacio' => "Final fase",
		'actuacioinici' => "Inici fase",
		'tasca' => "Final acció",
		'tascainici' => "Inici acció",
	);


	$caps = false;

	if (!empty($arrayDates))
	{
		
		
		$taula .= '
				    
				';

		foreach ($arrayDates as $key => $value) 
		{

			// Tipus (inici projecte, final projecte, inici fase / final fase / inici acció / final acció / fita) 
			
			// Capçaleres.
			if ($caps == false)
			{
				
				$taula .= '<table cellspacing="0" cellpadding="3" border="1">';

				$taula .= '<thead><tr style="background-color:#CECECE;font-weight: bold;" >';

				$taula .= '<td width="10%">Data</td>';
			    $taula .= '<td width="20%">Projecte</td>';
			    $taula .= '<td width="10%">Tipus</td>';
			    $taula .= '<td width="10%">Tipologia fita</td>';
			    $taula .= '<td width="50%">Descripció</td>';
				 
				$taula .= '</tr></thead>';

				$caps = true;
			}
			
			$taula .= '<tr>';

			$taula .= '<td width="10%">'.$value[datai].'</td>';

			$taula .= '<td>'.str_replace("<p>", "", str_replace("</p>", "", $value[nomprojecte])).'</td>';
			
			$taula .= '<td width="10%">'.$arraytipus[$value[tipus]].'</td>';

			$taula .= '<td width="10%">'.$value[tipologia].'</td>';

			$taula .= '<td width="50%">'.($value[tipus] == 'fita' 
				|| $value[tipus] == 'actuacio' || $value[tipus] == 'actuacioinici' 
				|| $value[tipus] == 'tasca' || $value[tipus] == 'tascainici'
				? str_replace("<p>", "", str_replace("</p>", "", $value[descripcio])) : '' ).'</td>';
			
			$taula .= '</tr>';

			
		}

		$taula .= '

				</table>
				';

		$pdf->writeHTMLCell(0, 0, $pdf->getX(), $pdf->getY(), $taula, 0, 0, 1, true, 'L', true);

		
	}

	

	

	// -----------------------------------------------------------------------------

		

		    //Close and output PDF document
		    //$pdf->Output('example_048.pdf', 'I');

		    //orientació L(horitzonal), P(vertical)

		    //Close and output PDF document
		    // D: download, F: Save file
		    $nomfile = 'Calendari_'.date("Y-m-d_H-i-s").'.pdf';
		    $rutafile = __DIR__."/../../../../secretfiles/informes/".$nomfile;
		    $pdf->Output($rutafile,'F');

		    $ranid = base64_encode($nomfile);

			$tocken = md5('TockenSecretViewDBB!'.$nomfile);

			

			if ($exportget == 3){
				$botodesc2 = '
							
							<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank">
								<button class="btn btn-success btn-xs desc" type="button" >
									<i class="fa fa-download"></i> DESCARREGAR
								</button>
							</a><br><br><br>
							<script>
								//$(".caixablanca").hide();
								$(".desc").click();
							</script>
					';
			}else{
				echo '
						<br>
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank">
							<button class="btn btn-success btn-xs desc" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR INFORME
							</button>
						</a><br><br><br>
						<script>
							//$(".caixablanca").hide();
							$(".desc").click();
						</script>
				';
				exit();
			}

		   // $pdf->Output($nomfile,'D');
		    /*
		    $tmp = ini_get('upload_tmp_dir');

		    $fileatt = "./".$rutafile;
		    $fileatttype = "application/pdf";
		    $fileattname = $nomfile;

		    $file = fopen($fileatt, 'rb');
		    $data = fread($file, filesize($fileatt));
		    fclose($file);
		    
		    ob_end_clean();

		    */

		    //============================================================+
		    // END OF FILE
		    //============================================================+

		

	

		    // Eliminar temporals.
		    $pathdel = __DIR__."/../../secretfiles/tmp/" ;


			if ($handle = opendir($pathdel)) {

		     	while (false !== ($file = readdir($handle))) {

		            if (preg_match('/\.png$/i', $file)) {
					      unlink($pathdel.$file);
					}
						
			     }
		   	}

	