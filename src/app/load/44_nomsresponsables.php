<?php
	
	$arrayusuari = array();

	$RegistreProjecte = $dbb->Llistats("projectes"," AND t.id = :id ",array("id"=>intval($config ['ctf'])), "id", false);	

	if (!empty($RegistreProjecte)){

		// Director.
		array_push($arrayusuari, $RegistreProjecte[1]["director"]);

		// Equip extern.
		$Operadors =  $dbb->FreeSql("SELECT u.titol_ca
								     FROM pfx_projectes_operadors u
								     WHERE u.estat = 1 AND FIND_IN_SET(u.id ,REPLACE( REPLACE( REPLACE( '".$RegistreProjecte[1]["operadors"]."','[', ''),']' ,'') ,'\"','')) > 0
								     ",
									  array());
		if (!empty($Operadors))
		foreach ($Operadors as $key => $value) {
			array_push($arrayusuari, $value["titol_ca"]);
		}


		$cap_projecte = $RegistreProjecte[1][cap_projecte];
		$usuaris_projecte = $RegistreProjecte[1][usuaris];

		if (!empty($usuaris_projecte)){
			$arrayusuaris = array_unique(json_decode($usuaris_projecte));
			if (!empty($cap_projecte)) array_push($arrayusuaris, $cap_projecte);
			$arrayusuaris = array_unique($arrayusuaris); // necessari per els casos que cap és el mateix id que l'usauri.
			
			$primer = false;
			$lastElement = end($arrayusuaris);
			foreach ($arrayusuaris as $key => $value) 
			{	
				if ($primer == false) { $condiciousuaris .= " AND ( "; $primer = true; }
				$condiciousuaris .= " u.id = '".intval($value)."' ";
				($value == $lastElement) ?  $condiciousuaris .= " ) " : $condiciousuaris .= " OR "; 
			}

		}else{
			if (!empty($cap_projecte)) {
				$condiciousuaris = " AND (u.id = '$cap_projecte')";
			}else{
				$condiciousuaris = " AND 1=2";
			}
		}
	}else{
		$condiciousuaris = " AND 1=2";
	}

	$Usuaris =  $dbb->FreeSql("SELECT u.*
							    FROM pfx_usuaris u
							    WHERE 1=1 $condiciousuaris
							     ",
								  array());

	

	if (!empty($Usuaris))
	foreach ($Usuaris as $key => $value) {
		array_push($arrayusuari, $value["nom"]." ".$value["cognoms"]);
	}


	// Montar array per autocomplete:

	$arrayautocomplete = array();
	if (!empty($arrayusuari))
	{
		foreach ($arrayusuari as $key => $value) 
		{
			$arrayautocomplete[] = array('id' => $key, 'value' => str_replace("&#39;", "'", _convert($value)), 'nom' => str_replace("&#39;", "'", _convert($value)) );
		}
	}

	echo json_encode($arrayautocomplete);

	exit();
	