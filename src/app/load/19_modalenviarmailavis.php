<?php
	
	
	$taula = intval($config['tf']);


	if ($taula == 19 || $taula == 22 || $taula == 24 || $taula == 23 || $taula == 25) 
	{	
		if ($taula == 19) $taulae = "plens";
		if ($taula == 22) $taulae = "comissiopermanent";
		if ($taula == 24) $taulae = "comissioinformativa";
		if ($taula == 23) $taulae = "consellparticipacio";
		if ($taula == 25) $taulae = "organdeliberatiu";

		$RegistreEnviar = $dbb->Llistats("$taulae"," AND t.id = :id ",array("id"=>$config['ctf']), "id", false);

		// id : 1 Ordre del dia.
		// id : 2 Acors presos.
		if ($id == 1)
		{
			$texttitomail = "l'ordre del dia.";
			$descripcio1 = $RegistreEnviar[1][descripcio_ca];
			if ($taula == 19) $descripcio1 = decryptRSA($descripcio1);
			if ($taula == 22) $descripcio1 = decryptRSA($descripcio1);
			if ($taula == 25) $descripcio1 = decryptRSA($descripcio1);
			$textemail = "
				Benvolgut/da, t'informem que s'ha convocat el <b>".$RegistreEnviar[1][titol_ca]."</b> en data <b>".($RegistreEnviar[1][inici]!="0000-00-00" ? date("d/m/Y",strtotime($RegistreEnviar[1][inici])) :"" )."</b> amb el següent ordre del dia: <br><br> 
				".$descripcio1."<br>Atentament.
			";
		}
		elseif ($id==2) {
			$texttitomail = "acords presos.";
			$textemail = "
				Benvolgut/da, t'informem t'informem que en el <b>".$RegistreEnviar[1][titol_ca]."</b> en data <b>".($RegistreEnviar[1][inici]!="0000-00-00" ? date("d/m/Y",strtotime($RegistreEnviar[1][inici])) :"" )."</b> es van acordar els següents punts: <br><br> 
				".$RegistreEnviar[1][acords]."<br>Atentament.
			";
		}
		
	}

	$Usuaris =  $dbb->FreeSql("SELECT u.*
							    FROM pfx_usuaris u
							    WHERE clau_permisos > 1
							    ORDER BY nom ",
							  array());
				
	
	$dadesplantilla = array (
		//'pagina' => 41,
		'RegistreEnviar' => $RegistreEnviar,
		'texttitomail' => $texttitomail,
		'textemail' => $textemail,
		'Usuaris' => $Usuaris,
		'tipus' => $taula,
		'subtipus' => $id,
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}

	echo $app['twig']->render('enviarmailform.html', $dadesplantilla);

	exit();
	