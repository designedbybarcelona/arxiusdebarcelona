<?php 
	
	$Detalls_CampsLliures = $dbb->Llistats("camps_lliures"," AND taula = 'tipus_actuacions' AND t.parent_id = :id ", array("id"=>$id),"t.ordre");

	// Carreguem dades per si estem a modificar i canviem de tipus d'actuació.
	if ($idact != 0)
	{
		//$Detalls_tipusact = $dbb->Llistats("actuacions_tipus_dades","   AND t.idact = $idact","t.id");
		$Detalls_tipusact = $dbb->Llistats("actuacions_camps_lliures"," AND parent_id = :idact AND t.idact = :idact2 AND tipus_actuacio = :id",
											array("idact" => $idact, "idact2" =>$idact, "id" => $id), "any asc, ordre asc" ); // No modificar ordre!!!

		// Generar un array per cada any.
		if (!empty($Detalls_tipusact))
		{
			$arrayanys = "";
			foreach ($Detalls_tipusact as $key => $value) 
			{
				$Detalls_tipusactnou[$value['any']][$value['ordre']] = $Detalls_tipusact[$key];
			}
		}

	}
	
	$dadesplantilla = array (
		'RegistreActD' => $Detalls_tipusactnou,
		'Detalls_CampsLliures' => $Detalls_CampsLliures,
		'tipus_actuacio' => $id,
		'any' => $any,
	);

	echo $app['twig']->render('actuacionstipusforms.html', $dadesplantilla);

	exit();