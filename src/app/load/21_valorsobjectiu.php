<?php
	
	$numvalors = $config['num'];

	$RegistreProcesIndicador = $dbb->Llistats("processos_indicadors"," AND t.id = :id ",array("id"=>$id), "id", false);
	
	if (!empty($RegistreProcesIndicador))
	{
		$objectiu = json_decode($RegistreProcesIndicador[1][objectiu],true);
	}

	echo '
		 <div class="row">
	';

	for ($i=1; $i < $numvalors+1; $i++) { 
		
		echo '<div class="row">
			<div class="col-lg-1" style="height: '.($i==1?'40':'30').'px !important;">
				<label style="margin-top:'.($i==1?'31':'24').'px;">'.$i.':</label>
			</div>
			<div class="col-lg-6" style="height: '.($i==1?'40':'30').'px !important;">
                '.($i==1?'<label>Text objectiu</label>':'<label></label>').'
                <input class="form-control" name="textobjectiu[]" value="'.(isset($objectiu[$i])?$objectiu[$i][text]:"").'" style="height: 26px !important;">
            </div>
			<div class="col-lg-4" style="height: '.($i==1?'40':'30').'px !important;">
                '.($i==1?'<label>Valor</label>':'<label></label>').'
                <input class="form-control" name="objectiu[]" value="'.(isset($objectiu[$i])?$objectiu[$i][valor]:"").'" style="height: 26px !important;">
            </div>
            </div>
		';
	}

	echo '</div>';
	

	exit();
	