<?php
	
	$db = new Db();

	$permisos = $app['session']->get(constant('General::nomsesiouser')."-permisos");

	require_once __DIR__."/../../pagines/consultesfiltres_inc.php";

	require_once __DIR__."/../../pagines/inici_inc.php";

	
	$Dades = '
		
	 	<td colspan="7">
          <div class="row">
              <div class="col-lg-12">

                    
                      <table class="table table-hover ">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Centre o servei</th>
                            <th>Ind.</th>
                            <th width="50%">Nom de projecte</th>
                            <th>Cap de projecte</th>
                            <th width="10%" style="text-align:center;">Inici</th>
                            <th width="15%" style="text-align:center;">Final</th>
                            <th width="7%">Estat</th>
                            <th width="8% style="text-align:center;"">% execució</th>
                            <th width="5%" style="text-align:center;">Actualizat</th>
                            '.($permisos == 2?'<th width="5%" style="text-align:center;">Accions</th>':'').'
                            '.($permisos != 3?' <th width="10%">Edició</th>':'').'
                            <th> Detalls </th>
                          </tr>
                        </thead>
                        <tbody>';
                        
                        
                        $tedadesproj = false;
                        foreach ($DadesProjectes_llistat as $key_p => $value_p) 
                        {
                        	$indicadorsok = 0;
	                        $indicadorsko = 0;
	                        $contaidicadors = 0;
                        
                        	$IndicadorsProj = $db->query("SELECT assolit
														  FROM pfx_projectes_indicadors_arxius
														  WHERE clau_projecte = :id
														  ",array("id"=>$value_p[id]));
                            foreach ($IndicadorsProj as $key_i => $value_i) {
                            	if ($value_i['assolit'] == 0) $indicadorsko++;
                            	if ($value_i['assolit'] == 1) $indicadorsok++;
                            	$contaidicadors++;
							}

							$simbolindicador = "";
							// UP: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que SI han estat assolits tots ells.
							// DOWN: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que NO han estats assolits cap d’ells.
							// WORK: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit + d’1 indicador, i s’hagi dit a la fitxa que algun SI ha estat assolit, i algun altre NO.
							if ($value_p['estat'] == 1 || $value_p['estat'] == 3 || $value_p['estat'] == 4 ){
								if ($indicadorsok > 0 && $indicadorsko == 0) $simbolindicador = '<i class="fa fa-thumbs-up"></i>';
								if ($indicadorsok == 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-thumbs-down"></i>';
								if ($indicadorsok > 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-exclamation-triangle"></i>';
							}

                        	
                          	$Dades .= '
                              <tr '.($value_p[idsactuacions]!=""?'class="mostarprojecte" data-id="'.$value_p[id].'" style="cursor:pointer;"':"").' >
                                <td>'.($value_p[idsactuacions]!=""?'<i class="fa fa-plus"></i>':"").'</td>
                                <td>'.$value_p[acronim].'</td>
                                <td>'.$simbolindicador.'</td>
                                <td>'.$value_p[nom].'</td>
                                <td>'.$value_p[nomresp].' '.$value_p[cognomsresp].'</td>
                                <td style="text-align:center;"><b>'.($value_p[inici]!= "00/00/0000"?$value_p[inici]:"").'</b></td>
                                <td style="text-align:center;"><b>';

                                	if ($value_p["final"] != "00/00/0000") {
                                		if ($value_p[colorsemafor]){
                                			if ($value_p[colorsemafor]== "1") 
                                				$colorsemafor="#7bd148"; //verd
                                			elseif ($value_p[colorsemafor]== "2") 
                                				$colorsemafor="#ffb878"; //ambar
                                			elseif ($value_p[colorsemafor]== "3") 
                                				$colorsemafor="#ff887c"; //vermell
                                			elseif ($value_p[colorsemafor]== "4") 
                                				$colorsemafor="#e1e1e1"; //gris
                                			elseif ($value_p[colorsemafor]== "5") 
                                				$colorsemafor="#5681de"; //blau
                                		}
                                		$Dades .= $value_p["final"].' '.$value_p["simbol"].'<span class="badge" style="background-color:'.$colorsemafor.';">&nbsp;&nbsp;</span>';
                                	}

                                $Dades .= '
                                        </b></td>
                                <td>'.$value_p[estatprojecte].'</td>
                                <td style="text-align:center;">'.$value_p[tantpercent].'</td>
                                
                                <td style="text-align:center;">'.$value_p[updated].'</td>';
                                if ($permisos == 2){
                                	if ($value_p[estrategic] == 1){
                                		$Dades .= '
											<td>
			                                    <a href="./7/subprojectes.html?n&idp='.$value_p[id].'">
			                                    <button type="button" class="btn btn-success btn-xs edicio" style="margin-bottom: 2px;">+ Nova fase</button>
			                                    </a>
			                                    <a href="./8/accions.html?n&idp='.$value_p[id].'">
			                                    <button type="button" class="btn btn-success btn-xs edicio">+ Nova accio</button>
			                                    </a>
	                                  		</td>
	                                	';
                                	}else{
                                		$Dades .= '
										<td>
		                                    <button type="button" class="btn btn-success btn-xs" style="margin-bottom: 2px;" onClick="alert(\'Per crear fases, cal marcar el projecte com estratègic\')">+ Nova fase</button>
		                                    <button type="button" class="btn btn-success btn-xs" onClick="alert(\'Per crear accions, cal marcar el projecte com estratègic\')">+ Nova accio</button>
		                                    
                                  		</td>
                                	';
                                	}
                                	
                                	
                                }
                               	
                                if ($permisos != 3){
                                	$Dades .= '
	                                <td>
	                                    <a href="./6/projectes.html?id='.$value_p[id].'">
	                                    <button type="button" class="btn btn-primary btn-xs edicio">Edició</button>
	                                    </a>
	                                </td>
	                            	';
                                }
                                $Dades .= '
                                <td>
                                	<button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_p["id"].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalprojectes"><i class="fa fa-search" aria-hidden="true"></i></button>
                                	<span class="export_ind" data-id="'.$value_p["id"].'" style="cursor:pointer;"><img src="./images/icon-pdf.png" width="20"/></span>
                                </td>
                              </tr>
                              <tr style="display:none;" class="proj_'.$value_p[id].'">';
                                if ($permisos == 2){
                                	$Dades .= '<td colspan="9">';
                                }else{
                                	$Dades .= '<td colspan="8">';
                                }
	                             	
	                             	$Dades .= '
	                                  <div class="row">
	                                      <div class="col-lg-12">

	                                          <div class="panel panel-info">
	                                            
	                                              <div class="panel-body">
	                                                  <table class="table  table-hover ">
	                                                    <thead>
	                                                      <tr>
	                                                        <th>#</th>
	                                                        <th>Fase</th>
	                                                        <th>Accions</th>
	                                                        <th>Estat</th>
	                                                        <th>Edició</th>
	                                                      </tr>
	                                                    </thead>
	                                                    <tbody>';

	                                                    	$Actuacions = $db->query("SELECT a.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio, a.id as id
																					  FROM pfx_actuacions a
																					  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
																					  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
																					  WHERE a.clau_projecte = :id
																					  GROUP By a.id ",array("id"=>$value_p[id]));

	                                                    	$tedades = false;

	                                                        foreach ($Actuacions as $key_a => $value_a) 
                        									{
		                                                        $Dades .= '
		                                                          <tr class="mostartasques" data-id="'.$value_a[id].'" style="cursor:pointer;">
		                                                            <td><i class="fa fa-plus"></i></td>
		                                                            <td>'.$value_a[titol].'</td>
		                                                            <td>';

		                                                            	$Tasques = $db->query("SELECT t.*
																							  FROM pfx_tasques t
																							  INNER JOIN pfx_actuacions a ON a.id = t.clau_actuacio
																							  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
																							  WHERE t.clau_projecte = :idp AND t.clau_actuacio = :ida
																							  GROUP By t.id ",array("idp"=>$value_p[id],"ida"=>$value_a[id]));
	

		                                                                $contatasques = 0;
		                                                                $contatasquestancades = 0;
		                                                                $contatasquespendents = 0;
		                                                                foreach ($Tasques as $key_t => $value_t) {
		                                                                	if ($value_t[estat]==1 || $value_t[estat]==3) $contatasques++;
		                                                                	if ($value_t[estat]==2 || $value_t[estat]==6) $contatasquestancades++;
		                                                                	if ($value_t[estat]==4 || $value_t[estat]==5 || $value_t[estat]==0) $contatasquespendents++;
                        												}
		                                                                $contatasques = $contatasques + $contatasquestancades + $contatasquespendents;
		                                                            $Dades .= $contatasques.'
		                                                                
		                                                            </td>
		                                                            <td>'.$value_a[estatactuacio].'</td>
		                                                            <td>';
		                                                            if ($permisos != 3){
		                                                            	$Dades .= '
		                                                                <a href="./7/actuacions.html?id='.$value_a[id].'">
		                                                                <button type="button" class="btn btn-primary btn-xs edicio">Edició</button>
		                                                                </a>
		                                                                ';
		                                                            }

	                                                             	$Dades .= '
						                                              <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_a["id"].'" data-t="2" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalprojectes"><i class="fa fa-search" aria-hidden="true"></i></button>
						                                         
		                                                            </td>
		                                                          </tr>
		                                                          <tr style="display:none;" class="tasc_'.$value_a[id].'">
		                                                            <td colspan="4">
		                                                                <div class="row">
		                                                                    <div class="col-lg-12">

		                                                                        <div class="panel panel-primary">
		                                                                            <div class="panel-body">
		                                                                                <table class="table  table-hover ">
		                                                                                  <thead>
		                                                                                    <tr>
		                                                                                      <th>Acció</th>
		                                                                                      <th>'.($permisos != 3?'Edició':'').'</th>
		                                                                                    </tr>
		                                                                                  </thead>
		                                                                                  <tbody>';

		                                                                                    $tedades = false;
		                                                                                    foreach ($Tasques as $key_t => $value_t) 
		                                                                                    {
		                                                                                        $Dades .= '
		                                                                                        <tr>
		                                                                                            <td>'.$value_t[titol_ca].'</td>
		                                                                                            <td>';
		                                                                                            	if ($permisos != 3){
			                                                                                            	$Dades .= '
			                                                                                                <a href="./8/accions.html?id='.$value_t[id].'">
			                                                                                                <button type="button" class="btn btn-primary btn-xs edicio">Edició</button>
			                                                                                                </a>';
		                                                                                            	}
		                                                                                            	$Dades .= '
								                                                                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_t["id"].'" data-t="3" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalprojectes"><i class="fa fa-search" aria-hidden="true"></i></button>
								                                                                          ';
		                                                                                            	$Dades .= '
		                                                                                            </td>
		                                                                                        </tr>';
		                                                                                        $tedades = true;
		                                                                                    }
		                                                                                    if ($tedades == false){
																	                          	$Dades .= '
																									<tr>
																		                                <td colspan="2">Sense accions.</td>
																		                            </tr>
																	                          	';
																                          	}
		                                                                                    
		                                                                                   $Dades .= '
		                                                                                  </tbody>
		                                                                                </table> 
		                                                                            </div>
		                                                                        </div>

		                                                                    </div>
		                                                                </div>
		                                                            </td>
		                                                          </tr>';
		                                                        $tedades = true;
	                                                        }
	                                                        if ($tedades == false){
									                          	$Dades .= '
																	<tr>
										                                <td colspan="2">Sense fases.</td>
										                            </tr>
									                          	';
								                          	}
	                                                    	
	                                                    $Dades .= '  
	                                                    </tbody>
	                                                  </table> 
	                                              </div>
	                                          </div>

	                                      </div>
	                                      
	                                  </div>
                                </td>
                              </tr>
                              <tr style="display:none;">
                                <td colspan="5">
                                </td>
                              </tr>';
                              $tedadesproj = true;
                        }

                        if ($tedadesproj == false){
	                      	$Dades .= '
								 <tr>
	                                <td colspan="2">Sense projectes.</td>
	                              </tr>
	                      	';
	                    }
                         
                          
                        $Dades .= '
                          
                        </tbody>
                      </table> 
                   

              </div>
          </div>
        </td>

        <script type="text/javascript">
      	$(document).ready(function($) {

	        $(document).off("click",".mostrarconsulta").on("click",".mostrarconsulta",function(event){

					event.preventDefault();

		            var id = $(this).data( "id" );	

		             var t = $(this).data( "t" );
                	if (t == ""){ t = 1; }
		              
		          	$(".mostrarmodal").html("<img src=\'images/loadingg.gif\'/>");
		          	$(".mostrarmodal").load("load",{o:3, id:id, t: t, c:{co:1  } }, function(){

		                    
		                $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmplens" ).attr(\'disabled\', \'disabled\');
		               
		                $("#myModalLabel").html("Detalls");
		                
		                $(\'.mostrarmodal\').find(\'.btn\').hide();

		                setTimeout(function () {
		                    $(\'.mostrarmodal\').find(\'.btn\').hide();
		                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                }, 1500);
		                
		                setTimeout(function () {
		                    $(\'.mostrarmodal\').find(\'.btn\').hide();
		                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                }, 2000);

		                setTimeout(function () {
		                    $(\'.mostrarmodal\').find(\'.btn\').hide();
		                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                }, 4000);

		                setTimeout(function () {
		                    $(\'.mostrarmodal\').find(\'.btn\').hide();
		                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                }, 5000);


		          }); 
		            
		            

		    });

		});
		</script>

	';

	echo $Dades;