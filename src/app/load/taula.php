<?php
    
    $clau_permis = $app['session']->get(constant('General::nomsesiouser')."-permisos");
    $arraypermisos = $dbb->ArrayPermisos();

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    $aColumns = $camps; //array( 'nom_ca', 'id');
     
    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
     
     
    /*
     * Paging
     */
    $sLimit = "";
    if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    {
        $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
    }
     
     
    /*
     * Ordering
     */
    $sOrder = "";
    if ( isset( $_GET['iSortCol_0'] ) )
    {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
        {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
                $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                    ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
        }
         
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" )
        {
            $sOrder = "";
        }
    }
     
     
    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */

    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    {
        $sWhere = "WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
            {
                $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
            }
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
    }
     
    /* Individual column filtering */
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        {
            if ( $sWhere == "" )
            {
                $sWhere = "WHERE ";
            }
            else
            {
                $sWhere .= " AND ";
            }
            $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
        }
    }
     
     
    /*
     * SQL queries
     * Get data to display
     */
    $db = new Db();
    $sQuerys = str_replace("¬sInner¬", $sInner, str_replace("¬sWhere¬", $sWhere, str_replace("¬sOrder¬", $sOrder, str_replace("¬sLimit¬", $sLimit, $sQuery))));
    
    // var_dump($sQuerys);

    $rResult = $db->query($sQuerys,$where);

    /* Data set length after filtering */
    $iFilteredTotal = count($rResult);
     
    /* Total data set length */
    $sQuerys = str_replace("¬sInner¬", $sInner, str_replace("¬sWhere¬", "", str_replace("¬sOrder¬", "", str_replace("¬sLimit¬", "", $sQuery))));
    $rResultTotal = $db->query($sQuerys,$where);
    $iTotal = count($rResultTotal);
     
     
    /*
     * Output
     */
    /*
	$output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
	*/
    
    // Configuracions.
    if (empty($configuracio['mostarestat']) ) $configuracio['mostarestat'] = si ;
    if (empty($configuracio['mostrareliminar']) ) $configuracio['mostrareliminar'] = si ;
    if (empty($configuracio['mostrarconsultar']) ) $configuracio['mostrarconsultar'] = no ;

    

    foreach ($rResult as $contrs =>$aRow) 
    {
        $row = array();
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( is_array($aColumns[$i]) )
            {
                /* Especial per quan el camp és compost. */
                $valor = "";
                foreach ($aColumns[$i] as $key => $value) {
                    if (substr($value, 0, 5) === ';json') break;// Especial JSON.
                     $valor .= (substr($value, 0, 1) === ':' ? ltrim($value, ':') : (substr($value, 0, 5) === ';rsa_' ? decryptRSA($aRow[str_replace(';rsa_','',$value)]) : $aRow[$value] )  ); 
                     // si comença per ":" ho tracta com un string. Si comença per ;rsa_ és un camp xifrat.
                }

                // JSON. Primer hi ha el camp a llegir, despres la configuració de quins index agafar i strings.
                if (substr($value, 0, 5) === ';json'){

                    $jsondecode = json_decode($aRow[$aColumns[$i][$key+1]],true);
                    if (!empty($jsondecode) && is_array($jsondecode)){
                        
                        foreach ($jsondecode as $key_json => $value_json)  {

                           foreach ($aColumns[$i] as $key2 => $value2) {    

                                if ($key2 > 1) {

                                    if (substr($value2, 0, 1) === ':') {
                                        $valor .= ltrim($value2, ':');
                                    } else { // Camp del json
                                        $valor .= $value_json[$value2];
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $valor .= $jsondecode;
                    }
                }

                $row[] = $valor;
            }
            else if ( $aColumns[$i] != ' ' )
            {
                /* General output */
                $row[] = $aRow[ $aColumns[$i] ];
            }
        }

        // Estat.
        if ($aRow["estat"] == 1){
            $estat = '<span class="label label-success">'.$app['translator']->trans("Actiu").'</span>';
        }else{
            $estat = '<span class="label label-default">'.$app['translator']->trans("Inactiu").'</span>';
        }
        if ($configuracio['mostarestat'] == si)
        {
            $row[] = $estat;
        }

        // Botons.
        $botoeliminar = "";
        if ($configuracio['mostrareliminar'] == si  )
        {
            if ( $sTable != "usuaris" || ($sTable == "usuaris" && $aRow["id"] != 1))
            {   
                if ($arraypermisos[$taula][eliminar] == true || $clau_permis == 1 || $clau_permis == 2 )
                {
                    $txteliminar = $app['translator']->trans("Eliminar");
                    if ($sTable == "passos" && $config['e'] == 1) $txteliminar=""; // per espai a la taula s'elimina el text.
                    $botoeliminar = '
                        <button class="btn btn btn-danger btn-xs elimregistre_'.$configuracio["time"].'" href="#" id="elim_'.$aRow["id"].'">
                            <i class="fa fa-trash"></i> '.$txteliminar.'
                        </button>
                    ';
                }
            }
        }

        if ($configuracio['mostrarconsultar'] == si  )
        {
              
            $txtconsultar = $app['translator']->trans("Consultar");
            $botoeliminar .= '
                <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$aRow["id"].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button>
            ';
                
        }

        if ($configuracio['enmodal'] == si  )
        {
            
            $botoeditar = $botoveureact;

            if ( ($arraypermisos[$taula][editar] == true || $clau_permis == 1 || $clau_permis == 2 )  ) //&& $sTable != "upfile"
            {
                if ($sTable != "dintres_projectes_tecnics") $datatoggle = ' data-toggle="modal" ';
                $botoeditar .= '
                        <button class="btn btn-info btn-xs botomodal edicio_'.$taula.'" id="modi_'.$aRow["id"].'_'.$taula.'" href="#" '.$datatoggle.'  data-id="'.$aRow["id"].'" data-ctf="'.$configuracio['ctf'].'" data-t="'.$taula.'" data-target="#'.$configuracio['idmodal'].'">
                            <i class="fa fa-edit"></i> '.$app['translator']->trans("Editar").'
                        </button>
                ';
            }
            if ($sTable == "upfile")
            {
                
                $ranid = base64_encode($aRow["id"]);

                $tocken = md5('TockenSecretViewDBB!'.$aRow["id"]);

                $botoeditar .= '

                        <a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'" target="_blank">
                            <button class="btn btn-success btn-xs" type="button" >
                                <i class="fa fa-download"></i>
                            </button>
                        </a>
                ';
            }

            if ($sTable == "passos" && $config['e'] == 1)
            {
                

                $botoeditar .= '
                    <button class="btn btn-success btn-xs consolidar" type="button" data-id="'.$aRow["id"].'">
                        Consolidar
                    </button>
                ';
            }
            
            
        }
        else
        {
            
            if (($arraypermisos[$taula][editar] == true || $clau_permis == 1 || $clau_permis == 2 ) AND ($configuracio['paginaconsulta'] == '') )
            {
                $botoeditar = '
                        <button class="btn btn-info btn-xs modiregistre_'.$configuracio["time"].'" href="#" id="modi_'.$aRow["id"].'">
                            <i class="fa fa-edit"></i> '.$app['translator']->trans("Editar").'
                        </button>
                ';
            }

            if ($configuracio['paginaconsulta'] == si )
            {
                if ($taula == "projectes") $idmostrar = $rs["id"];
                if ($taula == "actuacions") $idmostrar = $rs["idprojecte"];
                if ($taula == "tasques") $idmostrar = $rs["idprojecte"];
                $botoeditar .= '
                    
                        <button class="btn btn-primary btn-xs">
                            <a href="'.constant("DBB::url").'/19/consultes.html?id='.$idmostrar.'" target="_blank">CONSULTAR</a>
                        </button>
                    
                ';
            }

        }
        $row[] = $botoeditar.$botoeliminar;

        $output['aaData'][] = $row;
    }
     
    echo json_encode( $output );
?>