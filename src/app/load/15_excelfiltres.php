<?php
	

	$tipus = $_POST[tipus];

	/*
	foreach ($tipus as $key => $value) 
	{
		if ($value == 1) $tipus1 = 1;
		if ($value == 2) $tipus2 = 1;
		if ($value == 3) $tipus3 = 1;
	}
	*/

	// Modificació, sempre exportará tot l'arbre. Per tant posem tots els tipus a 1.
	$tipus1 = 1;
	$tipus2 = 1;
	$tipus3 = 1;

	echo "
		<hr>
		<input type='hidden' name='export' value='1' />
		<div class='col-lg-12'>
            <h3>Resultat filtre <button type='submit' class='btn btn-default btn-xs'>Exportar a XLS</button></h3>
        </div>
	";

	// Projectes.
	if (isset($tipus1))
	{
		$condicioprojectes = "";
		if (isset($_POST['cap_projecte'])){
			$cap_projecte = array_unique(stripslashes_deep($_POST['cap_projecte']));
			$primer = false;
			$lastElement = end($cap_projecte);
			foreach ($cap_projecte as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " p.cap_projecte = '$value' ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['estatprojecte'])){
			$estatprojecte = array_unique(stripslashes_deep($_POST['estatprojecte']));
			$primer = false;
			$lastElement = end($estatprojecte);
			foreach ($estatprojecte as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " p.estat = '$value' ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['tipologia'])){
			$tipologia = array_unique(stripslashes_deep($_POST['tipologia']));
			$primer = false;
			$lastElement = end($tipologia);
			foreach ($tipologia as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " p.tipologia = '$value' ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['dimensions'])){
			$dimensions = array_unique(stripslashes_deep($_POST['dimensions']));
			$primer = false;
			$lastElement = end($dimensions);
			foreach ($dimensions as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( p.dimensions,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['origens'])){
			$origens = array_unique(stripslashes_deep($_POST['origens']));
			$primer = false;
			$lastElement = end($origens);
			foreach ($origens as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " p.origens = '$value' ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['vinculacio'])){
			$vinculacio = array_unique(stripslashes_deep($_POST['vinculacio']));
			$primer = false;
			$lastElement = end($vinculacio);
			foreach ($vinculacio as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['districtes'])){
			$districtes = array_unique(stripslashes_deep($_POST['districtes']));
			$primer = false;
			$lastElement = end($districtes);
			foreach ($districtes as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( p.districtes,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['iniciprojecte'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['iniciprojecte'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			if (!empty($inici[2]))
			$condicioprojectes .= " AND p.inici >= '$inici[2]-$inici[1]-$inici[0]' ";
		}
		if (isset($_POST['finalprojecte'])){
			$final = filter_var(htmlspecialchars(trim($_POST['finalprojecte'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			if (!empty($final[2]))
			$condicioprojectes .= " AND p.final <= '$final[2]-$final[1]-$final[0]' ";
		}
		if (isset($_POST['usuaris'])){
			$usuaris = array_unique(stripslashes_deep($_POST['usuaris']));
			$primer = false;
			$lastElement = end($usuaris);
			foreach ($usuaris as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}
		if (isset($_POST['organs'])){
			$organs = array_unique(stripslashes_deep($_POST['organs']));
			$primer = false;
			$lastElement = end($organs);
			foreach ($organs as $key => $value) 
			{
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( p.organs,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			}
		}

		

		// Mostrar taula.
		/*
		$sTable = "projectes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu > 2)
			{
				$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
			}

			$sWhere = "WHERE 1 = 1 $condicio_permis $condicioprojectes";
			$sInner = "LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
					   LEFT JOIN pfx_usuaris u ON p.cap_projecte = u.id
					   LEFT JOIN pfx_paraules pa2 ON pa2.codi = 'tipologiaprojecte' AND pa2.idioma = 'ca' AND pa2.clau = p.tipologia
					   LEFT JOIN pfx_projectes_origens o ON p.origens = o.id  ";
			$sOrder = "ORDER by p.titol_ca desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'codi',
				'nom',
				'estatprojecte',
				array('cognomsresp',': ,','nomresp'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(p.id) as id, p.titol_ca as nom, p.id as id, p.estat as estat, p.*,
				  		p.ordre as ordre, p.codi as codi, IFNULL(pa.text,'----') as estatprojecte, u.nom as nomresp, u.cognoms as cognomsresp, pa2.text as nomtipologia,
				  		o.titol_ca as nomorigen
					   FROM pfx_$sTable p
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom del projecte'),
			'3' => $app['translator']->trans('Estat del projecte'),
			'4' => $app['translator']->trans('Cap del projecte'),

		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;
		*/

		// Mostrar taula.
		$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
		if ($permisusu > 3)
		{
			$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
		}

		$queryprojectes = "SELECT distinct(p.id) as id, p.titol_ca as nom, p.id as id, p.estat as estat, p.*,
				  p.ordre as ordre, p.codi as codi, IFNULL(pa.text,'----') as estatprojecte, u.nom as nomresp, u.cognoms as cognomsresp, pa2.text as nomtipologia, o.titol_ca as nomorigen
				  FROM pfx_projectes p
				  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
				  LEFT JOIN pfx_usuaris u ON p.cap_projecte = u.id
				  LEFT JOIN pfx_paraules pa2 ON pa2.codi = 'tipologiaprojecte' AND pa2.idioma = 'ca' AND pa2.clau = p.tipologia
				  LEFT JOIN pfx_projectes_origens o ON p.origens = o.id
				  WHERE 1=1 $condicio_permis $condicioprojectes
				  GROUP BY p.id
				  ORDER by p.titol_ca desc
				";
		$camps = array(
			'1' => '$rs["codi"]',
			'2' => '$rs["nom"]',
			'3' => '$rs["estatprojecte"]',
			'4' => '$rs["cognomsresp"].", ".$rs["nomresp"]',
		);
		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom del projecte'),
			'3' => $app['translator']->trans('Estat del projecte'),
			'4' => $app['translator']->trans('Cap del projecte'),

		);
		$Dades = $dbb->GenerarLlistats("projectes", $queryprojectes, $camps, $capcaleres,array('mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si'));
		

		echo $Dades;


	}

	
	// Actuacions.
	if (isset($tipus2))
	{

		$condicioactuacions = "";
		if (isset($_POST['projectes'])){
			$projectes = array_unique(stripslashes_deep($_POST['projectes']));
			$primer = false;
			$lastElement = end($projectes);
			foreach ($projectes as $key => $value) 
			{
				if ($primer == false) { $condicioactuacions .= " AND ( "; $primer = true; }
				$condicioactuacions .= " t.clau_projecte = '$value' ";
				($value == $lastElement) ?  $condicioactuacions .= " ) " : $condicioactuacions .= " OR "; 
			}
		}
		if (isset($_POST['responsable'])){
			$responsable = array_unique(stripslashes_deep($_POST['responsable']));
			$primer = false;
			$lastElement = end($responsable);
			foreach ($responsable as $key => $value) 
			{
				if ($primer == false) { $condicioactuacions .= " AND ( "; $primer = true; }
				$condicioactuacions .= " t.responsable = '$value' ";
				($value == $lastElement) ?  $condicioactuacions .= " ) " : $condicioactuacions .= " OR "; 
			}
		}
		if (isset($_POST['vinculacio'])){
			$vinculacio = array_unique(stripslashes_deep($_POST['vinculacio']));
			$primer = false;
			$lastElement = end($vinculacio);
			foreach ($vinculacio as $key => $value) 
			{
				if ($primer == false) { $condicioactuacions .= " AND ( "; $primer = true; }
				$condicioactuacions .= " t.vinculacio = '$value' ";
				($value == $lastElement) ?  $condicioactuacions .= " ) " : $condicioactuacions .= " OR "; 
			}
		}
		if (isset($_POST['estatactuacio'])){
			$estatactuacio = array_unique(stripslashes_deep($_POST['estatactuacio']));
			$primer = false;
			$lastElement = end($estatactuacio);
			foreach ($estatactuacio as $key => $value) 
			{
				if ($primer == false) { $condicioactuacions .= " AND ( "; $primer = true; }
				$condicioactuacions .= " t.estat = '$value' ";
				($value == $lastElement) ?  $condicioactuacions .= " ) " : $condicioactuacions .= " OR "; 
			}
		}
		if (isset($_POST['iniciactuacio'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['iniciactuacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			if (!empty($inici[2]))
			$condicioactuacions .= " AND t.inici >= '$inici[2]-$inici[1]-$inici[0]' ";
		}
		if (isset($_POST['finalactuacio'])){
			$final = filter_var(htmlspecialchars(trim($_POST['finalactuacio'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			if (!empty($final[2]))
			$condicioactuacions .= " AND t.final <= '$final[2]-$final[1]-$final[0]' ";
		}


		// Llistats.
		/*
		$sTable = "actuacions";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu > 2)
			{
				$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
			}

			$sWhere = "WHERE 1 = 1 $condicio_permis $condicioactuacions";
			$sInner = "LEFT JOIN pfx_projectes p ON t.clau_projecte = p.id
						  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
						  LEFT JOIN pfx_usuaris u ON t.responsable = u.id
						  LEFT JOIN pfx_projectes p2 ON p2.id = t.vinculacio";
			$sOrder = "ORDER by t.titol_ca desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'codi',
				'nom',
				'nomdelprojecte',
				'estatprojecte',
				array('cognomsresp',': ,','nomresp'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, t.*,
						  t.ordre as ordre, t.codi as codi, t.codi as codi, IFNULL(pa.text,'----') as estatprojecte,
						  p.titol_ca as nomdelprojecte, u.nom as nomresp, u.cognoms as cognomsresp, p.id as idprojecte,
						  p2.titol_ca as nomindirecte
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom de la proposta'),
			'3' => $app['translator']->trans('Projecte'),
			'4' => $app['translator']->trans('Estat de la proposta'),
			'5' => $app['translator']->trans('Responsable de la proposta'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;
		*/

		$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
		if ($permisusu > 3)
		{
			$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
		}

		$queryactuacions = "SELECT distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, t.*,
				  t.ordre as ordre, t.codi as codi, t.codi as codi, IFNULL(pa.text,'----') as estatprojecte,
				  p.titol_ca as nomdelprojecte, u.nom as nomresp, u.cognoms as cognomsresp, p.id as idprojecte,
				  p2.titol_ca as nomindirecte
				  FROM pfx_actuacions t
				  LEFT JOIN pfx_projectes p ON t.clau_projecte = p.id
				  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
				  LEFT JOIN pfx_usuaris u ON t.responsable = u.id
				  LEFT JOIN pfx_projectes p2 ON p2.id = t.vinculacio
				  WHERE 1=1 $condicio_permis $condicioactuacions
				  GROUP BY t.id
				  ORDER by t.titol_ca desc
				";
		$camps = array(
			'1' => '$rs["codi"]',
			'2' => '$rs["nom"]',
			'3' => '$rs["nomdelprojecte"]',
			'4' => '$rs["estatprojecte"]',
			'5' => '$rs["cognomsresp"].", ".$rs["nomresp"]',
		);
		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom de la proposta'),
			'3' => $app['translator']->trans('Projecte'),
			'4' => $app['translator']->trans('Estat de la proposta'),
			'5' => $app['translator']->trans('Responsable de la proposta'),
		);
		$Dades = $dbb->GenerarLlistats("actuacions", $queryactuacions, $camps, $capcaleres,array('mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si'));


		/*
		echo "
			<hr>
			<input type='hidden' name='export' value='1' />
			<div class='col-lg-12'>
                <h3>Reslutat filtre Actuacions <button type='submit' class='btn btn-default btn-xs'>Exportar a XLS</button></h3>
            </div>
		";
		*/

	}


	// Tasques.
	if (isset($tipus3))
	{

		$condicioatasques = "";
		if (isset($_POST['projectestasca'])){
			$projectestasca = array_unique(stripslashes_deep($_POST['projectestasca']));
			$primer = false;
			$lastElement = end($projectestasca);
			foreach ($projectestasca as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .= " t.clau_projecte = '$value' ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}
		if (isset($_POST['actuacions'])){
			$actuacions = array_unique(stripslashes_deep($_POST['actuacions']));
			$primer = false;
			$lastElement = end($actuacions);
			foreach ($actuacions as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .= " t.clau_actuacio = '$value' ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}
		if (isset($_POST['responsabletasca'])){
			$responsabletasca = array_unique(stripslashes_deep($_POST['responsabletasca']));
			$primer = false;
			$lastElement = end($responsabletasca);
			foreach ($responsabletasca as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .= " t.responsable = '$value' ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}
		if (isset($_POST['estattasca'])){
			$estattasca = array_unique(stripslashes_deep($_POST['estattasca']));
			$primer = false;
			$lastElement = end($estattasca);
			foreach ($estattasca as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .= " t.estat = '$value' ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}
		if (isset($_POST['inicitasca'])){
			$inici = filter_var(htmlspecialchars(trim($_POST['inicitasca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inici = explode("/",$inici);
			if (!empty($inici[2]))
			$condicioatasques .= " AND t.inici >= '$inici[2]-$inici[1]-$inici[0]' ";
		}
		if (isset($_POST['finaltasca'])){
			$final = filter_var(htmlspecialchars(trim($_POST['finaltasca'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$final = explode("/",$final);
			if (!empty($final[2]))
			$condicioatasques .= " AND t.final <= '$final[2]-$final[1]-$final[0]' ";
		}
		if (isset($_POST['inicipas'])){
			$inicipas = filter_var(htmlspecialchars(trim($_POST['inicipas'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$inicipas = explode("/",$inicipas);
			if (!empty($inicipas[2]))
			$condicioapassosinici = " $inicipas[2]-$inicipas[1]-$inicipas[0] ";
		}
		if (isset($_POST['finalpas'])){
			$finalpas = filter_var(htmlspecialchars(trim($_POST['finalpas'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$finalpas = explode("/",$finalpas);
			if (!empty($finalpas[2]))
			$condicioapassosfinal = " $finalpas[2]-$finalpas[1]-$finalpas[0] ";
		}
		if (isset($_POST['naturalesa'])){
			$naturalesa = array_unique(stripslashes_deep($_POST['naturalesa']));
			$primer = false;
			$lastElement = end($naturalesa);
			foreach ($naturalesa as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .= " FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( t.naturalesa,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}
		if (isset($_POST['agents'])){
			$agents = array_unique(stripslashes_deep($_POST['agents']));
			$primer = false;
			$lastElement = end($agents);
			foreach ($agents as $key => $value) 
			{
				if ($primer == false) { $condicioatasques .= " AND ( "; $primer = true; }
				$condicioatasques .=" FIND_IN_SET($value ,REPLACE( REPLACE( REPLACE( t.agents,'[', ''),']' ,'') ,'\"','')) > 0 ";
				($value == $lastElement) ?  $condicioatasques .= " ) " : $condicioatasques .= " OR "; 
			}
		}

		// Llistats.
		/*
		$sTable = "tasques";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu > 2)
			{
				$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
			}

			if ($condicioapassosinici != "" || $condicioapassosfinal != "")
			{
				$condicio_passos = " INNER JOIN pfx_passos ps ON ps.clau_tasca = t.id AND ps.clau_pla = $idpla  ";
				if ($condicioapassosinici != "") $condicio_passos .= " AND datai >= '$condicioapassosinici' ";
				if ($condicioapassosfinal != "") $condicio_passos .= " AND datai <= '$condicioapassosfinal' ";
			}

			$sWhere = "WHERE 1 = 1 $condicio_permis $condicioatasques";
			$sInner = " LEFT JOIN pfx_actuacions a ON t.clau_actuacio = a.id
						  LEFT JOIN pfx_projectes p ON a.clau_projecte = p.id
						  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estattasca' AND pa.idioma = '$idioma'
						  LEFT JOIN pfx_usuaris u ON t.responsable = u.id
						  LEFT JOIN pfx_projectes_agents o ON o.id = t.agents
						  $condicio_passos";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'codi',
				'nom',
				'nomactuacio',
				'nomdelprojecte',
				'estattasca',
				array('cognomsresp',': ,','nomresp'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, t.*,
						  t.ordre as ordre, t.codi as codi, IFNULL(pa.text,'----') as estattasca,
						  p.titol_ca as nomdelprojecte, a.titol_ca as nomactuacio, u.nom as nomresp, u.cognoms as cognomsresp, p.id as idprojecte
						  , o.titol_ca as nomagent, t.descripcio_ca as descripcio
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom de la tasca'),
			'3' => $app['translator']->trans('Nom de la proposta'),
			'4' => $app['translator']->trans('Nom del projecte'),
			'5' => $app['translator']->trans('Estat de la tasca'),
			'6' => $app['translator']->trans('Responsable de la tasca'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;
		*/

		/*
		echo "
			<hr>
			<input type='hidden' name='export' value='1' />
			<div class='col-lg-12'>
                <h3>Reslutat filtre Tasques <button type='submit' class='btn btn-default btn-xs'>Exportar a XLS</button></h3>
            </div>
		";
		*/

		

		$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
		if ($permisusu > 3)
		{
			$condicio_permis = " AND FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
		}

		if ($condicioapassosinici != "" || $condicioapassosfinal != "")
		{
			$condicio_passos = " INNER JOIN pfx_passos ps ON ps.clau_tasca = t.id  ";
			if ($condicioapassosinici != "") $condicio_passos .= " AND datai >= '$condicioapassosinici' ";
			if ($condicioapassosfinal != "") $condicio_passos .= " AND datai <= '$condicioapassosfinal' ";
		}

		$querytasques = "SELECT distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, t.*,
				  t.ordre as ordre, t.codi as codi, IFNULL(pa.text,'----') as estattasca,
				  p.titol_ca as nomdelprojecte, a.titol_ca as nomactuacio, u.nom as nomresp, u.cognoms as cognomsresp, p.id as idprojecte
				  , o.titol_ca as nomagent, t.descripcio_ca as descripcio
				  FROM pfx_tasques t
				  LEFT JOIN pfx_actuacions a ON t.clau_actuacio = a.id
				  LEFT JOIN pfx_projectes p ON a.clau_projecte = p.id
				  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estattasca' AND pa.idioma = '$idioma'
				  LEFT JOIN pfx_usuaris u ON t.responsable = u.id
				  LEFT JOIN pfx_projectes_agents o ON o.id = t.agents
				  $condicio_passos
				  WHERE 1=1 $condicio_permis $condicioatasques
				  GROUP BY id
				  ORDER by nom desc
				";
		$camps = array(
			'1' => '$rs["codi"]',
			'2' => '$rs["nom"]',
			'3' => '$rs["nomactuacio"]',
			'4' => '$rs["nomdelprojecte"]',
			'5' => '$rs["estattasca"]',
			'6' => '$rs["cognomsresp"].", ".$rs["nomresp"]',
		);
		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom de la tasca'),
			'3' => $app['translator']->trans('Nom de la proposta'),
			'4' => $app['translator']->trans('Nom del projecte'),
			'5' => $app['translator']->trans('Estat de la tasca'),
			'6' => $app['translator']->trans('Responsable de la tasca'),
		);
		$Dades = $dbb->GenerarLlistats("tasques", $querytasques, $camps, $capcaleres,array('mostarestat' =>'no', 'mostrareliminar' => 'no', 'paginaconsulta' => 'si'));


	}


	
	if (isset($_POST['export']) )
	{
		
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		ini_set('output_buffering','on');
		//ini_set('zlib.output_compression', 0);

		// Clases Php.
		require_once(__DIR__."/../include/class/excel/Classes/PHPExcel.php");

		// Nom fitxer.
		$nomfile = 'Projectes_'.date("Y-m-d_H-i-s").'.xlsx';
		$rutafile = __DIR__."/../../secretfiles/exports/".$nomfile;

		// Inicialitza Excel.
	    $objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 1;
		$colCount = 0;

		// Generar XLS Projectes.
		if ( isset($queryprojectes))
		{
		

			// Capçaleres.
			$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFill()
			->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			->getStartColor()->setRGB('CCFFCC');

			$BStyle = array(
			  'borders' => array(
			    'outline' => array(
			      'style' => PHPExcel_Style_Border::BORDER_MEDIUM 
			    )
			  )
			);

			$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->applyFromArray($BStyle);

			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setWidth(45);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Codi - Projecte1");
			$colCount++;

			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Estat del projecte");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data inici");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data fi prevista");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Pressupost total");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Vinculació estratégica");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Tipologia del projecte");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Origen");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Dimensions");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Impacte territorial");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Breu descripcio");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Objectius");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Factos de risc");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Indicadors del projecte");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Participació i seguiment");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Òrgans de participació i seguiment ad hoc");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Novetats genèriques");
			$colCount++;
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($colCount)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Equip de treball (intern)");
			$colCount++;


			$colCount = 0;
			$rowCount = 2;

			$DadesProjectes =  $dbb->FreeSql("$queryprojectes",array());

			if (!empty($DadesProjectes))
			{
				foreach ($DadesProjectes as $key => $value) 
				{
					$colCount = 0;

					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colCount, $rowCount)->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setRGB('FFFF00');

					$BStyle = array(
					  'borders' => array(
					    'top' => array(
					      'style' => PHPExcel_Style_Border::BORDER_MEDIUM 
					    )
					  )
					);

					$objPHPExcel->getActiveSheet()->getStyle("A$rowCount:R$rowCount")->applyFromArray($BStyle);

				    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[codi]." ".$value[nom]));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[estatprojecte]));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[inici]));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value["final"]));
					$colCount++;
					$txtexcel = "";
					if (!empty($value[pressupostos]))
					{
						$value[pressupostos] = json_decode($value[pressupostos], true);
						foreach ($value[pressupostos] as $key2 => $value2) 
						{
							$txtexcel .= $key2.":   ".$value2." €    \n";
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$txtexcel = "";
					if (!empty($value[vinculacio]))
					{
						$value[vinculacio] = array_unique(json_decode($value[vinculacio], true));
						foreach ($value[vinculacio] as $key2 => $value2) 
						{
							$Vinculacio = $dbb->Llistats("projectes_vinculacio"," AND t.id = '".$value2."' ",array(), "titol_ca");

							if (!empty($Vinculacio))
							{
								$txtexcel .= $Vinculacio[1][titol]."    \n";
							}
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[nomtipologia]));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[nomorigen]." ".($value[origens] == 1?$value[origen_text]:"") ));
					$colCount++;
					$txtexcel = "";
					if (!empty($value[dimensions]))
					{
						$value[dimensions] = array_unique(json_decode($value[dimensions], true));
						foreach ($value[dimensions] as $key2 => $value2) 
						{
							$Dimensions = $dbb->Llistats("projectes_dimensions"," AND t.id = '".$value2."' ",array(), "titol_ca");

							if (!empty($Dimensions))
							{
								$txtexcel .= $Dimensions[1][titol]."    \n";
							}
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$txtexcel = "";
					if (!empty($value[districtes]))
					{
						$value[districtes] = array_unique(json_decode($value[districtes], true));
						foreach ($value[districtes] as $key2 => $value2) 
						{
							$Districtes = $dbb->Llistats("projectes_districtes"," AND t.id = '".$value2."' ",array(), "titol_ca");

							if (!empty($Districtes))
							{
								$txtexcel .= $Districtes[1][titol]."    \n";
							}
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value["descripcio_ca"]));
					$colCount++;
					$Districtes = $dbb->Llistats("projectes_objectius"," AND t.clau_projecte = '".$value[id]."' ",array(), "titol_ca");
					$txtexcel = "";
					if (!empty($Districtes))
					{	
						foreach ($Districtes as $key2 => $value2) 
						{
							$txtexcel .= $value2[descripcio_ca]."    \n";
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value["risc"]));
					$colCount++;
					$Indicadors = $dbb->Llistats("projectes_indicadors"," AND t.clau_projecte = '".$value[id]."' ",array(), "titol_ca", false);		
					$txtexcel = "";
					if (!empty($Indicadors))
					{
						foreach ($Indicadors as $key2 => $value2) 
						{
							$txtexcel .= "Descripció:   ".$value2["descripcio_ca"]."     \n 
										  Inicial:  ".$valu2e["inicial"]."       \n
										  Actual: ".$value2["actual"]."    \n
										  Objectiu :  ".$value2["objectiu"]."    \n\n
										";
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$txtexcel = "";
					if (!empty($value[organs]))
					{
						$value[organs] = array_unique(json_decode($value[organs], true));

						foreach ($value[organs] as $key2 => $value2) 
						{
							$Origens = $dbb->Llistats("projectes_organs_e"," AND t.id = '".$value2."' ",array(), "titol_ca");
							
							if (!empty($Origens))
							{
								$txtexcel .= $Origens[1][titol]."    \n";
							}
						}
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
						$colCount++;
					}
					$Organs = $dbb->Llistats("projectes_organs"," AND t.clau_projecte = '".$value[id]."' ",array(), "titol_ca");
					$txtexcel = "";
					if (!empty($Organs))
					{
						foreach ($Organs as $key2 => $value2) 
						{
							$txtexcel .= "Identificació i participants : ".$value2["identipart"]."     \n 
										  Data: ".$value2["dataop"]."     \n 
										  Observacions: ".$value2["observacionsop"]."     \n\n
										  ";

						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value["novetats"]));
					$colCount++;		
					$txtexcel = "";
					if (!empty($value[usuaris]))
					{
						$value[usuaris] = array_unique(json_decode($value[usuaris], true));
						foreach ($value[usuaris] as $key2 => $value2) 
						{
							$Usuaris = $dbb->Llistats("usuaris"," AND t.id = '".$value2."' ",array(), "nom");
							
							if (!empty($Usuaris))
							{
								$txtexcel .= $Usuaris[1][nom]." ".$Usuaris[1][cognoms]."    \n ";
							}
						}
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
					$colCount++;


					$colCount = 0;
					$rowCount++;

						// Generar XLS Actuacions.
						if ( isset($queryactuacions))
						{


							$queryactuacionsnou = str_replace("GROUP BY", " AND t.clau_projecte = ".$value["id"]." GROUP BY ", $queryactuacions);

							$DadesProjectesActuacions =  $dbb->FreeSql("$queryactuacionsnou",array());

							if (!empty($DadesProjectesActuacions))
							{
								
								//$rowCount = 1;
								$rowCount++;
								$colCount = 1;

								// Capçaleres.
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Codi - Proposta");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Estat de la proposta");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data inici");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data fi prevista");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Responsable de la proposta");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Projecte vinculat");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Vinculació indirecta");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Pressupost total");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Breu descripcio");
								$colCount++;
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Novetats genèriques");
								$colCount++;




								$colCount = 1;
								//$rowCount = 2;
								$rowCount++;

								foreach ($DadesProjectesActuacions as $keyact => $valueact) 
								{
									
									$colCount = 1;

								    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[codi]." ".$valueact[nom]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[estatprojecte]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[inici]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact["final"]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[nomresp]." ".$valueact[cognomsresp]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[nomdelprojecte]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[nomindirecte]));
									$colCount++;
									$txtexcel = "";
									if (!empty($valueact[pressupostos]))
									{
										$valueact[pressupostos] = array_unique(json_decode($valueact[pressupostos], true));
										foreach ($valueact[pressupostos] as $key2 => $value2) 
										{
											$txtexcel .= $key2.":   ".$value2." €    \n";
										}
									}
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact["descripcio_ca"]));
									$colCount++;
									$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valueact[novetats]));
									$colCount++;



									$colCount = 1;
									$rowCount++;


										// Generar XLS Tasques.
										if ( isset($querytasques))
										{
										
											$querytasquesnou = str_replace("GROUP BY", " AND t.clau_actuacio = ".$valueact["id"]." GROUP BY ", $querytasques);

											$DadesProjectesTasques = $dbb->FreeSql("$querytasquesnou",array());

											if (!empty($DadesProjectesTasques))
											{
												
												//$rowCount = 1;
												$rowCount++;
												$colCount = 2;

												// Capçaleres.
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Codi - Acció");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Descripció");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Cost econòmic");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Estat de l'acció");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data inici");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data fi prevista");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Responsable de l'acció");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Projecte i actuació vinculat");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Òrgans executius de l'Ajuntament");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Agents externs a foment i rols");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Passos realitzats");
												$colCount++;
												$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Situació actual i propers passos");
												$colCount++;
											

											

												$colCount = 2;
												//$rowCount = 2;
												$rowCount++;

												foreach ($DadesProjectesTasques as $keytasc => $valuetasc) 
												{
													
													$colCount = 2;

												    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[codi]." ".$valuetasc[nom]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[descripcio]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[costeconomic]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[estattasca]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[inici]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc["final"]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[nomresp]." ".$valuetasc[cognomsresp]));
													$colCount++;
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[nomdelprojecte]." - ".$valuetasc[nomactuacio]));
													$colCount++;
													$txtexcel = "";
													if (!empty($valuetasc[naturalesa]))
													{
														$valuetasc[naturalesa] = array_unique(json_decode($valuetasc[naturalesa], true));
														foreach ($valuetasc[naturalesa] as $key3 => $value3) 
														{
															$Naturalesa = $dbb->Llistats("paraules"," AND t.clau = '".$value3."' AND t.codi = 'naturalesatasca' AND t.idioma='ca' ",array(), "text");
															
															if (!empty($Naturalesa))
															{
																$txtexcel .= $Naturalesa[1][text]."    \n";
																if ($value3 == 4)
																{
																	$txtexcel .= neteja_e($valuetasc[naturalesa_text])."    \n";
																}
															}
														}
													}
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
													$colCount++;
													//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($valuetasc[nomagent]));
													//$colCount++;
													$txtexcel = "";
													if (!empty($valuetasc[agents]))
													{
														if (!is_array(json_decode($valuetasc[agents], true))) $valuetasc[agents] = "[\"".$valuetasc[agents]."\"]"; // Es va fer un canvi de string a array, per prevenir que doni error.
														$valuetasc[agents] = array_unique(json_decode(str_replace('""', '"', $valuetasc[agents]), true));

														foreach ($valuetasc[agents] as $key3 => $value3) 
														{
															$Agents = $dbb->Llistats("projectes_agents"," AND t.id = '".$value3."' ",array(), "titol_ca");
															
															if (!empty($Agents))
															{
																$txtexcel .= $Agents[1][titol_ca]."    \n";
															}
														}

													}
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
													$colCount++;
													if ($condicioapassosinici != "") $condicio_passos1 = " AND datai >= '$condicioapassosinici' ";
													if ($condicioapassosfinal != "") $condicio_passos2 = " AND datai <= '$condicioapassosfinal' ";
													$Passos2 = $dbb->Llistats("passos"," $condicio_passos1 $condicio_passos2 AND estat = '2' AND t.clau_tasca = '".$valuetasc[id]."' ",array(), "titol_ca", false);
													$txtexcel = "";
													if (!empty($Passos2))
													{
														foreach ($Passos2 as $key2 => $value2) 
														{
															$txtexcel .= "Data:  ".$value2["datai"]."    \n
																		  Acció:  ".$value2["descripcio"]."    \n
																			";

														}
													}
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
													$colCount++;
													$Passos1 = $dbb->Llistats("passos"," AND estat = '1' AND t.clau_tasca = '".$valuetasc[id]."' ",array(), "titol_ca", false);
													$txtexcel = "";
													if (!empty($Passos1))
													{
														
														foreach ($Passos1 as $key3 => $value3) 
														{
															$txtexcel .= "Data:  ".$value3["datai"]."    \n
																		  Acció:  ".$value3["descripcio"]."    \n
																			";

														}
													}
													$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtexcel));
													$colCount++;
																		




													$colCount = 2;
													$rowCount++;

												}
												$rowCount++;


											}

										}


								}
								$rowCount++;

								
							}

						}


				}

				
			}

		}

	


		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($rutafile);

		
		$ranid = base64_encode($nomfile);
		$tocken = md5('TockenSecretViewDBB!'.$ranid);
		

		$ranid = base64_encode($nomfile);
		$tocken = md5('TockenSecretViewDBB!'.$nomfile);


		echo '
						<div class="col-lg-2">
							<div class="form-group">
							<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&e=1" target="_blank">
								<button class="btn btn-success" type="button" >
									<i class="fa fa-download"></i> DESCARREGAR
								</button>
							</a>
						</div>
					</div>
		';


	}

	exit();
