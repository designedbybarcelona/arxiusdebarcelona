<?php
	
	if (isset($_POST['ctf'])){
		$iddesti = filter_var(trim($_POST['ctf']), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($_POST['accio'])){
		$idorigen = filter_var(trim($_POST['accio']), FILTER_SANITIZE_NUMBER_INT);
	}

	$idok = false;
	if (isset($_POST['ctf']) && isset($_POST['ctfx'])){
		if(trim($_POST['ctfx']) == md5(constant('General::tockenid').trim($_POST['ctf']))){
			$idok = true;
		}
	}

	if ($idok == false) { echo "<script>alert('Error al guardar!');</script>";exit();}

	$RegistreTascaOrigen = $dbb->Llistats("tasques"," AND t.id = :id ",array("id"=>$idorigen), "titol_ca", false);

	$RegistreTascaDesti = $dbb->Llistats("tasques"," AND t.id = :id ",array("id"=>$iddesti), "titol_ca", false);

	$nouoperadors = $RegistreTascaOrigen[1][operadors];

	if (!empty($RegistreTascaOrigen[1][operadors]) && !empty($RegistreTascaDesti[1][operadors])){

		$operadorsDesti = array_unique(json_decode($RegistreTascaDesti[1][operadors]));
		$operadorsOrigen = array_unique(json_decode($RegistreTascaOrigen[1][operadors]));

		$nouoperadors = json_encode(array_merge($operadorsDesti,$operadorsOrigen));
	}

	$nouorgans = $RegistreTascaOrigen[1][organs];

	if (!empty($RegistreTascaOrigen[1][organs]) && !empty($RegistreTascaDesti[1][organs])){

		$organsDesti = array_unique(json_decode($RegistreTascaDesti[1][organs]));
		$organsOrigen = array_unique(json_decode($RegistreTascaOrigen[1][organs]));

		$nouorgans = json_encode(array_merge($organsDesti,$organsOrigen));
	}


	if (!empty($RegistreTascaOrigen[1][operadors])){

		$tipus = "m";
		
		$arrayguardarcamp = array('taula' =>  "tasques", 
									'id' => $iddesti,
			'dades' =>  array(	
								'operadors' => $nouoperadors,
								'organs' => $nouorgans, 

							 ),'tipus' =>  $tipus,);


		$dbb->GuardarRegistre2($arrayguardarcamp); 
	
		echo json_encode(array("1","1"));
	}

	exit();