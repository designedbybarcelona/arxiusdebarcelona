<?php
	
	$idpagina = 6;

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	if (isset($config['ctf'])){
		$id_projecte = filter_var(trim($config['ctf']), FILTER_SANITIZE_NUMBER_INT);
	}

	$RegistreProjecte = $dbb->Llistats("projectes"," AND t.id = :id ",array("id"=>$id_projecte), "titol_ca", false);

	$vinculacio = $RegistreProjecte[1][vinculacio];

	$idsactuacions = array();


	if (!empty($vinculacio)){
		$vinculacio = json_decode($vinculacio,true);

		if (!empty($vinculacio)){
			foreach ($vinculacio as $key => $value) {
				$value = substr(strstr($value, '_'), strlen('_'));
				// Extraíem les ACTUACIONS i guardem en array per buscar projectes relacionats.
				$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND t.id = :id AND nivell = 4",array("id"=>$value), "id", false);
				if (!empty($RegistreVinculacio) && !in_array($value, $idsactuacions)){
					array_push($idsactuacions, $value);
				}

			}

			// Buscar tots els projectes que apunten a alguna de les actuacions.
			$condicioactuacions = "";
			if (!empty($idsactuacions)){
				foreach ($idsactuacions as $key_a => $value_a) {
					if (!empty($condicioactuacions)) $condicioactuacions .= " OR ";
					$condicioactuacions .= " FIND_IN_SET('$value_a' ,REPLACE( REPLACE( REPLACE( REPLACE( t.vinculacio,'[', ''),']' ,'') ,'\"',''),'_' ,',')) > 0  ";
				}
				if (!empty($condicioactuacions)) $condicioactuacions = " AND ($condicioactuacions)";


				$ProjectesRelacionats = $dbb->Llistats("projectes"," AND t.id <> :id $condicioactuacions ",array("id"=>$id_projecte), "titol_ca", false);


				$htmlrelacionats = "<ul>";
				if (!empty($ProjectesRelacionats)){
					foreach ($ProjectesRelacionats as $key_p => $value_p) {
						$htmlrelacionats .= '<li><b>'.$value_p[pesrelatiu].' % relatiu del projecte</b>: '.$value_p[titol].'</li>';
					}
				}
				$htmlrelacionats .= "</ul>";
			}
		}
	}

	echo $htmlrelacionats;

	exit();