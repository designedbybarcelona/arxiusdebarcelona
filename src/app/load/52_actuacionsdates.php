<?php 
	
	$RegistreTasques = $dbb->Llistats("tasques"," AND t.clau_actuacio = :id ",array("id"=>$id), "id", false);	

	if (!empty($RegistreTasques)){

		$inici = $config['d1'];
		$final = $config['d2'];

		$myregex = '~^\d{2}/\d{2}/\d{4}$~';
		if (!DateTime::createFromFormat('d/m/Y', $inici) || !DateTime::createFromFormat('d/m/Y', $final) || empty($inici) || empty($final))
		{
			//echo "Format de data incorrecte.";exit();
		}

		$inici= date_create_from_format("d/m/Y",$inici); 
		$inicitime = strtotime(date_format($inici, 'Y-m-d'));
	    $inici= date_format($inici, 'Y');
	    $final= date_create_from_format("d/m/Y",$final); 
	    $finaltime = strtotime(date_format($final, 'Y-m-d'));

	    $dataanterior = false;
	    $datafutura = false;

	    foreach ($RegistreTasques as $key => $value) {
	    		
	    	$inicitasca = strtotime($value['inici']);
	    	$finaltasca = strtotime($value['final']);

	    	if ($inicitasca < $inicitime){
	    		$dataanterior = true;
	    	}

	    	if ($finaltasca > $finaltime){
	    		$datafutura = true;
	    	}

	    }

	    if ($dataanterior){
	    	echo '
	    		<div class="alert alert-dismissable alert-danger">Atenció! Existeixen Accions amb una data anterior a la inicial, aquestes seran actualizades automàticament quan guardis els canvis.</div>
	    	';
	    }

	    if ($datafutura){
	    	echo '
	    		<div class="alert alert-dismissable alert-danger">Atenció! Existeixen Accions amb una data mes recent a la final, aquestes seran actualizades automàticament quan guardis els canvis.</div>
	    	';
	    }


	}
	
	

   
	exit();