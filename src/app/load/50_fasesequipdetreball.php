<?php
	
	$RegistreActuacio = $dbb->Llistats("actuacions"," AND t.id = :id ",array("id"=>intval($config['a'])), "id", false);	

	if (!empty($RegistreActuacio)){
		$usuaris_acuacio = $RegistreActuacio[1][usuaris];
		$operadors_projecte = $RegistreActuacio[1][operadors];

		if (!empty($usuaris_acuacio)){
			$arrayusuaris = array_unique(json_decode($usuaris_acuacio));
			
			$primer = false;
			$lastElement = end($arrayusuaris);
			foreach ($arrayusuaris as $key => $value) 
			{	
				if ($primer == false) { $condiciousuaris .= " AND ( "; $primer = true; }
				$condiciousuaris .= " u.id = '".intval($value)."' ";
				($value == $lastElement) ?  $condiciousuaris .= " ) " : $condiciousuaris .= " OR "; 
			}

		}else{
			if (!empty($cap_projecte)) {
				$condiciousuaris = " AND (u.id = '$cap_projecte')";
			}else{
				$condiciousuaris = " AND 1=2";
			}
		}

		if (!empty($operadors_projecte)){
			$arrayusuaris = array_unique(json_decode($operadors_projecte));
			$primer = false;
			$lastElement = end($arrayusuaris);
			foreach ($arrayusuaris as $key => $value) 
			{	
				if ($primer == false) { $condiciooperadors .= " AND ( "; $primer = true; }
				$condiciooperadors .= " u.id = '".intval($value)."' ";
				($value == $lastElement) ?  $condiciooperadors .= " ) " : $condiciooperadors .= " OR "; 
			}

		}else{
			$condiciooperadors = " AND 1=2";
		}

	}else{
		$condiciousuaris = " AND 1=2";
		$condiciooperadors = " AND 1=2";
	}


	$RegistreTasca = $dbb->Llistats("tasques"," AND t.id = :id ",array("id"=>$id), "id", false);	
	$arrayusuaristasc = array();
	$arrayoperadorstasc = array();
	if (!empty($RegistreTasca[1][usuaris])){
		$arrayusuaristasc = json_decode($RegistreTasca[1][usuaris]);
		$arrayoperadorstasc = json_decode($RegistreTasca[1][operadors]);
	}


	$Usuaris = $dbb->FreeSql(" SELECT u.nom, u.cognoms, u.id
								FROM pfx_usuaris u
								WHERE 1=1 $condiciousuaris
							  ",array());


	$Operadors = $dbb->FreeSql(" SELECT u.titol_ca as titol, u.id
								FROM pfx_projectes_operadors u
								WHERE 1=1 $condiciooperadors
							  ",array());

	


	$Dades = '

		<div class="table-responsive"  style="overflow: hidden !important;">
	    <table class="table table-striped table-bordered table-hover dataTables dataTablesequip">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>Equip de projecte intern</th>
	            </tr>
	        </thead>   
	        <tbody>';

	        	if (!empty($Usuaris))
	        	foreach ($Usuaris as $key => $value) {
	        		
	        		$Dades .= '
	        			<tr class="odd">
		                    <td>
		                        <input name="usuaris[]" value="'.$value['id'].'" type="checkbox" '.(in_array($value['id'], (array)$arrayusuaristasc)?'checked="true"' : '' ).'/>
		                    </td>
		                    <td>
		                        '.$value['cognoms'].', '.$value['nom'].'
		                    </td>
		                </tr> 
	        		';

	        	}
	             
	              
	$Dades .= '</tbody>
	    </table>
	    </div>
	';

	$Dades .= '

		<div class="table-responsive"  style="overflow: hidden !important;">
	    <table class="table table-striped table-bordered table-hover dataTables dataTablesequip">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>Equip de projecte extern</th>
	            </tr>
	        </thead>   
	        <tbody>';
	        	 if (!empty($Operadors))
	        	foreach ($Operadors as $key => $value) {
	        		
	        		$Dades .= '
	        			<tr class="odd">
		                    <td>
		                        <input name="operadors[]" value="'.$value['id'].'" type="checkbox" '.(in_array($value['id'], (array)$arrayoperadorstasc)?'checked="true"' : '' ).'/>
		                    </td>
		                    <td>
		                        '.$value['titol'].'
		                    </td>
		                </tr> 
	        		';

	        	}
	             
	              
	$Dades .= '</tbody>
	    </table>
	    </div>
	';

	

	echo $Dades;