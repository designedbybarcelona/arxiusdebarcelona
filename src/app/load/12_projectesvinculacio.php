<?php

	$opciotriada = intval($config['opcio']);
	$idf = intval($config['idf']);

	$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND nivell = '".($opciotriada-1)."' ",array(),"t.id", false);
	$parent_id = intval($id);

	if (isset($idf)){
		$RegistreTriatVinculacio = $dbb->Llistats("projectes_vinculacio"," AND id = :id ",array("id" => $idf),"t.id", false);
		if (!empty($RegistreTriatVinculacio[1][parents_ids])){
			$parents_ids = json_decode($RegistreTriatVinculacio[1][parents_ids],true);
		}
		if (!empty($RegistreTriatVinculacio[1][clau_pam])){
			$parent_pam = $RegistreTriatVinculacio[1][clau_pam];
		}
		if (!empty($RegistreTriatVinculacio[1][clau_eix])){
			$parent_eix = $RegistreTriatVinculacio[1][clau_eix];
		}
	}
	
	if ($opciotriada == 4){
		$RegistreVinculacioPAM = $dbb->Llistats("projectes_vinculacio"," AND nivell = '1' ",array(),"t.id", false);
	}

	if ($opciotriada == 10){
		$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND nivell = '2' AND FIND_IN_SET(:id,REPLACE( REPLACE( REPLACE( t.parents_ids,'[', ''),']' ,'') ,'\"','')) > 0 ",array('id'=>$id),"t.id", false);
	}

	if ($opciotriada == 11){
		$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND nivell = '3' AND t.parent_id = :id ",array('id'=>$id),"t.id", false);
		$parent_id = $RegistreTriatVinculacio[1][parent_id];
	}

	
	
	$dadesplantilla = array (
		'Paraules' => $Paraules,
		'RegistreVinculacio' => $RegistreVinculacio,
		'RegistreVinculacioPAM' => $RegistreVinculacioPAM,
		'parent_id' => $parent_id,
		'parents_ids' => $parents_ids,
		'o' => $opciotriada,
		'idf' => $idf,
		'parent_pam' => $parent_pam,
		'parent_eix' => $parent_eix,
	);

	echo $app['twig']->render('projectes-vinculacioform.html', $dadesplantilla);

	exit();