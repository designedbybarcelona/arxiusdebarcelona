<?php
	
	$dades = array();

		
		
	$dades["id"] = $id;
	
	if (isset($_POST['tipus'])){
		$tipus = filter_var(htmlspecialchars(trim($_POST['tipus'])), FILTER_SANITIZE_NUMBER_INT);
		$dades["tipus"] = $tipus;
	}
	if (isset($_POST['subtipus'])){
		$subtipus = filter_var(htmlspecialchars(trim($_POST['subtipus'])), FILTER_SANITIZE_NUMBER_INT);
		$dades["subtipus"] = $subtipus;
	}
	if (isset($_POST['textmail'])){
		$textmail = (trim($_POST['textmail']));
		$dades["textmail"] = $textmail;
	}
	if (isset($_POST['convocats_email'])){
		if (!empty($_POST['convocats_email']))
		{
			$convocats_email = array_unique(stripslashes_deep($_POST['convocats_email']));
			$dades["convocats_email"] = json_encode($convocats_email);
		}
	}
	if (isset($_POST['emails'])){
		if (!empty($_POST['emails']))
		{
			$emails = array_unique(stripslashes_deep(json_decode($_POST['emails'])));
			$dades["emails"] = json_encode($emails);
		}
		
	}


	$dbb->EnviarMail($dades);

	exit();
