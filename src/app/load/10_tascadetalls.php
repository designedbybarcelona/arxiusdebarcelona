<?php
	
	$Actuacions = $dbb->Llistats("actuacions"," AND clau_projecte = :id",array("id"=>$id),"t.id", false);

	if (!empty($p))
	{
		$RegistreTasca = $dbb->Llistats("tasques"," AND t.id = :id",array("id"=>$p), "titol_ca", false);
	}
	
	$dadesplantilla = array (
		'Paraules' => $Paraules,
		'Actuacions' => $Actuacions,
		'RegistreTasca' => $RegistreTasca,
		'ida' => intval($config['a']),
	);

	echo $app['twig']->render('tasques-detallsform.html', $dadesplantilla);

	exit();