<?php

	$parent_id = intval($id);
	$nivell = intval($config['n']);

	$RegistreProjectesOrge = $dbb->Llistats("projectes_organs_e"," AND nivell = :nivell ",array('nivell' => $nivell-1),"t.id", false);
	
	$opcio = 2;
	if ($nivell == 1){
		$Vinculacio = $dbb->Llistats("projectes_vinculacio"," ", array(), "1*SUBSTRING_INDEX(titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(titol_ca, '.', -1) ASC",false);
		$RegistreProjectesOrge = $dbb->Llistats("projectes_organs_e"," AND id = :id ",array('id' => $parent_id),"t.id", false);
		$opcio = 1;
	}


	
	$dadesplantilla = array (
		'Paraules' => $Paraules,
		'RegistreProjectesOrge' => $RegistreProjectesOrge,
		'parent_id' => $parent_id,
		'Vinculacio' => $Vinculacio,
		'o' => $opcio,
	);

	echo $app['twig']->render('projectes-organseform.html', $dadesplantilla);

	exit();