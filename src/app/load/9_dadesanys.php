<?php
	
	if ($taula == 1)
	{
		$t = "projectes";
		$plantilla = "projectes-dadesanys.html";
	}
	if ($taula == 2)
	{
		$t = "actuacions";
		$plantilla = "actuacions-dadesanys.html";
	}
	if ($taula == 3)
	{
		$t = "tasques";
		$plantilla = "tasques-dadesanys.html";
	}
	if ($id != 0)
	{
		$RegistreDadesAnys = $dbb->Llistats("$t"," AND t.id = :id",array("id"=>$id),"t.id", false);
		//if (!empty($RegistreDadesAnys[1][pressupostos]))
		//{
			$RegistreDadesAnys[1][pressupostos] = json_decode($RegistreDadesAnys[1][pressupostos], true);
			if ($taula == 1){
				//$RegistreDadesAnys[1][pressupostos1] = json_decode($RegistreDadesAnys[1][pressupostos1], true);
				//$RegistreDadesAnys[1][pressupostos2] = json_decode($RegistreDadesAnys[1][pressupostos2], true);
				//$RegistreDadesAnys[1][executats] = json_decode($RegistreDadesAnys[1][executats], true);
				//$RegistreDadesAnys[1][executats1] = json_decode($RegistreDadesAnys[1][executats1], true);
				//$RegistreDadesAnys[1][executats2] = json_decode($RegistreDadesAnys[1][executats2], true);
				//$RegistreDadesAnys[1][totals] = json_decode($RegistreDadesAnys[1][totals], true);
				//$RegistreDadesAnys[1][totals2] = json_decode($RegistreDadesAnys[1][totals2], true);

				// Totals.
				$RegistreConceptes = $dbb->Llistats("projectes_conceptes"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$id), "titol_ca", false);
				if (!empty($RegistreConceptes)){
					foreach ($RegistreConceptes as $key_co => $value_co) {

						$RegistreDadesAnys[1][total][$value_co[any]] += $value_co[pressupost1];
						$RegistreDadesAnys[1][total][$value_co[any]] += $value_co[pressupost2];
						$RegistreDadesAnys[1][total][$value_co[any]] += $value_co[pressupost3];
						
					}


				}
			}

			if ($taula == 2)
			{
				$RegistreDadesAnysProjecte = $dbb->Llistats("projectes"," AND t.id = :id",array("id"=>$RegistreDadesAnys[1][clau_projecte]),"t.id", false);
				if (!empty($RegistreDadesAnysProjecte[1][pressupostos]))
				{
					$RegistreDadesAnysProjecte[1][pressupostos] = json_decode($RegistreDadesAnysProjecte[1][pressupostos], true);

					// Per el pressupost de cada any, restar el que ja está assignat en altres actuacions.
					$ActuacionsProjecte = $dbb->Llistats("actuacions"," AND t.clau_projecte = :clau_projecte",array("clau_projecte"=>$RegistreDadesAnys[1][clau_projecte]),"t.clau_projecte", false);
					if (!empty($ActuacionsProjecte))
					{
						foreach ($ActuacionsProjecte as $keya => $valuea) {
							$pressupostosa = json_decode($valuea[pressupostos], true);

							if (!empty($pressupostosa))
							{
								foreach ($pressupostosa as $keyp => $valuep) {
									$RegistreDadesAnysProjecte[1][pressupostos][$keyp] = $RegistreDadesAnysProjecte[1][pressupostos][$keyp] - $valuep;
								}
							}
						}
					}

					
				}
			}

			if ($taula == 3)
			{
				$RegistreDadesAnysActuacio = $dbb->Llistats("actuacions"," AND t.id = :id",array("id"=>$RegistreDadesAnys[1][clau_actuacio]),"t.id", false);

				if (!empty($RegistreDadesAnysActuacio[1][pressupostos]))
				{
					$RegistreDadesAnysActuacio[1][pressupostos] = json_decode($RegistreDadesAnysActuacio[1][pressupostos], true);

					// Per el pressupost de cada any, restar el que ja está assignat en altres actuacions.
					$TasquesActuacio = $dbb->Llistats("tasques"," AND t.clau_actuacio = :clau_actuacio",array("clau_actuacio"=>$RegistreDadesAnys[1][clau_actuacio]),"t.clau_actuacio", false);
					if (!empty($TasquesActuacio))
					{
						foreach ($TasquesActuacio as $keya => $valuea) {
							$pressupostosa = json_decode($valuea[pressupostos], true);

							if (!empty($pressupostosa))
							{
								foreach ($pressupostosa as $keyp => $valuep) {
									$RegistreDadesAnysActuacio[1][pressupostos][$keyp] = $RegistreDadesAnysActuacio[1][pressupostos][$keyp] - $valuep;
								}
							}
						}
					}

					
				}
			}

		//}

	}
	

	// Verificar el format de les dates.
	$inici = $config['d1'];
	$final = $config['d2'];

	$myregex = '~^\d{2}/\d{2}/\d{4}$~';
	if (!DateTime::createFromFormat('d/m/Y', $inici) || !DateTime::createFromFormat('d/m/Y', $final) || empty($inici) || empty($final))
	{
		echo "Format de data incorrecte.";exit();
	}

	// Extreure any de la data.
	$inici= date_create_from_format("d/m/Y",$inici); 
	$inicitime = strtotime(date_format($inici, 'Y-m-d'));
    $inici= date_format($inici, 'Y');
    $final= date_create_from_format("d/m/Y",$final); 
    $finaltime = strtotime(date_format($final, 'Y-m-d'));
    $final= date_format($final, 'Y');

    // Verificar data final > inicial.
    if ($inicitime > $finaltime)
    {
    	echo "La data final ha de ser més gran que la inicial."; exit();
    }
    
    // Calcul anys.
    $anys = ($final - $inici) + 1;

    

	$dadesplantilla = array (
		'Paraules' => $Paraules,
		'RegistreDadesAnys' => $RegistreDadesAnys,
		'RegistreDadesAnysProjecte' => $RegistreDadesAnysProjecte,
		'RegistreDadesAnysActuacio' => $RegistreDadesAnysActuacio,
		'anys' => $anys,
		'inici' => $inici,
		'final' => $final,
	);

	echo $app['twig']->render($plantilla, $dadesplantilla);

	exit();