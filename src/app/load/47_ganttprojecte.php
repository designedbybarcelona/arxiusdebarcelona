<?php
	

	$db = new Db();


	$idprojecte = (int) $id;


	$DadesProjectes =  $dbb->FreeSql("SELECT p.titol_ca as titol, p.id as id, p.inici, p.final, p.vinculacio, p.tantpercent,
									   IFNULL(CONCAT(u.nom,' ',u.cognoms),'') as responsable, p.estat,
									  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip,
									  IFNULL(pa.text,'----') as estatprojecte
									  FROM pfx_projectes p
									  $leftoperadors
									  LEFT JOIN pfx_usuaris u ON u.id = p.cap_projecte
									  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
									  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'

									  WHERE 1=1 $condicio_permis AND p.id = '$idprojecte'
									  GROUP By p.id
									  ORDER BY p.titol_ca 
									  ", array());
	

	if (!empty($DadesProjectes) )
	{

		$Dades = '

			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
			<script src="https://export.dhtmlx.com/gantt/api.js"></script>  
			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/ext/dhtmlxgantt_tooltip.js"></script>
			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/ext/dhtmlxgantt_marker.js"></script>
	        <link rel="stylesheet" href="'.constant("DBB::url").'/js/consultes/gantt/codebase/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

	        <script type="text/javascript">

	        	$(document).ready(function($) {

	        		//gantt.config.xml_date="%d-%m-%Y";

	                gantt.config.columns = [
	                    {name:"text",       label:"Detall Fases i accions del projecte",  width:"*", tree:true, resize: true, align: "left" },
	                    //{name:"start_date", label:"Inici", align: "center" },
	                    //{name:"duration",   label:"Durada",   align: "center" },
	                    //{name:"add",        label:"",           width:0, hide:true  }
	                ];

	                 gantt.config.start_date = new Date(2013, 02, 31);

	                gantt.config.drag_move = false;
	                gantt.config.drag_lightbox = false;
	                gantt.config.drag_links = false;
	                gantt.config.drag_progress = false;
	                gantt.config.drag_resize = false;

	                gantt.config.readonly = true;

	                gantt.config.scale_unit = "year";
	                gantt.config.step = 1;
	                gantt.config.date_scale = "%Y";
	                gantt.config.min_column_width = 50;
	                gantt.config.date_scale = "%Y";

	                gantt.config.scale_height = 90;

                  	gantt.templates.tooltip_text = function(start,end,task){
					    //return "<b>Task:</b> "+task.text+"<br/><b>Duration:</b> " + task.duration;
                  		var formatFunc = gantt.date.date_to_str("%d/%m/%Y");
						var inici = formatFunc(new Date(task.start_date));
						var final = formatFunc(new Date(task.end_date));
						if (task.tipus == "fita"){
							return task.text+"<br>" + task.fites;
						}else{
							//var progress = task.progress*100;
							//var progress = progress.toFixed(2);
							retorn = task.text+"<br/>"+task.descripcio+"<br/>"+inici+" - "+final+"<br/>";
							if (task.resp!= ""){
								retorn += task.resp;
							}
							if (task.equip!= ""){
								retorn += "<br/>"+ task.equip;
							}
							if (task.estat!= ""){
								retorn += "<br/><b>Estat:</b> "+ task.estat +"<br/>";
							}
							return retorn;
						}
                  		
					};
	                  
	                  var monthScaleTemplate = function(date){
	                    var dateToStr = gantt.date.date_to_str("%M");
	                    var endDate = gantt.date.add(date, 2, "month");
	                    return dateToStr(date) + " - " + dateToStr(endDate);
	                  };

	                  gantt.config.subscales = [
	                    {unit:"month", step:3, template:monthScaleTemplate},
	                    {unit:"month", step:1, date:"%M" }
	                  ];

	                gantt.templates.task_text=function(start,end,task){
					    //return "<b>Text:</b> "+task.text+",<b> Holders:</b> "+task.users;
					    var formatFunc = gantt.date.date_to_str("%d/%m/%Y");

					    if (task.tipus == "fita"){
					    	var inici = formatFunc(new Date(task.start_date));
							return inici + " " +task.text;
						}else{
						    
						    var inici = formatFunc(new Date(task.start_date));
							var final = formatFunc(new Date(task.end_date));
						    return inici+" - "+final;
						}
					};

	                  
					$(".loadgantt").html("");

				

	            });

            </script>
            <style>
            
				.status_line{
					background-color: #0ca30a;  		
				}	
				.status_line .gantt_marker_content{  
				  	top: 35px;
				}
				.gantt_marker .gantt_marker_content {
				   -ms-transform: rotate(-45deg); /* IE 9 */
				    -webkit-transform: rotate(-45deg); /* Safari */
				    transform: rotate(-45deg);
				    z-index: 10;
				    margin: 25px 0px 0px -25px;    
				}  
				/*
				.gantt_task_progress {
					opacity: 0.6;
				}
				*/
            </style>
	    ';

	    echo $Dades;

	    $colorsgantt = array(
	    	"0" => "#e1e1e1",
	    	"1" => "#7bd148",
	    	"2" => "#e1e1e1",
	    	"3" => "#ff887c",
	    	"4" => "#e1e1e1",
	    	"5" => "#e1e1e1",
	    );
	    // per projectes.
	    $arraycolors = array( 
			"1" => "#7bd148", // verd.
			"2" => "#ffb878", // ambar.
			"3" => "#ff887c", // vermell.
			"4" => "#e1e1e1", // gris.
			"5" => "#5681de", // blau.
		);


 		$GanttProjecte = "";


 		if ( !empty($DadesProjectes[1][inici]) && !empty($DadesProjectes[1]["final"]) && $DadesProjectes[1][inici] != "0000-00-00" && $DadesProjectes[1]["final"] != "0000-00-00" )
		{	
			
			if ($DadesProjectes[1][estat] == 1) { $DadesProjectes[1][colorsemafor] = 2; } // Iniciat, Àmbar.
			if ($DadesProjectes[1][estat] == 2) { $DadesProjectes[1][colorsemafor] = 5; } // No iniciat, Blau.
			if ($DadesProjectes[1][estat] == 3) { $DadesProjectes[1][colorsemafor] = 3; } // Amb dificultats, Vermell.
			if ($DadesProjectes[1][estat] == 4) { $DadesProjectes[1][colorsemafor] = 1; } // Finalitzat, Verd.
			if ($DadesProjectes[1][estat] == 5) { $DadesProjectes[1][colorsemafor] = 4; } // Descartat, Gris.
			if ($DadesProjectes[1][estat] == ""){ $DadesProjectes[1][colorsemafor] = 4; } // Gris.



			$durada = strtotime($DadesProjectes[1]['final']) - strtotime($DadesProjectes[1]['inici']);
			$durada = abs($durada)/60/60/24;
			$Projecteg = array(
				
				'id' 		 => $DadesProjectes[1][id],//1, 
				'text' 		 => $DadesProjectes[1][titol], // str_replace(".", "", $value_p[codipam]).'.'.
				'start_date' => date("d-m-Y",strtotime($DadesProjectes[1]['inici'])),
				'duration'   => round(abs($durada)),
				'order' 	 => '10',
				'progress' 	 => $DadesProjectes[1]['tantpercent']/100,//'0.0',
				'open'       => true,
				'fites'		 => "",
				'tipus'      => "",
				'color'		 => $arraycolors[$DadesProjectes[1][colorsemafor]],
				'equip' 	 => str_replace("'", "`", $DadesProjectes[1][equip]) ,
				'resp' 		 => str_replace("'", "`", $DadesProjectes[1][responsable]) ,
				'descripcio' => "",
				'estat'		 => str_replace("'", "`", $DadesProjectes[1][estatprojecte]),
				//'progressColor' =>  "#000000",
				//'textColor'
				
			
			);

			if (!empty($GanttProjecte)){
				$GanttProjecte = $GanttProjecte.",".json_encode($Projecteg);
			}else{
				$GanttProjecte =  json_encode($Projecteg);
			}
			

					
				
					// Fites del projecte.
					$fitesproj = "";
					$FitesProjecte =  $dbb->FreeSql("SELECT f.observacionsfita as fita,f.datai as inici,   DATE_FORMAT(f.datai, '%d/%m/%Y') as datai, f.id as id,
						 							  IFNULL(pa.titol_ca,'----') as tipologiafita
													  FROM pfx_projectes_fites f
													  LEFT JOIN pfx_projectes_fites_tipologies pa ON f.tipologia = pa.id
													  WHERE f.clau_projecte = :id 
													  ORDER BY f.datai desc", array("id"=>$DadesProjectes[1][id]));

					if (!empty($FitesProjecte)){
						$fitesproj .= "<hr><b>FITES</b>";
						foreach ($FitesProjecte as $key_fp => $value_fp) {
							$fitesproj .= "<li><b>".$value_fp[datai]."</b>: ".nl2br(wordwrap($value_fp[fita],80,"<br>"))."</li>";

						
							$arrayfites = array(
												'id' 		 => "fi_".$value_fp[id],//$contador,
												'text' 		 => "Fita: ".nl2br(wordwrap($value_fp[fita],80,"<br>")), // $value_fp[datai]." - ".nl2br(wordwrap($value_fp[fita],80,"<br>"))
												'start_date' => date("d-m-Y",strtotime($value_fp[inici])),
												'parent'     => $DadesProjectes[1][id],
												'color'		 => "#7100AD",
												'duration'   => "1",
												'fites'      => "<li><b>".$value_fp[datai]."</b>: ".$value_fp[tipologiafita]."</li><li>".nl2br(wordwrap($value_fp[fita],80,"<br>"))."</li>",
												'tipus'      => "fita",
											);

							$GanttProjecte = $GanttProjecte.",".json_encode($arrayfites);
						}
					}
					
					

						$Actuacions = $dbb->FreeSql("SELECT a.id, a.inici, a.final, a.titol_ca as titol, a.descripcio_ca , a.estat,
												 GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as operadors,
												 IFNULL(pa.text,'----') as estatactuacio
											  FROM pfx_actuacions a
											  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
											  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( a.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
											  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( a.operadors,'[', ''),']' ,'') ,'\"','')) > 0
											  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
											  WHERE a.clau_projecte = '".$DadesProjectes[1]['id']."'
											  GROUP By a.id 
											  ORDER BY a.id asc",array());
											  
						$Tasques = $dbb->FreeSql("SELECT t.id, t.inici, t.final, t.titol_ca as titol, t.descripcio_ca, p.titol_ca as nomprojecte, t.clau_actuacio, t.estat, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as operadors, IFNULL(pa.text,'----') as estattasca
												  FROM pfx_tasques t
												  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
												  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
												  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( t.operadors,'[', ''),']' ,'') ,'\"','')) > 0
												  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
												  WHERE t.clau_projecte = '".$DadesProjectes[1]['id']."'
												  GROUP BY t.id
												  ORDER BY t.id asc, t.titol_ca ", array());
					

						if (!empty($Actuacions))
						{
							$contador = 2;
							foreach ($Actuacions as $key_a => $value_a) 
							{
								if ( !empty($value_a['inici']) && !empty($value_a['final']) && $value_a[inici] != "0000-00-00" && $value_a["final"] != "0000-00-00" )
								{
									$durada = strtotime($value_a['final']) - strtotime($value_a['inici']);
									$durada = abs($durada)/60/60/24;
									$actuacioprojecte =  array(	
														'id' 		 => "a_".$value_a[id],//$contador,
														'text' 		 => $value_a[titol],
														'start_date' => date("d-m-Y",strtotime($value_a[inici])),
														'duration'   => round(abs($durada)),
														'order' 	 => '10',
														'progress' 	 => '0.0',
														'parent'     => $DadesProjectes[1]['id'],
														'color'		 => $colorsgantt[$value_a[estat]],
														'fites'		 => "",
														'open'       => 'false',
														'tipus'      => "",
														'equip' 	 => "<b>Equip:</b><br>".str_replace("'", "`", $value_a[equip])."<br>".str_replace("'", "`", $value_a[operadors]) ,
														'resp'		 => "",
														'descripcio' => strip_tags($value_a[descripcio_ca]),
														'estat'		 => strip_tags($value_a[estatactuacio]),
													
													);
									$GanttProjecte = $GanttProjecte.",".json_encode($actuacioprojecte);
									$contador++;

									if (!empty($Tasques))
									{
										foreach ($Tasques as $key_ta => $value_ta) 
										{
											if ($value_ta[clau_actuacio] == $value_a[id])
											{
												$durada = strtotime($value_ta['final']) - strtotime($value_ta['inici']);
												$durada = abs($durada)/60/60/24;
												$tasquesprojecte =  array(	
																	'id' 		 => "t_".$value_ta[id],//$contador,
																	'text' 		 => $value_ta[titol],
																	'start_date' => date("d-m-Y",strtotime($value_ta[inici])),
																	'duration'   => abs($durada),
																	'order' 	 => '10',
																	'progress' 	 => '0.0',
																	'parent'     => "a_".$value_a[id],
																	'color'		 => $colorsgantt[$value_ta[estat]],
																	'fites'		 => "",
																	'open'       => 'false',
																	'tipus'      => "",
																	'equip' 	 => "<b>Equip:</b><br>".str_replace("'", "`", $value_ta[equip])."<br>".str_replace("'", "`", $value_ta[operadors]) ,
																	'resp'		 => "",
																	'descripcio' => strip_tags($value_ta[descripcio_ca]),
																	'estat'		 => strip_tags($value_ta[estattasca]),
																
																);
												$GanttProjecte = $GanttProjecte.",".json_encode($tasquesprojecte);
											}
										}
									}
									
								}
							}
							
						}

					}

				
				
		
				


		$GanttProjecte = "{data:[".$GanttProjecte."]};";
		//echo $GanttProjecte;
		//exit();

		//var_dump($jsfites);exit();

		
	    
		$Dades = '

			<div class="row">
				<div class="col-lg-1" style="float:right; margin-right: 95px;margin-bottom:5px;" >
	                  <button class="btn btn-xs btn-default exportpdf" ><i class="fa fa-download" aria-hidden="true"></i> Descarregar en PDF</button>
	            </div>
				<div class="col-lg-1" style="float:right; margin-right: 95px;margin-bottom:5px;" >
	                  <button class="btn btn-xs btn-default zoomfit"><i class="fa fa-search-minus" aria-hidden="true"></i> Mostrar tots els anys</button>
	            </div>
	        </div>



			<div class="row">

	            <div class="col-lg-12">
	            	

	                <div id="gantt_1" style="width:100%; height:600px;"></div>

					   

	                <script type="text/javascript">

	                	$(document).ready(function($) {

		                    var tasks_1 = '.$GanttProjecte.'

		                    '.$jsfites.'
							

		                    gantt.init("gantt_1");


		                    gantt.parse(tasks_1);


		                    // Zoom to fit

								function toggleMode(toggle) {
									toggle.enabled = !toggle.enabled;
									if (toggle.enabled) {
										toggle.innerHTML = "<i class=\"fa fa-search-plus\" aria-hidden=\"true\"></i> Apropar escala";
										//Saving previous scale state for future restore
										saveConfig();
										zoomToFit();
									} else {

										toggle.innerHTML = "<i class=\"fa fa-search-minus\" aria-hidden=\"true\"></i> Mostrar tots els anys";
										//Restore previous scale state
										restoreConfig();
										gantt.render();
									}
								}

								var cachedSettings = {};
								function saveConfig() {
									var config = gantt.config;
									cachedSettings = {};
									cachedSettings.scale_unit = config.scale_unit;
									cachedSettings.date_scale = config.date_scale;
									cachedSettings.step = config.step;
									cachedSettings.subscales = config.subscales;
									cachedSettings.template = gantt.templates.date_scale;
									cachedSettings.start_date = config.start_date;
									cachedSettings.end_date = config.end_date;
								}
								function restoreConfig() {
									applyConfig(cachedSettings);
								}

								function applyConfig(config, dates) {
									gantt.config.scale_unit = config.scale_unit;
									if (config.date_scale) {
										gantt.config.date_scale = config.date_scale;
										gantt.templates.date_scale = null;
									}
									else {
										gantt.templates.date_scale = config.template;
									}

									gantt.config.step = config.step;
									gantt.config.subscales = config.subscales;

									if (dates) {
										gantt.config.start_date = gantt.date.add(dates.start_date, -1, config.unit);
										gantt.config.end_date = gantt.date.add(gantt.date[config.unit + "_start"](dates.end_date), 2, config.unit);
									} else {
										gantt.config.start_date = gantt.config.end_date = null;
									}
								}



								function zoomToFit() {
									var project = gantt.getSubtaskDates(),
											areaWidth = gantt.$task.offsetWidth;

									for (var i = 0; i < scaleConfigs.length; i++) {
										var columnCount = getUnitsBetween(project.start_date, project.end_date, scaleConfigs[i].unit, scaleConfigs[i].step);
										if ((columnCount + 2) * gantt.config.min_column_width <= areaWidth) {
											break;
										}
									}

									if (i == scaleConfigs.length) {
										i--;
									}

									applyConfig(scaleConfigs[i], project);
									gantt.render();
								}

								// get number of columns in timeline
								function getUnitsBetween(from, to, unit, step) {
									var start = new Date(from),
											end = new Date(to);
									var units = 0;
									while (start.valueOf() < end.valueOf()) {
										units++;
										start = gantt.date.add(start, step, unit);
									}
									return units;
								}

								//Setting available scales
								var scaleConfigs = [
									// minutes
									{ unit: "minute", step: 1, scale_unit: "hour", date_scale: "%H", subscales: [
										{unit: "minute", step: 1, date: "%H:%i"}
									]
									},
									// hours
									{ unit: "hour", step: 1, scale_unit: "day", date_scale: "%j %M",
										subscales: [
											{unit: "hour", step: 1, date: "%H:%i"}
										]
									},
									// days
									{ unit: "day", step: 1, scale_unit: "month", date_scale: "%F",
										subscales: [
											{unit: "day", step: 1, date: "%j"}
										]
									},
									// weeks
									{unit: "week", step: 1, scale_unit: "month", date_scale: "%F",
										subscales: [
											{unit: "week", step: 1, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%d %M");
												var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// months
									{ unit: "month", step: 1, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "month", step: 1, date: "%M"}
										]},
									// quarters
									{ unit: "month", step: 3, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "month", step: 3, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%M");
												var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// years
									{unit: "year", step: 1, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "year", step: 5, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%Y");
												var endDate = gantt.date.add(gantt.date.add(date, 5, "year"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// decades
									{unit: "year", step: 10, scale_unit: "year", template: function (date) {
										var dateToStr = gantt.date.date_to_str("%Y");
										var endDate = gantt.date.add(gantt.date.add(date, 10, "year"), -1, "day");
										return dateToStr(date) + " - " + dateToStr(endDate);
									},
										subscales: [
											{unit: "year", step: 100, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%Y");
												var endDate = gantt.date.add(gantt.date.add(date, 100, "year"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]}
								];

								$(document).off("click",".zoomfit").on("click",".zoomfit",function(e){
									e.preventDefault();
				                  toggleMode(this);
				                });

				                $(document).off("click",".exportpdf").on("click",".exportpdf",function(e){
									e.preventDefault();
				                 	gantt.exportToPDF();
				                });

				                
						
								
								$(".zoomfit").click();


							// --> END Zoom to fit.
						
						

		                });

	                </script>

	             </div>

	        </div>


		';

		echo $Dades;

	}else{
		echo '
			
			<script type="text/javascript">

	        	$(document).ready(function($) {

					$(".loadgantt").html("");
				
	            });

            </script>

		Sense registres.

		';
		exit();
	}

	

	

	exit();