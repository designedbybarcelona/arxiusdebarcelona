<?php
	
	$db = new Db();

	require_once __DIR__."/../../pagines/consultesfiltres_inc.php";

	$aplicarfiltrevinculacio = true;
	require_once __DIR__."/../../pagines/consultesprojectes_inc.php";
	
	if (!empty($Vinculacio4)){	
		
		foreach ($Vinculacio4 as $key_v => $value_v) 
		{
			$Dades .= " 
	        					  
	                                <div class=\"row\">
	                                    <div class=\"col-lg-12\">

	                                        <table class=\"table table-striped table-hover \">
	                                          <thead>
	                                            <tr>
	                                              <th>#</th>
	                                              <th width=\"5%\">Centre o servei</th>
	                                              <th width=\"5%\">Ind.</th>
	                                              <th width=\"30%\">Nom de projecte</th>
	                                              <th width=\"5%\">Cap de projecte</th>
	                                              <th width=\"7%\">Estat</th>
	                                              <th width=\"8% style=\"text-align:center;\"\">% execució</th>
	                                              <th width=\"10%\" style=\"text-align:center;\">Inici</th>
                                               	  <th width=\"15%\" style=\"text-align:center;\">Finalització</th>
	                                              <th width=\"5%\" style=\"text-align:center;\">Fites</th>
	                                              <th width=\"20%\">Detalls</th>
	                                            </tr>
	                                          </thead>
	                                          <tbody>
	       	";
	       											$idsproj = array();
									             	 if (!empty($value_v[idsproj])){
										              	$idsproj = explode(",", $value_v[idsproj]);
									              	}
									              	
									              	$tedadesproj = false;
									              	foreach ($DadesProjectes as $key_p => $value_p) 
													{

													if ($_GET[test]==1){
													//var_dump($idsproj);exit();
													}
														if (in_array($value_p[id], $idsproj))
														{

															$indicadorsok = 0;
									                        $indicadorsko = 0;
									                        $contaidicadors = 0;
								                        
								                        	$IndicadorsProj = $db->query("SELECT assolit
																						  FROM pfx_projectes_indicadors_arxius
																						  WHERE clau_projecte = :id
																						  ",array("id"=>$value_p[id]));
								                            foreach ($IndicadorsProj as $key_i => $value_i) {
								                            	if ($value_i['assolit'] == 0) $indicadorsko++;
								                            	if ($value_i['assolit'] == 1) $indicadorsok++;
								                            	$contaidicadors++;
															}

															$simbolindicador = "";
															// UP: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que SI han estat assolits tots ells.
															// DOWN: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que NO han estats assolits cap d’ells.
															// WORK: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit + d’1 indicador, i s’hagi dit a la fitxa que algun SI ha estat assolit, i algun altre NO.
															if ($value_p['estat'] == 1 || $value_p['estat'] == 3 || $value_p['estat'] == 4 ){
																if ($indicadorsok > 0 && $indicadorsko == 0) $simbolindicador = '<i class="fa fa-thumbs-up"></i>';
																if ($indicadorsok == 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-thumbs-down"></i>';
																if ($indicadorsok > 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-exclamation-triangle"></i>';
															}


															$Dades .= "
																<tr class=\"mostarprojecte\" data-id=\"".$value_p[id]."\" style=\"cursor:pointer;\">
												                    <td><i class=\"fa fa-plus\"></i></td>
												                    <td>".$value_p[acronim]."</td>
												                    <td>".$simbolindicador."</td>
												                    <td>".$value_p[nom]."</td>
												                    <td>".$value_p[nomresp]." ".$value_p[cognomsresp]."</td>
												                    <td>".$value_p[estatprojecte]."</td>
												                    <td style=\"text-align:center;\">".$value_p[tantpercent]."</td>
												                    <td style=\"text-align:center;\"><b>".($value_p["inici"]!="00/00/0000"?$value_p["inici"]:"")."</b></td>
												                    <td style=\"text-align:center;\"><b>".($value_p["final"]!="00/00/0000"?$value_p["final"]:"")."</b></td>
												                    <td style=\"text-align:center;\" class=\"detallsfites\" data-id=\"".$value_p[id]."\" data-backdrop=\"static\" data-keyboard=\"false\" data-toggle=\"modal\" data-target=\"#modalprojectes\" title=\"Veure els detalls de les fites\">".$value_p[contafites]."</td>
												                    <td><button type=\"button\" class=\"btn btn-primary btn-xs novetats\" data-id=\"".$value_p[id]."\" data-backdrop=\"static\" data-keyboard=\"false\" data-toggle=\"modal\" data-target=\"#modalprojectes\" style=\"padding: 0px 16px;\"><i class=\"fa fa-search\" aria-hidden=\"true\"></i></button>
												                    	<span class=\"export_ind\" data-id=\"".$value_p[id]."\" style=\"cursor:pointer;\"><img src=\"../images/icon-pdf.png\" width=\"30\"/></span>
												                    </td>
											                  	</tr>
																<tr style=\"display:none;\" class=\"proj_".$value_p[id]."\">
											                    <td colspan=\"8\">
											                      <div class=\"row\">
											                          <div class=\"col-lg-12\">
											                              <div class=\"panel panel-info\">
											                                  <div class=\"panel-body\">
											                                      <table class=\"table table-striped table-hover \">
											                                        <thead>
											                                          <tr>
											                                            <th>#</th>
											                                            <th>Fase</th>
											                                            <th>Accions</th>
											                                            <th>Estat</th>
											                                          </tr>
											                                        </thead>
											                                        <tbody>
											            ";

											                                        $tedades = false;
											                                        if (!empty($Actuacions))
											                                        {
												                                        foreach ($Actuacions as $key_a => $value_a) 
																						{
																							if ($value_a[clau_projecte] == $value_p[id])
																							{

													                                          	$Dades .= "
													                                              <tr class=\"mostartasques\" data-id=\"".$value_a[id]."\" style=\"cursor:pointer;\">
													                                                <td><i class=\"fa fa-plus\"></i></td>
													                                                <td>".$value_a[titol_ca]."</td>
													                                                <td>
													                                            ";
													                                                    $contatasques = $contatasquestancades = $contatasquespendents = 0;
													                                                    if (!empty($Tasques)){
														                                                    foreach ($Tasques as $key_t => $value_t) {
														                                                    	if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
														                                                    		if ($value_t[estat]==1 or $value_t[clau_projecte]==3) $contatasquex++;
														                                                    		if ($value_t[estat]==2 or $value_t[clau_projecte]==6) $contatasquestancades++;
														                                                    		if ($value_t[estat]==4 or $value_t[clau_projecte]==5 or $value_t[estat]==0) $contatasquespendents++;
														                                                    	}
														                                                    }
														                                                }
													                                                       
													                                                    $contatasques = $contatasques + $contatasquestancades + $contatasquespendents;
													                                            $Dades .= "".$contatasques."
													                                                </td>
													                                                <td>".$value_a[estatactuacio]."</td>
													                                              </tr>
													                                              <tr style=\"display:none;\" class=\"tasc_".$value_a[id]."\">
													                                                <td colspan=\"4\">
													                                                    <div class=\"row\">
													                                                        <div class=\"col-lg-12\">
													                                                            <div class=\"panel panel-primary\">
													                                                                <div class=\"panel-body\">
													                                                                    <table class=\"table table-striped table-hover \">
													                                                                      <thead>
													                                                                        <tr>
													                                                                          <th>Acció</th>
													                                                                        </tr>
													                                                                      </thead>
													                                                                      <tbody>
																								";
													                                                                        $tedades2 = false;
													                                                                        if (!empty($Tasques)){
													                                                                        	foreach ($Tasques as $key_t => $value_t) {
														                                                    						if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
															                                                                            $Dades .= "<tr><td>".$value_a[titol]."</td></tr>";
															                                                                            $tedades2 = true;
														                                                                        	}
														                                                                        }
													                                                                        }
													                                                                        
													                                                                        if ($tedades2 == false){
																					                                        	$Dades .= "<tr><td colspan=\"2\">Sense accions.</td></tr>";
																					                                        }
													                                                                        
													                                            $Dades .= "				 </tbody>
													                                                                    </table>
													                                                                </div>
													                                                            </div>
													                                                        </div>
													                                                    </div>
													                                                </td>
													                                              </tr>
													                                              ";
												                                              	$tedades = true;
												                                          	}
												                                        }
												                                    }

											                                        if ($tedades == false){
											                                        	$Dades .= "<tr><td colspan=\"4\">Sense subprojectes.</td></tr>";
											                                        }
											                                         
											                                        
											            $Dades .= "					 </tbody>
											                                      </table>
											                                  </div>
											                              </div>
											                          </div>
											                      </div>
											                    </td>
											                  </tr>
															";
															$tedadesproj = true;
														}
													}  

													if ($tedadesproj == false){
			                                        	$Dades .= "<tr><td colspan=\"8\">Sense projectes.</td></tr>";
			                                        }       

	                                                        
	                 $Dades .= "       </tbody>
	                                </table> 
	                            </div>
	                        </div>


		 
	      "; // final java.
	    } // For Vinculacio4
	}


	echo $Dades;
