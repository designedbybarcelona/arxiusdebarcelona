<?php

	 // Limit?
     $limit = 10;
     $pag = 1;
     if (isset($_POST["pag"])){
        $pag = (int) $_POST["pag"];
        if ($pag < 1)
        {
           $pag = 1;
        }
     }
     $paginacio = ($pag-1) * $limit;
	
	/*
	if (isset($_POST['projectes'])){
		$projectesget = stripslashes_deep($_POST['projectes']);
		$primer = false;
		$lastElement = end($projectesget);
		foreach ($projectesget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " p.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
		}
	}
	else{
		exit();
	}
	*/

	/*
	if (isset($_POST['vinculacions'])){
		$vinculacionsget = stripslashes_deep($_POST['vinculacions']);
		$primer = false;
		$lastElement = end($vinculacionsget);
		foreach ($vinculacionsget as $key => $value) 
		{	
			if ($primer == false) { $condiciovinculacions .= " AND ( "; $primer = true; }
			$condiciovinculacions .= " pv.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condiciovinculacions .= " ) " : $condiciovinculacions .= " OR "; 
		}
	}
	else{
		exit();
	}
	*/

	$db = new Db();

	// a consultes eliminem el filtre per tipus d'usuaris.
	$condicio_permis_idvinc = $condicio_permis_centres = "";


	require_once __DIR__."/../../pagines/consultesfiltres_inc.php";

	// càlculs on no afecta el filtre per mostrar dades a les actuacions.
	require_once __DIR__."/../../pagines/consultaprojectes_inccalcact.php";
	
	$saltaquery = true;
	include __DIR__."/../../pagines/consultaprojectes_query.php";

	include __DIR__."/../../pagines/consultesprojectes_inc.php"; // primera consulta per recopilar ID's de projectes que no compleixen les condicions del filtre.

	$saltaquery = true;
	include __DIR__."/../../pagines/consultaprojectes_query.php";

	if ($condicioclau0 == 1) $limit = 100; // Si venim amb el filtre d'altres actuacions, el "limit" del paginat deixa fora els projectes visibles al ser molt pocs. TODO: Millorar aquesta condició.


	$pos = strpos($queryVinculacio4orig, "SELECT");
	if ($pos !== false) {
	    $queryVinculacio4 = substr_replace($queryVinculacio4orig, "SELECT SQL_CALC_FOUND_ROWS * ,", $pos, strlen("SELECT"));
	}
	$queryVinculacio4 .= " LIMIT $paginacio, $limit";

	$queryVinculacio4 = str_replace("WHERE", " WHERE pv.clau_cero = '$condicioclau0' AND ", $queryVinculacio4);


	include __DIR__."/../../pagines/consultesprojectes_inc.php"; // Segona consulta on el camp $condicioprojectesfiltrats tindrà els id's dels projectes filtrats.


	//$condiciovinc = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

	/* V1
	$Vinculacio4 = $db->query("SELECT SQL_CALC_FOUND_ROWS * , pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.clau_cero, pv.clau_pam, pv.id as id,
									GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
									GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
									GROUP_CONCAT( DISTINCT IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents,
									GROUP_CONCAT( IFNULL(p.pesrelatiu,'') SEPARATOR ';') as pesosrelatius, 
									(Select LEFT(pv2.titol_ca, 2) FROM pfx_projectes_vinculacio pv2 WHERE pv2.id = pv.clau_pam) as codipam

								  FROM pfx_projectes_vinculacio pv
								  LEFT JOIN pfx_projectes_relacionsvinc rp ON rp.id_act = pv.id
								  LEFT JOIN pfx_projectes p ON rp.id_proj = p.id 
								  WHERE nivell = 4 $condiciovinculacions AND pv.clau_cero = 0 $condicio_permis $condicio_permis2 $condicioprojectes $condiciollistatprojectes
								  GROUP By pv.id
								  ORDER BY 1*SUBSTRING_INDEX(concat(codipam,pv.titol_ca), '.', 1) ASC, 1*SUBSTRING_INDEX(concat(codipam,pv.titol_ca), '.', -1) ASC
								  LIMIT $paginacio, $limit",array());

	$arrayVinculacions = array();
	$arrayIDsProjectes = array();
	if (!empty($Vinculacio4)){
		foreach ($Vinculacio4 as $key => $value) {
			$Vinculacio4[$key][titol] = str_replace(".", "", $value[codipam]).'.'.$value[titol];
			if (!empty($value[datesinici])){
				$datesinici = explode(",", $value[datesinici]);
				$datesinici = array_filter($datesinici, function($value){return !empty($value) && $value != "0000-00-00";});
				sort($datesinici);
				$Vinculacio4[$key][datesinici] = $datesinici;
			}
			if (!empty($value[datesfinal])){
				$datesfinal = explode(",", $value[datesfinal]);
				$datesfinal = array_filter($datesfinal, function($value){return !empty($value) && $value != "0000-00-00";});
				rsort($datesfinal);
				$Vinculacio4[$key][datesfinal]= $datesfinal;
			}
			/*
			if (!empty($value[tantspercents])){
				$tantspercents = explode(";", $value[tantspercents]);
				$contatants = count($tantspercents);
				$sumatants = 0;
				foreach ($tantspercents as $key_t => $value_t) {
					$sumatants += $value_t;
				}
				if ($contatants>0){
					$mitjaexec = $sumatants/$contatants;
				}else{
					$mitjaexec = 0;
				}
				$Vinculacio4[$key][tantspercents]= number_format($mitjaexec,2);
			}
			

			$arrayVinculacions[$value[id]] = $Vinculacio4[$key];

			$idsproj = array();
          	if (!empty($value[idsproj])){
              	$idsproj = explode(",", $value[idsproj]);
              	foreach ($idsproj as $key_idp => $value_idp) {
              		
              		array_push($arrayIDsProjectes, $value_idp);
              		
              	}
          	}

		}
	}*/


	/* V2
	$queryVinculacio4 = str_replace("SELECT", "SELECT SQL_CALC_FOUND_ROWS * ,", $queryVinculacio4orig)." LIMIT $paginacio, $limit";
	$queryVinculacio4 = str_replace("WHERE", " WHERE pv.clau_cero = 0 AND", $queryVinculacio4);


	$Vinculacio4 = $db->query("$queryVinculacio4",$arraycondiciollistat);

	$arrayVinculacions = array();
	$arrayIDsProjectes = array();
	if (!empty($Vinculacio4)){
		foreach ($Vinculacio4 as $key => $value) {
			if (!empty($value[idsproj]))
			{
				$Vinculacio4[$key][titol] = str_replace(".", "", $value[codipam]).'.'.$value[titol];
				if (!empty($value[datesinici])){
					$datesinici = explode(",", $value[datesinici]);
					$datesinici = array_filter($datesinici, function($value){return !empty($value) && $value != "0000-00-00";});
					sort($datesinici);
					$Vinculacio4[$key][datesinici] = $datesinici;
					$Vinculacio4[$key][iniciunix] = strtotime($datesinici[0]);
				}
				if (!empty($value[datesfinal])){
					$datesfinal = explode(",", $value[datesfinal]);
					$datesfinal = array_filter($datesfinal, function($value){return !empty($value) && $value != "0000-00-00";});
					rsort($datesfinal);
					$Vinculacio4[$key][datesfinal]= $datesfinal;
				}
				$mitjaexec = 0;
				if (($value[tantspercents])!=""){ // no posar !empty o es salta els % = 0 i dona resultats erronis.
					$tantspercents = explode(";", $value[tantspercents]);
					$pesosrelatius = explode(";", $value[pesosrelatius]);

					// Verificar els pesos relatius, en cas de tenir pes 0.00, es reparteix el % restant en aquests que tenen 0.
					$totalpesrelaiu = 0;
					$totalpesosindicats = 0;
					$contapesos = count($pesosrelatius);
					foreach ($pesosrelatius as $key_pes => $value_pes) {
						if ($value_pes != "0.00"){
							$totalpesrelaiu += $value_pes;
							$totalpesosindicats++;
						}
					}
					if ($totalpesrelaiu != "100" && $contapesos>$totalpesosindicats){
						$calcpes = 100-$totalpesrelaiu;
						$pescalculat = $calcpes/($contapesos-$totalpesosindicats);
					}

					$contatants = count($tantspercents);
					$sumatants = 0;
					foreach ($tantspercents as $key_t => $value_t) {
						if ($pesosrelatius[$key_t]!="0.00"){
							$sumatants += ($value_t)*($pesosrelatius[$key_t]/100);
						}else{
							$sumatants += $value_t*($pescalculat/100);
						}
						
					}
					
					$mitjaexec = $sumatants;
					
					
					$Vinculacio4[$key][tantspercents]= number_format($mitjaexec,2);

				}

				
				// Per el filtre de % d'actuació. Sinó compleix esborrem de l'array.
				if ($Vinculacio4[$key][tantspercents]>=$percent3get && $Vinculacio4[$key][tantspercents]<=$percent4get){

					$arrayVinculacions[$value[id]] = $Vinculacio4[$key];

					$idsproj = array();
		          	if (!empty($value[idsproj])){
		              	$idsproj = explode(",", $value[idsproj]);
		              	foreach ($idsproj as $key_idp => $value_idp) {
		              		
		              		array_push($arrayIDsProjectes, $value_idp);
		              		
		              	}
          			}


				}else{
					unset($Vinculacio4[$key]);

					if (!empty($value[idsproj])){
						$idsproj = explode(",", $value[idsproj]);
						foreach ($idsproj as $key_proj => $value_idproj) {
							if ($condicioprojectesfiltrats == "") { $condicioprojectesfiltrats .= " AND ( ";}else{$condicioprojectesfiltrats .= " AND ";}
							$condicioprojectesfiltrats .= " p.id <> '$value_idproj'";
						}
					}
					
					
				}

			}
		}
	}
	*/
	
	$sqlTotal =  $db->row("SELECT FOUND_ROWS() as total", array());           
    if(!empty($sqlTotal)){
        $total = $sqlTotal["total"];
        $totalPag = ceil($total/$limit);
    }

    /*
	if (!empty($arrayIDsProjectes)){
		$projectesget = stripslashes_deep($arrayIDsProjectes);
		$primer = false;
		$lastElement = end($projectesget);
		foreach ($projectesget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " p.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
		}
	}
	*/

	/* surt de consultesprojecte_inc.php
	$DadesProjectes =  $dbb->FreeSql("SELECT p.titol_ca as nom, p.id as id, p.inici, p.final, p.vinculacio, p.tantpercent,
									   IFNULL(CONCAT(u.nom,' ',u.cognoms),'') as responsable, p.estat,
									  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip
									  FROM pfx_projectes p
									  $leftoperadors
									  LEFT JOIN pfx_usuaris u ON u.id = p.cap_projecte
									  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0

									  WHERE 1=1 $condicio_permis $condicioprojectes $condicioprojectesfiltrats $condicioresponsables
									  GROUP By p.id
									  ORDER BY p.titol_ca 
									  ", array());
	*/
	

	if (!empty($DadesProjectes) ) // && !empty($arrayIDsProjectes);
	{

		$Dades = '

			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/ext/dhtmlxgantt_tooltip.js"></script>
			<script src="'.constant("DBB::url").'/js/consultes/gantt/codebase/ext/dhtmlxgantt_marker.js"></script>
	        <link rel="stylesheet" href="'.constant("DBB::url").'/js/consultes/gantt/codebase/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

	        <script type="text/javascript">

	        	$(document).ready(function($) {

	        		//gantt.config.xml_date="%d-%m-%Y";

	                gantt.config.columns = [
	                    {name:"text",       label:"Programes",  width:"*", tree:true, resize: true, align: "left" },
	                    //{name:"start_date", label:"Inici", align: "center" },
	                    //{name:"duration",   label:"Durada",   align: "center" },
	                    //{name:"add",        label:"",           width:0, hide:true  }
	                ];

	                gantt.config.start_date = new Date(2013, 02, 31);

	                gantt.config.drag_move = false;
	                gantt.config.drag_lightbox = false;
	                gantt.config.drag_links = false;
	                gantt.config.drag_progress = false;
	                gantt.config.drag_resize = false;

	                gantt.config.readonly = true;

	                gantt.config.scale_unit = "year";
	                gantt.config.step = 1;
	                gantt.config.date_scale = "%Y";
	                gantt.config.min_column_width = 50;
	                gantt.config.date_scale = "%Y";

	                gantt.config.scale_height = 90;

                  	gantt.templates.tooltip_text = function(start,end,task){
					    //return "<b>Task:</b> "+task.text+"<br/><b>Duration:</b> " + task.duration;
                  		var formatFunc = gantt.date.date_to_str("%d/%m/%Y");
						var inici = formatFunc(new Date(task.start_date));
						var final = formatFunc(new Date(task.end_date));
						if (task.tipus == "fita"){
							return task.text+"<br>" + task.fites;
						}else{
							var progress = task.progress*100;
							var progress = progress.toFixed(2);
							return task.text+"<br/><b>% execució:</b> "+ progress +"<br/>"+ task.resp +"<br/>"+ task.equip;
						}
                  		
					};

	                  var monthScaleTemplate = function(date){
	                    var dateToStr = gantt.date.date_to_str("%M");
	                    var endDate = gantt.date.add(date, 2, "month");
	                    return dateToStr(date) + " - " + dateToStr(endDate);
	                  };

	                  gantt.config.subscales = [
	                    {unit:"month", step:3, template:monthScaleTemplate},
	                    {unit:"month", step:1, date:"%M" }
	                  ];

	                gantt.templates.task_text=function(start,end,task){
					    //return "<b>Text:</b> "+task.text+",<b> Holders:</b> "+task.users;
					    var formatFunc = gantt.date.date_to_str("%d/%m/%Y");
					    var inici = formatFunc(new Date(task.start_date));
						var final = formatFunc(new Date(task.end_date));
					    return inici+" - "+final;
					};


	                  
					$(".loadgantt").html("");

				

	            });

            </script>
            <style>
            	.pagination > li > a {
            		background-color:#fff;
            		border: 0px solid #dfd7ca;
            		box-sizing: border-box;
				    display: inline-block;
				    min-width: 1.5em;
				    padding: 0.5em 1em;
				    margin-left: 2px;
				    font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
				    font-size: 14px;
				    line-height: 1.42857143;
				    color: #333 !important;

            	}
				.status_line{
					background-color: #0ca30a;  		
				}	
				.status_line .gantt_marker_content{  
				  	top: 35px;
				}
				.gantt_marker .gantt_marker_content {
				   -ms-transform: rotate(-45deg); /* IE 9 */
				    -webkit-transform: rotate(-45deg); /* Safari */
				    transform: rotate(-45deg);
				    z-index: 10;
				    margin: 25px 0px 0px -25px;    
				}  
				/*
				.gantt_task_progress {
					opacity: 0.6;
				}
				*/
            </style>
	    ';

	    echo $Dades;

	    $colorsgantt = array(
	    	"0" => "#e1e1e1",
	    	"1" => "#7bd148",
	    	"2" => "#e1e1e1",
	    	"3" => "#ff887c",
	    	"4" => "#e1e1e1",
	    	"5" => "#e1e1e1",
	    );
	    // per projectes.
	    $arraycolors = array( 
			"1" => "#7bd148", // verd.
			"2" => "#ffb878", // ambar.
			"3" => "#ff887c", // vermell.
			"4" => "#e1e1e1", // gris.
			"5" => "#5681de", // blau.
		);


 		$GanttProjecte = "";

		foreach ($Vinculacio4 as $key_p => $value_p) 
		{	

			if ( !empty($value_p[datesinici]) && !empty($value_p[datesfinal]) && $value_p[clau_cero] == $condicioclau0 )
			{
				if (date('Y',strtotime($Vinculacio4dates[$value_p[id]][datesinici][0])) > "2000" && date('Y',strtotime($Vinculacio4dates[$value_p[id]][datesfinal][0])) > "2000" ){

					if ($Vinculacio4dates[$value_p[id]][datesinici][0] != "0000-00-00" && $Vinculacio4dates[$value_p[id]][datesfinal][0] != "0000-00-00")
					{

						$durada = strtotime($Vinculacio4dates[$value_p[id]]['datesfinal'][0]) - strtotime($Vinculacio4dates[$value_p[id]]['datesinici'][0]);
						$durada = abs($durada)/60/60/24;
						$Projecteg = array(
							
							'id' 		 => $value_p[id],//1, 
							'text' 		 => $value_p[titol], // str_replace(".", "", $value_p[codipam]).'.'.
							'start_date' => date("d-m-Y",strtotime($Vinculacio4dates[$value_p[id]][datesinici][0])),
							'duration'   => round(abs($durada)),
							'order' 	 => '10',
							'progress' 	 => $Vinculacio4tantspercents[$value_p[id]][tantspercents]/100,//'0.0',
							'open'       => false,
							'fites'		 => "",
							'tipus'      => "",
							'color'		 => $colorsgantt[$value_p[estat]],
							'equip' 	 => "" ,
							'resp' 		 => "" ,
							//'progressColor' =>  "#000000",
							//'textColor'
							
						
						);

						if (!empty($GanttProjecte)){
							$GanttProjecte = $GanttProjecte.",".json_encode($Projecteg);
						}else{
							$GanttProjecte =  json_encode($Projecteg);
						}
						

						if (!empty($DadesProjectes))
						{
							foreach ($DadesProjectes as $key_pr => $value_pr) 
							{	

								if ($value_pr[estat] == 1) { $DadesProjectes[$key_pr][colorsemafor] = 2; } // Iniciat, Àmbar.
								if ($value_pr[estat] == 2) { $DadesProjectes[$key_pr][colorsemafor] = 5; } // No iniciat, Blau.
								if ($value_pr[estat] == 3) { $DadesProjectes[$key_pr][colorsemafor] = 3; } // Amb dificultats, Vermell.
								if ($value_pr[estat] == 4) { $DadesProjectes[$key_pr][colorsemafor] = 1; } // Finalitzat, Verd.
								if ($value_pr[estat] == 5) { $DadesProjectes[$key_pr][colorsemafor] = 4; } // Descartat, Gris.
								if ($value_pr[estat] == ""){ $DadesProjectes[$key_pr][colorsemafor] = 4; } // Gris.

								//$existeixvinculacio = strpos($value_pr[vinculacio], "_".$value_p[id]); 
								$codifinculacio = str_replace('"','', str_replace("]", "", substr($value_pr[vinculacio], strrpos($value_pr[vinculacio], '_') + 1)));
								if ($codifinculacio == $value_p[id])
								{
									// Fites del projecte.
									$fitesproj = "";
									$FitesProjecte =  $dbb->FreeSql("SELECT f.observacionsfita as fita,f.datai as inici,   DATE_FORMAT(f.datai, '%d/%m/%Y') as datai, f.id as id,
										 							  IFNULL(pa.titol_ca,'----') as tipologiafita
																	  FROM pfx_projectes_fites f
																	  LEFT JOIN pfx_projectes_fites_tipologies pa ON f.tipologia = pa.id
																	  WHERE f.clau_projecte = :id 
																	  ORDER BY f.datai desc", array("id"=>$value_pr[id]));

									if (!empty($FitesProjecte)){
										$fitesproj .= "<hr><b>FITES</b>";
										foreach ($FitesProjecte as $key_fp => $value_fp) {
											$fitesproj .= "<li><b>".$value_fp[datai]."</b>: ".nl2br(wordwrap($value_fp[fita],80,"<br>"))."</li>";

											/*
											$jsfites .= '
												
												var start = new Date('.date("Y",strtotime($value_fp[inici])).', '.(date("m",strtotime($value_fp[inici]))-1).', '.date("d",strtotime($value_fp[inici])).');
												gantt.addMarker({
													start_date: start,
													css: "status_line",
													text: "FITA de '.$value_p[titol].'",
													title:"Detall: '.nl2br(wordwrap($value_fp[fita],80,"<br>")).'"
												});
												
											';
											*/
											$arrayfites = array(
																'id' 		 => "fi_".$value_pr[id],//$contador,
																'text' 		 => "Fita", // $value_fp[datai]." - ".nl2br(wordwrap($value_fp[fita],80,"<br>"))
																'start_date' => date("d-m-Y",strtotime($value_fp[inici])),
																'parent'     => "pr_".$value_pr[id],
																'color'		 => "#7100AD",
																'duration'   => "20",
																'fites'      => "<li><b>".$value_fp[datai]."</b>: ".$value_fp[tipologiafita]."</li><li>".nl2br(wordwrap($value_fp[fita],80,"<br>"))."</li>",
																'tipus'      => "fita",
															);

											$GanttProjecte = $GanttProjecte.",".json_encode($arrayfites);
										}
									}
									
									if ( !empty($value_pr[inicibd]) && !empty($value_pr["finalbd"]) && $value_pr[inicibd] != "0000-00-00" && $value_pr["finalbd"] != "0000-00-00" )
									{	
										$durada = strtotime($value_pr['finalbd']) - strtotime($value_pr['inicibd']);
										$durada = abs($durada)/60/60/24;
										$tasquesprojecte =  array(	
															'id' 		 => "pr_".$value_pr[id],//$contador,
															'text' 		 => $value_pr[nom],
															'start_date' => date("d-m-Y",strtotime($value_pr[inicibd])),
															'duration'   => round(abs($durada)),
															'order' 	 => '10',
															'progress' 	 => $value_pr[tantpercent]/100,//'0.0',
															'parent'     => $value_p[id],
															"color" 	 => $arraycolors[$DadesProjectes[$key_pr][colorsemafor]],
															'fites'	     => "",//$fitesproj
															'open'       => 'false',
															'tipus'      => "",
															'equip' 	 => str_replace("'", "`", $value_pr[nomsequip]) ,
															'resp' 		 => str_replace("'", "`", $value_pr[nomresp]." ".$value_pr[cognomresp]) ,

														
														);
										$GanttProjecte = $GanttProjecte.",".json_encode($tasquesprojecte);

										$Actuacions = $dbb->FreeSql("SELECT a.id, a.inici, a.final, a.titol_ca as titol, a.estat,
																 GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip
															  FROM pfx_actuacions a
															  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
															  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( a.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
															  WHERE a.clau_projecte = '".$value_pr['id']."'
															  GROUP By a.id 
															  ORDER BY a.id asc",array());
															  
										$Tasques = $dbb->FreeSql("SELECT t.id, t.inici, t.final, t.titol_ca as titol, p.titol_ca as nomprojecte, t.clau_actuacio, t.estat
																  FROM pfx_tasques t
																  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
																  WHERE t.clau_projecte = '".$value_pr['id']."'
																  GROUP BY t.id
																  ORDER BY t.id asc, t.titol_ca ", array());
									

										if (!empty($Actuacions))
										{
											$contador = 2;
											foreach ($Actuacions as $key_a => $value_a) 
											{
												if ( !empty($value_a['inici']) && !empty($value_a['final']) && $value_a[inici] != "0000-00-00" && $value_a["final"] != "0000-00-00" )
												{
													$durada = strtotime($value_a['final']) - strtotime($value_a['inici']);
													$durada = abs($durada)/60/60/24;
													$actuacioprojecte =  array(	
																		'id' 		 => "a_".$value_a[id],//$contador,
																		'text' 		 => $value_a[titol],
																		'start_date' => date("d-m-Y",strtotime($value_a[inici])),
																		'duration'   => round(abs($durada)),
																		'order' 	 => '10',
																		'progress' 	 => '0.0',
																		'parent'     => "pr_".$value_pr['id'],
																		'color'		 => $colorsgantt[$value_a[estat]],
																		'fites'		 => "",
																		'open'       => 'false',
																		'tipus'      => "",
																		'equip' 	 => str_replace("'", "`", $value_a[equip]) ,
																	
																	);
													$GanttProjecte = $GanttProjecte.",".json_encode($actuacioprojecte);
													$contador++;

													if (!empty($Tasques))
													{
														foreach ($Tasques as $key_ta => $value_ta) 
														{
															if ($value_ta[clau_actuacio] == $value_a[id])
															{
																$durada = strtotime($value_ta['final']) - strtotime($value_ta['inici']);
																$durada = abs($durada)/60/60/24;
																$tasquesprojecte =  array(	
																					'id' 		 => "t_".$value_ta[id],//$contador,
																					'text' 		 => $value_ta[titol],
																					'start_date' => date("d-m-Y",strtotime($value_ta[inici])),
																					'duration'   => abs($durada),
																					'order' 	 => '10',
																					'progress' 	 => '0.0',
																					'parent'     => "a_".$value_a[id],
																					'color'		 => $colorsgantt[$value_ta[estat]],
																					'fites'		 => "",
																					'open'       => 'false',
																					'tipus'      => "",
																				
																				);
																$GanttProjecte = $GanttProjecte.",".json_encode($tasquesprojecte);
															}
														}
													}
													
												}
											}
											
										}

									}

								}
								
							}
						}

					}
				}

			}
		}


		$GanttProjecte = "{data:[".$GanttProjecte."]};";
		//echo $GanttProjecte;
		//exit();

		//var_dump($jsfites);exit();

		 // Paginador.

	    function createLinks( $links, $list_class, $limit, $totalPag, $pag, $parametres ) 
	    {
	        if ( $limit == 'all' ) {
	            return '';
	        }
	     
	        $last       = $totalPag;
	     
	        $start      = ( ( $pag - $links ) > 0 ) ? $pag - $links : 1;
	        $end        = ( ( $pag + $links ) < $last ) ? $pag + $links : $last;
	     
	        $html       = '';//'<ul class="' . $list_class . '">';
	     
	        $class      = ( $pag == 1 ) ? "disabled" : "";
	        $html       .= '<li class=""><a class="page-link" data-pag="' . ( $pag - 1 ) . '" href="#" >Anterior</a></li>';
	     
	        if ( $start > 1 ) {
	            $html   .= '<li class=""><a class="page-link" data-pag="1" href="#">1</a></li>';
	            $html   .= '<li class="page-link" readonly><span>...</span></li>';
	        }
	     
	        for ( $i = $start ; $i <= $end; $i++ ) {
	            $class  = ( $pag == $i ) ? "background: linear-gradient(to bottom, #fff 0%, #dcdcdc 100%);border: 1px solid #cacaca;" : "";
	            $html   .= '<li class=""><a class="page-link" style="'.$class.'" data-pag="'.$i.'" href="#" >' . $i . '</a></li>';
	        }
	     
	        if ( $end < $last ) {
	            $html   .= '<li class="" class="disabled"><span>...</span></li>';
	            $html   .= '<li class=""><a class="page-link" data-pag="'.$last.'" href="#">' . $last . '</a></li>';
	        }
	     
	        $class      = ( $pag == $last ) ? "disabled" : "";
	        $html       .= '<li class=""><a class="page-link paginate_button next" data-pag="'.( $pag + 1 ).'" href="#">Següent</a></li>';
	     
	        $html       .= '';//'</ul>';
	     
	        return $html;
	    }
	
	    
          
 		if ($totalPag > 1)
      	{
         	$Paginat = '
		
					<div class="row">
						<div class="col-lg-12">
							<nav style="float:right;">
					  			<ul class="pagination">
									 '.createLinks(5, "pager", $limit, $totalPag, $pag, $parametres).'
					  			</ul>
							</nav>
			            </div>
			        </div>

    		';

             
        
      	}


		$Dades = '

			<style>
				
				.page-link{
						font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
					    font-size: 14px;
					    font-weight: 200; 
					    line-height: 1.42857143;
					    box-sizing: border-box;
					    display: inline-block;
					    min-width: 1.5em;
					    padding: 0.5em 1em;
					    margin-left: 2px;
					    text-align: center;
					    text-decoration: none !important;
					    cursor: pointer;
					    color: #333 !important;
					    border: 1px solid transparent;
				}

			</style>

			<div class="row">
				<div class="col-lg-1" style="float:right; margin-right: 95px;margin-bottom:5px;" >
	                  <button class="btn btn-xs btn-default zoomfit"><i class="fa fa-search-minus" aria-hidden="true"></i> Mostrar tots els anys</button>
	            </div>
	        </div>


			<div class="row">

	            <div class="col-lg-12">
	            
	                <div id="gantt_1" style="width:100%; height:700px;"></div>

	                '.$Paginat.'
					    

	                <script type="text/javascript">

	                	$(document).ready(function($) {

		                    var tasks_1 = '.$GanttProjecte.'

		                    '.$jsfites.'
							

		                    gantt.init("gantt_1");


		                    gantt.parse(tasks_1);
							
							$(document).off("click",".page-link").on("click",".page-link",function(e){
								e.preventDefault();
								var pag = $(this).data("pag");
							    //$("#paginagant").val(pag);
								//$( "#frmgant" ).submit();
								var oTable = $(".dataTables2").dataTable();
  								oTable.fnPageChange( pag-1 );
							});

							
							// Zoom to fit

								function toggleMode(toggle) {
									toggle.enabled = !toggle.enabled;
									if (toggle.enabled) {
										toggle.innerHTML = "<i class=\"fa fa-search-plus\" aria-hidden=\"true\"></i> Apropar escala";
										//Saving previous scale state for future restore
										saveConfig();
										zoomToFit();
									} else {

										toggle.innerHTML = "<i class=\"fa fa-search-minus\" aria-hidden=\"true\"></i> Mostrar tots els anys";
										//Restore previous scale state
										restoreConfig();
										gantt.render();
									}
								}

								var cachedSettings = {};
								function saveConfig() {
									var config = gantt.config;
									cachedSettings = {};
									cachedSettings.scale_unit = config.scale_unit;
									cachedSettings.date_scale = config.date_scale;
									cachedSettings.step = config.step;
									cachedSettings.subscales = config.subscales;
									cachedSettings.template = gantt.templates.date_scale;
									cachedSettings.start_date = config.start_date;
									cachedSettings.end_date = config.end_date;
								}
								function restoreConfig() {
									applyConfig(cachedSettings);
								}

								function applyConfig(config, dates) {
									gantt.config.scale_unit = config.scale_unit;
									if (config.date_scale) {
										gantt.config.date_scale = config.date_scale;
										gantt.templates.date_scale = null;
									}
									else {
										gantt.templates.date_scale = config.template;
									}

									gantt.config.step = config.step;
									gantt.config.subscales = config.subscales;

									if (dates) {
										gantt.config.start_date = gantt.date.add(dates.start_date, -1, config.unit);
										gantt.config.end_date = gantt.date.add(gantt.date[config.unit + "_start"](dates.end_date), 2, config.unit);
									} else {
										gantt.config.start_date = gantt.config.end_date = null;
									}
								}



								function zoomToFit() {
									var project = gantt.getSubtaskDates(),
											areaWidth = gantt.$task.offsetWidth;

									for (var i = 0; i < scaleConfigs.length; i++) {
										var columnCount = getUnitsBetween(project.start_date, project.end_date, scaleConfigs[i].unit, scaleConfigs[i].step);
										if ((columnCount + 2) * gantt.config.min_column_width <= areaWidth) {
											break;
										}
									}

									if (i == scaleConfigs.length) {
										i--;
									}

									applyConfig(scaleConfigs[i], project);
									gantt.render();
								}

								// get number of columns in timeline
								function getUnitsBetween(from, to, unit, step) {
									var start = new Date(from),
											end = new Date(to);
									var units = 0;
									while (start.valueOf() < end.valueOf()) {
										units++;
										start = gantt.date.add(start, step, unit);
									}
									return units;
								}

								//Setting available scales
								var scaleConfigs = [
									// minutes
									{ unit: "minute", step: 1, scale_unit: "hour", date_scale: "%H", subscales: [
										{unit: "minute", step: 1, date: "%H:%i"}
									]
									},
									// hours
									{ unit: "hour", step: 1, scale_unit: "day", date_scale: "%j %M",
										subscales: [
											{unit: "hour", step: 1, date: "%H:%i"}
										]
									},
									// days
									{ unit: "day", step: 1, scale_unit: "month", date_scale: "%F",
										subscales: [
											{unit: "day", step: 1, date: "%j"}
										]
									},
									// weeks
									{unit: "week", step: 1, scale_unit: "month", date_scale: "%F",
										subscales: [
											{unit: "week", step: 1, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%d %M");
												var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// months
									{ unit: "month", step: 1, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "month", step: 1, date: "%M"}
										]},
									// quarters
									{ unit: "month", step: 3, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "month", step: 3, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%M");
												var endDate = gantt.date.add(gantt.date.add(date, 3, "month"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// years
									{unit: "year", step: 1, scale_unit: "year", date_scale: "%Y",
										subscales: [
											{unit: "year", step: 5, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%Y");
												var endDate = gantt.date.add(gantt.date.add(date, 5, "year"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]},
									// decades
									{unit: "year", step: 10, scale_unit: "year", template: function (date) {
										var dateToStr = gantt.date.date_to_str("%Y");
										var endDate = gantt.date.add(gantt.date.add(date, 10, "year"), -1, "day");
										return dateToStr(date) + " - " + dateToStr(endDate);
									},
										subscales: [
											{unit: "year", step: 100, template: function (date) {
												var dateToStr = gantt.date.date_to_str("%Y");
												var endDate = gantt.date.add(gantt.date.add(date, 100, "year"), -1, "day");
												return dateToStr(date) + " - " + dateToStr(endDate);
											}}
										]}
								];

								$(document).off("click",".zoomfit").on("click",".zoomfit",function(e){
				                  toggleMode(this);
				                });
						
								
								$(".zoomfit").click();


							// --> END Zoom to fit.


		                });

	                </script>

	             </div>

	        </div>


		';

		echo $Dades;

	}else{
		echo '
			
			<script type="text/javascript">

	        	$(document).ready(function($) {

					$(".loadgantt").html("");
				
	            });

            </script>

		Sense registres.

		';
		exit();
	}

	

	

	exit();