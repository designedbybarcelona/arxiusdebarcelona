<?php
	
	$grafics = "";

	$ambits = $config['a'];

	$noms_ambits = array("Estratègia", "Activitat", "Suport i Recursos", "Perspectiva ciutadana", "Mostrar");

	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 )
	{
		$condicio_permis = " AND pi.responsable = ".$app['session']->get(constant('General::nomsesiouser'));
	}

	if (!empty($ambits)){
		foreach ($ambits as $key => $value) {
			if ($value == 1) {
				if (empty($condicio_ambits)) $condicio_ambits = " AND (";
				$condicio_ambits .= ($condicio_ambits!=" AND ("?" OR ":"")." ambit = ".($key+1);
			}
		}
		if (!empty($condicio_ambits)) $condicio_ambits .= " ) ";
	}

	$condicio_ambits = $condicio_permis.$condicio_ambits;

	$Processos =  $dbb->FreeSql("SELECT p.titol_ca as titol, p.id as id, p.ambit as ambit,
								  (select count(id) from pfx_processos_indicadors pi where pi.clau_proces = p.id ) as containdicadors
								  FROM pfx_processos p
								  INNER JOIN pfx_processos_indicadors pi ON pi.clau_proces = p.id 
								  WHERE 1=1 $condicio_permis
								  GROUP By p.id
								  ORDER BY p.ambit, p.titol_ca ",array());

	$Indicadors = $dbb->FreeSql("SELECT pi.*
								  FROM pfx_processos_indicadors pi
								  INNER JOIN pfx_processos p ON p.id = pi.clau_proces
								  WHERE  1=1 $condicio_permis
								  GROUP By pi.id ",array());


	$Indicadorsvalors = $dbb->FreeSql("SELECT piv.*
									  FROM pfx_processos_indicadors_valors piv
									  INNER JOIN pfx_processos_indicadors pi ON pi.id = piv.parent_id
									  INNER JOIN pfx_processos p ON p.id = pi.clau_proces
									  WHERE  1=1 $condicio_permis2
									  GROUP By piv.id
									  ORDER BY piv.parent_id,piv.datai asc",array());

	$grafics .= '<div class="col-lg-12">';

	if (!empty($ambits)){
		foreach ($ambits as $key_a => $value_a) {
			if ($value_a == 1) 
			{
				$grafics .= "<h1><small>".$noms_ambits[$key_a]."</small></h1>";

				if (!empty($Processos))
				{
					foreach ($Processos as $key_p => $value_p) 
					{
						if ($value_p[ambit] == $key_a+1){

							$grafics .= '
								<div class="panel panel-info">
			                        <div class="panel-body">

										<h3><b>'.$value_p[titol]."</b></h3> (<b>".$value_p[containdicadors].'</b> Indicadors)<hr>
							';

								foreach ($Indicadors as $key_i => $value_i) 
								{
									if ($value_i[clau_proces] == $value_p[id])
									{ 
										$grafics .= "<div class=''>
														<h4> <button type=\"button\" class=\"btn btn-primary btn-xs mostraindicadors\" data-id='".$value_i[id]."'>Veure gràfics</button> <a href='$url/48/processos.html?id=".$value_i[clau_proces]."'><button type='button' class='btn btn-info btn-xs edicio2'>Edició</button></a> <b>".$value_i[titol]."</b> </h4>
														
													</div>";
													 

										// Tipus text lliure.
										if ($value_i[tipusindicador] == 1)
										{
											if (!empty($Indicadorsvalors)){
												foreach ($Indicadorsvalors as $key_iv => $value_iv) 
												{
													if ($value_iv[parent_id] == $value_i[id])
													{
														if (!empty($value_iv[valor]) )
														{
															if (is_array(json_decode($value_iv[valor],true)))
															{
																foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																{
																	$grafics .= '<div class="col-lg-12 amagaindicadors mostraindicadors_'.$value_i[id].'"><p><h3>'.date('d/m/Y',strtotime($value_iv[datai])).'</h3></p><p>'.$value_va[valor].'</p></div>';
																	
																}
																
															}

														}
													}
												}
											}
										}

										// Tipus gràfics.
										if (!empty($value_i[tipusgrafics]) )
										{

											if (is_array( json_decode($value_i[tipusgrafics],true) ))
											{

												if (in_array(1,json_decode($value_i[tipusgrafics],true))) // Sèrie temporal.
												{
													
													//$grafics .= "Sèrie temporal.<br>";

													$ykeys = $labels ="";
													$primeraentrada = true;
													$grafics .= '

														<div class="col-lg-9 amagaindicadors mostraindicadors_'.$value_i[id].'">
														<div id="temporal_'.$value_i[id].'" ></div>

													 	<script type="text/javascript">

        													$(document).ready(function($) {
															
															new Morris.Line({
															  element: \'temporal_'.$value_i[id].'\',
															 
															  data: [
													';
																if (!empty($Indicadorsvalors)){
																	foreach ($Indicadorsvalors as $key_iv => $value_iv) 
																	{
																		if ($value_iv[parent_id] == $value_i[id])
																		{

																			$grafics .= ' { y: \''.date('Y-m-d',strtotime($value_iv[datai])).'\' , ';
																		
																			if (!empty($value_iv[valor]) )
																			{
																				if (is_array(json_decode($value_iv[valor],true)))
																				{
																					foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																					{
																						$grafics .= ' value_'.$key_va.': \''.$value_va[valor].'\' ,';
																						if($primeraentrada == true)
																						{
																							$ykeys.= "'value_".$key_va."',";
																							$labels.= "'".$value_va[textvalor]."',";
																						}
																						
																					}
																					$primeraentrada = false;
																					
																				}

																			}
																			

																			$grafics .= ' },';

																		}
																		
																	}
																}

													$grafics .= '		
															    
															  ],
															  xkey: \'y\',
															  ykeys:['.rtrim($ykeys,",").'],
		  													  labels: ['.rtrim($labels,",").']
															});

															});

														</script>

														</div>

													';
												}

												if (in_array(2,json_decode($value_i[tipusgrafics],true))) // Foto darrer registre.
												{
													
													// Mode %

													$Indicadorsvalors2 = $dbb->FreeSql("SELECT piv.*
																					  FROM pfx_processos_indicadors_valors piv
																					  INNER JOIN pfx_processos_indicadors pi ON pi.id = piv.parent_id
																					  INNER JOIN pfx_processos p ON p.id = pi.clau_proces
																					  WHERE  1=1 $condicio_permis2 AND piv.parent_id = ".$value_i[id]."
																					  GROUP By piv.id
																					  ORDER BY piv.parent_id,piv.datai asc",array());
													if (!empty($Indicadorsvalors2)){

														$value_iv = end($Indicadorsvalors2);
														if (!empty($value_iv[valor]) )
														{
															if (is_array(json_decode($value_iv[valor],true)))
															{
																foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																{
																	$valordarrer = $value_va[valor];
																	$textvalordarrer = $value_va[textvalor];
																	
																	$grafics .= '
																		<div class="col-lg-3  amagaindicadors mostraindicadors_'.$value_i[id].'" style="text-align:center;">
												                            <div class="panel panel-info">
												                                <div class="panel-heading"><h4>'.$textvalordarrer.'</h4></div>
												                              <div class="panel-body">
												                                <span class="chart" data-percent="'.$valordarrer.'">
												                                  <i class="fa fa-clock-o"></i> <span class="percent"></span>
												                                </span>
												                              </div>
												                            </div>
												                        </div>
												                       
												                        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
												                        <script src="'.$url.'/js/consultes/jquery.easypiechart.min.js"></script>
												                        <script>
												                        $(function() {
												                            $(\'.chart\').easyPieChart({
												                                easing: \'easeOutBounce\',
												                                onStep: function(from, to, percent) {
												                                    $(this.el).find(\'.percent\').text(Math.round(percent));
												                                },
												                                size: 150,
												                                lineWidth: 10,
												                            });
												                            var chart = window.chart = $(\'.chart\').data(\'easyPieChart\');
												                            
												                        });
												                        </script>
																	';
													
																	
																}
																
															}

														}
														

														
															
														
													}


													

													// Mode barres
													/*
													$ykeys = $labels ="";
													$primeraentrada = true;

													$grafics .= '

														<div class="col-lg-3 amagaindicadors mostraindicadors_'.$value_i[id].'">

														<div id="fotodarrer_'.$value_i[id].'" ></div>
														
														<script type="text/javascript">

        													$(document).ready(function($) {
															
															new Morris.Bar({
															  element: \'fotodarrer_'.$value_i[id].'\',
															 
															  data: [
													';
																$Indicadorsvalors2 = $dbb->FreeSql("SELECT piv.*
																									  FROM pfx_processos_indicadors_valors piv
																									  INNER JOIN pfx_processos_indicadors pi ON pi.id = piv.parent_id
																									  INNER JOIN pfx_processos p ON p.id = pi.clau_proces
																									  WHERE  1=1 $condicio_permis2 AND piv.parent_id = ".$value_i[id]."
																									  GROUP By piv.id
																									  ORDER BY piv.parent_id,piv.datai asc",array());
																if (!empty($Indicadorsvalors2)){

																	$value_iv = end($Indicadorsvalors2);

																	
																	$grafics .= ' { y: \''.date('d/m/Y',strtotime($value_iv[datai])).'\' , ';
																
																	if (!empty($value_iv[valor]) )
																	{
																		if (is_array(json_decode($value_iv[valor],true)))
																		{
																			foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																			{
																				$grafics .= ' value_'.$key_va.': \''.$value_va[valor].'\' ,';
																				if($primeraentrada == true)
																				{
																					$ykeys.= "'value_".$key_va."',";
																					$labels.= "'".$value_va[textvalor]."',";
																				}
																				
																			}
																			$primeraentrada = false;
																			
																		}

																	}
																	
																	$grafics .= ' },';

																	
																		
																	
																}

													$grafics .= '		
															    
															  ],
															  xkey: \'y\',
															  ykeys:['.rtrim($ykeys,",").'],
		  													  labels: ['.rtrim($labels,",").'],
		  													  hideHover: true,
															});

															});

														</script>
															<hr>
														</div>

													';
													*/
												}

												if (in_array(3,json_decode($value_i[tipusgrafics],true))) // Semàfor temporal.
												{
													$grafics .= '

														<div class="col-lg-6 amagaindicadors mostraindicadors_'.$value_i[id].'">
													
															<div class="table-responsive"  style="overflow: hidden !important;">
								                                <table class="table table-striped table-bordered table-hover">
								                                    <thead>
								                                        <tr>
								                    ';	
								                    						$Indicadorsvalors2 = $dbb->FreeSql("SELECT piv.*, DATE_FORMAT(piv.datai, '%d/%m/%Y') as datai
																											  	FROM pfx_processos_indicadors_valors piv
																											  	INNER JOIN pfx_processos_indicadors pi ON pi.id = piv.parent_id
																											  	INNER JOIN pfx_processos p ON p.id = pi.clau_proces
																											  	WHERE  1=1 $condicio_permis2 AND piv.parent_id = ".$value_i[id]."
																											  	GROUP By piv.id
																											  	ORDER BY piv.parent_id,piv.datai asc",array());
								                    						if (!empty($Indicadorsvalors2))
								                    						{
								                    							foreach ($Indicadorsvalors2 as $key => $value) 
									                    						{
									                    							$grafics .= "<th>".$value[datai]."</th>";
									                    						}
								                    						}
								                    						
								                    $grafics .= '
								                                            
								                                        </tr>
								                                    </thead>   
								                                    <tbody>
								                    ';

								                    /*
								                    $LlistatProcessosTipus = $dbb->FreeSql("SELECT p.*
																						  	FROM pfx_processos_indicadors_tipus
																						  	WHERE  parent_id = ".$value_i[id]."
																						  	ORDER BY descripcio_ca asc",array());

								                    $arraytipus = array();
								                    foreach ($LlistatProcessosTipus as $key => $value) {
								                    	$arraytipus[$value[id]] = $value[descripcio_ca];
								                    }
								                    */

								                    $grafics .= '<tr>';
								                    if (!empty($Indicadorsvalors2))
								                    {
								                    	foreach ($Indicadorsvalors2 as $key => $value) 
			                    						{
		                    								if (!empty($value[valor]) )
															{
																if (is_array(json_decode($value[valor],true)))
																{
																	foreach (json_decode($value[valor],true) as $key_va => $value_va) 
																	{
																		$grafics .= '
										                                            
							                                                <td>
							                                                   '.$value_va[textvalor].'
							                                                </td>
												                                            
												                    	';
																	}
																	
																}
																else
																{
																	$grafics .= '
										                                            
							                                                <td>
							                                                  ---
							                                                </td>
												                                            
												                    	';
																}

															}
			                    							
			                    						}
								                    }
								                    

		                    						$grafics .= '</tr>';
								                    
								                    
								                    $grafics .= '           
								                                    </tbody>
								                                </table>
							                                </div>
													';

													$grafics .= '
															<hr>
														</div>

													';

												}

												if (in_array(4,json_decode($value_i[tipusgrafics],true))) // Foto - Pastís / Barres
												{
													//$grafics .= "Sèrie temporal.<br>";

													$labels ="";
													$primeraentrada = true;

													if (!empty($Indicadorsvalors)){
														foreach ($Indicadorsvalors as $key_iv => $value_iv) 
														{
															if ($value_iv[parent_id] == $value_i[id])
															{

																$grafics .= '

																	<div class="col-lg-4 amagaindicadors mostraindicadors_'.$value_i[id].'">
																	<br><br><br><br>
																	<h4>'.date('d/m/Y',strtotime($value_iv[datai])).'</h4>
																	<div id="fototemps_'.$value_iv[id].'" ></div>

																 	<script type="text/javascript">

		            													$(document).ready(function($) {
																		
																		new Morris.Bar({
																		  element: \'fototemps_'.$value_iv[id].'\',
																		 
																		  data: [
																';
																			if (!empty($value_iv[valor]) )
																			{
																				if (is_array(json_decode($value_iv[valor],true)))
																				{
																					foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																					{

																						$grafics .= ' { y: \''.$value_va[textvalor].'\' , ';
																				
																							$grafics .= ' value : \''.$value_va[valor].'\' ';
																							
																						$grafics .= ' }, ';

																						$labels.= "'".$value_va[textvalor]."',";
																					}

																					
																				}

																			}
																				

																$grafics .= '		
																		    
																		  ],
																		  xkey: \'y\',
																		  ykeys:[\'value\'],
					  													  labels: ['.rtrim($labels,",").']
																		});

																		});

																	</script>

																	</div>

																';

																// DONUT
																$grafics .= '

																	<div class="col-lg-2 amagaindicadors mostraindicadors_'.$value_i[id].'">
																	<br><br><br><br>
																	<h4>'.date('d/m/Y',strtotime($value_iv[datai])).'</h4>
																	<div id="fotodonut_'.$value_iv[id].'" ></div>

																 	<script type="text/javascript">

		            													$(document).ready(function($) {
																		
																		new Morris.Donut({
																		  element: \'fotodonut_'.$value_iv[id].'\',
																		 
																		  data: [
																';
																			if (!empty($value_iv[valor]) )
																			{
																				if (is_array(json_decode($value_iv[valor],true)))
																				{
																					foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																					{

																						$grafics .= ' {label: "'.$value_va[textvalor].'", value: '.$value_va[valor].' },';
																				
																					}
																					
																				}

																			}
																				

																$grafics .= '		
																		    
																		  ],
																		});

																		});

																	</script>

																	</div>

																';


															}
																			
														}
													}
												}

												if (in_array(5,json_decode($value_i[tipusgrafics],true))) // Sèrie - Barres / linia temporal.
												{

													//$grafics .= "Sèrie temporal.<br>";
													$ykeys = $labels = $valorsgrafic = "";
													$primeraentrada = true;

													// Dades objectiu actual per mostrar com a linea constant al gràfic.
													// TODO: de moment no està afegit al gràfic, veure com posar.
													$objactual = $value_i[objectiu];
													if (!empty($objactual) ){
														if (is_array(json_decode($objactual,true))){
															foreach (json_decode($objactual,true) as $key_vac => $value_vac) {

																$valorsgraficobjectiu .= ' value_0: \''.$value_vac[valor].'\' ,';
																$ykeys.= "'value_0',";
																$labelsobjectiu.= "'".$value_vac[text]."',";
																
																
															}
														}

													}

													$grafics .= '
													<div class="row">

														<div class="col-lg-6 amagaindicadors mostraindicadors_'.$value_i[id].'">
														<br><br><br><br>
														<div id="serietemporal_'.$value_i[id].'" ></div>

													 	<script type="text/javascript">

        													$(document).ready(function($) {
															
															new Morris.Line({
															  element: \'serietemporal_'.$value_i[id].'\',
															 
															  data: [
													';
																if (!empty($Indicadorsvalors)){
																	foreach ($Indicadorsvalors as $key_iv => $value_iv) 
																	{
																		$graficsobjectiu = "";
																		if ($value_iv[parent_id] == $value_i[id])
																		{

																			$grafics .= ' { y: \''.date('Y-m-d',strtotime($value_iv[datai])).'\' , ';
																			$graficsobjectiu .= ' { y: \''.date('Y-m-d',strtotime($value_iv[datai])).'\' , ';
																		
																			if (!empty($value_iv[valor]) )
																			{
																				if (is_array(json_decode($value_iv[valor],true)))
																				{
																					foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																					{
																						$grafics .= ' value_'.$key_va.': \''.$value_va[valor].'\' ,';
																						if($primeraentrada == true)
																						{
																							$ykeys.= "'value_".$key_va."',";
																							$labels.= "'".$value_va[textvalor]."',";
																						}
																						
																					}
																					$primeraentrada = false;
																					$graficsobjectiu .= $valorsgraficobjectiu;
																				}	

																			}
																			
																			$graficsobjectiu .= ' },';
																			$grafics .= ' },';//.$graficsobjectiu;

																		}
																		
																	}
																}

													$grafics .= '		
															    
															  ],
															  xkey: \'y\',
															  ykeys:['.rtrim($ykeys,",").'],
		  													  labels: ['.rtrim($labels,",").']
															});

															});

														</script>

														</div>
													
													';



													$ykeys = $labels ="";
													$primeraentrada = true;
													$grafics .= '
														<div class="col-lg-6 amagaindicadors mostraindicadors_'.$value_i[id].'">
														<br><br><br><br>
														<div id="serietemporalbar_'.$value_i[id].'" ></div>

													 	<script type="text/javascript">

        													$(document).ready(function($) {
															
															new Morris.Bar({
															  element: \'serietemporalbar_'.$value_i[id].'\',
															 
															  data: [
													';
																if (!empty($Indicadorsvalors)){
																	foreach ($Indicadorsvalors as $key_iv => $value_iv) 
																	{
																		if ($value_iv[parent_id] == $value_i[id])
																		{

																			$grafics .= ' { y: \''.date('Y-m-d',strtotime($value_iv[datai])).'\' , ';
																		
																			if (!empty($value_iv[valor]) )
																			{
																				if (is_array(json_decode($value_iv[valor],true)))
																				{
																					foreach (json_decode($value_iv[valor],true) as $key_va => $value_va) 
																					{
																						$grafics .= ' value_'.$key_va.': \''.$value_va[valor].'\' ,';
																						if($primeraentrada == true)
																						{
																							$ykeys.= "'value_".$key_va."',";
																							$labels.= "'".$value_va[textvalor]."',";
																						}
																						
																					}
																					$primeraentrada = false;
																					
																				}

																			}
																			

																			$grafics .= ' },';

																		}
																		
																	}
																}

													$grafics .= '		
															    
															  ],
															  xkey: \'y\',
															  ykeys:['.rtrim($ykeys,",").'],
		  													  labels: ['.rtrim($labels,",").']
															});

															});

														</script>

														</div>
													</div>

													';

												}

											}

										}
										

										$grafics .= "<hr>";
									}
								}


							$grafics .= '
									<hr>
									</div>
								</div>
							';
						}
					}
				}

			}
		}
	}
	

	$grafics .= '</div>
	
	<script type="text/javascript">

    	$(document).ready(function($) {
			$(".amagaindicadors").hide();
		});

	</script>

	';

	echo $grafics;

	exit();
	