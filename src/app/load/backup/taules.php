<?php


	if ($taula == "1")
	{
		
		// Llistats.
		$sTable = "projectes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu != 1 ) //if ($permisusu == 3)
			{	
				/*
				$usuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
		
				$condicio_permis = $condicio_permis2 = "";
				if (!empty($usuarivinculacio)){

					// Eliminar ID's sense "_" pq indiquen el primer niell.
					$usuarivinculacio = array_unique(stripslashes_deep(json_decode($usuarivinculacio)));
					foreach ($usuarivinculacio as $key_uv => $value_uv) 
					{
						if (strpos($value_uv, "_") === false) unset($usuarivinculacio[$key_uv]);
					}

					
					$primer = false; $lastElement = end($usuarivinculacio);
					foreach ($usuarivinculacio as $key_uv => $value_uv) 
					{
						if ($primer == false) { $condicio_permis .= " AND ( "; $condicio_permis2 .= " AND ( "; $primer = true; }
						$condicio_permis .= " FIND_IN_SET('$value_uv' ,REPLACE( REPLACE( REPLACE( t.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";
						$condicio_permis2 .= " concat(pv.clau_pam,'_',pv.id) = REPLACE( REPLACE( REPLACE( '$value_uv','[', ''),']' ,'') ,'\"','') ";
						if($value_uv == $lastElement) {  
							$condicio_permis .= " ) ";
							$condicio_permis2 .= " ) ";
						}else{ 
							$condicio_permis .= " OR "; 
							$condicio_permis2 .= " OR "; 
						}

					}
			
				}*/

				$condicio_permis = $condicio_permis_uvinc;


				if (empty($condicio_permis)){
					$condicio_permis .= " AND 1 = 2";
				}

				//$condiciovincusuari = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( t.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";
				//$innerpermis = " INNER JOIN pfx_projectes_vinculacio pv ON $condiciovincusuari ";
				$innerpermis = " INNER JOIN pfx_projectes_relacionsvinc rp ON rp.id_proj = t.id ";

				//$condicio_permis .= " AND pv.nivell = 4 ";
			}



			$sWhere = " WHERE 1=1 $condicio_permis $condicio_permis2";
			$sInner = " $innerpermis
						LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
				  		LEFT JOIN pfx_usuaris u ON t.cap_projecte = u.id";
			$sOrder = " ORDER by t.titol_ca desc";
			$sLimit = "";

			$camps = array(
				'codi',
				'nom',
				'estatprojecte',
				'inici',
				'final',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, t.codi as codi, IFNULL(pa.text,'----') as estatprojecte, u.nom as nomresp, u.cognoms as cognomsresp, DATE_FORMAT(t.inici, '%d/%m/%Y') as inici, DATE_FORMAT(t.final, '%d/%m/%Y') as final
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY t.id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom del projecte'),
			'3' => $app['translator']->trans('Estat del projecte'),
			'4' => $app['translator']->trans('Inici'),
			'5' => $app['translator']->trans('Final'),

		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();

	}
	elseif ($taula == "2") 
	{
		// Llistats.
		$sTable = "actuacions";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu != 1 )
			{
				$condicio_permis = " WHERE FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
									 OR cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))." ";
			}

			$sWhere = "$condicio_permis";
			$sInner = "LEFT JOIN pfx_projectes p ON t.clau_projecte = p.id
					   LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
					   LEFT JOIN pfx_usuaris u ON t.responsable = u.id";
			$sOrder = "ORDER by t.titol_ca desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'codi',
				'nom',
				'nomdelprojecte',
				'estatprojecte',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
						  t.ordre as ordre, t.codi as codi, t.codi as codi, IFNULL(pa.text,'----') as estatprojecte,
						  p.titol_ca as nomdelprojecte, u.nom as nomresp, u.cognoms as cognomsresp
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom del subprojecte'),
			'3' => $app['translator']->trans('Projecte'),
			'4' => $app['translator']->trans('Estat del subprojecte'),
		);


		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "3") 
	{
		// Llistats.
		$sTable = "tasques";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{
			$permisusu = $app['session']->get(constant('General::nomsesiouser')."-permisos");
			if ($permisusu != 1 )
			{
				$condicio_permis = " WHERE FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
									OR cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))." ";
			}

			$sWhere = "$condicio_permis";
			$sInner = " LEFT JOIN pfx_actuacions a ON t.clau_actuacio = a.id
						LEFT JOIN pfx_projectes p ON a.clau_projecte = p.id
						LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estattasca' AND pa.idioma = '$idioma'
						LEFT JOIN pfx_usuaris u ON t.responsable = u.id";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'codi',
				'nom',
				'nomactuacio',
				'nomdelprojecte',

			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
						  t.ordre as ordre, t.codi as codi, IFNULL(pa.text,'----') as estattasca,
						  p.titol_ca as nomdelprojecte, a.titol_ca as nomactuacio, u.nom as nomresp, u.cognoms as cognomsresp
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Codi'),
			'2' => $app['translator']->trans('Nom de l\'acció'),
			'3' => $app['translator']->trans('Nom del subprojecte'),
			'4' => $app['translator']->trans('Nom del projecte'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "5")
	{
		// Llistats.
		$sTable = "projectes_dimensions";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "6")
	{
		// Llistats.
		$sTable = "projectes_fites_tipologies";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "ORDER by t.titol_ca desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				 		 t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "7")
	{
		
		// Llistats.
		$sTable = "passos";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		if (isset($config['ctf2'])) $config['e'] = $config['ctf2']; // Quan venim d'eliminar el valor de e està a ctf2.

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['e'] );

		if ($carregadades) 
		{

			$sWhere = "WHERE estat = :estat AND clau_tasca = :clau_tasca";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("estat" => $config['e'], "clau_tasca" => $config['ctf']);

			$camps = array(
				'datai',
				'descripcio'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as descripcio, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Descripció'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "8")
	{
		// Llistats.
		$sTable = "projectes_objectius";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "9")
	{
		// Llistats.
		$sTable = "projectes_indicadors";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom',
				'inicial',
				'actual',
				'objectiu',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, inicial, actual, objectiu
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Descripció'),
			'2' => $app['translator']->trans('Inicial'),
			'3' => $app['translator']->trans('Actual'),
			'4' => $app['translator']->trans('Objectiu'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif($taula == "10")
	{
		// Llistats.
		$sTable = "projectes_vinculacio";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "";
			$sInner = "LEFT JOIN pfx_projectes_vinculacio t2 ON t2.id = t.parent_id 
					   LEFT JOIN pfx_projectes_vinculacio t3 ON FIND_IN_SET( t3.id ,REPLACE( REPLACE( REPLACE( t.parents_ids,'[', ''),']' ,'') ,'\"','')) > 0
					   LEFT JOIN pfx_projectes_vinculacio pam ON pam.id = t.clau_pam
					   LEFT JOIN pfx_projectes_vinculacio eix ON eix.id = t.clau_eix";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'tipus',
				'nom',
				array('nompam','nomeix','nomvincle','nomvincle2'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				        t.ordre as ordre, IFNULL(t2.titol_ca,'') as nomvincle,
				        GROUP_CONCAT( DISTINCT IFNULL(CONCAT(t3.titol_ca),'') SEPARATOR ' <br> ') as nomvincle2,
				        IFNULL(CONCAT(pam.titol_ca,' > '),'') as nompam, IFNULL(CONCAT(eix.titol_ca,' > '),'') as nomeix,
				        CASE t.nivell
					        when '1' then 'PDA'
					        when '2' then 'LÍNIA'
					        when '3' then 'ÀMBIT'
					        when '4' then 'OBJECTIU'
					    END as tipus
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Tipus'),
			'2' => $app['translator']->trans('Títol'),
			'3' => $app['translator']->trans('Relació'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "4")
	{
		// Llistats.
		$permisosact = $app['session']->get(constant('General::nomsesiouser')."-permisos");
		$idusuari = $app['session']->get(constant('General::nomsesiouser'));

		if ($permisosact != 1 AND $permisosact != 2 AND $permisosact != 3)
		{
			$condicioperm = " WHERE t.clau_permisos >= :clau_permisos ";//" WHERE t.clau_pla = $idpla AND t.clau_permisos >= '$permisosact' ";
			$where = array(
				"clau_permisos" => $permisosact
			);
		}else{
			$condicioperm = ""; //" WHERE t.clau_pla = $idpla OR t.clau_permisos = '$permisosact' ";
			$where = array();
		}

		$sTable = "usuaris";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time);

		if ($carregadades) 
		{

			$sInner = "INNER JOIN pfx_permisos p ON p.id = t.clau_permisos ";
			$sWhere = "$condicioperm";
			$sOrder = "ORDER by nom desc";
			$sLimit = "";

			$camps = array(
				//array('$rs["usuari"]',"(",'$rs["nom"]'," ",'$rs["cognoms"]',")"),
				array('usuari',': (','nom',': ','cognoms',':)'),
				'user_lastlogin',
				'nompermis',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.nom as nom, t.cognoms as cognoms, t.usuari as usuari,
				  		t.estat as estat, p.nom_ca as nompermis, t.user_lastlogin
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
			'2' => $app['translator']->trans('Últim accés'),
			'3' => $app['translator']->trans('Rol'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif ($taula == "12")
	{
		// Llistats.
		$sTable = "upfile";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['ctf2']);

		if ($carregadades) 
		{
			$coditaula = $dbb->Llistats("codis"," AND codi = :codi", array("codi"=>$config['ctf2']));
            if (!empty($coditaula))
            {
            	$nomtaula = $coditaula[1][nom];
            }
            if ($config['ctf2'] == "9999")
            {
            	$nomtaula = "login";
            }

			$sWhere = "WHERE clau_id = :clau_id AND taula = :taula";
			$sOrder = "ORDER by nom desc";
			$sLimit = "";

			$where = array("clau_id"=>$config['ctf'],"taula"=>$nomtaula);

			if ($config['ctf2'] == "9999")
            {
				$camps = array(
					'nomfile',
					'nom',
				);
			}
			else
			{
				$camps = array(
					'nomfile',
					'nom',
					array(': <span style="display:none;">','iniciunix',': </span>','intro'),
				);
			}

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.estat as estat, 
				 	   t.ordre as ordre,
				 	   CASE true

				            WHEN nomfile LIKE '%.jpg' OR nomfile LIKE '%.png'
				                THEN nomfile
				                ELSE nomfile
					    END as nomfile,
					    DATE_FORMAT(t.datareg, '%d/%m/%Y') as intro, UNIX_TIMESTAMP(t.datareg) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

					// CONCAT('<img width=200 src=".constant("DBB::url")."/../secretfiles/', nomfile ,' /> (',nomfile,')') 
					// TODO: al ser directori no accesible no es veu.

			include "taula.php";
			
		}

		// TODO: el server ha de tenir la versió 5.6 de mysql pq funcioni.
		//THEN CONCAT('<img width=200 src=".constant("DBB::url")."/docs/', TO_BASE64(id) ,'/?t=',MD5(concat('TockenSecretViewDBB!', id)),' />',' (',nomfile,')')

		if ($config['ctf2'] == "9999")
        {
			$capcaleres = array(
				'1' => $app['translator']->trans('Nom del fitxer'),
				'2' => $app['translator']->trans('Text'),
			);
		}
		else
		{
			$capcaleres = array(
				'1' => $app['translator']->trans('Nom Document / URL'),
				'2' => $app['translator']->trans('Descripció'),
				'3' => $app['translator']->trans('Data'),
			);
		}

		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "9999") 
	{

		$dbb->NomesAdmins();

		// Llistats.
		$query = "SELECT distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  t.ordre as ordre
				  FROM pfx_upfile t
				  WHERE  taula = 'login'
				  GROUP BY id
				  ORDER by nom desc
				";
		$camps = array(
			'1' => '$rs["nom"]',
			'2' => '$rs["ordre"]',
		);
		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
			'2' => $app['translator']->trans('Ordre'),
		);
		$Dades = $dbb->GenerarLlistats("upfile", $query, $camps, $capcaleres);

		echo $Dades;

			exit();
	}
	elseif($taula == "14")
	{
		// Llistats.
		$sTable = "projectes_organs";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'identipart',
				'dataop',
				'dataop2',
				'observacionsop',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, identipart, dataop, dataop2, observacionsop
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Identificació i participants'),
			'2' => $app['translator']->trans('Inici'),
			'3' => $app['translator']->trans('Final'),
			'4' => $app['translator']->trans('Observacions'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "15")
	{
		// Llistats.
		$sTable = "projectes_districtes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sInner = "LEFT JOIN pfx_projectes_districtes t2 ON t2.id = t.parent_id ";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom',
				'nomvincle'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  			t.ordre as ordre, IFNULL(t2.titol_ca,'----') as nomvincle
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
			'2' => $app['translator']->trans('Districte'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "16")
	{
		// Llistats.
		$sTable = "projectes_organs_e";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sInner = "LEFT JOIN pfx_projectes_organs_e t2 ON t2.id = t.parent_id ";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom',
				'nomvincle'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  			t.ordre as ordre, IFNULL(t2.titol_ca,'----') as nomvincle
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
			'2' => $app['translator']->trans('Vincle'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "17")
	{
		// Llistats.
		$sTable = "projectes_agents";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "ORDER by t.nom desc";
			$sLimit = "";

			$where = array();

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				       t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}


		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
		);

		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "18")
	{
		// Llistats.
		$sTable = "permisos";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostrareliminar'=>'no');

		if ($carregadades) 
		{

			$sWhere = "WHERE id <> 999";
			$sOrder = "ORDER by nom desc";
			$sLimit = "";

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.nom_ca as nom, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif ($taula == "19") 
	{
		// Llistats.
		$sTable = "plens";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "20")
	{
		// Llistats.
		$sTable = "plens_arees";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time);
		
		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "nom desc";
			$sLimit = "";

			$camps = array(
				'nom',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();

		$capcaleres = array(
			'1' => $app['translator']->trans('Títol'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "21") 
	{
		// Llistats.
		$sTable = "decrets";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "22") 
	{
		// Llistats.
		$sTable = "comissiopermanent";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "23") 
	{
		// Llistats.
		$sTable = "consellparticipacio";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "24") 
	{
		// Llistats.
		$sTable = "comissioinformativa";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "25") 
	{
		// Llistats.
		$sTable = "organdeliberatiu";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "26") 
	{
		// Llistats.
		$sTable = "processos";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'2' => $app['translator']->trans('Nom de l\'àrea/servei'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "28")
	{
		// Llistats.
		$sTable = "processos_indicadors";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_proces = :clau_proces";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_proces" => $config['ctf']);

			$camps = array(
				'nom',
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				array(';json','objectiu','text',': : ','valor',': <br>'),
				array(': <span style="background-color:','colorpropera',': ; padding:5px;"><b>','propera',': </b></span>'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, objectiu, DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix,
				  		
				  		CASE periodicitat
					        when '1' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 DAY), '%d/%m/%Y')
						    when '2' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 WEEK), '%d/%m/%Y')
							when '3' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 MONTH), '%d/%m/%Y')
							when '4' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 2 MONTH), '%d/%m/%Y')
							when '5' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 3 MONTH), '%d/%m/%Y')
							when '6' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 6 MONTH), '%d/%m/%Y')
							when '7' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 YEAR), '%d/%m/%Y')
					    END as propera,
					    CASE periodicitat
					    	when '1' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 DAY)

				            		) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '2' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 WEEK)
					            	) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '3' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 MONTH)
					            	) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '4' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 2 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
							when '5' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 3 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
					        when '6' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 6 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
					        when '7' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_processos_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 YEAR)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END

					    END as colorpropera

					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Descripció'),
			'2' => $app['translator']->trans('Inici'),
			'3' => $app['translator']->trans('Objectiu actual'),
			'4' => $app['translator']->trans('Propera presa de dades'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif($taula == "29")
	{
		// Llistats.
		$sTable = "processos_indicadors_valors";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "WHERE t.parent_id = :parent_id";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				array(': <span style="display:none;">','dataitime',': </span>','datai'),
				array(';json','valor','textvalor',': : ','valor',': <br>'),
				array(';json','valor_objectiu','text',': : ','valor',': <br>'),
			);

			$variable1 = '{"1":{"textvalor":"","valor":"';
			$variable2 = '","detalls":""}}';
			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		DATE_FORMAT(datai, '%d/%m/%Y') as datai, UNIX_TIMESTAMP(datai) as dataitime, valor_objectiu,
				  		valor
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data del càlcul'),
			'2' => $app['translator']->trans('Valors'),
			'3' => $app['translator']->trans('Valors objectius'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif ($taula == "30") 
	{
		// Llistats.
		$sTable = "bustialocal";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sInner = " LEFT JOIN pfx_paraules pa ON t.prioritat = pa.clau AND pa.codi = 'prioritatbustia' AND pa.idioma = 'ca' 
						LEFT JOIN pfx_paraules pa2 ON t.tipus = pa2.clau AND pa2.codi = 'tipusbustia' AND pa2.idioma = 'ca'";
			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				'descripcio',
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				'prioritatp',
				'tipusbustia',
				'status',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as descripcio, t.id as id, t.estat as estat, 
						DATE_FORMAT(inici, '%d/%m/%Y') as inici, IFNULL(pa.text,'----') as prioritatp, IFNULL(pa2.text,'----') as tipusbustia,
						CASE status
					        when '1' then 'OBERTA'
					        when '2' then 'TANCADA'
					    END as status,
					    UNIX_TIMESTAMP(inici) as iniciunix
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom del registre'),
			'2' => $app['translator']->trans('Alta incidència'),
			'3' => $app['translator']->trans('Prioritat'),
			'4' => $app['translator']->trans('Tipus'),
			'5' => $app['translator']->trans('Status'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "31") 
	{
		// Llistats.
		$sTable = "bustiaambits";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Àmbit'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "32") 
	{
		// Llistats.
		$sTable = "processos_indicadors_tipus";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "WHERE parent_id = :parent_id";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as titol, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";


			include "taula.php";

			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom categoria'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "34")
	{
		// Llistats.
		$sTable = "actuacions_objectius";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_actuacio = :clau_actuacio";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_actuacio" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif ($taula == "35") 
	{
		// Llistats.
		$sTable = "actuacions_ambits";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sInner = "LEFT JOIN pfx_actuacions_ambits t2 ON t2.id = t.parent_id";
			$sWhere = "";
			$sOrder = "ORDER by t.titol_ca desc";
			$sLimit = "";

			$camps = array(
				'titol',
				'nomvincle',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat, IFNULL(t2.titol_ca,'----') as nomvincle
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Àmbit'),
			'2' => $app['translator']->trans('Relació'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "36") 
	{
		// Llistats.
		$sTable = "tasques_colectius";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Colectiu'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif ($taula == "37")
	{
		
		// Llistats.
		$sTable = "dintres";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time,'mostarestat'=>'no');

		if ($carregadades) 
		{
			
			$sWhere = "$condicio_permis";
			$sInner = " LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatexpedient' AND pa.idioma = '$idioma'
						LEFT JOIN pfx_usuaris u ON FIND_IN_SET( u.id ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
			$sOrder = " ORDER by t.titol_ca desc";
			$sLimit = "";

			$camps = array(
				'codi',
				'estatexpedient',
				'nomsgestors',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, t.codi as codi, IFNULL(pa.text,'----') as estatexpedient,
				  		GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u.nom,' ',u.cognoms),'----') SEPARATOR ' - ') as nomsgestors
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Identificació expedient'),
			'2' => $app['translator']->trans('Estat de l\'expedient'),
			'3' => $app['translator']->trans('Gestors'),

		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();

	}
	elseif($taula == "38")
	{
		// Llistats.
		$sTable = "dintres_contactes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_dintres = :clau_dintres";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_dintres" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "39")
	{
		// Llistats.
		$sTable = "dintres_observacions";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_dintres = :clau_dintres";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_dintres" => $config['ctf']);

			$camps = array(
				array(';rsa_nom')
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "40")
	{
		// Llistats.
		$sTable = "dintres_projectes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_dintres = :clau_dintres";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_dintres" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "41")
	{
		// Llistats.
		$sTable = "dintres_projectes_tecnics";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "42")
	{
		// Llistats.
		$sTable = "dintres_observacions_tecniques";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_dintres = :clau_dintres";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_dintres" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "43")
	{
		// Llistats.
		$sTable = "dintres_sancionador";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{
			$sInner = " LEFT JOIN pfx_paraules pa ON t.tipussancionador = pa.clau AND pa.codi = 'tipussancionador' AND pa.idioma = '$idioma'";
			$sWhere = "WHERE clau_dintres = :clau_dintres";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_dintres" => $config['ctf']);

			$camps = array(
				'tipussancionador'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, IFNULL(pa.text,'----') as tipussancionador
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "44")
	{
		// Llistats.
		$sTable = "dintres_sancionador_propietaris";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_sancionador = :clau_sancionador";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_sancionador" => $config['ctf']);

			$camps = array(
				'nom'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "45")
	{
		// Llistats.
		$sTable = "projectes_fites";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sInner = "LEFT JOIN pfx_projectes_fites_tipologies ti ON t.tipologia = ti.id ";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nomtipologia',
				'observacionsfita',
				array(': <span style="display:none;">','iniciunix',': </span>','datai'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, observacionsfita, ti.titol_$idioma as nomtipologia,
				  		DATE_FORMAT(datai, '%d/%m/%Y') as datai, UNIX_TIMESTAMP(datai) as iniciunix,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis1
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Tipologia'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Data'),
			
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "46")
	{
		// Llistats.
		$sTable = "tasques_fites";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_tasca = :clau_tasca";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_tasca" => $config['ctf']);

			$camps = array(
				'datai',
				'observacionsfita',
				'avis',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, observacionsfita,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Avís'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "47")
	{
		// Llistats.
		$sTable = "projectes_operadors";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		if (!empty($config['ctf2'])) $taulafinal = $config['ctf2'];

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['ctf2']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE 1=1 ";//"WHERE clau_projecte = :clau_projecte";
			$sInner = "LEFT JOIN pfx_paraules pa ON t.tipologia = pa.clau AND pa.codi = 'tipologiaoperadors' AND pa.idioma = '$idioma'";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			//$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				array(': <span style="display:none;">','checked2',': </span>',': <input name="operadors[]" value="','id',':" type="checkbox" ','checked',':  />'), 
				'titol',
				'persones',
				'tipologiaoperador',
			);

			if ($taulafinal==t){
				$selectdeprojecte = "SELECT id FROM pfx_tasques p WHERE p.id = '".$config['ctf']."' AND FIND_IN_SET(t.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0 LIMIT 1";
				$sInner .= "INNER JOIN pfx_tasques ta ON ta.id = '".$config['ctf']."'
						    INNER JOIN pfx_projectes pr ON FIND_IN_SET(t.id ,REPLACE( REPLACE( REPLACE( pr.operadors,'[', ''),']' ,'') ,'\"','')) > 0 AND pr.id = ta.clau_projecte";
			}
			else{
				$selectdeprojecte = "SELECT id FROM pfx_projectes p WHERE p.id = '".$config['ctf']."' AND FIND_IN_SET(t.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0 LIMIT 1";

				if ($taulafinal=="consulta") {
					$sInner .= " INNER JOIN pfx_projectes pr ON FIND_IN_SET(t.id ,REPLACE( REPLACE( REPLACE( pr.operadors,'[', ''),']' ,'') ,'\"','')) > 0 ";
				}
			}

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, t.titol_ca as titol, persones, IFNULL(pa.text,'----') as tipologiaoperador,
				  		CASE WHEN (($selectdeprojecte) IS NULL )
  					     	 THEN ''
  							 ELSE 'checked=\"true\"'
 						END as checked,
 						CASE WHEN (($selectdeprojecte) IS NULL )
  					     	 THEN '0'
  							 ELSE '1'
 						END as checked2
				  	
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => '#',
			'2' => $app['translator']->trans('Denominació'),
			'3' => $app['translator']->trans('Persona/es referent/s'),
			'4' => $app['translator']->trans('Tipologia'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "48")
	{
		// Llistats.
		$sTable = "projectes_fites_com";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				array('datai',': - ','datafi'),
				'observacionsfita',
				'avis',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, datafi, observacionsfita,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Avís'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "49")
	{
		// Llistats.
		$sTable = "projectes_fites_part";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				array('datai',': - ','datafi'),
				'observacionsfita',
				'avis',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, datafi, observacionsfita,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Avís'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "50") 
	{
		// Llistats.
		$sTable = "projectes_projmotor";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();


		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{
			
			$where = array("parent_id" => $config['ctf']);

			$sInner = "";
			$sWhere = "WHERE parent_id = :parent_id";
			$sOrder = "";
			$sLimit = "";

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as titol, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Projecte'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "51")
	{
		// Llistats.
		$sTable = "projectes_indicadors_nou";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'nom',
				array(': <span style="display:none;">','iniciunix',': </span>','inici'),
				array(';json','objectiu','text',': : ','valor',': <br>'),
				array(': <span style="background-color:','colorpropera',': ; padding:5px;"><b>','propera',': </b></span>'),
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, objectiu, DATE_FORMAT(inici, '%d/%m/%Y') as inici, UNIX_TIMESTAMP(inici) as iniciunix,
				  		
				  		CASE periodicitat
					        when '1' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 DAY), '%d/%m/%Y')
						    when '2' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 WEEK), '%d/%m/%Y')
							when '3' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 MONTH), '%d/%m/%Y')
							when '4' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 2 MONTH), '%d/%m/%Y')
							when '5' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 3 MONTH), '%d/%m/%Y')
							when '6' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 6 MONTH), '%d/%m/%Y')
							when '7' then DATE_FORMAT(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 YEAR), '%d/%m/%Y')
					    END as propera,
					    CASE periodicitat
					    	when '1' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 DAY)

				            		) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '2' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 WEEK)
					            	) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '3' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 MONTH)
					            	) < DATE(NOW())
					                THEN '#d9534f'
					                ELSE ''
					            END
					        when '4' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 2 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
							when '5' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 3 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
					        when '6' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 6 MONTH)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END
					        when '7' 
					    		THEN 
					            CASE
					            WHEN DATE(DATE_ADD((CASE WHEN ((SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1) IS NULL )
					         	  		  					     	 THEN inici
					         	 		  							 ELSE (SELECT MAX(datai) FROM pfx_projectes_indicadors_valors WHERE parent_id = t.id LIMIT 1)
					                     						END), INTERVAL 1 YEAR)
					            	) < DATE(NOW())
					                THEN 'd9534f'
					                ELSE 'ffffff'
					            END

					    END as colorpropera

					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Descripció'),
			'2' => $app['translator']->trans('Inici'),
			'3' => $app['translator']->trans('Objectiu actual'),
			'4' => $app['translator']->trans('Propera presa de dades'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif($taula == "52")
	{
		// Llistats.
		$sTable = "projectes_indicadors_valors";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "WHERE t.parent_id = :parent_id";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				array(': <span style="display:none;">','dataitime',': </span>','datai'),
				array(';json','valor','textvalor',': : ','valor',': <br>'),
				array(';json','valor_objectiu','text',': : ','valor',': <br>'),
			);

			$variable1 = '{"1":{"textvalor":"","valor":"';
			$variable2 = '","detalls":""}}';
			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		DATE_FORMAT(datai, '%d/%m/%Y') as datai, UNIX_TIMESTAMP(datai) as dataitime, valor_objectiu,
				  		valor
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data del càlcul'),
			'2' => $app['translator']->trans('Valors'),
			'3' => $app['translator']->trans('Valors objectius'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();

	}
	elseif($taula == "53") 
	{
		// Llistats.
		$sTable = "projectes_indicadors_tipus";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'] );

		if ($carregadades) 
		{

			$sInner = "";
			$sWhere = "WHERE parent_id = :parent_id";
			$sOrder = "ORDER by t.descripcio_ca desc";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				'titol'
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.descripcio_ca as titol, t.id as id, t.estat as estat
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";


			include "taula.php";

			
		}

		$where = array();
		$capcaleres = array(
			'1' => $app['translator']->trans('Nom categoria'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);

		echo $Dades;

		exit();
	}
	elseif($taula == "54")
	{
		// Llistats.
		$sTable = "projectes_urls";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		if (!empty($config['ctf2'])) $taulafinal = $config['ctf2'];

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['ctf2']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE parent_id = :parent_id";
			$sInner = "";
			$sOrder = "";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				array(': <a href="','titol',': " title="','titol',': " target="_blank">','titolcurt',':</a>'),
				'descripcio',
				'inserted',
			);

			
			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, titol_ca as titol, descripcio_ca as descripcio, DATE_FORMAT(t.inserted, '%d/%m/%Y') as inserted,
				  		SUBSTRING(titol_ca,1, 100) as titolcurt
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans(''),
			'2' => $app['translator']->trans(''),
			'3' => $app['translator']->trans(''),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "55")
	{
		// Llistats.
		$sTable = "actuacions_urls";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		if (!empty($config['ctf2'])) $taulafinal = $config['ctf2'];

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['ctf2']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE parent_id = :parent_id";
			$sInner = "";
			$sOrder = "";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				array(': <a href="','titol',': " target="_blank">','titol',':</a>'),
				'descripcio',
			);

			
			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, titol_ca as titol, descripcio_ca as descripcio
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('URL'),
			'2' => $app['translator']->trans('Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "56")
	{
		// Llistats.
		$sTable = "tasques_urls";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		if (!empty($config['ctf2'])) $taulafinal = $config['ctf2'];

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf'], 'ctf2' => $config['ctf2']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE parent_id = :parent_id";
			$sInner = "";
			$sOrder = "";
			$sLimit = "";

			$where = array("parent_id" => $config['ctf']);

			$camps = array(
				array(': <a href="','titol',': " target="_blank">','titol',':</a>'),
				'descripcio',
			);

			
			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, titol_ca as titol, descripcio_ca as descripcio
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('URL'),
			'2' => $app['translator']->trans('Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "57")
	{
		// Llistats.
		$sTable = "projectes_general";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "ORDER by t.datai asc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'datai',
				'descripcio',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, descripcio_ca as descripcio
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "58")
	{
		// Llistats.
		$sTable = "projectes_novetats";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.datai desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','datai'),
				'descripcio',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, descripcio_ca as descripcio,
				  		DATE_FORMAT(datai, '%d/%m/%Y') as datai, UNIX_TIMESTAMP(datai) as iniciunix
				  		
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "59")
	{
		// Llistats.
		$sTable = "fites_marc";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'datai',
				'observacionsfita',
				'avis1',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, observacionsfita,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis1
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Avís'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "60")
	{
		// Llistats.
		$sTable = "projectes_projmotor_nov";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.datai desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'datai',
				'descripcio',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, descripcio_ca as descripcio
				  		
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "61")
	{
		// Llistats.
		$sTable = "projectes_projmotor_fites";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.observacionsfita desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'datai',
				'observacionsfita',
				'avis1',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, observacionsfita,
				  		CASE avis
					        when '1' then 'Si'
					        when '0' then 'No'
					    END as avis1
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
			'3' => $app['translator']->trans('Avís'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "62")
	{
		// Llistats.
		$sTable = "actuacions_novetats";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_actuacio = :clau_actuacio";
			$sOrder = "ORDER by t.datai desc";
			$sLimit = "";

			$where = array("clau_actuacio" => $config['ctf']);

			$camps = array(
				'datai',
				'descripcio',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, datai, descripcio_ca as descripcio
				  		
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "63")
	{
		// Llistats.
		$sTable = "projectes_riscos";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']  );

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.datai desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				array(': <span style="display:none;">','iniciunix',': </span>','datai'),
				'descripcio',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS  distinct(t.id) as id, t.id as id, t.estat as estat, 
				  		t.ordre as ordre, descripcio_ca as descripcio, DATE_FORMAT(datai, '%d/%m/%Y') as datai, UNIX_TIMESTAMP(datai) as iniciunix
				  		
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		$capcaleres = array(
			'1' => $app['translator']->trans('Data'),
			'2' => $app['translator']->trans('Nom/Descripció'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}
	elseif($taula == "65")
	{
		// Llistats.
		$sTable = "projectes_conceptes";

		if (!empty($config['t'])) $time = $config['t'];
		else $time = uniqid();

		$configuracio = array("time"=>$time, 'mostarestat' =>'no', 'enmodal' => $config['em'], 'idmodal' => $config['mod'], 'ctf' => $config['ctf']);

		if ($carregadades) 
		{

			$sWhere = "WHERE clau_projecte = :clau_projecte";
			$sOrder = "ORDER by t.titol_ca desc";
			$sLimit = "";

			$where = array("clau_projecte" => $config['ctf']);

			$camps = array(
				'any',
				'nom',
				'pressupost1',
				'pressupost2',
				'pressupost3',
			);

			$sQuery = "SELECT SQL_CALC_FOUND_ROWS distinct(t.id) as id, t.titol_ca as nom, t.id as id, t.any as any,
					   pressupost1, pressupost2, pressupost3
					   FROM pfx_$sTable t
					   ¬sInner¬
					   ¬sWhere¬ 
					   GROUP BY id
					   ¬sOrder¬
					   ¬sLimit¬
					";

			include "taula.php";
			
		}

		
		$capcaleres = array(
			'1' => $app['translator']->trans('Any'),
			'2' => $app['translator']->trans('Concepte'),
			'3' => $app['translator']->trans('Cap II'),
			'4' => $app['translator']->trans('Cap IV'),
			'5' => $app['translator']->trans('Cap VI'),
		);
		
		if (!$carregadades) 
		$Dades = $dbb->GenerarLlistatsNou("$sTable", $sQuery, $where, $camps, $capcaleres, $configuracio);
		
		echo $Dades;

		exit();
	}


	