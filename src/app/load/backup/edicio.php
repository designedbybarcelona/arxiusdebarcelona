<?php
	
	

	if ($taula == "1")
	{
		$RegistreProjecte = $dbb->Llistats("projectes"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 )
		{
			
			$usuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
			
			$nouarrayusuari = array();
			$condicio_permis2 = "";
			if (!empty($usuarivinculacio)){

				$usuarivinculacio = array_unique(stripslashes_deep(json_decode($usuarivinculacio)));
				foreach ($usuarivinculacio as $key_uv => $value_uv) 
				{
					if (strpos($value_uv, "_") === false) {
						//unset($usuarivinculacio[$key_uv]);
					}else{
						$value_uv = explode("_", $value_uv);
						foreach ($value_uv as $key1 => $value1) {
							if (!in_array($value1, $nouarrayusuari)){
								array_push($nouarrayusuari, $value1);
							}
						}	
						
					}
				}
			
				$primer = false; $lastElement = end($nouarrayusuari);
				foreach ($nouarrayusuari as $key_uv => $value_uv) 
				{
					if ($primer == false) { $condicio_permis2 .= " AND ( "; $primer = true; }
					$condicio_permis2 .= " p.id = '$value_uv' ";
					($value_uv == $lastElement)? $condicio_permis2 .= " ) " : $condicio_permis2 .= " OR "; 
				}
				
			}
			if (empty($condicio_permis2)){
				$condicio_permis2 .= " AND 1 = 2";
			}
			
			
		}


		$Vinculacio =  $dbb->FreeSql("SELECT p.*
									    FROM pfx_projectes_vinculacio p
									    WHERE p.estat = true $condicio_permis2
									    ORDER BY titol_ca = 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 1), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 2), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 3), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 4), '.', -1) + 0 ",
									  array());

		$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

		$Organse = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");

		$RegistreProjectesNov = $dbb->Llistats("projectes_novetats"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$id), "datai", false);	

		$idsnovetats = array();
		if (!empty($RegistreProjectesNov)){
			foreach ($RegistreProjectesNov as $key_projn => $value_projn) {
				array_push($idsnovetats, $value_projn[id]);
			}
		}	

		$ActuacionsProjecte = $dbb->Llistats("actuacions"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$id), "titol_ca", false);

		$TasquesProjecte = $dbb->Llistats("tasques"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$id), "titol_ca", false);

		/*
		if (!empty($RegistreProjecte)){
			// Per mosatrar només els usuaris que tenen vinculat el PAM/PAD al que pertany el projecte.
			$vinculaciodelproj = $RegistreProjecte[1][vinculacio];
			if (!empty($vinculaciodelproj)){
				$valors = json_decode($vinculaciodelproj,true);
				$valors = stripslashes_deep($valors); 
				$condiciovinc = "";
				$primer = false;
				$lastElement = end($valors);
				foreach ($valors as $key_vinc => $value_vinc) 
				{	
					if ($primer == false) { $condiciovinc .= " AND ( "; $primer = true; }
					$condiciovinc .= " FIND_IN_SET('".$value_vinc."' ,REPLACE( REPLACE( REPLACE( u.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";
					($value_vinc == $lastElement) ?  $condiciovinc .= " ) " : $condiciovinc .= " AND "; 
				}
			}

		}else{
			//$condiciovinc = " AND 1 = 2";
		}*/

		$Usuaris =  $dbb->FreeSql("SELECT u.*
								    FROM pfx_usuaris u
								    WHERE 1=1 $condiciovinc
								    ORDER BY cognoms ",
									  array());

		// Per defecte marquem tots els usuaris quan el registre no te dades.
		/*
		$arrayusuarisdef = array();
		if (empty($RegistreProjecte[1][usuaris])){
			if (!empty($Usuaris)){
				foreach ($Usuaris as $key => $value) {
					array_push($arrayusuarisdef, $value[id]);
				}
				$arrayusuarisdef = json_encode($arrayusuarisdef);
				$RegistreProjecte[1][usuaris] = $arrayusuarisdef;
			}
		}*/

		$Usuaris2 =  $dbb->FreeSql("SELECT u.*
								    FROM pfx_usuaris u
								    ORDER BY cognoms ",
									  array());

		
		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreProjecte' => $RegistreProjecte,
			'Paraules' => $Paraules,
			'Vinculacio' => $Vinculacio,
			'Districtes' => $Districtes,
			'Organse' => $Organse,
			'Operadors' => $Operadors,
			'idsnovetats' => $idsnovetats,
			'ActuacionsProjecte' => $ActuacionsProjecte,
			'TasquesProjecte' => $TasquesProjecte,
			'Usuaris' => $Usuaris,
			'Usuaris2' => $Usuaris2,
			);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if ($config['co'] == ""){
			echo $app['twig']->render('projectesform.html', $dadesplantilla);
		}else{
			echo $app['twig']->render('projectesconsulta.html', $dadesplantilla);
		}

		exit();
	}
	elseif ($taula == "2") 
	{
		$RegistreActuacio = $dbb->Llistats("actuacions"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		if (!empty($RegistreActuacio))
		{
			$Projectetriat = $dbb->Llistats("projectes"," $condicio_permis AND t.id = :id ",array("id"=>$RegistreActuacio[1][clau_projecte]), "titol_ca", false);

		}

		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 2 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 3  )
		{
			$condicio_permis = " AND  FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
								OR cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))." ";
		}

		$Projectes = $dbb->Llistats("projectes"," $condicio_permis ",array(), "titol_ca", false);

		/*
		$Usuaris =  $dbb->FreeSql("SELECT u.*
								    FROM pfx_usuaris u
								    WHERE clau_permisos > 1
								    ORDER BY nom ",
								  array());
								  */

		$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreActuacio' => $RegistreActuacio,
			'Paraules' => $Paraules,
			'Projectes' => $Projectes,
			'Districtes' => $Districtes,
			'Projectetriat' => $Projectetriat,
			'Usuaris' => $Usuaris,
			'AmbitsActuacions' => $AmbitsActuacions,
			'Projmotor' => $Projmotor,
			'ObjectiusProjecte' => $ObjectiusProjecte,
			'idsnovetats' => $idsnovetats,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if ($config['co'] == ""){
			echo $app['twig']->render('actuacionsform.html', $dadesplantilla);
		}else{
			echo $app['twig']->render('actuacionsconsulta.html', $dadesplantilla);
		}

		exit();
	}
	elseif ($taula == "3") 
	{
		$RegistreTasca = $dbb->Llistats("tasques"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		if (!empty($RegistreTasca))
		{
			$Actuacions = $dbb->Llistats("actuacions"," AND clau_projecte = :clau_projecte ", array("clau_projecte"=>$RegistreTasca[1][clau_projecte]),"t.id", false);
			
			$ActuacioTriada = $dbb->Llistats("actuacions"," AND id = :clau_actuacio ", array("clau_actuacio"=>$RegistreTasca[1][clau_actuacio]),"t.id", false);
		

		}

		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 2 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 3  )
		{
			$condicio_permis = " AND  FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
								 OR cap_projecte = ".$app['session']->get(constant('General::nomsesiouser'))." ";
		}

		$Projectes = $dbb->Llistats("projectes"," $condicio_permis ",array(), "titol_ca", false);

		$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

		
		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreTasca' => $RegistreTasca,
			'Paraules' => $Paraules,
			'Projectes' => $Projectes,
			'Actuacions' => $Actuacions,
			'Usuaris' => $Usuaris,
			'Agents' => $Agents,
			'Passos' => $Passos,
			'totalpresuactuacio' => $totalpresuactuacio,
			'ActuacioTriada' => $ActuacioTriada,
			'ColectiusTasques' => $ColectiusTasques,
			'AmbitsActuacions' => $AmbitsActuacions, // Es fa servir el mateix llistat d'actuacions.
			'AmbitsActuacionsSub' => $AmbitsActuacionsSub,
			'Projmotor' => $Projmotor,
			'Organse' => $Organse,
			'Operadors' => $Operadors,
			'Districtes' => $Districtes,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if ($config['co'] == ""){
			echo $app['twig']->render('tasquesform.html', $dadesplantilla);
		}else{
			echo $app['twig']->render('tasquesconsulta.html', $dadesplantilla);
		}

		exit();
	}
	elseif ($taula == "4")
	{
		
		
		$RegistreU = $dbb->FreeSql("SELECT u.*
								    FROM pfx_usuaris u
								    WHERE u.id = :id
								    ORDER BY nom ",

								   array("id"=>$id)
								 );


		// Permisos
		$claupermisos = $app['session']->get(constant('General::nomsesiouser')."-permisos");
		$Permisos = $dbb->Llistats("permisos"," AND estat = true  AND id >= :id",array("id"=>$claupermisos),"nom_ca");

		$Organse = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");

		//$Vinculacio = $dbb->Llistats("projectes_vinculacio"," ", array(), "1*SUBSTRING_INDEX(titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(titol_ca, '.', -1) ASC");


		$dadesplantilla = array (
			'pagina' => 3,
			'RegistreU' => $RegistreU,
			'Permisos' => $Permisos,
			'Organse' => $Organse,
			//'Vinculacio' => $Vinculacio,

		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if (!empty($RegistreU))
		{
			echo $app['twig']->render('usuarisform.html', $dadesplantilla);
		}
		

		exit();
	}
	elseif ($taula == "5")
	{
		$RegistreProjectesDim = $dbb->Llistats("projectes_dimensions"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			

		
		$dadesplantilla = array (
			'pagina' => 22,
			'RegistreProjectesDim' => $RegistreProjectesDim,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-dimensionsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "6")
	{
		$RegistreProjectesOrig = $dbb->Llistats("projectes_fites_tipologies"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			

		
		$dadesplantilla = array (
			'pagina' => 23,
			'RegistreProjectesOrig' => $RegistreProjectesOrig,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-origensform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "7")
	{
		$RegistrePas = $dbb->Llistats("passos"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 8,
			'RegistrePas' => $RegistrePas,
			'ctf' => $config['ctf'],
			'e' => $config['e'], // estat 1 situacio actual, 2 consolidat
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('passosform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "8")
	{

		$RegistreObjectiu = $dbb->Llistats("projectes_objectius"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreObjectiu' => $RegistreObjectiu,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-objectiusform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "9")
	{

		$RegistreIndicador = $dbb->Llistats("projectes_indicadors"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreIndicador' => $RegistreIndicador,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-indicadorsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "10")
	{
		$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND t.id =:id ",array("id"=>$id), "1*SUBSTRING_INDEX(titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(titol_ca, '.', -1) ASC", false);			

		
		$dadesplantilla = array (
			'pagina' => 27,
			'RegistreVinculacio' => $RegistreVinculacio,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-vinculacioform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "12")
	{
		
		$Upfiles = $dbb->Llistats("upfile"," AND t.id = :id ",array("id"=>$id), "descripcio_ca", false);	

		if ($config['ctf'] == "" && $config['ctf'] == "")
		{
			

			// ------------------------------------ Imatges. ------------------------------------

				$tipus = 1;
				$formats = "*.jpg; *.jpeg; *.gif; *.png;";
				$Imatges .= '<div id="queueimg">';
				$Imatges .= '<div id="galeriaimatges"> ';
					//include("galeria.php"); 
				$Imatges .= '</div></div> ';

				$Imatges .= $dbb->GenerarUpload($tipus, "login", $id, '', 'queueimg', 'galeriaimatges', $formats,'','210','130');


			// ------------------------------------ Final imatges. ------------------------------------		

			
			$dadesplantilla = array (
				'pagina' => 21,
				'Upfiles' => $Upfiles,
				'Imatges' => $Imatges,
				'idiomes' => $idiomes,
			);
		}
		else
		{
			$dadesplantilla = array (
				'Upfiles' => $Upfiles,
				'ctf' => $config['ctf'],
				'tf' => $config['tf'],
			);
		}


		

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if ($config['ctf'] == "" && $config['ctf'] == "")
		{
			echo $app['twig']->render('dadesloginform.html', $dadesplantilla);
		}
		else
		{
			echo $app['twig']->render('upfiles.html', $dadesplantilla);
		}

		exit();
	}
	elseif ($taula == "14")
	{

		$RegistreOrgans = $dbb->Llistats("projectes_organs"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 6,
			'RegistreOrgans' => $RegistreOrgans,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-organsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "15")
	{
		$RegistreDistrictes = $dbb->Llistats("projectes_districtes"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			

		
		$dadesplantilla = array (
			'pagina' => 29,
			'RegistreDistrictes' => $RegistreDistrictes,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-districtesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "16")
	{
		$RegistreProjectesOrge = $dbb->Llistats("projectes_organs_e"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			

		
		$dadesplantilla = array (
			'pagina' => 30,
			'RegistreProjectesOrge' => $RegistreProjectesOrge,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-organseform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "17")
	{
		$RegistreProjectesAgents = $dbb->Llistats("projectes_agents"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			

		
		$dadesplantilla = array (
			'pagina' => 31,
			'RegistreProjectesAgents' => $RegistreProjectesAgents,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-agentsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "18")
	{
		$RegistreRol = $dbb->Llistats("permisos"," AND t.id = :id  ", array("id"=>$id) ,"nom_ca", false);			

		// Permisosd
		$Permisosd = $dbb->Llistats("permisosd"," AND t.clau_pla = :idpla AND t.estat = true AND clau_rol = :id ", array("id"=>$id, "idpla"=>$idpla) ,"t.id");
	
		// Permisos en array.
		$cats = array();
		if(!empty($Permisosd)) {
	    	foreach($Permisosd as $key => $value) {
	    		array_push($cats, $value['clau_pagina']);
	    	}
	    }

		$Permisos = $dbb->ArbrePagines('0', '', $cats);
		
		$dadesplantilla = array (
			'pagina' => 32,
			'RegistreRol' => $RegistreRol,
			'Permisos' => $Permisos,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('rolsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "19")
	{
		$RegistrePle = $dbb->Llistats("plens"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false, array("descripcio_ca"));			
		
		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		// AreesAct
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 41,
			'RegistrePle' => $RegistrePle,
			'Usuaris' => $Usuaris,
			'AreesAct' => $AreesAct,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		if ($config['co'] == ""){
			echo $app['twig']->render('plensform.html', $dadesplantilla);
		}else{
			echo $app['twig']->render('plensconsulta.html', $dadesplantilla);
		}

		exit();
	}
	elseif ($taula == "20")
	{
		$RegistrePlensArees = $dbb->Llistats("plens_arees"," AND t.id = :id ", array("id"=>$id) ,"titol_ca", false);			
		
		$dadesplantilla = array (
			'pagina' => 42,
			'RegistrePlensArees' => $RegistrePlensArees,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('plens-areesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "21")
	{
		$RegistreDecret = $dbb->Llistats("decrets"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
	
		// AreesAct
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 43,
			'RegistreDecret' => $RegistreDecret,
			'AreesAct' => $AreesAct,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('decretsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "22")
	{
		$RegistreComissio = $dbb->Llistats("comissiopermanent"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false, array("descripcio_ca"));	

		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");		
		
		// AreesAct
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 44,
			'RegistreComissio' => $RegistreComissio,
			'Usuaris' => $Usuaris,
			'AreesAct' => $AreesAct,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('comissiopermanentform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "23")
	{
		$RegistreConsell = $dbb->Llistats("consellparticipacio"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		
		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		// AreesAct
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 45,
			'RegistreConsell' => $RegistreConsell,
			'Usuaris' => $Usuaris,
			'AreesAct' => $AreesAct,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('consellparticipacioform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "24")
	{
		$RegistreComissio = $dbb->Llistats("comissioinformativa"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		
		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");
		
		$dadesplantilla = array (
			'pagina' => 46,
			'RegistreComissio' => $RegistreComissio,
			'Usuaris' => $Usuaris,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('comissioinformativaform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "25")
	{
		$RegistreOrgan = $dbb->Llistats("organdeliberatiu"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false,array("descripcio_ca"));			
		
		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		// AreesAct
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 47,
			'RegistreOrgan' => $RegistreOrgan,
			'Usuaris' => $Usuaris,
			'AreesAct' => $AreesAct,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('organdeliberatiuform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "26")
	{
		$RegistreProces = $dbb->Llistats("processos"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		
		$dadesplantilla = array (
			'pagina' => 48,
			'RegistreProces' => $RegistreProces,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('processosform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "28")
	{

		$RegistreIndicador = $dbb->Llistats("processos_indicadors"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);	

		$valorultim = $dbb->FreeSql("SELECT DATE_FORMAT(datai, '%d/%m/%Y') as datai2, valor 
			                         FROM pfx_processos_indicadors_valors 
			                         WHERE parent_id = :parent_id 
			                         ORDER BY datai desc
			                         LIMIT 1",array("parent_id"=>$id));	
		
		if(!empty($valorultim) && !empty($RegistreIndicador))
		{
			if (!empty($RegistreIndicador[1][objectiu]) && is_numeric($valorultim[1][valor]))
			{
				if (is_numeric($RegistreIndicador[1][objectiu])) {
					$valorultim[1][valor] = ( $valorultim[1][valor] / $RegistreIndicador[1][objectiu] ) * 100;
					$valorultim[1][valor] = number_format($valorultim[1][valor],2);
					$valorultim[1][datai] = "a ".$valorultim[1][datai2];
				}
			}
			else
			{
				$valorultim[1][valor] = 0;
				$valorultim[1][datai] = "";
			}
		}
		else
		{
			$valorultim[1][valor] = 0;
			$valorultim[1][datai] = "";
		}


		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		$dadesplantilla = array (
			'pagina' => 49,
			'RegistreIndicador' => $RegistreIndicador,
			'Usuaris' => $Usuaris,
			'valorultim' => $valorultim,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('processos-indicadorsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "29")
	{
		

		$RegistreProcessosValors = $dbb->Llistats("processos_indicadors_valors"," AND t.id = :id ",array("id"=>$id), "descripcio_ca", false);

		if (!empty($RegistreProcessosValors))
		{
			$valors = json_decode($RegistreProcessosValors[1][valor],true);
			$objectiu = json_decode($RegistreProcessosValors[1][valor_objectiu],true); // Si venim a modificar sortirà d'aqui les dades dels textos
			if (!empty($objectiu)) {
				if ( is_array($objectiu) ){
					$numvalors = max(array_keys($objectiu));
				}
			}
			$RegistreProces = $dbb->Llistats("processos_indicadors"," AND t.id = :id ",array("id"=>$RegistreProcessosValors[1][parent_id]), "id", false);	
			if (!empty($RegistreProces))
			{
				$tipusindicador = $RegistreProces[1][tipusindicador];
			}
		}

		// Extreure valor objectiu actual.
		if ($id == 0) 
		{
			$numvalors = $config['num'];
			$RegistreProces = $dbb->Llistats("processos_indicadors"," AND t.id = :id ",array("id"=>$config['ctf']), "id", false);	
			if (!empty($RegistreProces))
			{
				$objectiu = json_decode($RegistreProces[1][objectiu],true);
				$tipusindicador = $RegistreProces[1][tipusindicador];
			}
		}
		

		$dadesform = '
			
			<div class="col-lg-2">
	            <label>Data del càlcul</label><br>
	            <input class="form-control datepicker required ignore" name="datai" id="datai" placeholder="dd/mm/aaaa" value="'.(isset($RegistreProcessosValors[1][datai])?($RegistreProcessosValors[1][datai] != "0000-00-00"?date("d/m/Y",strtotime($RegistreProcessosValors[1][datai])):""):"").'">
	        </div>

	        <div class="col-lg-10">
	        	
	    ';

	    if ($tipusindicador != 2 && $tipusindicador != 1)
	    {
	    	 for ($i=1; $i < $numvalors+1; $i++) { 
			
				if (isset($objectiu[$i]))
				{

					$dadesform .= '
						<div class="row" style="margin-top:5px;">
				            <input type="hidden" name="textvalor[]" value="'.$objectiu[$i][text].'" />
				            <div class="col-lg-3">
					            <label>'.$objectiu[$i][text].'</label>
					            <input class="form-control required ignore" name="valors[]"  value="'.(isset($valors[$i][valor])?$valors[$i][valor]:"").'">
					        </div>
					        
					        <div class="col-lg-8">
					            <label>Detalls</label>
					            <input class="form-control" name="detalls[]" value="'.(isset($valors[$i][detalls])?$valors[$i][detalls]:"").'">
					        </div>
					    </div>
					';
				}
			}
	    }
	    elseif ($tipusindicador == 2) 
	    {
	    	$LlistatProcessosTipus = $dbb->Llistats("processos_indicadors_tipus"," AND t.parent_id = :parent_id ",array("parent_id"=>$config['ctf']), "descripcio_ca", false);	

	    	$dadesform .= '
				<div class="col-lg-3" style="margin-top:5px;">
		            <label></label>
		            <input type="hidden" name="textvalor[]" class="textvalorind" value="" />
			        <select class="form-control required selecttipusind" name="valors[]" >
			          <option value="" selected >Seleccioni</option>
			';			
						if (!empty($LlistatProcessosTipus))
						{
							foreach ($LlistatProcessosTipus as $key_t => $value_t) 
							{
								$dadesform .= ' <option value="'.$value_t[id].'" '.(isset($valors[1][valor])?($valors[1][valor]==$value_t[id]?"selected":""):"").' >'.$value_t[descripcio].'</option>';
							}
						}
						
			           
			$dadesform .= '      
			        </select>
			    </div>

			    <script>
					$(document).off("change",".selecttipusind").on("change",".selecttipusind",function(event){
						
						var text = $(".selecttipusind option:selected").text();
						$(".textvalorind").val(text);


            		});
			    </script>
			';
	    }
	    elseif ($tipusindicador == 1) 
	    {
	    	$dadesform .= '
				<div class="row" style="margin-top:5px;">
		            <input type="hidden" name="textvalor[]" value="" />
		            <input type="hidden" name="detalls[]" value="" />
			        <div class="col-lg-8">
			            <label>Detalls</label>
			            <input class="form-control" name="valors[]" value="'.(isset($valors[$i][valor])?$valors[$i][valor]:"").'">
			        </div>
			    </div>
			';
	    }

	   

		$dadesform .= '</div>';


		$dadesplantilla = array (
			'pagina' => 50,
			'RegistreProcessosValors' => $RegistreProcessosValors,
			'RegistreProces' => $RegistreProces,
			'ctf' => $config['ctf'],
			'dadesform' => $dadesform,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('processos-indicadorsvalors.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "30")
	{
		$RegistreBustia = $dbb->Llistats("bustialocal"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		
		// Usuaris.
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		// AreesAct.
		$AreesAct = $dbb->Llistats("plens_arees"," ", array() ,"titol_ca");

		// AmbitsBustia.
		$AmbitsBustia = $dbb->Llistats("bustiaambits"," ", array() ,"titol_ca");
		
		$dadesplantilla = array (
			'pagina' => 51,
			'RegistreBustia' => $RegistreBustia,
			'Usuaris' => $Usuaris,
			'AreesAct' => $AreesAct,
			'AmbitsBustia' => $AmbitsBustia,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('bustialocalform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "31")
	{
		$RegistreBustiaAmbit = $dbb->Llistats("bustiaambits"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		

		$dadesplantilla = array (
			'pagina' => 52,
			'RegistreBustiaAmbit' => $RegistreBustiaAmbit,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('bustia-ambitsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "32")
	{

		$RegistreProcessosTipus = $dbb->Llistats("processos_indicadors_tipus"," AND t.id = :id ",array("id"=>$id), "descripcio_ca", false);	

		if ($config['p'] == 1)
		{
			$LlistatProcessosTipus = $dbb->Llistats("processos_indicadors_tipus"," AND t.parent_id = :parent_id ",array("parent_id"=>$config['ctf']), "descripcio_ca", false);	
		}		
		
		$dadesplantilla = array (
			'pagina' => 53,
			'RegistreProcessosTipus' => $RegistreProcessosTipus,
			'LlistatProcessosTipus' => $LlistatProcessosTipus, // per quan es vol carregar el llistat al select.
			'ctf' => $config['ctf'],
			'p' => $config['p'],
			'vo' => $config['vo'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('processos-indicadorstipus.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "34")
	{

		$RegistreObjectiu = $dbb->Llistats("actuacions_objectius"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 7,
			'RegistreObjectiu' => $RegistreObjectiu,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('actuacions-objectiusform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "35")
	{
		$RegistreActuacionsAmbit = $dbb->Llistats("actuacions_ambits"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		

		$dadesplantilla = array (
			'pagina' => 56,
			'RegistreActuacionsAmbit' => $RegistreActuacionsAmbit,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('actuacions-ambitsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "36")
	{
		$RegistreTasquesColectius = $dbb->Llistats("tasques_colectius"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);			
		

		$dadesplantilla = array (
			'pagina' => 57,
			'RegistreTasquesColectius' => $RegistreTasquesColectius,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('tasques-colectiusform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "37")
	{
		$RegistreDintres = $dbb->Llistats("dintres"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		// Usuaris.
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u ORDER BY cognoms "); // WHERE clau_permisos > 1
		
		$dadesplantilla = array (
			'pagina' => 58,
			'RegistreDintres' => $RegistreDintres,
			'Usuaris' => $Usuaris,
			'Paraules' => $Paraules,
			'Dimensions' => $Dimensions,
			'Origens' => $Origens,
			'Objectius' => $Objectius,
			'Vinculacio' => $Vinculacio,
			'Districtes' => $Districtes,
			'Organse' => $Organse,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintresform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "38")
	{
		$RegistreContactes = $dbb->Llistats("dintres_contactes"," AND t.id = :id ", array("id"=>$id), "titol_ca", false, array("descripcio_ca","cp","ciutat","telefon","email"));

		$dadesplantilla = array (
			'pagina' => 58,
			'RegistreContactes' => $RegistreContactes,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-contactesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "39")
	{
		$RegistresObservacions = $dbb->Llistats("dintres_observacions"," AND t.id = :id ", array("id"=>$id), "titol_ca", false, array("descripcio_ca"));
		
		$dadesplantilla = array (
			'pagina' => 60,
			'RegistresObservacions' => $RegistresObservacions,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-observacionsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "40")
	{
		$RegistreProjecte = $dbb->Llistats("dintres_projectes"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		$dadesplantilla = array (
			'pagina' => 61,
			'RegistreProjecte' => $RegistreProjecte,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-projectesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "41")
	{
		$RegistreProjecteTecnic = $dbb->Llistats("dintres_projectes_tecnics"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);

		$dadesplantilla = array (
			'pagina' => 62,
			'RegistreProjecteTecnic' => $RegistreProjecteTecnic,
			'ctf' => $config['ctf'],
			'ctf2' => $config['ctf2'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-projectes-tecnicsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "42")
	{
		$RegistresObservacions = $dbb->Llistats("dintres_observacions_tecniques"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);
		
		$dadesplantilla = array (
			'pagina' => 63,
			'RegistresObservacions' => $RegistresObservacions,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-observacions-tecniquesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "43")
	{
		$RegistresSancionador = $dbb->Llistats("dintres_sancionador"," AND t.id = :id ", array("id"=>$id), "titol_ca", false);
		
		$dadesplantilla = array (
			'pagina' => 64,
			'RegistresSancionador' => $RegistresSancionador,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-sancionadorform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "44")
	{
		$RegistresSancionadorPropietari = $dbb->Llistats("dintres_sancionador_propietaris"," AND t.id = :id ", array("id"=>$id), "titol_ca", false, array("descripcio_ca","cp","ciutat","telefon","email","observacions"));

		$dadesplantilla = array (
			'pagina' => 65,
			'RegistresSancionadorPropietari' => $RegistresSancionadorPropietari,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('pladintres-sancionador-propietarisform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "45")
	{

		$RegistreFites = $dbb->Llistats("projectes_fites"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);	

		if (!empty($config['ctf'])){
			$RegistreProjecte = $dbb->Llistats("projectes"," AND t.id = :id ",array("id"=>$config['ctf']), "titol_ca", false);			
			if (!empty($RegistreProjecte)){
				$mindate = ($RegistreProjecte[1]["inici"] != "0000-00-00"? "minDate: new Date(".date("Y", strtotime($RegistreProjecte[1]["inici"])).", ".date("n", strtotime($RegistreProjecte[1]["inici"]))." - 1, ".date("j", strtotime($RegistreProjecte[1]["inici"]))."), ":"");
				$maxdate = ($RegistreProjecte[1]["final"] != "0000-00-00"? "maxDate: new Date(".date("Y", strtotime($RegistreProjecte[1]["final"])).", ".date("n", strtotime($RegistreProjecte[1]["final"]))." - 1, ".date("j", strtotime($RegistreProjecte[1]["final"]))."), ":"");
			}
		}

		$Tipologies = $dbb->Llistats("projectes_fites_tipologies"," ", array() ,"titol_ca");

		$dadesplantilla = array (
			'pagina' => 66,
			'RegistreFites' => $RegistreFites,
			'Tipologies' => $Tipologies,
			'mindate' => $mindate,
			'maxdate' => $maxdate,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-fitesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "46")
	{

		$RegistreFites = $dbb->Llistats("tasques_fites"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);			


		$dadesplantilla = array (
			'pagina' => 67,
			'RegistreFites' => $RegistreFites,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('tasques-fitesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "47")
	{

		$RegistreOperador = $dbb->Llistats("projectes_operadors"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 68,
			'RegistreOperador' => $RegistreOperador,
			'ctf' => $config['ctf'],
			'ctf2' => $config['ctf2'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-operadorsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "48")
	{

		$RegistreFites = $dbb->Llistats("projectes_fites_com"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);

		$Tasques = $dbb->Llistats("tasques"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$config['ctf']), "titol_ca", true);			


		$dadesplantilla = array (
			'pagina' => 69,
			'RegistreFites' => $RegistreFites,
			'Tasques' => $Tasques,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-fitescomform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "49")
	{

		$RegistreFites = $dbb->Llistats("projectes_fites_part"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);	

		$Tasques = $dbb->Llistats("tasques"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$config['ctf']), "titol_ca", true);		


		$dadesplantilla = array (
			'pagina' => 70,
			'RegistreFites' => $RegistreFites,
			'Tasques' => $Tasques,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-fitespartform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "50")
	{
		$RegistreProjectesProjMotor = $dbb->Llistats("projectes_projmotor"," AND t.id = :id  ", array("id"=>$id) ,"titol_ca", false);	

		$RegistreProjmotorNov = $dbb->Llistats("projectes_projmotor_nov"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$id), "datai", false);	

		$idsnovetats = array();
		if (!empty($RegistreProjmotorNov)){
			foreach ($RegistreProjmotorNov as $key_projn => $value_projn) {
				array_push($idsnovetats, $value_projn[id]);
			}
		}			

		$dadesplantilla = array (
			'pagina' => 71,
			'RegistreProjectesProjMotor' => $RegistreProjectesProjMotor,
			'ctf' => $config['ctf'],
			'idsnovetats' => $idsnovetats,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-projmotorform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "51")
	{

		$RegistreIndicador = $dbb->Llistats("projectes_indicadors_nou"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);	

		$valorultim = $dbb->FreeSql("SELECT DATE_FORMAT(datai, '%d/%m/%Y') as datai2, valor 
			                         FROM pfx_projectes_indicadors_valors 
			                         WHERE parent_id = :parent_id 
			                         ORDER BY datai desc
			                         LIMIT 1",array("parent_id"=>$id));	
		
		if(!empty($valorultim) && !empty($RegistreIndicador))
		{
			if (!empty($RegistreIndicador[1][objectiu]) && is_numeric($valorultim[1][valor]))
			{
				if (is_numeric($RegistreIndicador[1][objectiu])) {
					$valorultim[1][valor] = ( $valorultim[1][valor] / $RegistreIndicador[1][objectiu] ) * 100;
					$valorultim[1][valor] = number_format($valorultim[1][valor],2);
					$valorultim[1][datai] = "a ".$valorultim[1][datai2];
				}
			}
			else
			{
				$valorultim[1][valor] = 0;
				$valorultim[1][datai] = "";
			}
		}
		else
		{
			$valorultim[1][valor] = 0;
			$valorultim[1][datai] = "";
		}


		// Usuaris
		$Usuaris =  $dbb->FreeSql("SELECT u.* FROM pfx_usuaris u WHERE clau_permisos > 1 ORDER BY cognoms ");

		$dadesplantilla = array (
			'pagina' => 49,
			'RegistreIndicador' => $RegistreIndicador,
			'Usuaris' => $Usuaris,
			'valorultim' => $valorultim,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-indicadorsnouform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "52")
	{
		

		$RegistreProcessosValors = $dbb->Llistats("projectes_indicadors_valors"," AND t.id = :id ",array("id"=>$id), "descripcio_ca", false);

		if (!empty($RegistreProcessosValors))
		{
			$valors = json_decode($RegistreProcessosValors[1][valor],true);
			$objectiu = json_decode($RegistreProcessosValors[1][valor_objectiu],true); // Si venim a modificar sortirà d'aqui les dades dels textos
			if (!empty($objectiu)) {
				if ( is_array($objectiu) ){
					$numvalors = max(array_keys($objectiu));
				}
			}
			$RegistreProces = $dbb->Llistats("projectes_indicadors_nou"," AND t.id = :id ",array("id"=>$RegistreProcessosValors[1][parent_id]), "id", false);	
			if (!empty($RegistreProces))
			{
				$tipusindicador = $RegistreProces[1][tipusindicador];
			}
		}

		// Extreure valor objectiu actual.
		if ($id == 0) 
		{
			$numvalors = $config['num'];
			$RegistreProces = $dbb->Llistats("projectes_indicadors_nou"," AND t.id = :id ",array("id"=>$config['ctf']), "id", false);	
			if (!empty($RegistreProces))
			{
				$objectiu = json_decode($RegistreProces[1][objectiu],true);
				$tipusindicador = $RegistreProces[1][tipusindicador];
			}
		}
		

		$dadesform = '
			
			<div class="col-lg-2">
	            <label>Data del càlcul</label><br>
	            <input class="form-control datepicker required ignore" name="datai" id="datai12" placeholder="dd/mm/aaaa" value="'.(isset($RegistreProcessosValors[1][datai])?($RegistreProcessosValors[1][datai] != "0000-00-00"?date("d/m/Y",strtotime($RegistreProcessosValors[1][datai])):""):"").'">
	        </div>

	        <div class="col-lg-10">
	        	
	    ';

	    if ($tipusindicador != 2 && $tipusindicador != 1)
	    {
	    	 for ($i=1; $i < $numvalors+1; $i++) { 
			
				if (isset($objectiu[$i]))
				{

					$dadesform .= '
						<div class="row" style="margin-top:5px;">
				            <input type="hidden" name="textvalor[]" value="'.$objectiu[$i][text].'" />
				            <div class="col-lg-3">
					            <label>'.$objectiu[$i][text].'</label>
					            <input class="form-control required ignore" name="valors[]"  value="'.(isset($valors[$i][valor])?$valors[$i][valor]:"").'">
					        </div>
					        
					        <div class="col-lg-8">
					            <label>Detalls</label>
					            <input class="form-control" name="detalls[]" value="'.(isset($valors[$i][detalls])?$valors[$i][detalls]:"").'">
					        </div>
					    </div>
					';
				}
			}
	    }
	    elseif ($tipusindicador == 2) 
	    {
	    	$LlistatProcessosTipus = $dbb->Llistats("projectes_indicadors_tipus"," AND t.parent_id = :parent_id ",array("parent_id"=>$config['ctf']), "descripcio_ca", false);	

	    	$dadesform .= '
				<div class="col-lg-3" style="margin-top:5px;">
		            <label></label>
		            <input type="hidden" name="textvalor[]" class="textvalorind" value="" />
			        <select class="form-control required selecttipusind" name="valors[]" >
			          <option value="" selected >Seleccioni</option>
			';			
						if (!empty($LlistatProcessosTipus))
						{
							foreach ($LlistatProcessosTipus as $key_t => $value_t) 
							{
								$dadesform .= ' <option value="'.$value_t[id].'" '.(isset($valors[1][valor])?($valors[1][valor]==$value_t[id]?"selected":""):"").' >'.$value_t[descripcio].'</option>';
							}
						}
						
			           
			$dadesform .= '      
			        </select>
			    </div>

			    <script>
					$(document).off("change",".selecttipusind").on("change",".selecttipusind",function(event){
						
						var text = $(".selecttipusind option:selected").text();
						$(".textvalorind").val(text);


            		});
			    </script>
			';
	    }
	    elseif ($tipusindicador == 1) 
	    {
	    	$dadesform .= '
				<div class="row" style="margin-top:5px;">
		            <input type="hidden" name="textvalor[]" value="" />
		            <input type="hidden" name="detalls[]" value="" />
			        <div class="col-lg-8">
			            <label>Detalls</label>
			            <input class="form-control" name="valors[]" value="'.(isset($valors[$i][valor])?$valors[$i][valor]:"").'">
			        </div>
			    </div>
			';
	    }

	   

		$dadesform .= '</div>';


		$dadesplantilla = array (
			'pagina' => 72,
			'RegistreProcessosValors' => $RegistreProcessosValors,
			'RegistreProces' => $RegistreProces,
			'ctf' => $config['ctf'],
			'dadesform' => $dadesform,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-indicadorsvalors.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "53")
	{

		$RegistreProcessosTipus = $dbb->Llistats("projectes_indicadors_tipus"," AND t.id = :id ",array("id"=>$id), "descripcio_ca", false);	

		if ($config['p'] == 1)
		{
			$LlistatProcessosTipus = $dbb->Llistats("projectes_indicadors_tipus"," AND t.parent_id = :parent_id ",array("parent_id"=>$config['ctf']), "descripcio_ca", false);	
		}		
		
		$dadesplantilla = array (
			'pagina' => 74,
			'RegistreProcessosTipus' => $RegistreProcessosTipus,
			'LlistatProcessosTipus' => $LlistatProcessosTipus, // per quan es vol carregar el llistat al select.
			'ctf' => $config['ctf'],
			'p' => $config['p'],
			'vo' => $config['vo'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-indicadorstipus.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "54")
	{

		$RegistreURL = $dbb->Llistats("projectes_urls"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 75,
			'RegistreURL' => $RegistreURL,
			'ctf' => $config['ctf'],
			'ctf2' => $config['ctf2'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-urlsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "55")
	{

		$RegistreURL = $dbb->Llistats("actuacions_urls"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 76,
			'RegistreURL' => $RegistreURL,
			'ctf' => $config['ctf'],
			'ctf2' => $config['ctf2'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('actuacions-urlsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "56")
	{

		$RegistreURL = $dbb->Llistats("tasques_urls"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);			


		$dadesplantilla = array (
			'pagina' => 77,
			'RegistreURL' => $RegistreURL,
			'ctf' => $config['ctf'],
			'ctf2' => $config['ctf2'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('tasques-urlsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "57")
	{

		$RegistreProjecteGen = $dbb->Llistats("projectes_general"," AND t.id = :id ",array("id"=>$id), "datai desc", false);			


		$dadesplantilla = array (
			'pagina' => 78,
			'RegistreProjecteGen' => $RegistreProjecteGen,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-generalform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "58")
	{

		$RegistreProjectesNov = $dbb->Llistats("projectes_novetats"," AND t.id = :id ",array("id"=>$id), "datai", false);			


		$dadesplantilla = array (
			'pagina' => 79,
			'RegistreProjectesNov' => $RegistreProjectesNov,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-novetatsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "59")
	{

		$RegistreFites = $dbb->Llistats("fites_marc"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);			


		$dadesplantilla = array (
			'pagina' => 66,
			'RegistreFites' => $RegistreFites,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('fitesmarcform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "60")
	{

		$RegistreProjectesMotorNov = $dbb->Llistats("projectes_projmotor_nov"," AND t.id = :id ",array("id"=>$id), "datai", false);			


		$dadesplantilla = array (
			'pagina' => 81,
			'RegistreProjectesMotorNov' => $RegistreProjectesMotorNov,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-projmotornovetatsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "61")
	{

		$RegistreFites = $dbb->Llistats("projectes_projmotor_fites"," AND t.id = :id ",array("id"=>$id), "observacionsfita", false);			


		$dadesplantilla = array (
			'pagina' => 82,
			'RegistreFites' => $RegistreFites,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-projmotorfitesform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "62")
	{

		$RegistreActuacionsNov = $dbb->Llistats("actuacions_novetats"," AND t.id = :id ",array("id"=>$id), "datai", false);			


		$dadesplantilla = array (
			'pagina' => 79,
			'RegistreActuacionsNov' => $RegistreActuacionsNov,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('actuacions-novetatsform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "63")
	{

		$RegistreProjectesNov = $dbb->Llistats("projectes_riscos"," AND t.id = :id ",array("id"=>$id), "datai", false);			


		$dadesplantilla = array (
			'pagina' => 85,
			'RegistreProjectesNov' => $RegistreProjectesNov,
			'ctf' => $config['ctf'],
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-riscosform.html', $dadesplantilla);

		exit();
	}
	elseif ($taula == "65")
	{

		$RegistreConceptes = $dbb->Llistats("projectes_conceptes"," AND t.id = :id ",array("id"=>$id), "titol_ca", false);		

		// Verificar el format de les dates.
		$inici = $config['d1'];
		$final = $config['d2'];

		$myregex = '~^\d{2}/\d{2}/\d{4}$~';
		if (!DateTime::createFromFormat('d/m/Y', $inici) || !DateTime::createFromFormat('d/m/Y', $final) || empty($inici) || empty($final))
		{
			echo "Format de data incorrecte.";exit();
		}

		// Extreure any de la data.
		$inici= date_create_from_format("d/m/Y",$inici); 
		$inicitime = strtotime(date_format($inici, 'Y-m-d'));
	    $inici= date_format($inici, 'Y');
	    $final= date_create_from_format("d/m/Y",$final); 
	    $finaltime = strtotime(date_format($final, 'Y-m-d'));
	    $final= date_format($final, 'Y');

	    // Verificar data final > inicial.
	    if ($inicitime > $finaltime)
	    {
	    	echo "La data final ha de ser més gran que la inicial."; exit();
	    }
	    
	    // Calcul anys.
	    //$anys = ($final - $inici) + 1;	


		$dadesplantilla = array (
			'pagina' => 87,
			'RegistreConceptes' => $RegistreConceptes,
			'ctf' => $config['ctf'],
			'inici' => $inici,
			'final' => $final,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('projectes-conceptesform.html', $dadesplantilla);

		exit();
	}


	//---------------------------------------


	/*
	
	if ($taula == "upfile")
	{
		$Upfiles = $dbb->Llistats("upfile"," AND t.id = $id ", "descripcio_ca", false);	

		// ------------------------------------ Imatges. ------------------------------------

			$tipus = 1;
			$formats = "*.jpg; *.jpeg; *.gif; *.png;";
			$Imatges .= '<div id="queueimg">';
			$Imatges .= '<div id="galeriaimatges"> ';
				//include("galeria.php"); 
			$Imatges .= '</div></div> ';

			$Imatges .= $dbb->GenerarUpload($tipus, "login", $id, '', 'queueimg', 'galeriaimatges', $formats,'','210','130');


		// ------------------------------------ Final imatges. ------------------------------------		

		
		$dadesplantilla = array (
			'pagina' => 21,
			'Upfiles' => $Upfiles,
			'Imatges' => $Imatges,
		);

		echo $app['twig']->render('dadesloginform.html', $dadesplantilla);

		exit();
	}
	
	*/

	if ($taula == "9999")
	{
		
		/*
		if (isset($_POST['id_prov'])){
			$id_prov = filter_var(htmlspecialchars(trim($_POST['id_prov'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		}
		if($id != "" && $id != 0){
			$id_prov = 0;
		}
		if (isset($_POST['tipus'])){
			$tipus = filter_var(htmlspecialchars(trim($_POST['tipus'])), FILTER_SANITIZE_NUMBER_INT);
		}
		*/

		if ($id != 1 && !empty($id))
		{
			$condicio = " AND id = $id";
		}
		else
		{
			$condicio = " AND clau_prov = '".$app['session']->get('clau_prov')."'";
		}


		// Upfiles.
		$Upfiles = $dbb->Llistats("upfile"," AND tipus = 1 AND taula = '".$config[0]['t']."' $condicio ","id");

		
		$dadesplantilla = array (
			'pagina' => 999,
			'Upfiles' => $Upfiles,
			'idiomes' => $idiomes
		);

		echo $app['twig']->render('uploadview.html', $dadesplantilla);

		exit();
	}



	/*
	if ($taula == "tipus_actuacions")
	{
		$RegistreTipusAct = $dbb->Llistats("tipus_actuacions"," AND t.id = $id ", "titol_$idioma", false);			

		// Tipus camps.
		$TipusCamps = $dbb->Llistats("tipus_camps"," ", "titol_$idioma" );

		// Grups Consulta
		$GrupsConsulta = $dbb->Llistats("grups_consulta"," ", "titol_$idioma" );

		// CampsLliures
		$CampsLliures = $dbb->CampsLliures(array('taula' => 'tipus_actuacions', 'id' => $id, 'taulalliures' => ''));

		
		$dadesplantilla = array (
			'pagina' => 23,
			'RegistreTipusAct' => $RegistreTipusAct,
			'TipusCamps' => $TipusCamps,
			'GrupsConsulta' => $GrupsConsulta,
			'CampsLliures' => $CampsLliures,
		);

		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}

		echo $app['twig']->render('tipus-actuacionsform.html', $dadesplantilla);

		exit();
	}

	if ($taula == "plans_ambits")
	{
		$RegistreAp = $dbb->Llistats("plans_ambits"," AND t.id = $id", "titol_$idioma");		

		// Tipus actuacions
		$TipusActuacions = $dbb->Llistats("tipus_actuacions"," ","titol_$idioma");

		$RegistreAp[1][tipus_actuacions] = json_decode($RegistreAp[1][tipus_actuacions]);


		$dadesplantilla = array (
			'pagina' => 24,
			'RegistreAp' => $RegistreAp,
			'TipusActuacions' => $TipusActuacions,
			
		);

		echo $app['twig']->render('ambits-plaform.html', $dadesplantilla);

		exit();
	}

	if ($taula == "grups_consulta")
	{
		$RegistreGc = $dbb->Llistats("grups_consulta"," AND t.id = $id", "titol_$idioma");		

	
		$dadesplantilla = array (
			'pagina' => 25,
			'RegistreGc' => $RegistreGc,
			
		);

		echo $app['twig']->render('grups-consultaform.html', $dadesplantilla);

		exit();
	}
	*/
