<?php
	
	if (isset($config ['s'])){
		if (!empty($config ['s'])){
			$exiosget = stripslashes_deep($config ['s']);
			$primer = false;
			$lastElement = end($exiosget);
			foreach ($exiosget as $key => $value) 
			{	
				if ($primer == false) { $condicioeixos .= " AND ( "; $primer = true; }
				$condicioeixos .= " t.parent_id = '".intval($value)."' ";
				($value == $lastElement) ?  $condicioeixos .= " ) " : $condicioeixos .= " OR "; 
			}
		}
		else{
			echo "
				<div class=\"panel panel-default\" style=\"padding:12px;\">
                    Tria la línia que t'interessa.
                </div>
			";
			exit();
		}
	}
	else{
		exit();
	}

	if (isset($config['l'])){
		$json = str_replace('&quot;', '"', $config['l']);
		$linies =  json_decode($json);
	}
	if (empty($linies)) $linies = array();

	if (isset($config['a'])){
		$actuacionsgetjson = $config['a'];
	}


	$Vinculacio = $dbb->Llistats("projectes_vinculacio","AND estat = 1 AND (nivell = 3) $condicioeixos ", array(), "1*SUBSTRING_INDEX(titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(titol_ca, '.', -1) ASC ");

	$Dades = '
		<select name="linies[]"  class="selectpickerlinies" multiple data-actions-box="true" data-width="100%">
	';
		if (!empty($Vinculacio)){
			foreach ($Vinculacio as $key => $value) {
				$Dades .= ' <option value="'.$value[id].'" '.(in_array($value[id], $linies)?"selected":"").' >'.$value[titol].'</option> ';
			}
		}
	            
	$Dades .= '   
	    </select>
		
		<script type="text/javascript">

	        $(document).ready(function($) {


	            $(".selectpickerlinies").selectpicker({
	              
	            }); 

				$(".selectpickerlinies").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                
                	var selected = $(e.currentTarget).val();
                	var eixos = $(".selectpickereixos").val();
                	var pampad = $(".selectpicker").val();
	                $("#divact").html("<img src=\'../images/loading.gif\'/>");
	                $("#divact").load("../load",{o:38, id:1, c:{s:selected, e:eixos, p:pampad, a:\''.$actuacionsgetjson.'\'} });   

	            });

 				$(".selectpickerlinies").change();

	        
	        });

	    </script>

	';

	echo $Dades;