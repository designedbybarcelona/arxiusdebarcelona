<?php
	
  $permisos = $app['session']->get(constant('General::nomsesiouser')."-permisos");

	$db = new Db();
	
     	$Dades .= '
          <div class="row">
              <div class="col-lg-12">

                  <div class="panel panel-info">
                    
                      <div class="panel-body">
                          <table class="table  table-hover ">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Fase</th>
                                <th>Accions</th>
                                <th>Estat</th>
                                <th>'.($permisos != 3?'Edició':'').'</th>
                              </tr>
                            </thead>
                            <tbody>';

                            	$Actuacions = $db->query("SELECT a.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio, a.id as id
														  FROM pfx_actuacions a
														  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
														  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
														  WHERE a.clau_projecte = :id
														  GROUP By a.id ",array("id"=>$id));

                            	$tedades = false;

                                foreach ($Actuacions as $key_a => $value_a) 
								{
                                    $Dades .= '
                                      <tr class="mostartasques" data-id="'.$value_a[id].'" style="cursor:pointer;">
                                        <td><i class="fa fa-plus"></i></td>
                                        <td>'.$value_a[titol].'</td>
                                        <td>';

                                        	$Tasques = $db->query("SELECT t.*
																  FROM pfx_tasques t
																  INNER JOIN pfx_actuacions a ON a.id = t.clau_actuacio
																  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
																  WHERE t.clau_projecte = :idp AND t.clau_actuacio = :ida
																  GROUP By t.id ",array("idp"=>$id,"ida"=>$value_a[id]));


                                            $contatasques = 0;
                                            $contatasquestancades = 0;
                                            $contatasquespendents = 0;
                                            foreach ($Tasques as $key_t => $value_t) {
                                            	if ($value_t[estat]==1 || $value_t[estat]==3) $contatasques++;
                                            	if ($value_t[estat]==2 || $value_t[estat]==6) $contatasquestancades++;
                                            	if ($value_t[estat]==4 || $value_t[estat]==5 || $value_t[estat]==0) $contatasquespendents++;
											}
                                            $contatasques = $contatasques + $contatasquestancades + $contatasquespendents;
                                        $Dades .= $contatasques.'
                                            
                                        </td>
                                        <td>'.$value_a[estatactuacio].'</td>
                                        <td>';
                                          if ($permisos != 3){
                                            $Dades .= '
                                            <a href="../7/actuacions.html?id='.$value_a[id].'">
                                            <button type="button" class="btn btn-primary btn-xs edicio">Edició</button>
                                            </a>';
                                          }
                                            $Dades .= '
                                              <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_a["id"].'" data-t="2" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            ';
                                        
                                          $Dades .= '
                                        </td>
                                      </tr>
                                      <tr style="display:none;" class="tasc_'.$value_a[id].'">
                                        <td colspan="5">
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="panel panel-primary">
                                                        <div class="panel-body">
                                                            <table class="table  table-hover ">
                                                              <thead>
                                                                <tr>
                                                                  <th>Acció</th>
                                                                  <th>'.($permisos != 3?'Edició':'').'</th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>';

                                                                $tedades = false;
                                                                foreach ($Tasques as $key_t => $value_t) 
                                                                {
                                                                    $Dades .= '
                                                                    <tr>
                                                                        <td>'.$value_t[titol_ca].'</td>
                                                                        <td>';
                                                                        if ($permisos != 3){
                                                                          $Dades .= '
                                                                            <a href="../8/accions.html?id='.$value_t[id].'">
                                                                            <button type="button" class="btn btn-primary btn-xs edicio">Edició</button>
                                                                            </a>
                                                                          ';
                                                                        }
                                                                          $Dades .= '
                                                                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_t["id"].'" data-t="3" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                                          ';
                                                                        
                                                                        $Dades .= '
                                                                        </td>
                                                                    </tr>';
                                                                    $tedades = true;
                                                                }
                                                                if ($tedades == false){
										                          	$Dades .= '
																		<tr>
											                                <td colspan="2">Sense accions.</td>
											                            </tr>
										                          	';
									                          	}
                                                                
                                                               $Dades .= '
                                                              </tbody>
                                                            </table> 
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                      </tr>';
                                    $tedades = true;
                                }
                                if ($tedades == false){
		                          	$Dades .= '
										<tr>
			                                <td colspan="2">Sense fases.</td>
			                            </tr>
		                          	';
	                          	}
                            	
                            $Dades .= '  
                            </tbody>
                          </table> 
                      </div>
                  </div>

              </div>
              
          </div>
  		
  		';

  		echo $Dades;
	

