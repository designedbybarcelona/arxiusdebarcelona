<?php 

	header("Cache-Control: no-cache, must-revalidate"); 
	header('Pragma: no-cache'); 
	
	if($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		
		// Eliminar fitxers antics.
		// OJO!! no posar altres carpetes!!!!!
		for ($i=0; $i < 2; $i++) 
		{ 
			if ($i == 0) $path = __DIR__."/../../../secretfiles/informes/" ;
			//if ($i == 1) $path = __DIR__."/../../../secretfiles/exports/" ;


			if ($handle = opendir($path)) {

		     	while (false !== ($file = readdir($handle))) {

			        if ((time()-filectime($path.$file)) > 86400) {  
			            if (preg_match('/\.pdf$/i', $file)) {
						     unlink($path.$file);
						}
						if (preg_match('/\.xlsx$/i', $file)) {
						     unlink($path.$file);
						}
			        }
			     }
		   	}

		}
		
		// Variables necesaries.
		if (isset($iddoc) && isset($_GET[t]))
		{
			$iddoc =  base64_decode(htmlspecialchars(trim($iddoc)));
			$tocken =  htmlspecialchars(trim($_GET[t]));
			//$nomfile = base64_decode($iddoc);

			$VerifTocken = md5('TockenSecretViewDBB!'.$iddoc);

			// per informes.
			$i = htmlspecialchars(trim($_GET[i]));
			// per export.
			$e = htmlspecialchars(trim($_GET[e]));

		}else
		{
			exit();
		}

		// Verificar tocken.
		if ($VerifTocken == $tocken)
		{
			
			if ($app['env'] == "dev") require_once __DIR__.'/../include/global.php';
			if ($app['env'] == "prod") require_once __DIR__.'/include/global.php';

			$idioma = $app['session']->get('current_language');
			$dbb = new General($idioma, $app);

			$dbb->isLogged(6);

			// Upfile.
			if (empty($i))
			{	
				// AND clau_pla = '".$idpla =$app['session']->get('idpla')."'
				$Upfile = $dbb->Llistats("upfile","  AND id = :id ",array("id"=>$iddoc),"id");
			}

			if (!empty($Upfile) || $i == 1 || $e == 1)
			{
				if (empty($i) && empty($e))
				{
					$nomfile = $Upfile[1][nomfile];
				}
				else
				{
					$nomfile = $iddoc;
				}


				$extensio = StripFileExt($nomfile);


				if (isset($nomfile)){
					switch($extensio) {
						
						case ".pdf":
							$ctype = "application/pdf";
							break;
						case ".xls":
							$ctype = "application/vnd.ms-excel";
							break;
						case ".xlsx":
							$ctype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
							break;
						case ".doc":
							$ctype = "application/msword";
							break;
						case ".docx":
							$ctype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
							break;
						case ".ppt":
							$ctype = "application/vnd.ms-powerpoint";
							break;
						case ".pptx":
							$ctype = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
							break;




						//case "exe":
						//    $ctype = "application/octet-stream";
						//    break;
							
						case ".gif":
							$ctype = "image/gif";
							break;
						case ".png":
							$ctype = "image/png";
							break;
						case ".jpeg":
							$ctype = "image/jpeg";
							break;
						case ".jpg":
							$ctype = "image/jpeg";
							break;
						
						default:
							exit();
							//$ctype = "application/force-download";
					}
					
						
					//md5($file)

					if (empty($i) && empty($e))
					{
						$fileDir = __DIR__."/../../../secretfiles/" ;
					}
					else if ($i == 1)
					{
						$fileDir = __DIR__."/../../../secretfiles/informes/" ;
					}
					else if ($e == 1)
					{
						$fileDir = __DIR__."/../../../secretfiles/exports/" ;
					}

					
					if(file_exists($fileDir . $nomfile ))
					{   
						header('Content-type:  '.$ctype);
						header('Content-Disposition: attachment; filename="'.$nomfile.'"'); 
					    header('Content-Transfer-Encoding: binary');
					    header('Expires: 0');
					    header('Pragma: no-cache');

						readfile($fileDir . $nomfile); 

						//$contents = file_get_contents($fileDir . $nomfile);

						//echo $contents;
					} 
					else 
					{   
						?>
							<script>
							alert("No se ha entrontrado el fichero.");
							</script>
						<?php
					} 
				}
			}

			
		}
		else{

			exit();

		}



		
	}



	
	flush(); 
	exit();
	
?>