<?php 

	// Variables Generals.
	require_once __DIR__.'/general.php';

	// Dades formulari.

	if (isset($_POST['email'])){
		$email = filter_var(htmlspecialchars(trim($_POST['email'])), FILTER_VALIDATE_EMAIL);
	}
	if (isset($_POST['passw'])){
		$passw = filter_var(htmlspecialchars(trim($_POST['passw'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}
	if (isset($_POST['authToken'])){
		$authToken = htmlspecialchars(trim($_POST['authToken']));
	}


	// Verificar dades.

	/*
	if ($authToken != $app['session']->get('tockenseguretatlogin') ) {
        echo $app['translator']->trans('base.alerts.introdades');  // Error.
        header("Location: login");
		exit();
    }
    */

	if($passw == "" || $email == "") {
		echo $app['translator']->trans('base.alerts.introdades');  // Error.
		header("Location: login");
		exit();
	}
	
	// Login.

	$Login = $dbb->Login($email, $passw);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}

	if ($Login){

		//echo $app['twig']->render('index.html', $dadesplantilla);

		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 1 )
		{
			//header("Location: ./19/consultes.html");
			header("Location: ./5/els-meus-projectes.html");
			exit();
		}
		else{

			header("Location: ./5/els-meus-projectes.html");
			exit();

			/*
			echo '

				<script>
					top.location.href = "inici.html";
				</script>

			';*/

		}

		
	}
	else{

		echo '

			<body style="background-color:#000;">
				<head>
					<link href="css/bootstrap.min.css" rel="stylesheet">
				    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
				    <link href="css/sb-admin.css" rel="stylesheet">
					<script src="js/jquery-1.10.2.js"></script>
					<script type="text/javascript" language="javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>
    				<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css?v=2.1.5" media="screen" />
				</head>
				<html>
				<div id="divinfo">

					<div style="width:400px;text-align:center;" class="fancybox">
					    <h3 class="buscar">Error!</h3>
					    <p class="frame_subtit">
					        <br>Les dades d\'accès no son correctes, torni a provar-ho.
					    </p>
					</div>
				
				</div>
			</html>
			</body>


			<script>

				jQuery(document).ready(function($) {

			        $(".fancybox").fancybox({
					    "afterClose":function () {
					      top.location.href = "login.html";
					    },
					}).trigger("click");

			    });
				
			</script>

		';

		//echo $app['twig']->render('login.html', $dadesplantilla);
	}
