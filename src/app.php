<?php 
	

	$app->register(new Silex\Provider\TwigServiceProvider(), array(
	    'twig.path' => __DIR__ . '/../templates',
	    //'twig.options' => array('cache' => __DIR__.'/../cache/twig'),
	));

	$app->extend('twig', function($twig, $app) {
	    //$twig->addGlobal('pi', 3.14);
	    $twig->addFilter('md5', new \Twig_Filter_Function('md5'));
	    //$twig->addExtension(new Testextension\Twig\FpExtension($app));
	    return $twig;
	});
	


	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	use Symfony\Component\HttpFoundation\BinaryFileResponse;
	use Symfony\Component\Translation\Loader\YamlFileLoader;

	// Errors.
	
	$app->error(function (\Exception $e, $code)  use ($app) {
	    if ($app['debug']) {
	        return;
	    }

	    switch ($code) {
	        case 404:
	            
	            /*
	            $message = '<style type="text/css" >
							body {background:white url("images/error.png")  50% 0% no-repeat !important;z-index:10000000;}
							.sidebar, footer, .content, .header, .columns-inner, .footer-top {display:none;}
							body {min-width: 0px !important;}
							</style>';
				*/

			    include "error.php";
		        return $app['twig']->render('error.html', $dadesplantilla);
				

	            break;
	        default:
	            $message = 'We are sorry, but something went terribly wrong.';
	    }

	    return new Response($message);
	});

	// Idiomes.

	$app->register(new Silex\Provider\SessionServiceProvider(), array(
		'session.storage.options' => array('cookie_lifetime' => 20000),
		// sino indiquem el temps la sessió esta tanca amb el navegador.
		'session.storage.save_path' => '/tmp/sessions'
	));

	$app->before(function ($request) {
	    $request->getSession()->start();
	});

	$app->register(new Silex\Provider\TranslationServiceProvider(), array(
	    'locale_fallbacks' => array('es'),
	));

	$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
		$translator->addLoader('yaml', new YamlFileLoader());

		return $translator;
	}));

	$lang="ca";
	// Si afegim més idiomes eliminar aquesta linea.
	$app['session']->set('current_language', $lang);
	
	if ($app['session']->get('current_language')) {
		$lang = $app['session']->get('current_language');
	}else{
		$app['session']->set('current_language', $lang);
	}
	
	if (is_dir(__DIR__ . '/app/locales/'. $lang)) {
		foreach (glob(__DIR__ . '/app/locales/'. $lang . '/*.yml') as $locale) {
			$app['translator']->addResource('yaml', $locale, $lang);
		}
	}else
	{
		$lang="ca";
		foreach (glob(__DIR__ . '/app/locales/'. $lang . '/*.yml') as $locale) {
			$app['translator']->addResource('yaml', $locale, $lang);
		}
	}


	/* sets current language */
	$app['translator']->setLocale($lang);

	$app->get('/lang/{lang}', function($lang) use($app) {
		if (is_dir(__DIR__ . '/app/locales/'. $lang)) {
			/* save user selection in session */		
			$app['session']->set('current_language', $lang);
		}
		return $app->redirect($_SERVER['HTTP_REFERER']);
	});

	// Log.
	$app->register(new Silex\Provider\MonologServiceProvider(), array(
	    'monolog.logfile' => __DIR__.'/../../logpmla/logs.log',
	));
	
	// Caché.
	/*
	$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
	    'http_cache.cache_dir' => __DIR__.'/../cache/',
	));
	*/
	


	// Controllers.

	// Login.

	$app->get('/', function () use ($app) {
    	include "pagines/login.php";
        return $app['twig']->render('login.html', $dadesplantilla);
    });

    $app->match('/login.html', function () use ($app) {
    	include "pagines/login.php";
        return $app['twig']->render('login.html', $dadesplantilla);
    });

    $app->match('/login-web.html', function () use ($app) {
    	include "pagines/login-web.php";
        return $app['twig']->render('login-web.html', $dadesplantilla);
    });


    $app->post('/login', function () use ($app) {
    	include "app/validar.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
    });
    $app->get('/login', function () use ($app) {
    	include "pagines/login.php";
        return $app['twig']->render('login.html', $dadesplantilla);
    });

    // General.
	$app->match('/load', function (Request $request) use ($app) {
	    include "app/load.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->match('/taula', function (Request $request) use ($app) {
	    $carregadades = true;
	    include "app/load.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->post('/guardar', function (Request $request) use ($app) {
	    include "app/guardar.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	
    // Download.
    $app->get('/docs/{iddoc}/', function ($iddoc) use ($app) {
	    include "app/download.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});


	// Inici.
	$app->match('/inici.html', function (Request $request) use ($app) {
	    include "pagines/inici.php";
        return $app['twig']->render('inici.html', $dadesplantilla);
	});

	// Upload
	//TODO!!!!! deixar nomes ->post. Esta posat match per fer proves de passar dades.
	$app->match('/up', function (Request $request) use ($app) {
	    //include "upload.php";
	    include __DIR__."/app/server/php/index.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->match('/upload', function (Request $request) use ($app) {
	    include "app/upload.php";
	    // només serveix per dades login.
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});

	// Importador.
	$app->match('/importador', function (Request $request) use ($app) {
	    include "pagines/importador.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->match('/importador-resultado', function (Request $request) use ($app) {
	    include "pagines/importador-resultados.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});

	

	// Popups.
	$app->match('/info/{categoria}/{id}/', function ($categoria,$id) use ($app) {
	    include "app/popup.php";
        return $app['twig']->render('popup.html', $dadesplantilla);
	});

	// Login.
	$app->post('/login', function (Request $request) use ($app) {
	    include "pagines/login.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->get('/logout', function (Request $request) use ($app) {
	    $app['session']->clear();
    	return $app->redirect('./login.html');
	});
	$app->get('/dadesusuari.html', function () use ($app) {
    	include "pagines/dadesusuari.php";
        return $app['twig']->render('dadesusuari.html', $dadesplantilla);
    });
    $app->post('/enviaregistro', function (Request $request) use ($app) {
	    include "app/enviarmail.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	

	// Descargas.
	$app->get('/descargar/{nomfile}/', function ($nomfile) use ($app) {
	    include "app/load.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});

	
	// Indicadors.
	$app->match('/indicador/{id}/', function ($id) use ($app) {
	    include "pagines/indicadors.php";
        return $app['twig']->render('indicadors.html', $dadesplantilla);
	});

	// Generador.
	$app->match('/generador', function (Request $request) use ($app) {
	    include "pagines/export.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	$app->match('/generador2', function (Request $request) use ($app) {
	    include "pagines/exportactuacions.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});

	// Crear relacions de projectes.
	$app->match('/crearrelacions/', function (Request $request) use ($app) {
	    include "pagines/crearrelacions.php";
        return $app['twig']->render('blanc.html', $dadesplantilla);
	});
	


	// Págines del menú esquerra. Dinámiques.
	$app->match('/{idpagina}/{url}.html', function ($idpagina) use ($app) {
	    include "app/pagines.php";
	    include "pagines/$srcdesti";
        return $app['twig']->render($urldesti, $dadesplantilla);
	});


	// Consulta interactiva.
	$app->match('/consultainteractiva.html', function () use ($app) {
    	include "pagines/consultainteractiva.php";
        return $app['twig']->render('consultainteractiva.html', $dadesplantilla);
    });

    // Master.
    $app->match('/master.html', function () use ($app) {
    	include "app/master.php";
        return $app['twig']->render('master.html', $dadesplantilla);
    });



	$pages = array(
	    '/index.html' => 'inici',
	    '/cookies.html' => 'cookies',
	    '/termesicondicions.html' => 'termesicondicions',
	    '/noaccess.html' => 'noaccess',
	);
	 
	foreach ($pages as $route => $view) {
	    $dadesplantilla ="";
	    $app->get($route, function () use ($app, $view) {
	    	include "pagines/$view.php";
	        return $app['twig']->render($view.'.html', $dadesplantilla);
	    })->bind($view);
	}