<?php
	
	if (isset($_GET['f']) || isset($_POST['f']))
	{	
		$getf = (isset($_GET['f'])?$_GET['f']:$_POST['f']);
		$getenc = encrypt_decrypt('decrypt',$getf);
		$getenc = json_decode($getenc,true);
	}
	$condicioprojectes = "";
	$altresprojectes = false;
	$condicioclau0 = 0;
	if (isset($getenc['vinculacions'])){
		$vinculacionsget = stripslashes_deep($getenc['vinculacions']);
		$primer = false;
		$lastElement = end($vinculacionsget);
		$nomespam = 1;
		foreach ($vinculacionsget as $key => $value) 
		{	
			if ($value != "999999"){ // els altres projectes.
				if ($value != 1) $nomespam = 0;
				if ($primer == false) { $condicioprojectes .= " AND ( "; $condicioprojectesinici .= " AND ( "; $primer = true; }
				$condicioprojectes .= " pv.clau_pam = '".intval($value)."' ";//$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE( REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
				$condicioprojectesinici .= " pv.clau_pam = '".intval($value)."' ";
				($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR ";
				($value == $lastElement) ?  $condicioprojectesinici .= " ) " : $condicioprojectesinici .= " OR ";
			}else{
				$altresprojectes = true;
				$condicioclau0 = 1;
			}
			 
		}
		$hihafiltre = true;
		// $nomespam = 1  Es fa per que mostri les dades dels % d'execució de tots els pad al triar només el PAM.
		if($nomespam == 1){
			$condicioaeliminar = $condicioprojectes;
		}
		// Si estema l'inici esborre el filtre inicial de projectes per poder mostrar les actuacions sense cap projecte
		if ($idpagina == 999){
			$condicioprojectes = "";
		}
	}
	else{
		$nomespam = 1;
		if ($idpagina != 999)$vinculacionsget = array("1"); // D'entrada només mostrem PAM.
	}
	if (isset($getenc['eixos'])){
		$eixosget = stripslashes_deep($getenc['eixos']);
		$primer = false;
		$lastElement = end($eixosget);
		foreach ($eixosget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			//$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			$condicioprojectes .= " pv.clau_eix = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
		}
		$hihafiltre = true;
	}
	if (isset($getenc['linies'])){
		$liniesget = stripslashes_deep($getenc['linies']);
		$primer = false;
		$lastElement = end($liniesget);
		foreach ($liniesget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			//$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			$condicioprojectes .= " pv.parent_id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
		}
		$hihafiltre = true;
	}
	if (isset($getenc['actuacions'])){
		$actuacionsget = stripslashes_deep($getenc['actuacions']);
		$primer = false;
		$lastElement = end($actuacionsget);
		foreach ($actuacionsget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
		}
		$hihafiltre = true;
	}
	/* versió per buscar dins dels ambits dels projectes, es modifica per buscar dins de les vinculacions*/
	if (isset($getenc['nomesprojectes'])){
		$nomesprojectesget = filter_var(htmlspecialchars(trim($getenc['nomesprojectes'])), FILTER_SANITIZE_NUMBER_INT);
	}
	if (isset($getenc['ambits'])){
		$ambitsget = stripslashes_deep($getenc['ambits']);

		/* Aquesta funció queda obsoleta al afegir els nivells inveriors al filtre.
		$primer = false;
		$lastElement = end($ambitsget);
		foreach ($ambitsget as $key => $value) 
		{	
			//if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			//$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			//($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 

			// Sempre son nivell 1, incloure a la query tots els ID's que en pengen.
			$OrganseInferiors = $db->query("SELECT id FROM pfx_projectes_organs_e WHERE parent_id = :id AND nivell = 2", array("id"=>intval($value)));

			foreach ($OrganseInferiors as $key_n2 => $value_n2) {
				array_push($ambitsget, $value_n2["id"]);

			}

		}*/
		$hihafiltre = true;

		$primer = false;
		$lastElement = end($ambitsget);
		foreach ($ambitsget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes_centres .= " AND ( "; $primer = true; }
			$condicioprojectes_centres .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes_centres .= " ) " : $condicioprojectes_centres .= " OR "; 
		}
		// A la pàgina de "Els meus projectes" només mostrar els projectes que compleixen aquesta condició, en els altres casos es mostren objectius i per tant ha de mostrar també els que estan buits de projectes.
		if ($idpagina == 6 ) $condicioprojectes .= $condicioprojectes_centres; 

	}
	if (isset($getenc['ambits2'])){
		$ambits2get = stripslashes_deep($getenc['ambits2']);
		$primer = false;
		$lastElement = end($ambits2get);
		foreach ($ambits2get as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes_centres .= " AND ( "; $primer = true; }
			$condicioprojectes_centres .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes_centres .= " ) " : $condicioprojectes_centres .= " OR "; 
		}
		$hihafiltre = true;
		if ($nomesprojectesget == 1) $condicioprojectes .= $condicioprojectes_centres; 

	}
	if (isset($getenc['serveis'])){
		$serveisget = stripslashes_deep($getenc['serveis']);
		$primer = false;
		$lastElement = end($serveisget);
		foreach ($serveisget as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes_centres .= " AND ( "; $primer = true; }
			$condicioprojectes_centres .= " FIND_IN_SET('".intval($value)."' ,REPLACE(REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes_centres .= " ) " : $condicioprojectes_centres .= " OR "; 
		}
		$hihafiltre = true;
		if ($nomesprojectesget == 1) $condicioprojectes .= $condicioprojectes_centres; 
	}

	// Per buscar dins de les vinculacions del centre i no dins del projecte.
	// Extraiem del centre corresponent els ids de vinculacio y busquem vinculacions associades.
	
	if (isset($getenc['ambits'])){
		$ambitsget = stripslashes_deep($getenc['ambits']);
		foreach ($ambitsget as $key => $value) 
		{	
			if (!empty($condiciobuscacentres)) { $condiciobuscacentres .= " OR  "; }
			$condiciobuscacentres .= " poe.id = '".intval($value)."' ";
		}
	}
	if (isset($getenc['ambits2'])){
		$ambits2get = stripslashes_deep($getenc['ambits2']);
		foreach ($ambits2get as $key => $value) 
		{	
			if (!empty($condiciobuscacentres)) { $condiciobuscacentres .= " OR  "; }
			$condiciobuscacentres .= " poe.id = '".intval($value)."' ";
		}
	}
	if (isset($getenc['serveis'])){
		$serveisget = stripslashes_deep($getenc['serveis']);
		foreach ($serveisget as $key => $value) 
		{	
			if (!empty($condiciobuscacentres)) { $condiciobuscacentres .= " OR  ";   }
			$condiciobuscacentres .= " poe.id = '".intval($value)."' ";
		}
	}


	// Es comenta de moment pq: // El AND al triar un centre només al filtre, no surten els projectes relacionats a objectius que no tinguin el centre marcat.
	if (!empty($condiciobuscacentres) and 1==2){

		$db = new Db();
		$VinculaciodelsCentres = $db->query("SELECT vinculacio FROM pfx_projectes_organs_e poe WHERE $condiciobuscacentres",array()); 
		$arrayidsvinculacio = array();
		if (!empty($VinculaciodelsCentres)){
			foreach ($VinculaciodelsCentres as $key => $value) {
				$vinculacionscentre = json_decode($value['vinculacio']);
				if (is_array($vinculacionscentre))
				foreach ($vinculacionscentre as $key_v => $value_v) {
					$value_v = explode("_", $value_v);
					if (isset($value_v[1])) if (!in_array($value_v[1], $arrayidsvinculacio)) array_push($arrayidsvinculacio, $value_v[1]);
				}
			}
		}

		$primer = false;
		$lastElement = end($arrayidsvinculacio);
		foreach ($arrayidsvinculacio as $key => $value) 
		{	
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; } 
			$condicioprojectes .= " pv.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR ";
		
			 
		}
	}
	

	$estatsafiltrar = array();
	if (isset($getenc['estats'])){
		$estatsget = stripslashes_deep($getenc['estats']);
		$primer = false;
		$lastElement = end($estatsget);
		foreach ($estatsget as $key => $value) 
		{	
		
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " p.estat = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			
			/* Versió context
			array_push($estatsafiltrar, intval($value));

			// per la query de projectes.
			if ($primer == false) { $condicioprojectes2 .= " AND ( "; $primer = true; }
			$condicioprojectes2 .= " p.estat = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes2 .= " ) " : $condicioprojectes2 .= " OR "; 
			*/
		}
		$hihafiltre = true;
	}
	if (isset($getenc['inici'])){
		$iniciget = filter_var(htmlspecialchars(trim($getenc['inici'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		if (!empty($iniciget)){
			$iniciget = explode("/",$iniciget);
			$iniciget = date("$iniciget[2]-$iniciget[1]-$iniciget[0]");
			if (!empty($iniciget)) {
				$condicioprojectes .= " AND p.inici >= '$iniciget' ";
				$condicioconceptes .= " AND a.any >= '".date("Y",strtotime($iniciget))."' ";
			}
			$hihafiltre = true;
		}
	}
	if (isset($getenc['final'])){
		$finalget = filter_var(htmlspecialchars(trim($getenc['final'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		if (!empty($finalget)){
			$finalget = explode("/",$finalget);
			$finalget = date("$finalget[2]-$finalget[1]-$finalget[0]");
			if (!empty($finalget)) {
				$condicioprojectes .= " AND p.final <= '$finalget' ";
				$condicioconceptes .= " AND a.any <= '".date("Y",strtotime($finalget))."' ";
			}
			$hihafiltre = true;
		}
	}
	$percent1get = 0;
	if (isset($getenc['percent1'])){
		$percent1get = filter_var(htmlspecialchars(trim($getenc['percent1'])), FILTER_SANITIZE_NUMBER_INT);
		if ($percent1get != 0){
			if ($percent1get!="") $condicioprojectes .= " AND p.tantpercent >= '$percent1get' ";
			$hihafiltre = true;
		}
	}
	$percent2get = 100;
	if (isset($getenc['percent2'])){
		$percent2get = filter_var(htmlspecialchars(trim($getenc['percent2'])), FILTER_SANITIZE_NUMBER_INT);
		if ($getenc['percent2'] == "") $percent2get = 100; // sinó, no mostra resultats amb el filtr en blanc.
		if ($percent2get != 100){
			if ($percent2get!="") $condicioprojectes .= " AND p.tantpercent <= '$percent2get' ";
			$hihafiltre = true;
		}
	}
	$percent3get = 0;
	if (isset($getenc['percent3'])){
		$percent3get = filter_var(htmlspecialchars(trim($getenc['percent3'])), FILTER_SANITIZE_NUMBER_INT);
		if ($percent3get != 100){
			$hihafiltre = true;
		}
	}
	$percent4get = 100;
	if (isset($getenc['percent4'])){
		$percent4get = filter_var(htmlspecialchars(trim($getenc['percent4'])), FILTER_SANITIZE_NUMBER_INT);
		if ($getenc['percent4'] == "") $percent4get = 100; // sinó, no mostra resultats amb el filtr en blanc.
		if ($percent4get != 100){
			$hihafiltre = true;
		}
	}
	if (isset($getenc['pressu1'])){
		$pressu1get = filter_var(htmlspecialchars(trim($getenc['pressu1'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if (!empty($pressu1get)){
			if (!empty($pressu1get)) $condicioprojectes .= " AND p.totalagregat >= '$pressu1get' ";
			$hihafiltre = true;
		}
	}
	if (isset($getenc['pressu2'])){
		$pressu2get = filter_var(htmlspecialchars(trim($getenc['pressu2'])), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if (!empty($pressu2get)){
			if (!empty($pressu2get)) $condicioprojectes .= " AND p.totalagregat <= '$pressu2get' ";
			$hihafiltre = true;
		}
	}
	$districteafiltrar = array();
	if (isset($getenc['districtes'])){
		$districtesget = stripslashes_deep($getenc['districtes']);
		$primer = false;
		$lastElement = end($districtesget);
		foreach ($districtesget as $key => $value) 
		{	
			
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " FIND_IN_SET('".intval($value)."' ,REPLACE( REPLACE( REPLACE( p.districtes,'[', ''),']' ,'') ,'\"','')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 

			/* Versió context.
			if ($primer == false) { $condicioprojectes2 .= " AND ( "; $primer = true; }
			$condicioprojectes2 .= " FIND_IN_SET('".intval($value)."' ,REPLACE( REPLACE( REPLACE( p.districtes,'[', ''),']' ,'') ,'\"','')) > 0 ";
			($value == $lastElement) ?  $condicioprojectes2 .= " ) " : $condicioprojectes2 .= " OR "; 
			
			array_push($districteafiltrar, intval($value));
			*/
		}
		$hihafiltre = true;
	}
	if (isset($getenc['estrategic'])){
		$estrategicget = stripslashes_deep($getenc['estrategic']);
		$primer = false;
		$lastElement = end($estrategicget);
		foreach ($estrategicget as $key => $value) 
		{	
			$valoraposar = $value;
			if ($value == 2) $valoraposar = 0;
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " p.estrategic = '".intval($valoraposar)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 

		}
		$hihafiltre = true;
	}
	if (isset($getenc['paraules'])){
		$paraulesget = filter_var(htmlspecialchars(trim($getenc['paraules'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
		if (!empty($paraulesget)){
			$paraulesget = explode(",", $paraulesget);
			$primer = false;
		    $lastElement = end($paraulesget);
		    $paraulesclau .= " <div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b>Paraules clau</b>: ";
			foreach ($paraulesget as $key => $value) 
			{	
				
				if ($primer == false) { $condicioparaules .= " AND ( "; $condicioparaulesproj .= " AND ( "; $condiciovinculacio4 .= " AND ( "; $primer = true; }
				$condicioparaulesproj .= " prefix_taula.titol_ca like '%".trim($value)."%' OR po.titol_ca like '%".trim($value)."%' ";
				$condicioparaules .= " prefix_taula.titol_ca like '%".trim($value)."%' ";
				// Per buscar al nom de l'actuació.
				$condiciovinculacio4 .= " pv.titol_ca like '%".trim($value)."%' OR  p.titol_ca like '%".trim($value)."%'";
				($value == $lastElement) ?  $condiciovinculacio4 .= " ) " : $condiciovinculacio4 .= " OR "; 

				($value == $lastElement) ?  $condicioparaules .= " ) " : $condicioparaules .= " OR "; 

				($value == $lastElement) ?  $condicioparaulesproj .= " ) " : $condicioparaulesproj .= " OR "; 
				
				
				/* Versió context.
				if ($primer == false) { $condicioparaules2 .= " AND ( "; $primer = true; }
				$condicioparaules2 .= " prefix_taula.titol_ca like '%".trim($value)."%' ";
				($value == $lastElement) ?  $condicioparaules2 .= " ) " : $condicioparaules2 .= " OR "; 
				*/
				// Filtre.
				$paraulesclau .= "$value, ";
			}
			$condicioprojectes .= str_replace("prefix_taula", "p", $condicioparaulesproj);
			$condicioactuacions = str_replace("prefix_taula", "a", $condicioparaules);
			$condiciotasques = str_replace("prefix_taula", "t", $condicioparaules);
			//$condicioprojectes2 .= str_replace("prefix_taula", "p", $condicioparaules2);
			//$condicioactuacions2 = str_replace("prefix_taula", "a", $condicioparaules2);
			//$condiciotasques2 = str_replace("prefix_taula", "t", $condicioparaules2);
			$hihafiltre = true;

			// Per buscar dins d'altre operadors i agents...
			$leftoperadors = " LEFT JOIN pfx_projectes_operadors po ON FIND_IN_SET(po.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0 ";
			
		}

	}
	if (isset($getenc['export'])){
		$exportget = filter_var(htmlspecialchars(trim($getenc['export'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}
	if (isset($getenc['export_ind'])){
		$exportget_ind = filter_var(htmlspecialchars(trim($getenc['export_ind'])), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	}

	if (isset($_GET[f5])){ // Consulta predefinida.

		$usuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
		
		if (!empty($usuarivinculacio)){

			$db = new Db();

			$vinculacionsget = array(); // per eliminar el PAM per defecte.
			$nomespam = 0;



			// Eliminar ID's sense "_" pq indiquen el primer niell.
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{
				if (strpos($value_uv, "_") === false) {
					//$condicioprojectes .= " AND ( pv.clau_pam = '".intval($value_uv)."' ) ";
					array_push($vinculacionsget, $value_uv);
					unset($usuarivinculacio[$key_uv]);
				}
			}

			if (!isset($eixosget)) $eixosget = array();
			
			if (!isset($liniesget)) $liniesget = array();

			//if (!isset($actuacionsget)) $actuacionsget = array();
			
			$primer = false; $lastElement = end($usuarivinculacio);
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{	
				/*
				if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
				$condicioprojectes .= " concat(pv.clau_pam,'_',pv.id) = REPLACE( REPLACE( REPLACE( '$value_uv','[', ''),']' ,'') ,'\"','') ";
				if($value_uv == $lastElement) {  
					$condicioprojectes .= " ) ";
				}else{ 
					$condicioprojectes .= " OR "; 
				}*/

				$idsvinc = explode("_", $value_uv);

				// Recuperar nivell per posar on toca.
				$NivVinc = $db->query("SELECT t.nivell FROM pfx_projectes_vinculacio t WHERE t.id = :id AND nivell < 4 ",array("id" => $idsvinc[1]));
				if (!empty($NivVinc)){
					if ($NivVinc[0][nivell] == 2) array_push($eixosget, $idsvinc[1]);
					if ($NivVinc[0][nivell] == 3) array_push($liniesget, $idsvinc[1]);
					//if ($NivVinc[0][nivell] == 4) array_push($actuacionsget, $idsvinc[1]);
				}
				
			}
			
		}

	}else{
		if ($idpagina != 999) $condicio_permis_uvinc = "";
	}

	// Fitlres admin.
	if (isset($getenc['sensible'])){
		$sensibleget = stripslashes_deep($getenc['sensible']);
		$primer = false;
		$lastElement = end($sensibleget);
		foreach ($sensibleget as $key => $value) 
		{	
		
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			$condicioprojectes .= " p.sensible = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			
		
		}
		$hihafiltre = true;
	}
	if (isset($getenc['teprojectes'])){
		$teprojectesget = stripslashes_deep($getenc['teprojectes']);
		$primer = false;
		$lastElement = end($teprojectesget);
		foreach ($teprojectesget as $key => $value) 
		{	
		
			if ($primer == false) { $condicioprojectes .= " AND ( "; $primer = true; }
			if ($value == 0 ) $condicioprojectes .= " (Select count(rp2.id_proj) FROM pfx_projectes_relacionsvinc rp2 WHERE  rp2.id_act = pv.id) = 0 ";
			if ($value == 1 ) $condicioprojectes .= " (Select count(rp2.id_proj) FROM pfx_projectes_relacionsvinc rp2 WHERE rp2.id_act = pv.id) > 0 ";
			($value == $lastElement) ?  $condicioprojectes .= " ) " : $condicioprojectes .= " OR "; 
			
		
		}
		$hihafiltre = true;
	}
	if (isset($getenc['responsables'])){
		$responsablesget = stripslashes_deep($getenc['responsables']);
		$primer = false;
		$lastElement = end($responsablesget);
		foreach ($responsablesget as $key => $value) 
		{	
			if ($primer == false) { $condicioresponsables .= "  ( "; $primer = true; }
			$condicioresponsables .= "  u.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioresponsables .= " ) " : $condicioresponsables .= " OR "; 
		}
		$hihafiltre = true;
		$innerresponsables = " LEFT JOIN pfx_usuaris u ON u.id = p.cap_projecte  ";
	}
	if (isset($getenc['membres'])){
		$membresget = stripslashes_deep($getenc['membres']);
		$primer = false;
		$lastElement = end($membresget);
		foreach ($membresget as $key => $value) 
		{	
			if ($primer == false) { $condicioresponsables2 .= " ( "; $primer = true; }
			$condicioresponsables2 .= "  u2.id = '".intval($value)."' ";
			($value == $lastElement) ?  $condicioresponsables2 .= " ) " : $condicioresponsables2 .= " OR "; 
		}
		$hihafiltre = true;
		$innerresponsables .= " LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 ";
	}

	if (!empty($condicioresponsables) && !empty($condicioresponsables2)){
		$condicioresponsables = " AND ($condicioresponsables OR $condicioresponsables2)";
	}else if (!empty($condicioresponsables) && empty($condicioresponsables2)) {
		$condicioresponsables = " AND ($condicioresponsables)";
	}else if (empty($condicioresponsables) && !empty($condicioresponsables2)) {
		$condicioresponsables = " AND ($condicioresponsables2)";
	}

	//var_dump($condicioprojectes);exit();

	//var_dump($condicioresponsables);exit();

	/*
	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 )
	{
		
		$usuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
		
		$condicio_permis = $condicio_permis2 = "";
		if (!empty($usuarivinculacio)){

			// Eliminar ID's sense "_" pq indiquen el primer niell.
			$usuarivinculacio = array_unique(stripslashes_deep(json_decode($usuarivinculacio)));
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{
				if (strpos($value_uv, "_") === false) unset($usuarivinculacio[$key_uv]);
			}

			
			$primer = false; $lastElement = end($usuarivinculacio);
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{
				if ($primer == false) { $condicio_permis .= " AND ( "; $condicio_permis2 .= " AND ( "; $primer = true; }
				$condicio_permis .= " FIND_IN_SET('$value_uv' ,REPLACE(REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"',''),'_',',')) > 0 ";
				$condicio_permis2 .= " concat(pv.clau_pam,'_',pv.id) = REPLACE( REPLACE( REPLACE( '$value_uv','[', ''),']' ,'') ,'\"','') ";
				if($value_uv == $lastElement) {  
					$condicio_permis .= " ) ";
					$condicio_permis2 .= " ) ";
				}else{ 
					$condicio_permis .= " OR "; 
					$condicio_permis2 .= " OR "; 
				}

			}
			
		}


		if (empty($condicio_permis)){
			$condicio_permis .= " AND 1 = 2";
			$condicio_permis2 .= " AND 1 = 2";
		}
		
		
	}*/
