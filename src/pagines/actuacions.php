<?php 

	$idpagina = 7;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	/*
	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 3  )
	{
		$condicio_permis = " AND  FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0";
	}
	*/

	$Projectes = $dbb->Llistats("projectes"," $condicio_permis ", array(), "titol_ca", false);

	// Usuaris
	/*
	$Usuaris =  $dbb->FreeSql("SELECT u.*
							    FROM pfx_usuaris u
							    WHERE clau_permisos > 1
							    ORDER BY nom ",
							  array());

	// AmbitsActuacions.
	$AmbitsActuacions = $dbb->Llistats("actuacions_ambits"," AND nivell = 1 ", array() ,"titol_ca");
	*/

	
	// JS.

	$js = '

			$(document).off("click",".nou").on("click",".nou",function(event){

                if ($("#idactuacio").val() != "")
                {   
                    $(".2npas").html("");
                    $("#idactuacio").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmactuacions").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmactuacions").removeAttr("checked").removeAttr("selected");
                    $("div.erroractuacions span").html("");
                    $(".panelldadesactuacions").show();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultprojectes").html("");
                	$("#spancodi").html("Serà assignat al crear l\'actuació");
                	$(".linus").html("Nova fase");

                }else{
                    $(".panelldadesactuacions").toggle();
                    $(".clau_projecte option[value=\''.intval($_GET['idp']).'\']").attr("selected", "selected");
                }



            });

			$(".llistat_2").html("<img src=\''.$url.'/images/loading.gif\'/>");
			$(".llistat_2").load("'.$url.'/load",{o:2,id:1,t:"2"}, function(){
				
				setTimeout(function () {

					//$("select").val("-1").change();

    ';
				// Per iniciar el popup automàticament.
				if (isset($_GET['id']))
				{
					$idproj = intval($_GET['id']);

					$js .= ' 
						
						$("#divcamps").html("<div  style=\"text-align: center;\"><img src=\"../images/loading.gif\" /></div>");
			        	$(".amagamissatges").html("");
			        	$(".panelldades").show();
			        	$("#divcamps").show();
			    		$("#divcamps").load("../load", {id: '.$idproj.', o: 3, t: 2});
				
					';

				}
	$js .='		
					//$("select").val("10").change();
				}, 1500);
            });


	';
			if (isset($_GET['n']))
			{
	$js .='		$(".nou").click(); ';
			}

	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		'RegistreActuacio' => $RegistreActuacio,
		'Projectes' => $Projectes,
		'Usuaris' => $Usuaris,
		'AmbitsActuacions' => $AmbitsActuacions,
		'idproj' => $idproj,
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

