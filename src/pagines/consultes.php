<?php 

	$idpagina = 19;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	if (isset($_GET['id']))
	{
		$idproj = intval($_GET['id']);
		$condicioprojecte = " AND p.id = $idproj";
		$condicioprojecte2 = " AND t.clau_projecte = $idproj";
	}
	$paginaconsulta = 1; // per defecte ja entrem a la primera.
	if (isset($_GET['p']))
	{
		$paginaconsulta = intval($_GET['p']);
	}
	if (isset($_GET['a']))
	{
		$idambit = intval($_GET['a']);
	}
	/*
	if (isset($_GET['t']))
	{
		$tipusprojecte = intval($_GET['t']);
		if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 5 ||  $app['session']->get(constant('General::nomsesiouser')."-permisos") == 4)
		{
			$tipusprojecte = 1;
		}
	}
	*/
	if (isset($_GET['o']))
	{
		$idopcio = intval($_GET['o']);
	}

	// Filtre per mostrar les opcions en les que participes.
	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 2 AND $app['session']->get(constant('General::nomsesiouser')."-permisos") != 3)
	{
		/*$condicio_permis = " AND (p.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser')). "
								OR
								  FIND_IN_SET(".$app['session']->get(constant('General::nomsesiouser'))." ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 
								 )";
		
		$condicio_permis2 = " AND pi.responsable = ".$app['session']->get(constant('General::nomsesiouser'));*/
	}

  
	$text1 = "<h1><small>".$app['translator']->trans('Segueix l\'estat de les teves dades.')."</small></h1>";


	// Projectes.
	if ($paginaconsulta == 1){
		include "consultesprojectes.php";
	}

	

	
	

	
	/*
	$RegistreConfig = $dbb->FreeSql("SELECT * FROM pfx_configuracio c
									   ",array());
	
	if (!empty($RegistreConfig))
	{
		foreach ($RegistreConfig as $key => $value) {
			$RegistreConfig2[1]["c_".$value[parent_id]."_".$value[clau]] = $value[estat];
		}
	}else{$RegistreConfig2[1] = "";}
	*/
	

	// Per els usuaris tipus 5.
	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 5 ){
		if ($paginaconsulta != 2 && $paginaconsulta != 0) $paginaconsulta = 2;
	}
	
	if (!empty($iniciget)){
		$iniciget = explode("-",$iniciget);
		$iniciget = date("$iniciget[2]/$iniciget[1]/$iniciget[0]");
	}
	
	if (!empty($finalget)){
		$finalget = explode("-",$finalget);
		$finalget = date("$finalget[2]/$finalget[1]/$finalget[0]");
	}
	
	$dadesplantilla = array(
		
		'paginaconsulta' => $paginaconsulta,
		'idopcio' => $idopcio,


		// Projectes.
		
		'tipusprojecte' => $tipusprojecte,
		'Vinculacio' => $Vinculacio,
		'Districtes' => $Districtes,
		'Organse' => $Organse,
		'UsuarisFiltre' => $UsuarisFiltre, // Només admins.
		'UsuarisFiltre2' => $UsuarisFiltre2, // Només admins.
		'vinculacionsget' => $vinculacionsget,
		'eixosget' => $eixosget,
		'liniesget' => $liniesget,
		'liniesgetjson' => json_encode($liniesget),
		'actuacionsget' => $actuacionsget,
		'actuacionsgetjson' => json_encode($actuacionsget),
		'estatsget' => $estatsget,
		'districtesget' => $districtesget,
		'estrategicget' => $estrategicget,
		'sensibleget' => $sensibleget,
		'teprojectesget' => $teprojectesget,
		'responsablesget' => $responsablesget,
		'membresget' => $membresget,
		'paraulesget' => $paraulesget,
		'percent1get' => $percent1get,
		'percent2get' => $percent2get,
		'percent3get' => $percent3get,
		'percent4get' => $percent4get,
		'hihafiltre' => $hihafiltre,
		'datamenor' => $datamenor,
		'datamajor' => $datamajor,
		'iniciget' => $iniciget,
		'finalget' => $finalget, 
		'altresprojectes' => $altresprojectes,
		'nomesprojectesget' => $nomesprojectesget,
		'ambitsget' => $ambitsget, 
		'ambits2get' => $ambits2get, 
		'ambits2getjson' => json_encode($ambits2get),
		'serveisget' => $serveisget,
		'serveisgetjson' => json_encode($serveisget),
		'pressu1get' => $pressu1get, 
		'pressu2get' => $pressu2get, 

		'arrayCalendari' => $arrayCalendari,

		// General.
		'Dades' => $Dades,

		// Cerques.
		'pas' => $pas,

		'text1' => $text1,
		'text2' => $text2,

		'RegistreConfig' => $RegistreConfig2,

		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;