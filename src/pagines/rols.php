<?php 

	$idpagina = 32;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);
	
	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	

	
	// JS.

	$js = '

			$(".llistat_18").html("<img src=\''.$url.'/images/loading.gif\'/>");
			$(".llistat_18").load("'.$url.'/load",{o:2,id:1,t:18}, function(){});


	';
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		'Permisos' => $Permisos,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

