<?php
	
	//ini_set('display_errors', 0);
	
	ini_set('memory_limit', '512M'); // -1 sense límit.
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Path secret.
    $imagePATHsecret=pathfiles."/../../../secretfiles/tmp/";
	
	function myFlush() {
	    echo(str_repeat(' ', 256));
	    if (@ob_get_contents()) {
	        @ob_end_flush();
	    }
	    flush();
	}

	if (ob_get_level() == 0) ob_start();


	// Generar PDF.
 	if ($exportget == 1){


 		//echo "Generant PDF...<br>";
		//myFlush();

		

			// Inici creació PDF.

			// Include the main TCPDF library (search for installation path).
		    require_once( __DIR__.'/../../js/tcpdf/tcpdf.php');

		 	class MYPDF extends TCPDF {

			    //Page header
			    public function Header() {

			    	if ($this->tocpage) {
			            // *** replace the following parent::Header() with your code for TOC page
			            //parent::Header();

			        } else { 
			        	// *** replace the following parent::Header() with your code for normal pages
			        }
			            
		            // Logo
			        $image_file = pathfiles."/../images/".'logopdf.png';
			        $this->Image($image_file, 10, 5, 30, '');
			        // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

			        $dbb = new General($idioma, $app);
			        $DadesGenerals = $dbb->Llistats("general","",array(),'id');

			        $this->SetFont('helvetica', 'B', 7);
			        $this->SetTextColor(255, 0, 0);
			        $this->SetXY(158, 10);
			        $this->Cell(60, 0, $DadesGenerals[1][descripcio_ca], 0, false, 'C', 0, '', 0, false, 'M', 'M');
			        $this->SetTextColor(0, 0, 0);
			        $this->SetXY(107, 15);
			        $this->Cell(100, 0, 'Actuacions i Projectes', 0, false, 'C', 0, '', 0, false, 'M', 'M');

					$this->SetXY(7, 20);
					$this->setCellHeightRatio(0.2);
					$this->SetDrawColor(0,0,0);
					$this->Cell(195, 0,'', '', 1, 'C', 1, '', 0, false, 'T', 'C');
		       
			        
			    }

			    // Page footer
			    
			    public function Footer() {
			    	
			        if ($this->tocpage) {
			            // *** replace the following parent::Footer() with your code for TOC page
			            //parent::Footer();
			        } else {
			            // *** replace the following parent::Footer() with your code for normal pages
			            // Position at 15 mm from bottom
		        		$this->SetY(-15);
				        // Set font
				        $this->SetFont('helvetica', 'I', 8);
				        // Page number
				        $this->Cell(0, 20, 'Pàgina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		        	
				        
			        }
			    }
			    
			}
		  

		    // create new PDF document
		    $pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		    // Path imatges.
		    $imagePATH=pathfiles."/../../images/";

		    // set document information
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetTitle($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetSubject($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetKeywords($app['translator']->trans('PDA Objectius i Projectes'));

		    //$pdf->startPage( $orientation = '',$format = '', true );
		   
		    //$pdf->SetHeaderData("../../images/logo.jpg", '50', '', ""); 
		    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Seguiment de projectes");
		    $pdf->SetHeaderData('','','','');

		    $pdf->setPrintHeader(false);
		    //$pdf->setPrintHeader();
		    // set header and footer fonts
		    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
		    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		    $pdf->setPrintFooter(false);

		    // set default monospaced font
		    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		    // set margins
		    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetMargins(7, 7, 7);
		    $pdf->SetHeaderMargin(0);
		    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		    $pdf->SetFooterMargin(0); // PDF_MARGIN_FOOTER

		    // set auto page breaks
		    $pdf->SetAutoPageBreak(TRUE, 5);

		    // set image scale factor
		    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		    // set some language-dependent strings (optional)
		    //if (@file_exists(dirname(__FILE__).'/lang/esp.php')) {
		    //  require_once(dirname(__FILE__).'/lang/esp.php');
		    //  $pdf->setLanguageArray($l);
		    //}

		    $tbl = "";


				 // ---------------------------------------------------------

		    // -- Portada.

		    $pdf->AddPage();
			$pdf->SetFont('helvetica', 'B', 12);
			//$pdf->SetFontSize(20);

			$image_file = pathfiles."/../images/".'logopdf.png';
		    $pdf->Image($image_file, 2, 2, 30, '');

			$pdf->SetXY(40, 3);
			$pdf->Write(0, date('d/m/Y'), '', 0, 'L', true, 0, false, false, 0);
			$pdf->SetXY(18, 3);
			$pdf->Write(0, 'PDA Objectius i Projectes', '', 0, 'C', true, 0, false, false, 0);
			
			$pdf->SetFont('helvetica', '', 7);

			$pdf->Ln(5);
			/*
			if (!empty($ConfigPla)){

				$pdf->SetXY(7, 20);
				$pdf->Write(0, str_replace("<p>", "", str_replace("</p>", "\n", $ConfigPla[1][descripcio_ca])), '', 0, 'L', true, 0, false, false, 0);

			}

			$pdf->Ln(2);*/

			$pdf->SetFillColor(255, 255, 255);
		    $pdf->SetTextColor(0,0,0);
		    $pdf->SetLineWidth(1.2);
		    $pdf->SetDrawColor(0,0,0);
			$pdf->setCellHeightRatio(2);


	  

		$caps = false;

		if (!empty($Vinculacio4))
		{
			
			
			$taula .= '
					    
					';

			foreach ($Vinculacio4 as $key_exp => $value_exp) 
			{
		

				
				// Capçaleres.
				if ($caps == false)
				{
					
					$taula .= '<table cellspacing="0" cellpadding="3" border="1">';

					$taula .= '<tr style="background-color:#CECECE;font-weight: bold;" >';

					$taula .= '<td width="50%">Objectius i projectes</td>';
					$taula .= '<td width="6%">Estat</td>';
				    $taula .= '<td width="11%">Calendari</td>';
				    $taula .= '<td width="11%">Gestor/a projecte<br>Equip de projecte intern<br>Equip de projecte extern</td>';
				    $taula .= '<td width="7%">% d\'execució</td>';
				    $taula .= '<td width="8%">Recursos<br>Pressupost</td>';
				    $taula .= '<td width="7%">Actualitzat</td>';	 
					$taula .= '</tr>';

					$caps = true;
				}
				
				// Condicions i format de dades copiat de consultaprojectes_llistat.php
				//if ($value_exp[clau_cero] == 0 &&(($nomespam == 1 && $value_exp[clau_pam] == 9) || $nomespam == 0) ) 
				//{
					$textdatesv = "<b>";
									if ($Vinculacio4dates[$value_exp[id]][datesinici][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesinici][0]!= "") $textdatesv .= date_format(date_create($Vinculacio4dates[$value_exp[id]][datesinici][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "") $textdatesv .= " - ".date_format(date_create($Vinculacio4dates[$value_exp[id]][datesfinal][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_exp[id]][datesinici][0] == "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]== "0000-00-00") $textdatesv .= "------";
									$textdatesv .= "</b>";

					$taula .= '<tr style="background-color:#79bd99;">';

					$taula .= '

						<td> '.$value_exp[titol].'</td>
						<td></td>
	                    <td> '.$textdatesv.'</td>
	                    <td></td>
	                    <td> <b>'.$Vinculacio4tantspercents[$value_exp[id]][tantspercents].''.($Vinculacio4tantspercents[$value_exp[id]][tantspercents]==""?"0.00":"").' %</b></td>
	                    <td></td>
	                    <td></td>

					';
								
					$taula .= '</tr>';

					$idsproj = array();
					$tedadesproj = false;
	             	if (!empty($value_exp[idsproj])){
		              	$idsproj = explode(",", $value_exp[idsproj]);

		              	foreach ($idsproj as $key_idsproj => $value_idsproj) 
						{

			              	$DadesProjectes_exp =  $db->query("SELECT p.titol_ca as nom, IFNULL(pa.text,'----') as estatprojecte, DATE_FORMAT(p.updated, '%d/%m/%Y') as updated,
																  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id, DATE_FORMAT(p.inici, '%d/%m/%Y') as inici,
																  p.tantpercent, p.pesrelatiu,
																  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(po.acronim),'') SEPARATOR ' <br> ') as acronim, u.nom as nomresp, u.cognoms as cognomsresp,
														   		  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as nomsequip,
																  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as nomextern,
																  p.recursos
																  FROM pfx_projectes p
																  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
																  LEFT JOIN pfx_projectes_organs_e po ON FIND_IN_SET(po.id ,REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"','')) > 0
																  LEFT JOIN pfx_usuaris u ON p.cap_projecte = u.id
																  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
																	  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0
																  WHERE p.id = :idproj
																  GROUP By p.id
																  ORDER BY p.titol_ca ",array("idproj" => $value_idsproj));  

			              	//foreach ($DadesProjectes_exp as $key_pexp => $value_pexp) 
							//{	
			              	if (!empty($DadesProjectes_exp[0])){

								if ($tedadesproj == false){
									$taula .= '<tr><td colspan="8"><table cellpadding="2">';
									$tedadesproj = true;
								}

								// Per l'export desde index i projectes posem 2 dades més.
								if ($idpagina == 999 || $idpagina == 5 || $idpagina == 6){
									$acronim = "(".$DadesProjectes_exp[0][acronim].") ";
									$cap_projecte = " (".$DadesProjectes_exp[0][nomresp]." ".$DadesProjectes_exp[0][cognomsresp].")";
								}

								// Recursos.
								$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $DadesProjectes_exp[0]['recursos']),'id');

							
								// Pressupostos.
								$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_idsproj),'any');
								$totalpressu = 0; //$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
								if (!empty($Pressupostos_ind)){
									foreach ($Pressupostos_ind as $key_a => $value_a) {
										//$totalpressu1 += $value_a['pressupost1'];
										//$totalpressu2 += $value_a['pressupost2'];
										//$totalpressu3 += $value_a['pressupost3'];
										$totalpressu += $value_a['pressupost1'];
										$totalpressu += $value_a['pressupost2'];
										$totalpressu += $value_a['pressupost3'];
									}
								}


								$taula .= '

									<tr>
										<td width="50%" style="border-bottom:0.1px solid black;">'.$acronim.' '.$DadesProjectes_exp[0][nom].' '.$cap_projecte.'</td>
					                    <td width="6%" style="border-bottom:0.1px solid black;">'.$DadesProjectes_exp[0][estatprojecte].'</td>
					                    <td width="11%" style="border-bottom:0.1px solid black;"><b>'.($DadesProjectes_exp[0]["inici"]!="00/00/0000"?$DadesProjectes_exp[0]["inici"]:"").' - '.($DadesProjectes_exp[0]["final"]!="00/00/0000"?$DadesProjectes_exp[0]["final"]:"").'</b></td>
					                    <td width="11%" style="border-bottom:0.1px solid black;">'.$DadesProjectes_exp[0][nomresp]." ".$DadesProjectes_exp[0][cognomsresp]."<br>----<br>".$DadesProjectes_exp[0][nomsequip]."<br>----<br>".$DadesProjectes_exp[0][nomextern].'</td>
					                    <td width="7%" style="border-bottom:0.1px solid black;text-align:center;">'.$DadesProjectes_exp[0][tantpercent].' %</td>
					                    <td width="8%" style="border-bottom:0.1px solid black;">'.$Recursos_ind[1][text].'<br>'.number_format($totalpressu,2).' €</td>
					                    <td width="7%" style="border-bottom:0.1px solid black;text-align:right;">'.$DadesProjectes_exp[0][updated].'</td>
					                </tr>

								'; // ($DadesProjectes_exp[0][pesrelatiu]>"0"? $DadesProjectes_exp[0][tantpercent]*($DadesProjectes_exp[0][pesrelatiu]/100): $DadesProjectes_exp[0][tantpercent])
							//}
							}

							// Subprojectes.
							$Actuacions = $db->query("SELECT a.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio,
													  DATE_FORMAT(a.final, '%d/%m/%Y') as final, DATE_FORMAT(a.inici, '%d/%m/%Y') as inici, a.id
													  FROM pfx_actuacions a
													  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
													  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
													  WHERE a.clau_projecte = :idproj
													  GROUP By a.id ",array("idproj" => $value_idsproj));
													  
							if (!empty($Actuacions)){
                                foreach ($Actuacions as $key_a => $value_a) 
								{
									
									$taula .= ' 

										<tr>
											<td width="5%"></td>
											<td width="50%" style="border-bottom:0.1px solid dotted;">'.$value_a["titol"].'</td>
						                    <td width="6%" style="border-bottom:0.1px solid dotted;">'.$value_a["estatactuacio"].'</td>
						                    <td width="39%" style="border-bottom:0.1px solid dotted;"><b>'.($value_a["inici"]!="00/00/0000"?$value_a["inici"]:"").' - '.($value_a["final"]!="00/00/0000"?$value_a["final"]:"").'</b></td>
						                </tr>

									';

									// Accions.
									$Tasques = $db->query("SELECT t.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio,
															  DATE_FORMAT(t.final, '%d/%m/%Y') as final, DATE_FORMAT(t.inici, '%d/%m/%Y') as inici
															  FROM pfx_tasques t
															  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
															  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
															  WHERE t.clau_actuacio = :idact
															  GROUP By t.id ",array("idact" => $value_a["id"]));
															  
									if (!empty($Tasques)){
		                                foreach ($Tasques as $key_t => $value_t) 
										{
											
											$taula .= ' 

												<tr>
													<td width="10%"></td>
													<td width="45%" style="border-bottom:0.1px solid dotted;">'.$value_t["titol"].'</td>
								                    <td width="6%" style="border-bottom:0.1px solid dotted;">'.$value_t["estatactuacio"].'</td>
								                    <td width="39%" style="border-bottom:0.1px solid dotted;"><b>'.($value_t["inici"]!="00/00/0000"?$value_t["inici"]:"").' - '.($value_t["final"]!="00/00/0000"?$value_t["final"]:"").'</b></td>
								                </tr>

											';

										}
									}

								}
							}
						}

	              	}
	              	
					if ($tedadesproj == true){
						$taula .= '</table></td></tr>';
					}

				//}
				
			}

			$taula .= '

					</table>
					';

			$pdf->writeHTMLCell(0, 0, $pdf->getX(), $pdf->getY(), $taula, 0, 0, 1, true, 'L', true);

			
		}

		

		

		// -----------------------------------------------------------------------------

			

			    //Close and output PDF document
			    //$pdf->Output('example_048.pdf', 'I');

			    //orientació L(horitzonal), P(vertical)

			    //Close and output PDF document
			    // D: download, F: Save file
			    $nomfile = 'PDA_Objectius_Projectes_'.date("Y-m-d_H-i-s").uniqid().'.pdf';
			    $rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;
			    $pdf->Output($rutafile,'F');

			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank" class="desc">
							<button class="btn btn-success btn-xs" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a>
						<script>
							$(document).ready(function($) {
								//$(".caixablanca").hide();
								window.open("'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1");
							});
						</script>
				';
				

				/*
			    $pdf->Output($nomfile,'D');
			    
			    $tmp = ini_get('upload_tmp_dir');

			    $fileatt = "./".$rutafile;
			    $fileatttype = "application/pdf";
			    $fileattname = $nomfile;

			    $file = fopen($fileatt, 'rb');
			    $data = fread($file, filesize($fileatt));
			    fclose($file);
			    
			    ob_end_clean();*/

			    

			    //============================================================+
			    // END OF FILE
			    //============================================================+

			

		

	

 	}


 	// Generar XLS.
 	if ($exportget == 2){


		// Clases Php.
		require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel.php");

		// Nom fitxer.
		$nomfile = 'PDA_Objectius_Projectes_'.date("Y-m-d_H-i-s").uniqid().'.xlsx';
		$rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;

		
		// Inicialitza Excel.
	    $objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 1;
		$colCount = 0;



		if (!empty($Vinculacio4)){

				// Capçaleres.
				$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('CCFFCC');

				$BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_MEDIUM 
				    )
				  )
				);

				$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($BStyle);
				
				// Capçaleres.
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Objectiu");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Acrònim centre/servei");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Nom del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Gestor/a projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Equip de projecte intern");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Equip de projecte extern");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data inici");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data final");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Estat del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "% d'execució del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Recursos");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Pressupost total agregat");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Darrera actualització projecte");
				$colCount++;
				

				$colCount = 0;
				$rowCount = 2;

													
				
				foreach ($Vinculacio4 as $key_exp => $value_exp) 
				{

					// Condicions i format de dades copiat de consultaprojectes_llistat.php
					//if ($value_exp[clau_cero] == 0 &&(($nomespam == 1 && $value_exp[clau_pam] == 9) || $nomespam == 0) ) 
					//{
						/*
						$textdatesv = "";
						if ($Vinculacio4dates[$value_exp[id]][datesinici][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesinici][0]!= "") $textdatesv .= date_format(date_create($Vinculacio4dates[$value_exp[id]][datesinici][0]),"d/m/Y");
						if ($Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "") $textdatesv .= " - ".date_format(date_create($Vinculacio4dates[$value_exp[id]][datesfinal][0]),"d/m/Y");
						if ($Vinculacio4dates[$value_exp[id]][datesinici][0] == "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]== "0000-00-00") $textdatesv .= "------";
						*/



						$idsproj = array();
		             	if (!empty($value_exp[idsproj])){
			              	$idsproj = explode(",", $value_exp[idsproj]);
			              	
			              	foreach ($idsproj as $key_idsproj => $value_idsproj) 
							{
							
								// Si hi ha suprojectes, posem tantes linies com subprojectes.
								// Subprojectes.
								/*
								$arraysuproj = array();
								$Actuacions = $db->query("SELECT a.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio,
														  DATE_FORMAT(a.final, '%d/%m/%Y') as final, DATE_FORMAT(a.inici, '%d/%m/%Y') as inici
														  FROM pfx_actuacions a
														  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
														  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
														  WHERE a.clau_projecte = :idproj
														  GROUP By a.id ",array("idproj" => $value_idsproj));
														  
								if (!empty($Actuacions)){
	                                foreach ($Actuacions as $key_a => $value_a) 
									{
										$arraysuproj[] = array("nom" => $value_a["titol"],
															   "estat" => $value_a["estatactuacio"],
															   "dates" => ($value_a["inici"]!="00/00/0000"?$value_a["inici"]:"").' - '.($value_a["final"]!="00/00/0000"?$value_a["final"]:"")
															  ); 
									}
								}else{
									$arraysuproj[] = array("nom"=>"","estat"=>"","dates"=>"");
								}
								*/


				              	$DadesProjectes_exp =  $db->query("SELECT p.titol_ca as nom, IFNULL(pa.text,'----') as estatprojecte, DATE_FORMAT(p.updated, '%d/%m/%Y') as updated,
																	  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id, DATE_FORMAT(p.inici, '%d/%m/%Y') as inici,
																	  p.tantpercent,
																	  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(po.acronim),'') SEPARATOR ' | ') as acronim,
																	  u.nom as nomresp, u.cognoms as cognomsresp,
																	  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as nomsequip,
																	  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as nomextern,
																	  p.recursos
																	  FROM pfx_projectes p
																	  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
																	  LEFT JOIN pfx_projectes_organs_e po ON FIND_IN_SET(po.id ,REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"','')) > 0
																	  LEFT JOIN pfx_usuaris u ON p.cap_projecte = u.id
																	  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
																	  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0
																	  WHERE p.id = :idproj 
																	  GROUP By p.id
																	  ORDER BY p.titol_ca ",array("idproj" => $value_idsproj));  

				              	if (!empty($DadesProjectes_exp)){

					              	//foreach ($arraysuproj as $key_suproj => $value_suproj) // Per mostrar una linea per cada actuació.
									//{	
										$colCount = 0;

										preg_match('/(\d+(\.\d+)*)/', $value_exp[titol], $codiactuacio);
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e( $codiactuacio[0]));
										$colCount++;
										//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($textdatesv));
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($DadesProjectes_exp[0][acronim])));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][nom]));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][nomresp]." ".$DadesProjectes_exp[0][cognomsresp]));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][nomsequip]));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][nomextern]));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($DadesProjectes_exp[0]["inici"]!="00/00/0000"?$DadesProjectes_exp[0]["inici"]:"")));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($DadesProjectes_exp[0]["final"]!="00/00/0000"?$DadesProjectes_exp[0]["final"]:"")));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][estatprojecte]));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($DadesProjectes_exp[0][tantpercent])));
										$colCount++;

										// Recursos.
										$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $DadesProjectes_exp[0]['recursos']),'id');

										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($Recursos_ind[1][text])));
										$colCount++;

										// Pressupostos.
										$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_idsproj),'any');
										$totalpressu = 0; //$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
										if (!empty($Pressupostos_ind)){
											foreach ($Pressupostos_ind as $key_a => $value_a) {
												//$totalpressu1 += $value_a['pressupost1'];
												//$totalpressu2 += $value_a['pressupost2'];
												//$totalpressu3 += $value_a['pressupost3'];
												$totalpressu += $value_a['pressupost1'];
												$totalpressu += $value_a['pressupost2'];
												$totalpressu += $value_a['pressupost3'];
											}
										}
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(number_format($totalpressu,2).' €'));
										$colCount++;
										$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($DadesProjectes_exp[0][updated]));
										$colCount++;



										$rowCount++;

												
											
										
									//}
									
								}
								
							}

		              	}

		              	//$rowCount++;


					//}

				}

				
				

				//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				//$objWriter->save('php://output');
				$objWriter->save($rutafile);


			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank">
							<button class="btn btn-success btn-xs desc" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a><br><br><br>
						<script>
							$(document).ready(function($) {
								//$(".caixablanca").hide();
								window.open("'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1");
							});
						</script>
				';
		}

		$dadesplantilla = array();
		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}
		return $dadesplantilla;

		

 	}


 	// Generar XLS.
 	if ($exportget == 3){


		// Clases Php.
		require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel.php");

		// Nom fitxer.
		$nomfile = 'PDA_Objectius_Projectes_'.date("Y-m-d_H-i-s").uniqid().'.xlsx';
		$rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;

		
		// Inicialitza Excel.
	    $objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 1;
		$colCount = 0;



		if (!empty($Vinculacio4)){

				// Capçaleres.
				$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()->setRGB('CCFFCC');

				$BStyle = array(
				  'borders' => array(
				    'outline' => array(
				      'style' => PHPExcel_Style_Border::BORDER_MEDIUM 
				    )
				  )
				);

				$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($BStyle);
				
				// Capçaleres.
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Objectiu");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Acrònim centre/servei");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Nom del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Gestor/a projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Equip de projecte intern");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Equip de projecte extern");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data inici");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Data final");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Estat del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "% d'execució del projecte");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Recursos");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Pressupost total agregat");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Darrera actualització projecte");
				$colCount++;
				

				$colCount = 0;
				$rowCount = 2;

				foreach ($Vinculacio4 as $key_exp => $value_exp) 
				{
					
					if (!empty($value_exp[idsproj])){
					
						if($idpagina != 6){
							$id = $value_exp[id];
					

							include "inici_inc.php";	
						}
													
					
						foreach ($DadesProjectes_llistat as $key_exp => $value_exp) 
						{

									
			             

							$colCount = 0;

							preg_match('/(\d+(\.\d+)*)/', $value_exp[nomvinc], $codiactuacio);
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e( $codiactuacio[0]));
							$colCount++;
							//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($textdatesv));
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e((str_replace("<br>", "|", $value_exp["acronim"]))));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp["nom"]));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp["nomresp"]." ".$value_exp["cognomsresp"]));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp["nomsequip"]));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp["nomextern"]));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($value_exp["inici"]!="00/00/0000"?$value_exp["inici"]:"")));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($value_exp["final"]!="00/00/0000"?$value_exp["final"]:"")));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp["estatprojecte"]));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($value_exp["tantpercent"])));
							$colCount++;

							// Recursos.
							$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $value_exp['recursos']),'id');

							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(($Recursos_ind[1][text])));
							$colCount++;

							// Pressupostos.
							$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_exp['id']),'any');
							$totalpressu = 0; //$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
							if (!empty($Pressupostos_ind)){
								foreach ($Pressupostos_ind as $key_a => $value_a) {
									//$totalpressu1 += $value_a['pressupost1'];
									//$totalpressu2 += $value_a['pressupost2'];
									//$totalpressu3 += $value_a['pressupost3'];
									$totalpressu += $value_a['pressupost1'];
									$totalpressu += $value_a['pressupost2'];
									$totalpressu += $value_a['pressupost3'];
								}
							}
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e(number_format($totalpressu,2).' €'));
							$colCount++;
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_exp[updated]));
							$colCount++;



							$rowCount++;

											
										
								

						}

						if($idpagina == 6) break;

					}

				}
				

				//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
				$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
				//$objWriter->save('php://output');
				$objWriter->save($rutafile);


			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank">
							<button class="btn btn-success btn-xs desc" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a><br><br><br>
						<script>
							$(document).ready(function($) {
								//$(".caixablanca").hide();
								window.open("'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1");
							});
						</script>
				';
		}

		$dadesplantilla = array();
		foreach ($arraygeneral as $key => $value) 
		{
			$dadesplantilla[$key] = $value;
		}
		return $dadesplantilla;

		

 	}

 	// Generar PDF.
 	if ($exportget == 4){


 		//echo "Generant PDF...<br>";
		//myFlush();

		

			// Inici creació PDF.

			// Include the main TCPDF library (search for installation path).
		    require_once( __DIR__.'/../../js/tcpdf/tcpdf.php');

		 	class MYPDF extends TCPDF {

			    //Page header
			    public function Header() {

			    	if ($this->tocpage) {
			            // *** replace the following parent::Header() with your code for TOC page
			            //parent::Header();

			        } else { 
			        	// *** replace the following parent::Header() with your code for normal pages
			        }
			            
		            // Logo
			        $image_file = pathfiles."/../images/".'logopdf.png';
			        $this->Image($image_file, 10, 5, 30, '');
			        // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

			        $dbb = new General($idioma, $app);
			        $DadesGenerals = $dbb->Llistats("general","",array(),'id');

			        $this->SetFont('helvetica', 'B', 7);
			        $this->SetTextColor(255, 0, 0);
			        $this->SetXY(158, 10);
			        $this->Cell(60, 0, $DadesGenerals[1][descripcio_ca], 0, false, 'C', 0, '', 0, false, 'M', 'M');
			        $this->SetTextColor(0, 0, 0);
			        $this->SetXY(107, 15);
			        $this->Cell(100, 0, 'Actuacions i Projectes', 0, false, 'C', 0, '', 0, false, 'M', 'M');

					$this->SetXY(7, 20);
					$this->setCellHeightRatio(0.2);
					$this->SetDrawColor(0,0,0);
					$this->Cell(195, 0,'', '', 1, 'C', 1, '', 0, false, 'T', 'C');
		       
			        
			    }

			    // Page footer
			    
			    public function Footer() {
			    	
			        if ($this->tocpage) {
			            // *** replace the following parent::Footer() with your code for TOC page
			            //parent::Footer();
			        } else {
			            // *** replace the following parent::Footer() with your code for normal pages
			            // Position at 15 mm from bottom
		        		$this->SetY(-15);
				        // Set font
				        $this->SetFont('helvetica', 'I', 8);
				        // Page number
				        $this->Cell(0, 20, 'Pàgina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		        	
				        
			        }
			    }
			    
			}
		  

		    // create new PDF document
		    $pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		    // Path imatges.
		    $imagePATH=pathfiles."/../../images/";

		    // set document information
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetTitle($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetSubject($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetKeywords($app['translator']->trans('PDA Objectius i Projectes'));

		    //$pdf->startPage( $orientation = '',$format = '', true );
		   
		    //$pdf->SetHeaderData("../../images/logo.jpg", '50', '', ""); 
		    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Seguiment de projectes");
		    $pdf->SetHeaderData('','','','');

		    $pdf->setPrintHeader(false);
		    //$pdf->setPrintHeader();
		    // set header and footer fonts
		    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
		    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		    $pdf->setPrintFooter(false);

		    // set default monospaced font
		    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		    // set margins
		    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetMargins(7, 7, 7);
		    $pdf->SetHeaderMargin(0);
		    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		    $pdf->SetFooterMargin(0); // PDF_MARGIN_FOOTER

		    // set auto page breaks
		    $pdf->SetAutoPageBreak(TRUE, 5);

		    // set image scale factor
		    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		    // set some language-dependent strings (optional)
		    //if (@file_exists(dirname(__FILE__).'/lang/esp.php')) {
		    //  require_once(dirname(__FILE__).'/lang/esp.php');
		    //  $pdf->setLanguageArray($l);
		    //}

		    $tbl = "";


				 // ---------------------------------------------------------

		    // -- Portada.

		    $pdf->AddPage();
			$pdf->SetFont('helvetica', 'B', 12);
			//$pdf->SetFontSize(20);

			$image_file = pathfiles."/../images/".'logopdf.png';
		    $pdf->Image($image_file, 2, 2, 30, '');

			$pdf->SetXY(40, 3);
			$pdf->Write(0, date('d/m/Y'), '', 0, 'L', true, 0, false, false, 0);
			$pdf->SetXY(18, 3);
			$pdf->Write(0, 'PDA Objectius i Projectes', '', 0, 'C', true, 0, false, false, 0);
			
			$pdf->SetFont('helvetica', '', 7);

			$pdf->Ln(5);
			/*
			if (!empty($ConfigPla)){

				$pdf->SetXY(7, 20);
				$pdf->Write(0, str_replace("<p>", "", str_replace("</p>", "\n", $ConfigPla[1][descripcio_ca])), '', 0, 'L', true, 0, false, false, 0);

			}

			$pdf->Ln(2);*/

			$pdf->SetFillColor(255, 255, 255);
		    $pdf->SetTextColor(0,0,0);
		    $pdf->SetLineWidth(1.2);
		    $pdf->SetDrawColor(0,0,0);
			$pdf->setCellHeightRatio(2);


	  

		$caps = false;

		if (!empty($Vinculacio4))
		{
			
			
			$taula .= '
					    
					';

			foreach ($Vinculacio4 as $key_exp => $value_exp) 
			{
		

				
				// Capçaleres.
				if ($caps == false)
				{
					
					$taula .= '<table cellspacing="0" cellpadding="3" border="1">';

					$taula .= '<tr style="background-color:#CECECE;font-weight: bold;" >';

					$taula .= '<td width="50%">Objectius i projectes</td>';
					$taula .= '<td width="6%">Estat</td>';
				    $taula .= '<td width="11%">Calendari</td>';
				    $taula .= '<td width="11%">Gestor/a projecte<br>Equip de projecte intern<br>Equip de projecte extern</td>';
				    $taula .= '<td width="7%">% d\'execució</td>';
				    $taula .= '<td width="8%">Recursos<br>Pressupost</td>';
				    $taula .= '<td width="7%">Actualitzat</td>';	 
					$taula .= '</tr>';

					$caps = true;
				}
				
				// Condicions i format de dades copiat de consultaprojectes_llistat.php
				//if ($value_exp[clau_cero] == 0 &&(($nomespam == 1 && $value_exp[clau_pam] == 9) || $nomespam == 0) ) 
				//{
					$textdatesv = "<b>";
									if ($Vinculacio4dates[$value_exp[id]][datesinici][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesinici][0]!= "") $textdatesv .= date_format(date_create($Vinculacio4dates[$value_exp[id]][datesinici][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]!= "") $textdatesv .= " - ".date_format(date_create($Vinculacio4dates[$value_exp[id]][datesfinal][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_exp[id]][datesinici][0] == "0000-00-00" && $Vinculacio4dates[$value_exp[id]][datesfinal][0]== "0000-00-00") $textdatesv .= "------";
									$textdatesv .= "</b>";

					$taula .= '<tr style="background-color:#79bd99;">';

					$taula .= '

						<td> '.$value_exp[titol].'</td>
						<td></td>
	                    <td> '.$textdatesv.'</td>
	                    <td></td>
	                    <td> <b>'.$Vinculacio4tantspercents[$value_exp[id]][tantspercents].''.($Vinculacio4tantspercents[$value_exp[id]][tantspercents]==""?"0.00":"").' %</b></td>
	                    <td></td>
	                    <td></td>

					';
								
					$taula .= '</tr>';



					$tedadesproj = false;
					if (!empty($value_exp[idsproj])){
					
						$id = $value_exp[id];
					

						include "inici_inc.php";

		              	foreach ($DadesProjectes_llistat as $key_p => $value_p) 
	                    {
			              	

							if ($tedadesproj == false){
								$taula .= '<tr><td colspan="8"><table cellpadding="2">';
								$tedadesproj = true;
							}

							// Per l'export desde index i projectes posem 2 dades més.
							if ($idpagina == 999 || $idpagina == 5 || $idpagina == 6){
								$acronim = "(".$value_p[acronim].") ";
								$cap_projecte = " (".$value_p[nomresp]." ".$value_p[cognomsresp].")";
							}

							// Recursos.
							$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $value_p['recursos']),'id');

						
							// Pressupostos.
							$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_p["id"]),'any');
							$totalpressu = 0; //$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
							if (!empty($Pressupostos_ind)){
								foreach ($Pressupostos_ind as $key_a => $value_a) {
									//$totalpressu1 += $value_a['pressupost1'];
									//$totalpressu2 += $value_a['pressupost2'];
									//$totalpressu3 += $value_a['pressupost3'];
									$totalpressu += $value_a['pressupost1'];
									$totalpressu += $value_a['pressupost2'];
									$totalpressu += $value_a['pressupost3'];
								}
							}


							$taula .= '

								<tr>
									<td width="50%" style="border-bottom:0.1px solid black;">'.$acronim.' '.$value_p[nom].' '.$cap_projecte.'</td>
				                    <td width="6%" style="border-bottom:0.1px solid black;">'.$value_p[estatprojecte].'</td>
				                    <td width="11%" style="border-bottom:0.1px solid black;"><b>'.($value_p["inici"]!="00/00/0000"?$value_p["inici"]:"").' - '.($value_p["final"]!="00/00/0000"?$value_p["final"]:"").'</b></td>
				                    <td width="11%" style="border-bottom:0.1px solid black;">'.$value_p[nomresp]." ".$value_p[cognomsresp]."<br>----<br>".$value_p[nomsequip]."<br>----<br>".$value_p[nomextern].'</td>
				                    <td width="7%" style="border-bottom:0.1px solid black;text-align:center;">'.$value_p[tantpercent].' %</td>
				                    <td width="8%" style="border-bottom:0.1px solid black;">'.$Recursos_ind[1][text].'<br>'.number_format($totalpressu,2).' €</td>
				                    <td width="7%" style="border-bottom:0.1px solid black;text-align:right;">'.$value_p[updated].'</td>
				                </tr>

							'; 

							// Subprojectes.
							$Actuacions = $db->query("SELECT a.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio,
													  DATE_FORMAT(a.final, '%d/%m/%Y') as final, DATE_FORMAT(a.inici, '%d/%m/%Y') as inici, a.id
													  FROM pfx_actuacions a
													  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
													  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
													  WHERE a.clau_projecte = :idproj
													  GROUP By a.id ",array("idproj" => $value_p[id]));
													  
							if (!empty($Actuacions)){
	                            foreach ($Actuacions as $key_a => $value_a) 
								{
									
									$taula .= ' 

										<tr>
											<td width="5%"></td>
											<td width="50%" style="border-bottom:0.1px solid dotted;">'.$value_a["titol"].'</td>
						                    <td width="6%" style="border-bottom:0.1px solid dotted;">'.$value_a["estatactuacio"].'</td>
						                    <td width="39%" style="border-bottom:0.1px solid dotted;"><b>'.($value_a["inici"]!="00/00/0000"?$value_a["inici"]:"").' - '.($value_a["final"]!="00/00/0000"?$value_a["final"]:"").'</b></td>
						                </tr>

									';

									// Accions.
									$Tasques = $db->query("SELECT t.titol_ca as titol, IFNULL(pa.text,'----') as estatactuacio,
															  DATE_FORMAT(t.final, '%d/%m/%Y') as final, DATE_FORMAT(t.inici, '%d/%m/%Y') as inici
															  FROM pfx_tasques t
															  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
															  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
															  WHERE t.clau_actuacio = :idact
															  GROUP By t.id ",array("idact" => $value_a["id"]));
															  
									if (!empty($Tasques)){
		                                foreach ($Tasques as $key_t => $value_t) 
										{
											
											$taula .= ' 

												<tr>
													<td width="10%"></td>
													<td width="45%" style="border-bottom:0.1px solid dotted;">'.$value_t["titol"].'</td>
								                    <td width="6%" style="border-bottom:0.1px solid dotted;">'.$value_t["estatactuacio"].'</td>
								                    <td width="39%" style="border-bottom:0.1px solid dotted;"><b>'.($value_t["inici"]!="00/00/0000"?$value_t["inici"]:"").' - '.($value_t["final"]!="00/00/0000"?$value_t["final"]:"").'</b></td>
								                </tr>

											';

										}
									}

								}
							}
						}

	              	}

	              	
					if ($tedadesproj == true){
						$taula .= '</table></td></tr>';
					}

				//}
				
			}

			$taula .= '

					</table>
					';

			$pdf->writeHTMLCell(0, 0, $pdf->getX(), $pdf->getY(), $taula, 0, 0, 1, true, 'L', true);

			
		}

		

		

		// -----------------------------------------------------------------------------

			

			    //Close and output PDF document
			    //$pdf->Output('example_048.pdf', 'I');

			    //orientació L(horitzonal), P(vertical)

			    //Close and output PDF document
			    // D: download, F: Save file
			    $nomfile = 'PDA_Objectius_Projectes_'.date("Y-m-d_H-i-s").uniqid().'.pdf';
			    $rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;
			    $pdf->Output($rutafile,'F');

			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank" class="desc">
							<button class="btn btn-success btn-xs" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a>
						<script>
							$(document).ready(function($) {
								//$(".caixablanca").hide();
								window.open("'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1");
							});
						</script>
				';
				

				/*
			    $pdf->Output($nomfile,'D');
			    
			    $tmp = ini_get('upload_tmp_dir');

			    $fileatt = "./".$rutafile;
			    $fileatttype = "application/pdf";
			    $fileattname = $nomfile;

			    $file = fopen($fileatt, 'rb');
			    $data = fread($file, filesize($fileatt));
			    fclose($file);
			    
			    ob_end_clean();*/

			    

			    //============================================================+
			    // END OF FILE
			    //============================================================+

			

		

	

 	}


 	// Generar PDF.
 	if ($exportget == 5){


 		//echo "Generant PDF...<br>";
		//myFlush();

		

			// Inici creació PDF.

			// Include the main TCPDF library (search for installation path).
		    require_once( __DIR__.'/../../js/tcpdf/tcpdf.php');

		 	class MYPDF extends TCPDF {

			    //Page header
			    public function Header() {

			    	if ($this->tocpage) {
			            // *** replace the following parent::Header() with your code for TOC page
			            //parent::Header();

			        } else { 
			        	// *** replace the following parent::Header() with your code for normal pages
			        }
			            
		            // Logo
			        $image_file = pathfiles."/../images/".'logopdf.png';
			        $this->Image($image_file, 10, 5, 30, '');
			        // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

			        $dbb = new General($idioma, $app);
			        $DadesGenerals = $dbb->Llistats("general","",array(),'id');

			        $this->SetFont('helvetica', 'B', 7);
			        $this->SetTextColor(255, 0, 0);
			        $this->SetXY(158, 10);
			        $this->Cell(60, 0, $DadesGenerals[1][descripcio_ca], 0, false, 'C', 0, '', 0, false, 'M', 'M');
			        $this->SetTextColor(0, 0, 0);
			        $this->SetXY(107, 15);
			        $this->Cell(100, 0, 'Actuacions i Projectes', 0, false, 'C', 0, '', 0, false, 'M', 'M');

					$this->SetXY(7, 20);
					$this->setCellHeightRatio(0.2);
					$this->SetDrawColor(0,0,0);
					$this->Cell(195, 0,'', '', 1, 'C', 1, '', 0, false, 'T', 'C');
		       
			        
			    }

			    // Page footer
			    
			    public function Footer() {
			    	
			        if ($this->tocpage) {
			            // *** replace the following parent::Footer() with your code for TOC page
			            //parent::Footer();
			        } else {
			            // *** replace the following parent::Footer() with your code for normal pages
			            // Position at 15 mm from bottom
		        		$this->SetY(-15);
				        // Set font
				        $this->SetFont('helvetica', 'I', 8);
				        // Page number
				        $this->Cell(0, 20, 'Pàgina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		        	
				        
			        }
			    }
			    
			}
		  

		    // create new PDF document
		    $pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		    // Path imatges.
		    $imagePATH=pathfiles."/../../images/";

		    // set document information
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetTitle($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetSubject($app['translator']->trans('PDA Objectius i Projectes'));
		    $pdf->SetKeywords($app['translator']->trans('PDA Objectius i Projectes'));

		    //$pdf->startPage( $orientation = '',$format = '', true );
		   
		    //$pdf->SetHeaderData("../../images/logo.jpg", '50', '', ""); 
		    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Seguiment de projectes");
		    $pdf->SetHeaderData('','','','');

		    $pdf->setPrintHeader(false);
		    //$pdf->setPrintHeader();
		    // set header and footer fonts
		    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
		    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		    $pdf->setPrintFooter(false);

		    // set default monospaced font
		    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		    // set margins
		    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetMargins(7, 7, 7);
		    $pdf->SetHeaderMargin(0);
		    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		    $pdf->SetFooterMargin(0); // PDF_MARGIN_FOOTER

		    // set auto page breaks
		    $pdf->SetAutoPageBreak(TRUE, 5);

		    // set image scale factor
		    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		    // set some language-dependent strings (optional)
		    //if (@file_exists(dirname(__FILE__).'/lang/esp.php')) {
		    //  require_once(dirname(__FILE__).'/lang/esp.php');
		    //  $pdf->setLanguageArray($l);
		    //}

		    $tbl = "";


				 // ---------------------------------------------------------

		    // -- Portada.

		    $pdf->AddPage();
			$pdf->SetFont('helvetica', 'B', 12);
			//$pdf->SetFontSize(20);

			$image_file = pathfiles."/../images/".'logopdf.png';
		    $pdf->Image($image_file, 2, 2, 30, '');

			$pdf->SetXY(40, 3);
			$pdf->Write(0, date('d/m/Y'), '', 0, 'L', true, 0, false, false, 0);
			$pdf->SetXY(18, 3);
			$pdf->Write(0, 'PDA Objectius i Projectes', '', 0, 'C', true, 0, false, false, 0);
			
			$pdf->SetFont('helvetica', '', 7);

			$pdf->Ln(5);
			/*
			if (!empty($ConfigPla)){

				$pdf->SetXY(7, 20);
				$pdf->Write(0, str_replace("<p>", "", str_replace("</p>", "\n", $ConfigPla[1][descripcio_ca])), '', 0, 'L', true, 0, false, false, 0);

			}

			$pdf->Ln(2);*/

			$pdf->SetFillColor(255, 255, 255);
		    $pdf->SetTextColor(0,0,0);
		    $pdf->SetLineWidth(1.2);
		    $pdf->SetDrawColor(0,0,0);
			$pdf->setCellHeightRatio(2);


	  

		$caps = false;

		if (!empty($Vinculacio4))
		{
			
			
			$taula .= '
					    
					';
			
			foreach ($Vinculacio4 as $key_exp => $value_exp) 
			{
				
				if (!empty($value_exp[idsproj])){
					
					
					foreach ($DadesProjectes_llistat as $key_exp => $value_exp) 
					{
				
						// Capçaleres.
						if ($caps == false)
						{
							
							$taula .= '<table cellspacing="0" cellpadding="3" border="1">';

							$taula .= '<tr style="background-color:#CECECE;font-weight: bold;" >';

							$taula .= '<td width="7%">(Acrònim centre/servei) Objectiu</td>';
						    $taula .= '<td width="45%">Nom del projecte</td>';
						    $taula .= '<td width="11%">Gestor/a projecte<br>Equip de projecte intern<br>Equip de projecte extern</td>';
						    $taula .= '<td width="11%">Data inici - Data final</td>';	
						    $taula .= '<td width="6%">Estat del projecte</td>';	
						    $taula .= '<td width="7%">% d\'execució del projecte</td>';	 
						    $taula .= '<td width="8%">Recursos<br>Pressupost total agregat</td>';
						    $taula .= '<td width="7%">Darrera actualització projecte</td>';	
							$taula .= '</tr>';

							$caps = true;
						}
				
				

						// Per l'export desde index i projectes posem 2 dades més.
						if ($idpagina == 999 || $idpagina == 5 || $idpagina == 6){
							$acronim = "(".$value_p[acronim].") ";
							$cap_projecte = " (".$value_p[nomresp]." ".$value_p[cognomsresp].")";
						}

						// Recursos.
						$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $value_p['recursos']),'id');

					
						// Pressupostos.
						$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_p["id"]),'any');
						$totalpressu = 0; //$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
						if (!empty($Pressupostos_ind)){
							foreach ($Pressupostos_ind as $key_a => $value_a) {
								//$totalpressu1 += $value_a['pressupost1'];
								//$totalpressu2 += $value_a['pressupost2'];
								//$totalpressu3 += $value_a['pressupost3'];
								$totalpressu += $value_a['pressupost1'];
								$totalpressu += $value_a['pressupost2'];
								$totalpressu += $value_a['pressupost3'];
							}
						}

						preg_match('/(\d+(\.\d+)*)/', $value_exp[nomvinc], $codiactuacio);
						$taula .= '

							<tr>

								<td width="7%" style="border-bottom:0.1px solid black;">'.$codiactuacio[0].' '.$acronim.'</td>
								<td width="45%" style="border-bottom:0.1px solid black;">'.$value_exp[nom].'</td>
								<td width="11%" style="border-bottom:0.1px solid black;">'.$value_exp[nomresp]." ".$value_exp[cognomsresp]."<br>----<br>".$value_exp[nomsequip]."<br>----<br>".$value_exp[nomextern].'</td>
								<td width="11%" style="border-bottom:0.1px solid black;"><b>'.($value_exp["inici"]!="00/00/0000"?$value_exp["inici"]:"").' - '.($value_exp["final"]!="00/00/0000"?$value_exp["final"]:"").'</b></td>
			                    <td width="6%" style="border-bottom:0.1px solid black;">'.$value_exp[estatprojecte].'</td>
			                    <td width="7%" style="border-bottom:0.1px solid black;text-align:center;">'.$value_exp[tantpercent].' %</td>
			                    <td width="8%" style="border-bottom:0.1px solid black;">'.$Recursos_ind[1][text].'<br>'.number_format($totalpressu,2).' €</td>
			                    <td width="7%" style="border-bottom:0.1px solid black;text-align:right;">'.$value_exp[updated].'</td>
			                </tr>

						'; 

						

	              	}

	              	
					break;

				}
				
			}

			$taula .= '

					</table>
					';

			$pdf->writeHTMLCell(0, 0, $pdf->getX(), $pdf->getY(), $taula, 0, 0, 1, true, 'L', true);

			
		}

		

		

		// -----------------------------------------------------------------------------

			

			    //Close and output PDF document
			    //$pdf->Output('example_048.pdf', 'I');

			    //orientació L(horitzonal), P(vertical)

			    //Close and output PDF document
			    // D: download, F: Save file
			    $nomfile = 'PDA_Objectius_Projectes_'.date("Y-m-d_H-i-s").uniqid().'.pdf';
			    $rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;
			    $pdf->Output($rutafile,'F');

			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank" class="desc">
							<button class="btn btn-success btn-xs" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a>
						<script>
							$(document).ready(function($) {
								//$(".caixablanca").hide();
								window.open("'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1");
							});
						</script>
				';
				

				/*
			    $pdf->Output($nomfile,'D');
			    
			    $tmp = ini_get('upload_tmp_dir');

			    $fileatt = "./".$rutafile;
			    $fileatttype = "application/pdf";
			    $fileattname = $nomfile;

			    $file = fopen($fileatt, 'rb');
			    $data = fread($file, filesize($fileatt));
			    fclose($file);
			    
			    ob_end_clean();*/

			    

			    //============================================================+
			    // END OF FILE
			    //============================================================+

			

		

	

 	}


 	// Eliminar temporals.
    $pathdel = __DIR__."/../../../secretfiles/tmp/" ;


	if ($handle = opendir($pathdel)) {

     	while (false !== ($file = readdir($handle))) {

            if (preg_match('/\.png$/i', $file)) {
			      unlink($pathdel.$file);
			}
				
	     }
   	}




	

	