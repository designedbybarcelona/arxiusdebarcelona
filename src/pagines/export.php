<?php

	$idpagina = 6;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';
	
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Clases Php.
	require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel.php");

	// Nom fitxer.
	$nomfile = 'projectmonitor_'.date('YmdHis').'.csv';
	
	// Inicialitza Excel.
    $objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	$rowCount = 1;
	$colCount = 0;

	$DadesProjectes = $dbb->Llistats("projectes"," " ,array(""), "id", false );

	$estatsprojectes = array();
	foreach ($Paraules as $key_p => $value_p) 
	{
		if ($value_p['codi'] == 'estatprojecte' && $value_p['idioma'] == 'ca')
		{
			$estatsprojectes[$value_p['clau']] = $value_p['text'] ;
		}
	}




	if (!empty($DadesProjectes)){
			
			// Capçaleres.
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "result_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "decidim_category_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "decidim_scope_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "parent_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "external_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "parent_external_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "start_date");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "end_date");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "decidim_accountability_status_id");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "progress");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "proposal_ids");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "title_ca");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "title_es");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "title_en");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "description_ca");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "description_es");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "description_en");
			$colCount++;

			$colCount = 0;
			$rowCount = 2;
	
			foreach ($DadesProjectes as $key => $value) 
			{
				$colCount = 0;

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[id]));
				$colCount++;

				$txtactuacions = "";
				if (!empty($value[vinculacio])){
					$vinculacio = json_decode($value[vinculacio]);
					if (is_array($vinculacio)){
						foreach ($vinculacio as $key_v => $value_v) 
						{
							$value_v = substr(strstr($value_v, '_'), strlen('_'));
							$RegistreVinculacio = $dbb->Llistats("projectes_vinculacio"," AND t.id = :id AND nivell = 4",array("id"=>$value_v), "id", false);
							if (!empty($RegistreVinculacio)){
								//$txtactuacions .= str_replace(";", ":", $RegistreVinculacio[1][codi])." ".str_replace(";", ":", $RegistreVinculacio[1][titol]).";";
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value_v));
				
							}
						}
					}
				}
				//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($txtactuacions));
				$colCount++;
				
				$datainici = date("d/m/Y",strtotime($value[inici]));
				if ($datainici == "30/11/-0001") $datainici="";
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($datainici)); 
				$colCount++;
				$datafinal = date("d/m/Y",strtotime($value["final"]));
				if ($datafinal == "30/11/-0001") $datafinal="";
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($datafinal));
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[tantpercent]));
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
			    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[titol_ca]));
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[descripcio]));
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "");
				$colCount++;
				//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($estatsprojectes[$value[estat]]));
				//$colCount++;
				
				
				

				$rowCount++;

			}

			// We'll be outputting an excel file
			header('Content-Encoding: UTF-8');
			header('Content-type: text/csv; charset=UTF-8');
			header('Content-Disposition: attachment; filename="'.$nomfile.'"');
			header("Pragma: no-cache");
			header("Expires: 0");
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save('php://output');
	}

	$dadesplantilla = array();
	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

	exit();