<?php 
	
	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 && $primeraentrada!=true )
	{
		
		$usuarivinculacio = $dbb->app['session']->get(constant('General::nomsesiouser')."-vinculacio");
		
		/*
		$condicio_permis = " AND (p.cap_projecte = ".$app['session']->get(constant('General::nomsesiouser')). "
								OR
								  FIND_IN_SET('".$app['session']->get(constant('General::nomsesiouser'))."' ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0 
								 )";
		*/

		$condicio_permis = $condicio_permis2 = "";
		if (!empty($usuarivinculacio)){

			// Eliminar ID's sense "_" pq indiquen el primer nivell.
			//$usuarivinculacio = array_unique(stripslashes_deep(json_decode($usuarivinculacio)));
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{
				if (strpos($value_uv, "_") === false) unset($usuarivinculacio[$key_uv]);
			}

			
			$primer = false; $lastElement = end($usuarivinculacio);
			foreach ($usuarivinculacio as $key_uv => $value_uv) 
			{
				if ($primer == false) { $condicio_permis .= " AND ( "; $condicio_permis2 .= " AND ( "; $primer = true; }
				$condicio_permis .= " FIND_IN_SET('$value_uv' ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";
				$condicio_permis2 .= " concat(pv.clau_pam,'_',pv.id) = REPLACE( REPLACE( REPLACE( '$value_uv','[', ''),']' ,'') ,'\"','') ";
				if($value_uv == $lastElement) {  
					$condicio_permis .= " ) ";
					$condicio_permis2 .= " ) ";
				}else{ 
					$condicio_permis .= " OR "; 
					$condicio_permis2 .= " OR "; 
				}

			}
			
		}


		if (empty($condicio_permis)){
			//$condicio_permis .= " AND 1 = 2";
			//$condicio_permis2 .= " AND 1 = 2";
		}
		
		
	}


	/*	Versió Inner

	$condiciovincusuari = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

	$DadesProjectes =  $dbb->FreeSql("SELECT p.titol_ca as nom, IFNULL(pa.text,'----') as estatprojecte,
									  (select count(a2.id) from pfx_projectes_fites a2 where a2.clau_projecte = p.id  ) as contafites, 
									  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id,
									  p.tantpercent, p.estat as estat, DATE_FORMAT(p.final, '%Y/%m/%d') as final2, pv.nivell as nivell,
									  DATE_FORMAT(p.inici, '%d/%m/%Y') as inici
									  
									  FROM pfx_projectes p
									  INNER JOIN pfx_projectes_vinculacio pv ON $condiciovincusuari
									  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
									  WHERE  1=1 $condicio_permis $condicio_permis2 AND pv.nivell = 4
									  GROUP By p.id
									  ORDER BY p.titol_ca ",array());
	*/

	$condidact = "";	
	unset($arraycondicio);					  
	if (!empty($id)){
		$condidact = " AND rp.id_act = :id ";
		$arraycondicio = array("id"=>$id);
	}
	
	
	$DadesProjectes_llistat =  $db->query("SELECT p.titol_ca as nom, IFNULL(pa.text,'----') as estatprojecte,
								  (select count(a2.id) from pfx_projectes_fites a2 where a2.clau_projecte = p.id  ) as contafites, 
								  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id,
								  p.tantpercent, p.estat as estat, DATE_FORMAT(p.final, '%Y/%m/%d') as final2, pv.nivell as nivell, DATE_FORMAT(p.inici, '%Y/%m/%d') as inici2,
								  DATE_FORMAT(p.inici, '%d/%m/%Y') as inici, DATE_FORMAT(p.updated, '%d/%m/%Y') as updated, UNIX_TIMESTAMP(p.updated) as updatedunix, p.final as datafinal, u3.nom as nomresp, u3.cognoms as cognomsresp,
								  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(po.acronim),'') SEPARATOR ' <br> ') as acronim,
								  GROUP_CONCAT( DISTINCT IFNULL(a.id,'') SEPARATOR ',') as idsactuacions, p.estrategic,
								  UNIX_TIMESTAMP(p.inici) as iniciunix, UNIX_TIMESTAMP(p.final) as finalunix,
								  pv.titol_ca as nomvinc, p.recursos,
								  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u5.nom,' ',u5.cognoms),'') SEPARATOR ' | ') as nomsequip,
								  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u4.titol_ca),'') SEPARATOR ' | ') as nomextern
								  
								  FROM pfx_projectes p
								  INNER JOIN pfx_projectes_relacionsvinc rp ON rp.id_proj = p.id
								  INNER JOIN pfx_projectes_vinculacio pv ON pv.id = rp.id_act
								  $innerresponsables
								  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau 
								  LEFT JOIN pfx_usuaris u5 ON FIND_IN_SET(u5.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
								  LEFT JOIN pfx_projectes_operadors u4 ON FIND_IN_SET(u4.id ,REPLACE( REPLACE( REPLACE( p.operadors,'[', ''),']' ,'') ,'\"','')) > 0
								  LEFT JOIN pfx_usuaris u3 ON p.cap_projecte = u3.id
								  LEFT JOIN pfx_projectes_organs_e po ON FIND_IN_SET(po.id ,REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"','')) > 0
								  LEFT JOIN pfx_actuacions a ON a.clau_projecte = p.id
								  WHERE 1=1 $condicio_permis  $condicio_permis2 $condidact $condicioprojectes  $condicioresponsables $condicio_permis_centres
								  AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'  AND pv.nivell = 4
								  GROUP By p.id
								  ORDER BY p.titol_ca ",$arraycondicio);  

	// $condicio_permis_uvinc $condicio_permis_projectes

	if (!empty($DadesProjectes_llistat)){
		foreach ($DadesProjectes_llistat as $key_p => $value_p) 
		{	
			$DadesProjectes_llistat[$key_p][colorsemafor] = 0;
			$diesfinsfinal = (strtotime($value_p["final2"]) - time()) / 60 / 60 / 24;
			$DadesProjectes_llistat[$key_p][diesfinsfinal] = $diesfinsfinal;

		
			//if ($diesfinsfinal > 0){;
			//	if ( $diesfinsfinal >= $ConfigAlertes[1][diesi]   ) {
			//		$DadesProjectes_llistat[$key_p][colorsemafor] = 1; // Verd.
			//	}elseif ($diesfinsfinal > $ConfigAlertes[2][diesi] && $diesfinsfinal < $ConfigAlertes[2][diesf] ) {
			//		$DadesProjectes_llistat[$key_p][colorsemafor] = 2; // Àmbar.
			//	}elseif ($diesfinsfinal <= $ConfigAlertes[3][diesi] ) {
			//		$DadesProjectes_llistat[$key_p][colorsemafor] = 3; // Vermell.
			//	}
			//}else{
			//	$DadesProjectes_llistat[$key_p][colorsemafor] = 3; // Vermell.
			//}
			
			

			// Colors.
			if ($value_p[estat] == 1) { $DadesProjectes_llistat[$key_p][colorsemafor] = 2; } // Iniciat, Àmbar.
			if ($value_p[estat] == 2) { $DadesProjectes_llistat[$key_p][colorsemafor] = 5; } // No iniciat, Blau.
			if ($value_p[estat] == 3) { $DadesProjectes_llistat[$key_p][colorsemafor] = 3; } // Amb dificultats, Vermell.
			if ($value_p[estat] == 4) { $DadesProjectes_llistat[$key_p][colorsemafor] = 1; } // Finalitzat, Verd.
			if ($value_p[estat] == 5) { $DadesProjectes_llistat[$key_p][colorsemafor] = 4; } // Descartat, Gris.
			if ($value_p[estat] == ""){ $DadesProjectes_llistat[$key_p][colorsemafor] = 4; } // Gris.


			// Símbols.
			$diesfinsinici = (strtotime($value_p["inici2"]) - time()) / 60 / 60 / 24;
			$DadesProjectes_llistat[$key_p][diesfinsinici] = $diesfinsinici;
			// No iniciat i data inici inferior a l'actual.
			if ($value_p[estat] == 2 && $diesfinsinici <= 0) {
				$DadesProjectes_llistat[$key_p][simbol] = '<i class="fa fa-exclamation-triangle"></i>';
			}
			// Iniciat o Amb dificultats i data final inferior a l'actual.
			if ( ($value_p[estat] == 1 || $value_p[estat] == 3) && $diesfinsfinal <= 0) {
				$DadesProjectes_llistat[$key_p][simbol] = '<i class="fa fa-exclamation-triangle"></i>';
			}

			$codnomvinc = explode(".", $value_p[nomvinc]);
			$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
			$DadesProjectes_llistat[$key_p][codnomvinc] = $nomvinc;

		}
	}
	
	

	$Organse = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");

