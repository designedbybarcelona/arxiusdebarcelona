<?php

	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	ini_set('zlib.output_compression', 0);

	$text1 = "<h1>".$app['translator']->trans('Segueix l\'evolució del PDA 2016-2019')." <small>".$app['translator']->trans('Filtra els objectius i projectes que t\'interessin')."</small></h1>";
	$text2 = "<li class=\"active\">".$app['translator']->trans('PDA+ 2016-2019')."</li>";

	$pas = 0;
	if (isset($_GET['pa']))
	{
		$pas = intval($_GET['pa']);
	}

	$nusprojecte = "Estàs veient el desplegament del PDA. <button class=\"btn btn-xs btn-default\" style=\"padding: 0px 16px; background:#cd0d2d;\"><i class=\"fa fa-search\"></i></button>";
	$nusfiltre = "";
	//$hihafiltre = false;
	$hihafiltre = true;

	// Filtres
	require_once("consultesfiltres_inc.php");

	if ($hihafiltre) $nusfiltre = '<h5 style="cursor:pointer;"><span class="mostrabuscador" ><i class="fa fa-search" aria-hidden="true"></i> Estàs veient: </span><small class="esborrafiltre">(<i class="fa fa-trash" aria-hidden="true"></i> Esborrar)</small> <div class="gotop" style="display:inline-block; float:right;"><button class="btn btn-xs btn-default" style="padding: 0px 16px; background:#cd0d2d; font-size: 14px;"><i class="fa fa-search"></i> Cercador de projectes</button><div></h5>';

	// Final filtres.

	// Inicialitza contadors.
	foreach ($Paraules as $key => $value) 
	{
		if ($value['codi'] == 'estatprojecte' && $value['idioma'] == 'ca')
		{
			$estatsprojecte[$value['clau']] = 0 ;
			$paraulesestatsprojecte[$value['clau']] = $value[text] ;

			// Detalls nus.
	    	if (!empty($estatsget)){
	    		if(in_array($value[clau], $estatsget)){
	    			$estatsfiltre .= $value[text].", ";
	    		}
	    	}
		}
	}

	// Colors eixos.
	$colorseixos = array("#e3dfee","#ffff4f","#c1ff4d","#b9dde9","#fdc200");


	$db = new Db();

	require_once("consultesprojectes_inc.php");


	if (empty($getenc['inici'])){
		if ($datamenor == 9999999999) {
			$datamenor = "";
		}else{
			$datamenor = date("Y-m-j",$datamenor);
			$datamenor = explode("-", $datamenor);
		}
	}else{
		$datamenor = $getenc['inici'];
		$datamenor = explode("-", $datamenor);
	}
	if (empty($getenc['final'])){
	    if ($datamajor == 0000000000) {
	    	$datamajor = "";
	    }else{
	    	$datamajor = date("Y-m-j",$datamajor);
			$datamajor = explode("-", $datamajor);
	    }
	}else{
		$datamajor = $getenc['final'];
		$datamajor = explode("-", $datamajor);
	}

	

	$Vinculacio = $dbb->Llistats("projectes_vinculacio"," AND (nivell = 1 OR nivell = 2 OR nivell = 3) AND estat = true ", array(), " SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 1), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 2), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 3), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 4), '.', -1) + 0");


	$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

	$Centres = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");

	// Noms eixos.
	$arraynomseixos = array();
	foreach ($Vinculacio as $key_di => $value_di) {
		if ($value_di[nivell]==2){
			//if (empty($eixosget)){
				$arraynomseixos[$value_di[id]] = $value_di[titol];
			//}else{
			//	if(in_array($value_di[id], $eixosget)){
	    	//		$arraynomseixos[$value_di[id]] = $value_di[titol];
	    	//	}
			//}
		}
		if ($value_di[nivell]==3){
			//if (empty($eixosget)){
				$arraynomslinies[$value_di[id]] = $value_di[titol];
			//}else{
			//	if (empty($liniesget)){
			//		if(in_array($value_di[parent_id], $eixosget)){
		    //			$arraynomslinies[$value_di[id]] = $value_di[titol];
		    //		}
			//	}else{
			//		if(in_array($value_di[id], $liniesget)){
		    //			$arraynomslinies[$value_di[id]] = $value_di[titol];
		    //		}
			//	}
			//}
		}
		if ($value_di[nivell]==1){
			$arraynomspads[$value_di[id]] = $value_di[titol];
		}
		// Detalls nus.
		if ($value_di[nivell]==1 && !empty($vinculacionsget)){
    		if(in_array($value_di[id], $vinculacionsget)){
    			$nusfiltrepam .= $value_di[titol].", ";
    		}
    	}
		if (!empty($eixosget)){
			if(in_array($value_di[id], $eixosget)){
				$nusfiltreeix .= $value_di[titol].", ";
			}
		}
		if ($value_di[nivell]==3 && !empty($liniesget)){
    		if(in_array($value_di[id], $liniesget)){
    			$nusfiltrelinies .= $value_di[titol].", ";
    		}
    	}
	}
	ksort($tantsxcentseixos);
	ksort($tantsxcentslinies);
	ksort($arraynomseixos);
	//ksort($arraynomslinies);


	$blocsactius = array('1','1','1','1','1');

	// D'entrada no mostrem res.
	/*
	if (!isset($_GET['f'])){
		$blocsactius = array('0','0','0','0','0');
	}
	*/


	

	// 1. Xifres.
	if ($blocsactius[0] == 1)
	{

		$Dades = '
			
			<div class="row">
				<div class="col-md-12">
		           <br><br>
		        </div>
	        </div>

			<div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                  Principals indicadors
		                 <hr style="border-top: 1px solid #cc032f;">
		            </h3>
		        </div>
	        </div>


	    ';
	   	

	   	// Sense filtre mostra pam i tots els pad.
	   	if (empty($vinculacionsget)){
	   		//$espam = 1;
	   		$pampads = array(1);

	   	}else{
	   		$pampads = $vinculacionsget;
	   	}

	   	$pads10mostrat = false;
	    foreach ($pampads as $key_pm => $value_pm) 
	    {


	    	$Dades .= ' 
			<div class="row">
	    	';
	    
	    		/*
			    foreach ($Vinculacio as $key_di => $value_di) {
			    	
			    	
			    	if ($value_di[nivell]==2){
			    		$Dades .= '
						
							<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
				                <div class="card">
				                    <div class="content">
				                    	<div class="header">
					                       '.$value_di[titol].'
					                    </div>
					                    <div class="text-center">
					                        <span class="chart" data-percent="'.number_format($tantsxcentseixos[$value_di[id]][tantpercent]/$tantsxcentseixos[$value_di[id]][conta],2).'">
					                          <i class="fa fa-clock-o"></i> <span class="percent"></span>
					                        </span>
				                        </div>
				                        <p class="category">% d\'execució </p>
				                    </div>
				                </div>
				            </div>

				    	';

				    	
				    	
			    	}
			    	
			    }

			    
			    */

			    if ($getenc['percent1'] != ""||$getenc['percent2'] != "" && ($percent1get != 0 || $percent2get != 100) ){
			    	$nusfiltretantpercent = $percent1get.($getenc['percent2'] != ""?" i <= ".$percent2get:"");
			    }

		
			    if ($getenc['percent3'] != ""||$getenc['percent4'] != "" && ($percent3get != 0 || $percent4get != 100) ){
			    	$nusfiltretantpercent2 = $percent3get.($getenc['percent4'] != ""?" i <= ".$percent4get:"");
			    }

			   
				if ($contaprojecte>0){
              		$mitja = $sumatoritant/$contaprojecte;
              		$mitja = number_format($mitja,2);
              	}else{
              		$mitja = 0;
              	}

              	/*
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	                    	<div class="header">
		                       PROJECTES EN EXECUCIÓ
		                    </div>
	                        <h1 class="text-center">'.($estatsprojecte[1]+$estatsprojecte[3]).' <i class="fa fa-pencil-square-o fa-2" style="font-size: 30px;"></i></h1>
	                    </div>
	           			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	                    	<div class="header">
		                       PROJECTES AMB DIFICULTATS
		                    </div>
	                        <h1 class="text-center">'.$estatsprojecte[4].' <i class="fa fa-exclamation-triangle fa-2" style="font-size: 30px;"></i></h1>
	                    </div>
		            </div>    
              	*/

		        $tantpercentexecucio = ($espam==1?$tantsxcentspads[$value_pm]:($tantsxcentspads[$value_pm][conta]!=0?number_format($tantsxcentspads[$value_pm][tantpercent]/$tantsxcentspads[$value_pm][conta],2):0));

	        
			    $Dades .= '
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
			    ';
			    			if (count($vinculacionsget)<=10 || (count($vinculacionsget)>10 && $value_pm == 1 ))
							{
			                    $Dades .= '
			                    <div class="content ">
			                    	<div class="header">
				                       <h4>'.($espam==1?"PAM":$arraynomspads[$value_pm]).'  </h4>
				                    </div>
				                    
			                    	<div id="myChart_'.$value_pm.'" style="height: 200px;"></div>

			                    </div>';
			                }
			    $Dades .= '
			                    <div class="content">
			                    	

									<div id="chart-area-'.$value_pm.'" style="height: 200px;"></div>
							        	

								</div>
			                
			            </div>
						
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
							
					        <div id="chart-area2-'.$value_pm.'" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
										
						</div>
							


	            ';
				    

				    /*
						<div style="height: 36vh; position: relative; ">
							<div style="width: 100%; height: 40px; position: absolute; top: 50%; left: 0; margin-top: -30px; line-height:19px; text-align: center; z-index: 998">
							    <h3>'.$tantpercentexecucio.'%</h3>
							</div>
	                    	<canvas id="myChart_'.$value_pm.'" width="220" height="220"></canvas>
                    	</div>
				    */
				

	            
	    $Dades .= '
	            

	        </div>

	        <div class="row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		               
		                 <div id="chart-area3-'.$value_pm.'"></div>

					</div>
						    
				
	        </div>

	    ';

	    /*
			<div id="canvas-holder2" style="position: relative; height: 50vh">
		        <canvas id="chart-area2-'.$value_pm.'" />
		    </div>

		    <div id="canvas-holder3" style="position: relative; height: 50vh">
		        <canvas id="chart-area3-'.$value_pm.'" />
		    </div>
	    */

		$Dades .= '

			<div class="row">

					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		               
		                 <div id="chart-area4-'.$value_pm.'"></div>

					</div>

					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		               
		                 <div id="chart-area5-'.$value_pm.'"></div>

					</div>
						    
	        </div>

		';

	    if ((count($vinculacionsget)>10 || $value_pm == 1) && $pads10mostrat==false)
		{
			$pads10mostrat = true;
			//$Dades .= '<div class="row"> <div style="text-align:center;"><h4>GRAU D\'EXECUCIÓ DELS PAD</h4></div>';
			foreach ($Vinculacio as $key_di => $value_di) {
				    	
				    	
		    	if ($value_di[nivell]==1 and $value_di[id] != 1){
		    		$Dades .= '
					
						<div class="col-lg-15 col-md-3 col-sm-3 col-xs-6" >
			                <div  style="height:260px;">
			                    <div class="content">
			                    	<div class="header" style="font-size: 13px !important;">
				                       '.str_replace("2016", "<br>2016", $value_di[titol]).'
				                    </div>
				                    <div class="text-center">
				                        
										<div id="tantpercent-'.$value_di[id].'" style="min-width: 200px; height: 200px; max-width: 300px; margin: 0 auto"></div>

			                        </div>
			                    </div>
			                </div>
			            </div>

			    	';

			    	/*
						<span class="chart" data-percent="'.($tantsxcentspads[$value_di[id]][conta]!=0?number_format($tantsxcentspads[$value_di[id]][tantpercent]/$tantsxcentspads[$value_di[id]][conta],2):0).'">
                          <i class="fa fa-clock-o"></i> <span class="percent"></span>
                        </span>
			    	*/

			    	
			    	
		    	}
		    	
		    }
		    $Dades .= '</div>';
		    
		    
		}

	    $Dades .= '

	        <script src="../js/jquery.easing.min.js"></script>
	        <script src="../js/consultes/jquery.easypiechart.min.js"></script>
	        <script src="../js/consultes/chartjs/Chart.js"></script>
	        <script>
	        $(function() {
				
				// Scroll menú consultes
			 	$(\'a[href*=#]:not([href=#])\').click(function() {
				    if (location.pathname.replace(/^\//,\'\') == this.pathname.replace(/^\//,\'\') && location.hostname == this.hostname) {
				      var target = $(this.hash);
				      target = target.length ? target : $(\'[name=\' + this.hash.slice(1) +\']\');
				      if (target.length) {
				        $(\'html,body\').animate({
				          scrollTop: target.offset().top-150
				        }, 1000);
				        return false;
				      }
				    }
			  	});

				Highcharts.setOptions({
				    chart: {
				        style: {
				            fontFamily: "Roboto",
				        }
				    },
				    credits: {
				        enabled: false
				    },
				});

	            
				';

				if (count($vinculacionsget)>10 || $value_pm == 1)
				{
					
					foreach ($Vinculacio as $key_di => $value_di) {

						if ($value_di[nivell]==1 and $value_di[id] != 1){

							$Dades .= '

								Highcharts.chart("tantpercent-'.$value_di[id].'", {
				                    chart: {
				                        plotBackgroundColor: null,
				                        plotBorderWidth: 0,
				                        plotShadow: false
				                    },
				                    colors: ["#cd0d2d", "#e1e1e1"],
				                    title: {
				                        text: \''.($tantsxcentspads[$value_di[id]][conta]!=0?number_format($tantsxcentspads[$value_di[id]][tantpercent]/$tantsxcentspads[$value_di[id]][conta],2):0).'%\',
				                        align: \'center\',
				                        verticalAlign: \'middle\',
				                        y: 40
				                    },
				                    tooltip: {
				                    	enabled: false,
				                        //pointFormat: \'{series.name}: <b>{point.percentage:.1f}%</b>\'
				                    },
				                    plotOptions: {
				                        pie: {
				                            dataLabels: {
				                                enabled: false,
				                                distance: -50,
				                                style: {
				                                    fontWeight: \'bold\',
				                                    color: \'white\'
				                                }
				                            },
				                            startAngle: -90,
				                            endAngle: 90,
				                            center: [\'50%\', \'75%\']
				                        }
				                    },
				                    series: [{
				                        type: \'pie\',
				                        name: "% GRAU D\'EXECUCIÓ GENERAL",
				                        innerSize: \'50%\',
				                        data: [
				                            [\''.($tantsxcentspads[$value_di[id]][conta]!=0?number_format($tantsxcentspads[$value_di[id]][tantpercent]/$tantsxcentspads[$value_di[id]][conta],2):0).' %\',   '.($tantsxcentspads[$value_di[id]][conta]!=0?number_format($tantsxcentspads[$value_di[id]][tantpercent]/$tantsxcentspads[$value_di[id]][conta],2):0).'], 
				                            [\'\',       '.(100-($tantsxcentspads[$value_di[id]][conta]!=0?number_format($tantsxcentspads[$value_di[id]][tantpercent]/$tantsxcentspads[$value_di[id]][conta],2):0)).'],
				                            {
				                                name: \'\',
				                                y: 0.2,
				                                dataLabels: {
				                                    enabled: false
				                                },
				                            }
				                            
				                        ]
				                    }]
				                });
							';
						}
					}
				}

				$Dades .= '

	          	
				
				Highcharts.theme = {
   					colors: ["#cd0d2d", "#e1e1e1"]
   				}
				Highcharts.setOptions(Highcharts.theme);

				Highcharts.chart("myChart_'.$value_pm.'", {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        margin: [0, 0, 0, 0]
                    },
                    title: {
                        text: \''.$tantpercentexecucio.'%\',
                        align: \'center\',
                        verticalAlign: \'middle\',
                        y: 50,
                    },
                    subtitle: {
                        text: "GRAU D\'EXECUCIÓ GENERAL",
                        align: \'center\',
                        y: 10,
                        style:{
                        	fontSize: "14px",
                        }
                    },
                    lang:{
						downloadPNG: "Descarregar imatge PNG"
                    },
                    tooltip: {
                    	enabled: false,
                        //pointFormat: \'{series.name}: <b>{point.percentage:.1f}%</b>\'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: false,
                                distance: -50,
                                style: {
                                    fontWeight: \'bold\',
                                    color: \'white\'
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: [\'50%\', \'75%\']
                        }
                    },
                    series: [{
                        type: \'pie\',
                        name: "GRAU D\'EXECUCIÓ GENERAL",
                        innerSize: \'50%\',
                        data: [
                            [\''.$tantpercentexecucio.' %\',   '.$tantpercentexecucio.'], 
                            [\'\',       '.(100-$tantpercentexecucio).'],
                            {
                                name: \'\',
                                y: 0.2,
                                dataLabels: {
                                    enabled: false
                                },
                            }
                            
                        ]
                    }]
                });
				
				

		        Highcharts.theme = {
   					colors: [  "#79BD9A",
			                    "#CFF09E",
			                    "#0B486B",
			                    "#e1e1e1"]
   				}
				Highcharts.setOptions(Highcharts.theme);

				Highcharts.chart("chart-area-'.$value_pm.'", {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        margin: [0, 0, 0, 0]
                    },
                    title: {
                        text: \''.$contaprojecte.'\',
                        align: \'center\',
                        verticalAlign: \'middle\',
                        y: 50,
                    },
                    subtitle: {
                        text: \'PROJECTES SEGONS EL SEU ESTAT\',
                        y: 10,
                        style:{
                        	fontSize: "14px",
                        }
                    },
                    lang:{
						downloadPNG: "Descarregar imatge PNG"
                    },
                    tooltip: {
                        pointFormat: \'{series.name}: <b>{point.percentage:.1f}%</b>\'
                    },
                    legend: {
						enabled: true,
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: 0,
                                style: {
                                    fontWeight: \'normal\',
                                    color: \'balck\'
                                }
                            },
                            startAngle: -90,
                            endAngle: 90,
                            center: [\'50%\', \'75%\']
                        }
                    },
                    series: [{
                        type: \'pie\',
                        name: "PROJECTES SEGONS EL SEU ESTAT",
                        innerSize: \'50%\',
                        data: [';
                            	
                            	if ($espam==1 || count($vinculacionsget)>10){
			                		foreach ($paraulesestatsprojecte as $key => $value) {
			                			//if ($key != 3){ // no mostrem "Amb dificultats"

			                				$tantpercentestat=0;
				                			if (($contaprojecte)!=0)$tantpercentestat=(100/($contaprojecte)) * $estatsprojecte[$key];

			                			 	$Dades .='["'.$value." (".($estatsprojecte[$key]==""?"0":$estatsprojecte[$key]).")".'", '.number_format($tantpercentestat,2).' ],';
			                			//}
			                		}
			                	}else{
			                		if (!empty($estatsprojectepad[$value_pm])){
			                			foreach ($paraulesestatsprojecte as $key => $value) {
				                			//if ($key != 3){ // no mostrem "Amb dificultats"

				                				$tantpercentestat=0;
					                			if ($contaprojectepad[$value_pm])$tantpercentestat=(100/($contaprojectepad[$value_pm])) * $estatsprojectepad[$value_pm][$key];
					                			
				                			 	$Dades .='["'.$value." (".($estatsprojectepad[$value_pm][$key]==""?"0":$estatsprojectepad[$value_pm][$key]).")".'",  '.number_format($tantpercentestat,2).'],';
				                			//}
				                		}
			                		}
			                	}

                        $Dades .='    
                        ]
                    }]
                });

	
				Highcharts.chart("chart-area2-'.$value_pm.'", {
				    chart: {
				        type: \'bar\'
				    },
				    title: {
				        text: "GRAU D\'EXECUCIÓ PER LÍNIES ESTRATÈGIQUES DEL PDA"
				    },
				    xAxis: {
				        categories: [';

			        		if (!empty($arraynomseixos)){
	            				foreach ($arraynomseixos as $key => $value) {
	            					if ($espam==1 || count($vinculacionsget)>10){
		            					//if (!empty($tantsxcentseixos[$key])){
			                			 	$Dades .='"'.$value.'",';
			                			//}
			                		}else{
			                			//if (!empty($tantsxcentseixospad[$value_pm][$key])){
			                			 	$Dades .='"'.$value.'",';
			                			//}
			                		}
		                		}
	            			}

						$Dades .= '		
				        ]
				    },
				    yAxis: {
				    	min: 0,
				    	max: 100,
				    	reversed: false,
				    	title: {
				    		text: "%",
				    	}
				    },
				    legend: {
				    	enabled: false,
				        reversed: false
				    },
				    plotOptions: {
				        series: {
				            stacking: \'normal\'
				        }
				    },
				    series: [{
				        name: \'Executat\',
				        index:1,
				        data: [';
							$arraypendent=array();

							if ($espam==1 || count($vinculacionsget)>10){
		                		foreach ($arraynomseixos as $key => $value) {
		                			$nomeix = str_replace("EIX ", "", $arraynomseixos[$key]);
									$codieix = substr($nomeix , 0, strpos($nomeix , '.'));
		                			if (!empty($tantsxcentseixos[$key]) ){ // && in_array($key, $eixosamostrar)
			                			$tantpercentestat = 0;
		                				if ($tantsxcentseixos[$key][conta] > 0 )$tantpercentestat= $tantsxcentseixos[$key][tantpercent]/$tantsxcentseixos[$key][conta];
		                				$Dades .='{y:'.number_format($tantpercentestat,2).',color:"'.$colorseixos[$codieix-1].'"},';
			                			$arraypendent[] = 100 - $tantpercentestat;
			                		}else{
			                			$Dades .='{y:0,color:"'.$colorseixos[$codieix-1].'"},';
			                		}
		                		}
		                	}else{
		                		//if (!empty($tantsxcentseixospad[$value_pm])){
									foreach ($arraynomseixos as $key => $value) {
										$nomeix = str_replace("EIX ", "", $arraynomseixos[$key]);
										$codieix = substr($nomeix , 0, strpos($nomeix , '.'));
		                				$tantpercentestat = 0;
		                				if ($tantsxcentseixospad[$value_pm][$key][conta] > 0  ) $tantpercentestat= $tantsxcentseixospad[$value_pm][$key][tantpercent]/$tantsxcentseixospad[$value_pm][$key][conta]; // && in_array($key, $eixosamostrar)
		                				$Dades .='{y:'.number_format($tantpercentestat,2).',color:"'.$colorseixos[$codieix-1].'"},';
		                				$arraypendent[] = 100 - $tantpercentestat;
		                			}
		                		//}
		                	}

						$Dades .= '	
				        ]
				    }, {
				        name: \'Pendent\',
				        index:0,
				        color: "#fcfcfc",
				        data: [';
				        	foreach ($arraypendent as $key => $value) {
				        		$Dades .=''.number_format($value,2).',';
				        	}
				        $Dades .= '	
				        ]
				    }]
				});


		        Highcharts.chart("chart-area3-'.$value_pm.'", {
				    chart: {
				        type: "column"
				    },
				    title: {
				        text: "GRAU D\'EXECUCIÓ PER ÀMBITS DE RESULTATS DEL PDA"
				    },
				    subtitle: {
				       // text: ""
				    },
				    colors: ["#cd0d2d"],
				    xAxis: {
				        categories: [';

				    		if (!empty($arraynomslinies)){
	                			foreach ($arraynomslinies as $key => $value) {
		                			if ($espam==1 || count($vinculacionsget)>10){
		            					//if (!empty($tantsxcentslinies[$key])){
			                			 	$Dades .='"'.$value.'",';
			                			//}
			                		}else{
			                			//if (!empty($tantsxcentsliniespad[$value_pm][$key][conta])){
			                			 	$Dades .='"'.$value.'",';
			                			//}
			                		}
		                		}
	                		}

				    $Dades .= '


				        ],
				        crosshair: true
				    },
				    yAxis: {
				        min: 0,
				        max: 100,
				        title: {
				            text: "%"
				        }
				    },
				    legend: {
				    	enabled:false,
				    },
			     	tooltip: {
				        headerFormat: \'<span style="font-size:10px">{point.key}</span><table>\',
				        pointFormat: \'<tr><td style="color:{series.color};padding:0">{series.name}: </td>\' +
				            \'<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>\',
				        footerFormat: \'</table>\',
				        shared: false,
				        useHTML: true
				    },
				    plotOptions: {
				        column: {
				            pointPadding:-0.20,
				            borderWidth: 0
				        }
				    },
				    series: [{
				    	name: "Executat",
						data: [
				    ';

				    		if (!empty($arraynomslinies)){
	                			foreach ($arraynomslinies as $key => $value) {
									
	                				if ($espam==1 || count($vinculacionsget)>10 ){
	                					if (!empty($tantsxcentslinies[$key][conta]) ){ // && in_array($key, $liniesamostrar)
				                			$tantpercentestat = 0;
				                			if ($tantsxcentslinies[$key][conta]!=0) $tantpercentestat= $tantsxcentslinies[$key][tantpercent]/$tantsxcentslinies[$key][conta];
				                			$Dades .='{y:'.number_format($tantpercentestat,2).',color:"'.$colorseixos[substr($value-1, 0, 1)].'"},';
				                		}else{
				                			$Dades .='{y:0,color:"'.$colorseixos[substr($value-1, 0, 1)].'"},';
				                		}
				                	}else{
				                		if (!empty($tantsxcentsliniespad[$value_pm][$key][conta]) ){ // && in_array($key, $liniesamostrar)
				                			$tantpercentestat = 0;
				                			if ($tantsxcentsliniespad[$value_pm][$key][conta]!=0) $tantpercentestat= $tantsxcentsliniespad[$value_pm][$key][tantpercent]/$tantsxcentsliniespad[$value_pm][$key][conta];
				                			$Dades .='{y:'.number_format($tantpercentestat,2).',color:"'.$colorseixos[substr($value-1, 0, 1)].'"},';
					                		
					                	}else{
				                			$Dades .='{y:0,color:"'.$colorseixos[substr($value-1, 0, 1)].'"},';
				                		}
				                	}
				                			       		

		                		}
	                		}

				    $Dades .= ' ]
				    }]
				});
			    

				Highcharts.chart("chart-area4-'.$value_pm.'", {

					chart: {
				        plotBackgroundColor: null,
				        plotBorderWidth: null,
				        plotShadow: false,
				        type: \'pie\'
				    },
				    title: {
				        text: "PRESSUPOST GLOBAL AGREGAT"
				    },
				    tooltip: {
				        pointFormat: \'{series.name}: <b>{point.percentage:.1f}%</b><br> {point.y} €\'
				    },
				    plotOptions: {
				        pie: {
				            allowPointSelect: true,
				            cursor: \'pointer\',
				            dataLabels: {
				                enabled: true,
				                format: \'<b>{point.name}</b>: {point.percentage:.1f} %\',
				                style: {
				                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || \'black\'
				                }
				            }
				        }
				    },
				    series: [{
				        name: \'Pressupost\',
				        colorByPoint: true,
				        data: [
				        ';
				            foreach ($array_cap as $key_c => $value_c) {
				            	$Dades .= '{ name: \'CAP '.$key_c.'\', y: '.$value_c.' },';
				            }
				            
				        $Dades .= '
				        ]
				    }]

				});

				Highcharts.chart("chart-area5-'.$value_pm.'", {
				    chart: {
				        type: \'column\'
				    },
				    title: {
				        text: \'PRESSUPOST GLOBAL PER ANUALITATS\'
				    },
				    xAxis: {
				        categories: [
				            \'2015\',
				            \'2016\',
				            \'2017\',
				            \'2018\',
				            \'2019\',
				        ],
				        crosshair: true
				    },
				    yAxis: {
				        min: 0,
				        title: {
				            text: \'Import €\'
				        }
				    },
				    tooltip: {
				        headerFormat: \'<span style="font-size:10px">{point.key}</span><table>\',
				        pointFormat: \'<tr><td style="color:{series.color};padding:0">{series.name}: </td>\' +
				            \'<td style="padding:0"><b>{point.y:.1f} €</b></td></tr>\',
				        footerFormat: \'</table>\',
				        shared: true,
				        useHTML: true
				    },
				    plotOptions: {
				        column: {
				            pointPadding: 0.2,
				            borderWidth: 0
				        }
				    },
				    series: [';

				    	foreach ($array_cap_anys as $key_c=> $value_c) {
				    		$Dades .= ' { name: \'CAP '.$key_c.'\',  data: [';
				    			foreach ($value_c as $key_a => $value_a) {
				    				$Dades .= $value_a.",";
				    			}
				    		$Dades .= ']}, ';
				    	}

				    $Dades .= '
					]
				});


	            
	        });
	        </script>
			
		';

		
		
		/*
			
	            <div class="col-md-6">
	                <div class="card">
	                    <div class="header">
	                        NASDAQ: AAPL
	                        <p class="category">Line Chart with Points</p>
	                    </div>
	                    <div class="content">
	                        <div id="chartStock" class="ct-chart "><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="260px" class="ct-chart-line" style="width: 100%; height: 260px;"><g class="ct-grids"><line x1="50" x2="50" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="95.88888888888889" x2="95.88888888888889" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="141.77777777777777" x2="141.77777777777777" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="187.66666666666666" x2="187.66666666666666" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="233.55555555555554" x2="233.55555555555554" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="279.44444444444446" x2="279.44444444444446" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="325.3333333333333" x2="325.3333333333333" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="371.2222222222222" x2="371.2222222222222" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line x1="417.1111111111111" x2="417.1111111111111" y1="15" y2="225" class="ct-grid ct-horizontal"></line><line y1="225" y2="225" x1="50" x2="463" class="ct-grid ct-vertical"></line><line y1="183" y2="183" x1="50" x2="463" class="ct-grid ct-vertical"></line><line y1="141" y2="141" x1="50" x2="463" class="ct-grid ct-vertical"></line><line y1="99" y2="99" x1="50" x2="463" class="ct-grid ct-vertical"></line><line y1="57" y2="57" x1="50" x2="463" class="ct-grid ct-vertical"></line><line y1="15" y2="15" x1="50" x2="463" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M50,187.704L95.889,166.368L141.778,153.97L187.667,137.758L233.556,120.487L279.444,90.214L325.333,120.487L371.222,87.038L417.111,52.8L463,44.854" class="ct-line ct-green"></path><line x1="50" y1="187.704" x2="50.01" y2="187.704" class="ct-point ct-green" value="22.2"></line><line x1="95.88888888888889" y1="166.368" x2="95.89888888888889" y2="166.368" class="ct-point ct-green" value="34.9"></line><line x1="141.77777777777777" y1="153.96959999999999" x2="141.78777777777776" y2="153.96959999999999" class="ct-point ct-green" value="42.28"></line><line x1="187.66666666666666" y1="137.75760000000002" x2="187.67666666666665" y2="137.75760000000002" class="ct-point ct-green" value="51.93"></line><line x1="233.55555555555554" y1="120.4872" x2="233.56555555555553" y2="120.4872" class="ct-point ct-green" value="62.21"></line><line x1="279.44444444444446" y1="90.21360000000001" x2="279.45444444444445" y2="90.21360000000001" class="ct-point ct-green" value="80.23"></line><line x1="325.3333333333333" y1="120.4872" x2="325.3433333333333" y2="120.4872" class="ct-point ct-green" value="62.21"></line><line x1="371.2222222222222" y1="87.0384" x2="371.23222222222216" y2="87.0384" class="ct-point ct-green" value="82.12"></line><line x1="417.1111111111111" y1="52.80000000000001" x2="417.1211111111111" y2="52.80000000000001" class="ct-point ct-green" value="102.5"></line><line x1="463" y1="44.8536" x2="463.01" y2="44.8536" class="ct-point ct-green" value="107.23"></line></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="50" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'07</span></foreignObject><foreignObject style="overflow: visible;" x="95.88888888888889" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'08</span></foreignObject><foreignObject style="overflow: visible;" x="141.77777777777777" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'09</span></foreignObject><foreignObject style="overflow: visible;" x="187.66666666666666" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'10</span></foreignObject><foreignObject style="overflow: visible;" x="233.55555555555554" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'11</span></foreignObject><foreignObject style="overflow: visible;" x="279.44444444444446" y="230" width="45.888888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'12</span></foreignObject><foreignObject style="overflow: visible;" x="325.3333333333333" y="230" width="45.88888888888886" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'13</span></foreignObject><foreignObject style="overflow: visible;" x="371.2222222222222" y="230" width="45.888888888888914" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'14</span></foreignObject><foreignObject style="overflow: visible;" x="417.1111111111111" y="230" width="45.888888888888914" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 46px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">\'15</span></foreignObject><foreignObject style="overflow: visible;" y="183" x="10" height="42" width="30"><span class="ct-label ct-vertical ct-start" style="height: 42px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$0</span></foreignObject><foreignObject style="overflow: visible;" y="141" x="10" height="42" width="30"><span class="ct-label ct-vertical ct-start" style="height: 42px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$25</span></foreignObject><foreignObject style="overflow: visible;" y="99" x="10" height="42" width="30"><span class="ct-label ct-vertical ct-start" style="height: 42px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$50</span></foreignObject><foreignObject style="overflow: visible;" y="57" x="10" height="42" width="30"><span class="ct-label ct-vertical ct-start" style="height: 42px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$75</span></foreignObject><foreignObject style="overflow: visible;" y="15" x="10" height="42" width="30"><span class="ct-label ct-vertical ct-start" style="height: 42px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$100</span></foreignObject><foreignObject style="overflow: visible;" y="-15" x="10" height="30" width="30"><span class="ct-label ct-vertical ct-start" style="height: 30px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">$125</span></foreignObject></g></svg></div>
	                    </div>
	                </div>
	            </div>
		*/

	    if (count($vinculacionsget)>10) break;

	    }
	}

	// 4. Llistat de projectes.

	if ($blocsactius[3] == 1)
	{

		/* TODO per evitar carregar la pàgina a l'entrada.
		$Dades .= '<div id="llistatprojectes"></div>


			<script type="text/javascript">

        		$(document).ready(function($) {

					
					$("#llistatprojectes").html("<img src=\'../images/loading.gif\'/>");
					$.ajax({
		                    type: "POST",
		                    url: "../load",
		                    data: "o=37&id=1",
		                    beforeSend:function(){
		                        
		                    },
		                    error: function(jqXHR, textStatus, errorThrown){
		                        // Error.
		                    },
		                    success: function(data){
		                        //$(".llistatprojectes").append(data);
		                    }
		                });
		          

				});

				

    		</script>

	
		';
		*/

		// Generar Export.
		if (!empty($exportget)) include("consulta_export.php");

		// Export individual
		if (!empty($exportget_ind)) include("consulta_export_ind.php");

		include ("consultaprojectes_llista.php");

	}

	// 5. Gantt.
	if ($blocsactius[4] == 1)
	{	
		/*	 // Filtre per poder triar les actuacions. (Es mou al filtre superior.)
				<div class="col-lg-6">
										<div class="form-group">

		                                	<label><h5><b>Actuacions</b></h5></label><br>
		                                  
		                                	<select name="vinculacions[]" id="selectprojectes" class="selectpicker" multiple data-actions-box="true" data-live-search="true" data-width="100%" data-max-options="10">
	        ';
			                                    if (!empty($Vinculacio4))
												{
													foreach ($Vinculacio4 as $key_v => $value_v) 
													{ 
			                                        	if (!empty($value_v[idsproj])){
			                                        		$Dades .= '<option value="'.$value_v[id].'" selected>'.$value_v[titol].'</option>';
			                                        	}
			                                        }
			                                    }
			$Dades .= '                    	                    
		                                	</select>
		                                </div>
	                                 
	                        		</div>

		                            
			*/

		$Dades .= '
			
			<a name="gantt"></a>
	        <div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                Planificació i fites
		            </h3>
		            <hr style="border-top: 1px solid #cc032f;">
		        </div>
	        </div>

	        <div class="row">

                <div class="col-md-12">

                   

                        <div class="content">

                        	<form role="form" action="#" method="get" name="frmgant" id="frmgant" style="display:none;">

				            <input type="hidden" name="o" value="33">
				            <input type="hidden" name="id" value="1">
				            <input type="hidden" name="pag" value="1" id="paginagant">
				            ';

				            if (!empty($Vinculacio4))
							{
								foreach ($Vinculacio4 as $key_v => $value_v) 
								{ 
                                	if (!empty($value_v[idsproj])){
                                		$Dades .= '<input type="hidden" class="vinculacionsgantt" name="vinculacions[]" value="'.$value_v[id].'" >';
                                	}
                                }
                            }

				            $Dades .= '


							<div class="row" style="display:none;">

								<div class="col-lg-1">
	                                <div class="form-group">
	                                    <label><h5 style="color:#fff;"><b>.</b></h5></label><br>
	                                    <button type="submit" class="btn btn-default"><i class="fa fa-bar-chart"></i> Mostrar</button>
	                                </div>

	                            </div>
								

                            </div>

                            </form>

                            <div class="row" style="height:800px;">
                            	<div class="loadgantt text-center"></div>
								<div class="col-lg-12 divgants text-center">
			                               
								</div>
							</div>

                       	</div>

                   

                </div>
            </div>
			
			<script type="text/javascript">

        		$(document).ready(function($) {


		            $( "#frmgant" ).submit(function( event ) {
		                event.preventDefault();

		                $(".divgants").html("");
		                $(".loadgantt").html("<img src=\'../images/loadingg.gif\'/>");

		                var pag = $("#paginagant").val();

		                var contagantts = 0;
		                var stringvinculacions =  "";
						/*
						$("#selectprojectes > option").each(function() {

							if($(this).is(\':selected\')){

								contagantts = contagantts + 1;
								if (contagantts <= 10){
									stringvinculacions = stringvinculacions + "&vinculacions[]="+this.value;
							    }
							}
						    
						});
						*/

						$(".vinculacionsgantt").each(function() {

							stringvinculacions = stringvinculacions + "&vinculacions[]="+this.value;
						   						    
						});
						
						//if (contagantts > 10){
							//alert("Es mostraran un máxim de 10 projectes.");
						//}


						$.ajax({
		                    type: "POST",
		                    url: "../load",
		                    data: "o=33&id=1&pag="+pag+"&"+stringvinculacions+"&f='.$_GET['f'].'",
		                    beforeSend:function(){
		                        //
		                    },
		                    error: function(jqXHR, textStatus, errorThrown){
		                        // Error.
		                    },
		                    success: function(data){
		                        $(".divgants").append(data);
		                    }
		                });
						
		                
		            });   


					$( "#frmgant" ).submit();
					
					
				});

				

    		</script>
	    ';

	}


    // 2. Calendari.
    /*
	if ($blocsactius[1] == 1)
	{
        $arrayCalendari = array();
	
	   	$Dates = $dbb->FreeSql("SELECT *
							    FROM (
							    		SELECT  observacionsfita as titol, t.datai, t.datai as datafi, '6/projectes.html?id=' as pagina, t.id, p.titol_ca as nomprojecte,
							    		'' as color
							    	    FROM pfx_projectes_fites t
							    	    INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
							    	    WHERE 1 = 1
								    
								    			
								) a
								ORDER BY datai",
							  array());	

	   	if (!empty($Dates)){

	   		foreach ($Dates as $key_d => $value_d) 
			{
				array_push($arrayCalendari, array("id"=> "Cp_".$value_d[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[titol])))), 
												  "start" =>date("Y-m-d",strtotime($value_d[datai])), 
												  "end" =>date("Y-m-d",strtotime($value_d[datafi])), 
												  "pagina"=> $value_d[pagina].$idproj, 
												  "inici" =>date("d/m/Y",strtotime($value_d[datai])), 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[nomprojecte])))), 
												  "color" => $value_d[color],
												  ));
			}

	   	}

        $Dades .= '

	        <div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                 2. Calendari de fites
		                 <hr style="border-top: 1px solid #cc032f;">
		            </h3>
		        </div>
	        </div>

            
            <div class="row" id="pdb_calendari">
                                                
                <div class="col-lg-12">
                	<div class="card">
                		<br>
                    	<div id="fullcal" style="max-width: 1200px;margin: 0 auto;"></div>
                    	<br>
                    </div>
                </div>

            </div>

            <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet" />
            <link href="../js/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print" />
            <script src="../js/fullcalendar/lib/moment.min.js"></script>
            <script src="../js/fullcalendar/fullcalendar.min.js"></script>
            <script src="../js/fullcalendar/locale-all.js"></script>
            <script src="../js/fullcalendar/qtip/jquery.qtip.js"></script>
            <link href="../js/fullcalendar/qtip/jquery.qtip.min.css" rel="stylesheet" />

            <script type="text/javascript">

               $(document).ready(function($) {

                var tooltip = $(\'<div/>\').qtip({
                    id: \'fullcal\',
                    prerender: true,
                    content: {
                        text: \' \',
                        title: {
                            button: true
                        }
                    },
                    position: {
                        my: \'bottom center\',
                        at: \'top center\',
                        target: \'event\',
                        viewport: $(\'#fullcal\'),
                        adjust: {
                            mouse: false,
                            scroll: false
                        }
                    },
                    show: false,
                    hide: false,
                    style: \'qtip-light\'
                }).qtip(\'api\');

                var events2 = '.json_encode($arrayCalendari).'

                
                var initialLocaleCode = \'ca\';
                $(\'#fullcal\').fullCalendar({
                    header: {
                        left: \'prev,next today\',
                        center: \'title\',
                        right: \'month,basicWeek,listYear \'//month,agendaWeek,agendaDay,listMonth\'
                    },
                    //defaultDate: \'2016-03-03\',
                    locale: initialLocaleCode,
                    buttonIcons: false, // show the prev/next text
                    weekNumbers: true,
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    eventLimitText: "Veure tots",
                    events: events2,
                    eventClick: function(data, event, view) {
                        var content = \'<h5>\'+data.nomprojecte+\'</h5>\' + 
                            \'<p><b>Fita:</b> \'+data.title+\'</p>\'; // <br /><a href="../\'+data.pagina+\'">Més informació</a></p> + 
                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');

                        tooltip.set({
                            \'content.text\': content
                        })
                        .reposition(event).show(event);
                    },
                    dayClick: function() { tooltip.hide() },
                    eventResizeStart: function() { tooltip.hide() },
                    eventDragStart: function() { tooltip.hide() },
                    viewDisplay: function() { tooltip.hide() },

                    
                });

                $(document).off("click",".exporcal").on("click",".exporcal",function(e){
                  e.preventDefault();
                  $("#frmcal").submit();
                });

                jQuery.validator.messages.required = "<br>'.$app['translator']->trans('base.alerts.introvalor').'<br>";
                $("#frmcal").validate({
                    errorElement: "em",
                    errorPlacement: function(error, element) {
                        error.appendTo( element.parent("div") );
                    },
                    success: function(label) {
                        //label.text("ok!").addClass("success");
                    },
                    //errorLabelContainer: $("div.error"),
                    submitHandler: function(form) {
                        document.frmcal.verif.value = "ok";
                        var table = $(".dataTables").DataTable();
                        var dadestaules = table.$("input, select").serialize();
                        var stringdades = $("#frmcal").serialize();
                        stringdades = stringdades + "&" +dadestaules;
                            $.ajax({
                                type: "POST",
                                url: "../load",
                                data: stringdades,
                                beforeSend:function(){
                                    $("#resultinformescal").html("<img src=\'../images/loading.gif\'/>");
                                },
                                error: function(jqXHR, textStatus, errorThrown){
                                    // Error.
                                },
                                success: function(data){
                                  $("div.errorinformescal").hide();
                                  $("#resultinformescal").html(data);
                                    
                                }
                            });
                        return false;
                    },
                    invalidHandler: function(e, validator) {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            var message = errors == 1
                                ? "Has oblidat 1 camp."
                                : "Has oblidat " + errors + " camps. ";

                            var cuadre = "<div class=\"alert alert-dismissable alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" + message + "</div>";

                            $("div.errorinformescal span").html(cuadre);
                            $("div.errorinformescal").show();
                        } else {
                            $("div.errorinformescal").hide();
                        }
                    },
                    onkeyup: false,
                    debug:true
                });
              
              });
            </script>

            <style type="text/css">
                .fc-event-time, .fc-event-title {
                    padding: 0 1px;
                    white-space: nowrap;
                }
                .fc-day-grid-event > .fc-content {
                    white-space: normal;
                }
            </style>




		';

	}
	*/

	// 3. Mapes.
	if ($blocsactius[2] == 1)
	{
		
		
	}


	// 4. Calendari.
	if ($blocsactius[2] == 1)
	{

		$Dates = $dbb->FreeSql("SELECT *
						    FROM (
						    		SELECT  observacionsfita as titol, t.datai, t.datai as datafi, '6/projectes.html?id=' as pagina, t.id, p.titol_ca as nomprojecte, ti.titol_ca as tipologiafita,
						    		'' as color, t.clau_projecte
						    	    FROM pfx_projectes_fites t
						    	    INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
						    	    INNER JOIN pfx_projectes_relacionsvinc rp ON rp.id_proj = p.id
						    	    LEFT JOIN pfx_projectes_fites_tipologies ti ON ti.id = t.tipologia
						    	    WHERE 1 = 1 $condicio_permis_uvinc
							    
							    			
							) a
							ORDER BY datai",
						  array());	

	   	if (!empty($Dates)){

	   		foreach ($Dates as $key_d => $value_d) 
			{
				array_push($arrayCalendari, array("id"=> "Cp_".$value_d[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[titol])))), 
												  "start" =>date("Y-m-d",strtotime($value_d[datai])), 
												  "end" =>date("Y-m-d",strtotime($value_d[datafi])), 
												  "pagina"=> $value_d[pagina].$value_d[clau_projecte], 
												  "inici" =>date("d/m/Y",strtotime($value_d[datai])), 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[nomprojecte])))), 
												  "color" => $value_d[color],
												  'tipus' => "fita",
												  'tipologia' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[tipologiafita])))), 
												  'className' => "calfites",
												  'color' => "#5681de",
												  'icon' => "calendar",
												  ));

				if (in_array($value_d["clau_projecte"], $arrayprojectesmeus )){

					array_push($arrayCalendari, array("id"=> "Cp_".$value_d[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[titol])))), 
												  "start" =>date("Y-m-d",strtotime($value_d[datai])), 
												  "end" =>date("Y-m-d",strtotime($value_d[datafi])), 
												  "pagina"=> $value_d[pagina].$value_d[clau_projecte], 
												  "inici" =>date("d/m/Y",strtotime($value_d[datai])), 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[nomprojecte])))), 
												  "color" => $value_d[color],
												  'tipus' => "fita",
												  'tipologia' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[tipologiafita])))), 
												  'className' => "calmeus",
												  'color' => "#5681de",
												  'icon' => "calendar",
												  ));

				}
			}

	   	}


		$Dades .= '
			
			<a name="mapes"></a>


	        <div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                Calendari
		            </h3>
		            <hr style="border-top: 1px solid #cc032f;">
		        </div>
	        </div>

	        <div class="row">
                <div class="col-lg-3"><input type="checkbox" class="filtres filtre1" name="filtre1"  data-size="mini" value="calfites" checked> <b>Fites</b></div>
            
                <div class="col-lg-3"><input type="checkbox" class="filtres filtre2" name="filtre2"  data-size="mini" value="calprojectes" checked> <b>Inici/Final Projectes</b></div>
            
                <div class="col-lg-3"><input type="checkbox" class="filtres filtre3" name="filtre3"  data-size="mini" value="calactuacions" >  <b>Inici/Final Fases</b></div>
            
                <div class="col-lg-3"><input type="checkbox" class="filtres filtre4" name="filtre4"  data-size="mini" value="caltasques" > <b>Inici/Final Accions</b></div>
            </div>

            <div class="row">
	        	<div class="col-lg-12 text-center">
	        		<div id="fullcalload"></div>
	        	</div>
	        </div>


           <hr>

			<div class="row">
	        	<div class="col-lg-12">
	        		<div class="card">
                		<br>
                    	<div id="fullcal" style="max-width: 1200px;margin: 0 auto;"></div>
                    	<div id="fullcaldades" style="display:none;"></div>
                    	<br>
                    </div>
                </div>
            </div>

            <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet" />
            <link href="../js/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print" />
            <script src="../js/fullcalendar/lib/moment.min.js"></script>
            <script src="../js/fullcalendar/fullcalendar.min.js"></script>
            <script src="../js/fullcalendar/lang-all.js"></script>
            <script src="../js/fullcalendar/qtip/jquery.qtip.js"></script>
            <link href="../js/fullcalendar/qtip/jquery.qtip.min.css" rel="stylesheet" />

            <link href="../js/consultes/switch/bootstrap-switch.min.css" rel="stylesheet">
             <script src="../js/consultes/switch/bootstrap-switch.min.js"></script>


            <script type="text/javascript">

                $(document).ready(function($) {

                	var events2 = '.json_encode($arrayCalendari).'

                	var eventsenviar = JSON.stringify('.json_encode($arrayCalendari).');



                	$(".filtres").bootstrapSwitch();

                        
                	$(".filtres").on("switchChange.bootstrapSwitch", function(event, state) {


	                    var nomclass = $(this).val();

	                    var filtre1 = $(".filtre1").val();
		              	var filtre2 = $(".filtre2").val();
		              	var filtre3 = $(".filtre3").val();
		              	var filtre4 = $(".filtre4").val();

		              	var calfites = 0;
		              	var calprojectes = 0;
		              	var calactuacions = 0;
		              	var caltasques = 0;

	                    if ($(".filtre1").is(":checked")) { calfites = 1; }else{ calfites = 0; }
		              	if ($(".filtre2").is(":checked")) { calprojectes = 1; }else{ calprojectes = 0; }
		              	if ($(".filtre3").is(":checked")) { calactuacions = 1; }else{ calactuacions = 0; }
		              	if ($(".filtre4").is(":checked")) { caltasques = 1; }else{ caltasques = 0; }


	                    $("#fullcalload").html("<img src=\'../images/loadingg.gif\'/>");
	                    $("#fullcal").fullCalendar( "destroy" );

	                    $("#fullcaldades").load("../load",{o:48, id:1, t: "", c:{events:eventsenviar, calfites: calfites, calprojectes: calprojectes, calactuacions: calactuacions, caltasques: caltasques  } },function(data){
	                    	
	                    	
                			var obj = JSON.parse(data);
			                crearcalendari(obj);
			                $("#fullcalload").html("");

				               
			            }); 

			            $("#fullcaldades").html("");
			            
	                
	                });

		            $(document).off("click",".fc-prev-button").on("click",".fc-prev-button",function(e){
		               //repassarfiltres();
		            });

		            $(document).off("click",".fc-next-button").on("click",".fc-next-button",function(e){
		              // repassarfiltres();
		            });

		            /*
		            function repassarfiltres(){
		              var filtre1 = $(".filtre1").val();
		              var filtre2 = $(".filtre2").val();
		              var filtre3 = $(".filtre3").val();
		              var filtre4 = $(".filtre4").val();

		              if ($(".filtre1").is(":checked")) { $("."+filtre1).show(); }else{ $("."+filtre1).hide(); }
		              if ($(".filtre2").is(":checked")) { $("."+filtre2).show(); }else{ $("."+filtre2).hide(); }
		              if ($(".filtre3").is(":checked")) { $("."+filtre3).show(); }else{ $("."+filtre3).hide(); }
		              if ($(".filtre4").is(":checked")) { $("."+filtre4).show(); }else{ $("."+filtre4).hide(); }
		            }
		            */

		            function crearcalendari(events2){

		            	var tooltip = $(\'<div/>\').qtip({
		                    id: \'fullcal\',
		                    prerender: true,
		                    content: {
		                        text: \' \',
		                        title: {
		                            button: true
		                        }
		                    },
		                    position: {
		                        my: \'bottom center\',
		                        at: \'top center\',
		                        target: \'event\',
		                        viewport: $(\'#fullcal\'),
		                        adjust: {
		                            mouse: false,
		                            scroll: false
		                        }
		                    },
		                    show: false,
		                    hide: false,
		                    style: \'qtip-light\'
		                }).qtip(\'api\');

	                

	                
		                var initialLocaleCode = \'ca\';
		                $(\'#fullcal\').fullCalendar({
		                    header: {
		                        left: \'prev,next today\',
		                        center: \'title\',
		                        right: \'year,month,basicWeek,listYear \'//month,agendaWeek,agendaDay,listMonth\'
		                    },
		                    //defaultDate: \'2016-03-03\',
		                    lang: initialLocaleCode,
		                    buttonIcons: false, // show the prev/next text
		                    weekNumbers: true,
		                    editable: false,
		                    eventLimit: true, // allow "more" link when too many events
		                    eventLimitText: "Veure tots",
		                    events: events2,
		                    eventClick: function(data, event, view) {
		                      
		                         if (data.tipus == \'projecte\'){
			                        var content = \'<h5>\'+data.title+\'</h5>\' + 
			                            \'<p><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }
			                      if (data.tipus == \'projecteinici\'){
			                        var content = \'<h5>\'+data.title+\'</h5>\' + 
			                            \'<p><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }
			                      if (data.tipus == \'fita\'){
			                         var content = \'<h5>\'+data.nomprojecte+\'</h5>\' + 
			                            \'<p><br />\'+data.tipologia+\'<br /> \'+data.title+\'<br /><b>Data:</b> \'+data.inici+\' </p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }

			                      if (data.tipus == \'actuacio\'){
			                        var content = \'<h5>Fase: \'+data.title+\'</h5>\' + 
			                            \'<p>\'+data.nomprojecte+\' <br><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }
			                      if (data.tipus == \'actuacioinici\'){
			                        var content = \'<h5>Fase: \'+data.title+\'</h5>\' + 
			                            \'<p>\'+data.nomprojecte+\' <br><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }

			                      if (data.tipus == \'tasca\'){
			                        var content = \'<h5>Acció: \'+data.title+\'</h5>\' + 
			                            \'<p>\'+data.nomprojecte+\' <br><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }
			                      if (data.tipus == \'tascainici\'){
			                        var content = \'<h5>Acció: \'+data.title+\'</h5>\' + 
			                            \'<p>\'+data.nomprojecte+\' <br><b>Inici:</b> \'+data.inici+\'  <b>Final:</b> \'+data.final+\'</p><br /><a href="{{ app.request.basepath }}/\'+data.pagina+\'"></a></p>\';
			                            //(data.end && \'<p><b>End:</b> \'+data.end+\'</p>\' || \'\');
			                      }
			                      

		                        tooltip.set({
		                            \'content.text\': content
		                        })
		                        .reposition(event).show(event);
		                    },
		                    dayClick: function() { tooltip.hide() },
		                    eventResizeStart: function() { tooltip.hide() },
		                    eventDragStart: function() { tooltip.hide() },
		                    viewDisplay: function() { tooltip.hide() },
		                    eventRender: function(event, element) {
		                     if(event.icon){          
		                        element.find(".fc-title").prepend("<i class=\'fa fa-"+event.icon+"\' style=\'color:#fff;\'></i> ");
		                     }
		                  }  
		                    
		                });

		            }

		            // Càrrega inicil.
	            	$("#fullcalload").html("<img src=\'../images/loadingg.gif\'/>");
                   
                    $("#fullcaldades").load("../load",{o:48, id:1, t: "", c:{events:eventsenviar, calfites: "1", calprojectes: "1", calactuacions: "0", caltasques: "0"  } },function(data){
                    	
            			var obj = JSON.parse(data);
		                crearcalendari(obj);
		                $("#fullcalload").html("");
			               
	           		 }); 

	                //crearcalendari(events2);
		            //repassarfiltres();

	            });
            </script>
         

	    ';
	}
	

	/* Versió taula simple.
	if ($blocsactius[3] == 1)
	{

		$Dades .= '
			
	        <div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                4. LLISTA DE PROJECTES
		            </h3>
		            <hr style="border-top: 1px solid #cc032f;">
		        </div>
	        </div>

			<div class="row">
				<div class="col-md-12">

					<div class="card">
						<div class="panel-body">
		        		 	<table class="table table-striped table-hover dataTables2">
					            <thead>
					              <tr>
					                <th width="30%">Projectes</th>
					                <th>Actuacions</th>
					              </tr>
					            </thead>
					            <tbody>
		';
									if (!empty($DadesProjectes))
									{
										foreach ($DadesProjectes as $key_p => $value_p) 
										{
											$actuacionsvinc = "";
											if (!empty($value_p[vinculacio])){
												foreach (json_decode($value_p[vinculacio]) as $key_v => $value_v) {
													$idacts = explode("_", $value_v);
													if (!empty($arrayVinculacions[$idacts[1]][titol])){
														$actuacionsvinc .= $arrayVinculacions[$idacts[1]][titol]."<br>";
													}
												}
											}
											$Dades .= '
												
												<tr>
								                    <td>'.$value_p[nom].'</td>
								                    <td>'.$actuacionsvinc.'</td>
							                  	</tr>

											';
										}
									}else{
										$Dades .= '
												
											<tr>
							                    <td>Sense projectes</td>
							                    <td></td>
						                  	</tr>

										';
									}
									

		$Dades .= '       
					            </tbody>
				          	</table> 
				        </div>
		            </div>
		        </div>
	        </div>

          	<script type="text/javascript">
	          $(document).ready(function($) {

	                  

                  var table = $(\'.dataTables2\').DataTable({
                    "language": {
                        "url": "../js/plugins/dataTables/dataTables.catala.lang"
                    }
                  });

	                 


	          });
	        </script>

	     ';

	}
	*/

	// D'entrada el filtre te de 0 a 100 % però no ho mostrem.
	if ($percent1get == 0 && $getenc['percent1'] == "") $percent1get = "";
	if ($percent2get == 100 && $getenc['percent2'] == "") $percent2get = "";
	if ($percent3get == 0 && $getenc['percent3'] == "") $percent3get = "";
	if ($percent4get == 100 && $getenc['percent4'] == "") $percent4get = "";



	// Nus d'ariadna movil.
	$filtredef = 0;

	if (!empty($nusfiltrepam)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")." <div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'>".$nusfiltrepam."</div>";
		$filtredef=1;
	}

	if (!empty($nusfiltreeix)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")." <div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Línies</b>: ".$nusfiltreeix."</div>";
		$filtredef=1;
	}

	if (!empty($nusfiltrelinies)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Àmbits</b>: ".$nusfiltrelinies."</div>";
		$filtredef=1;
	}

	if (!empty($nusfiltreactuacions)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Objectius</b>: ".$nusfiltreactuacions."</div>";
		$filtredef=1;
	}

    if (!empty($iniciget)||!empty($finalget)){
    	$nusfiltredates = (!empty($iniciget)?date("d/m/Y",strtotime($iniciget)):"----")." i ".(!empty($finalget)?date("d/m/Y",strtotime($finalget)):"----");
    }

	if (!empty($nusfiltredates)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Entre</b> ".$nusfiltredates."</div>";
		$filtredef=1;
	}

	if (!empty($estatsfiltre)) {
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Amb estats</b>: ".$estatsfiltre."</div>";
		$filtredef=1;
	}

	if (!empty($nusfiltretantpercent)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Amb % d'execució de projectes >= </b> ".$nusfiltretantpercent."</div>";
		$filtredef=1;
	}

	if (!empty($nusfiltretantpercent2)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Amb % d'execució d'objectius >= </b> ".$nusfiltretantpercent2."</div>";
		$filtredef=1;
	}

	
	if (!empty($ambitsget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Centre associat</b>: ";
		foreach ($Centres as $key_di => $value_di) {
			
			if(in_array($value_di[id], $ambitsget)){
				$nusfiltre .= $value_di[titol].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}
	if (!empty($ambits2get)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Àmbit</b>: ";
		foreach ($Centres as $key_di => $value_di) {
			
			if(in_array($value_di[id], $ambits2get)){
				$nusfiltre .= $value_di[titol].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}
	if (!empty($serveisget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Servei</b>: ";
		foreach ($Centres as $key_di => $value_di) {
			
			if(in_array($value_di[id], $serveisget)){
				$nusfiltre .= $value_di[titol].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($districtesget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Territori</b>: ";
		foreach ($Districtes as $key_di => $value_di) {
			
			if(in_array($value_di[id], $districtesget)){
				$nusfiltre .= $value_di[titol].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($responsablesget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Responsables</b>: ";
		foreach ($UsuarisFiltre as $key_di => $value_di) {
			
			if(in_array($value_di[id], $responsablesget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($membresget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Membres</b>: ";
		foreach ($UsuarisFiltre as $key_di => $value_di) {
			
			if(in_array($value_di[id], $membresget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($paraulesclau)) {

		$nusfiltre .= (!empty($nusfiltre)?" ":""). $paraulesclau."</div>";
	}

	if (!empty($teprojectesget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Te projectes que el desenvolupen</b>: ";
		foreach ($teprojectesget as $key_di => $value_di) {
			
			if ($value_di == 1) $nusfiltre .= "Amb projectes,";
			if ($value_di == 0) $nusfiltre .= " Sense projectes";
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}
	

	if ($filtredef == 0) $nusfiltre .= $nusprojecte;
	
	
	$Dades .= '
	
		<script type="text/javascript">
          $(document).ready(function($) {

                 

              	var $scrollingDiv = $(".nussup");
		        var $scrollingDivtext = $(".linus");

		        $scrollingDivtext.html("'.str_replace('"', '\"', (!empty($nusfiltre)?$nusfiltre:$nusprojecte)).'");
				
				/* Mostrar / Amagar amb scroll.
		        $(window).scroll(function(){     
		            if ($(window).scrollTop()>50)       {

		                $scrollingDiv
		                    .fadeIn()
		                    .css("position","fixed" )  
		                    .css("top","65px" )           
		            } else {
		                $scrollingDiv
		                    .css("position","")    
		                    .css("top","" )   
		                    .hide()  //.fadeOut()          
		            }
		        });   
				*/

				$scrollingDiv
                    .fadeIn()
                    .css("position","fixed" )  
                    .css("top","65px" )   

				$(document).off("click",".linus").on("click",".linus",function(event){
					$(".buscador").show();
					var target = $(".mostrabuscador").html();
				  	if (target.length) {
					    $("html,body").animate({
					      scrollTop: $(target).offset().top
					    }, "slow");
						
						$scrollingDiv
		                    .css("position","")    
		                    .css("top","" )   
		                    .hide()  //.fadeOut()    
				  	}

				  	
				});
		        


          });
        </script>
		
	';
	
	
	if (!empty($nusfiltre)) $text2.= "<br><li>".$nusfiltre."</li>";
	