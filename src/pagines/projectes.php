<?php 

	$idpagina = 6;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	// Usuaris
	$Usuaris =  $dbb->FreeSql("SELECT u.*
							    FROM pfx_usuaris u
							    WHERE clau_permisos <> 4 and clau_permisos <> 5
							    ORDER BY nom ",
							  array());



	$db = new Db();

   
	// Filtres
	require_once("consultesfiltres_inc.php");

	require_once("inici_inc.php");

	// Resum projectes.
	$arrayresum = array("1"=>array(),"2"=>array(),"3"=>array(),"4"=>array());

	require_once("consultesprojectes_inc.php");

	// càlculs on no afecta el filtre per mostrar dades a les actuacions.
	include("consultaprojectes_inccalcact.php");




	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") != 1 )
	{
		
		$condicio_permis2 = $condicio_permis_uvinc;

		$condicio_permis2 = str_replace("rp.id_act", "p.id", $condicio_permis2);

		if (empty($condicio_permis2)){
			$condicio_permis2 .= " AND 1 = 2";
		}else{
			$condicio_permis2 = "AND ( (nivell = 4 $condicio_permis2) OR (nivell = 1 OR nivell = 2 OR nivell = 3)) ";
		}
		
	}
   	$Vinculacio =  $dbb->FreeSql("SELECT p.*
									    FROM pfx_projectes_vinculacio p
									    WHERE p.estat = true $condicio_permis2
									    ORDER BY titol_ca = 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 1), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 2), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 3), '.', -1) + 0
															    , SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 4), '.', -1) + 0 ",
									  array());


	$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

	

	//$time_end = microtime(true);
	//$execution_time = ($time_end - $time_start);
	//echo '<b>Total Execution Time:</b> '.$execution_time.' Sec<br><br>';
	//echo memory_get_usage() - $startMemory, ' bytes <br>';

	// D'entrada el filtre te de 0 a 100 % però no ho mostrem.
	if ($percent1get == 0 && $getenc['percent1'] == "") $percent1get = "";
	if ($percent2get == 100 && $getenc['percent2'] == "") $percent2get = "";
	if ($percent3get == 0 && $getenc['percent3'] == "") $percent3get = "";
	if ($percent4get == 100 && $getenc['percent4'] == "") $percent4get = "";

	if (!empty($iniciget)){
		$iniciget = explode("-",$iniciget);
		$iniciget = date("$iniciget[2]/$iniciget[1]/$iniciget[0]");
	}
	
	if (!empty($finalget)){
		$finalget = explode("-",$finalget);
		$finalget = date("$finalget[2]/$finalget[1]/$finalget[0]");
	}


	if (!empty($responsablesget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Responsables</b>: ";
		foreach ($UsuarisFiltre as $key_di => $value_di) {
			
			if(in_array($value_di[id], $responsablesget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($membresget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Membres</b>: ";
		foreach ($UsuarisFiltre2 as $key_di => $value_di) {
			
			if(in_array($value_di[id], $membresget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	

	unset($id);
	unset($arraycondicio);
	$condicio_permis2 = "";	
	include("inici_inc.php");

	// Generar Export.
	if (!empty($exportget)) include("consulta_export.php");

	// Export individual
	if (!empty($exportget_ind)) include("consulta_export_ind.php");

	require_once("projectes_llista.php");

	
	
	// JS.

	$js = '

			$(".nou").click(function(){

                if ($("#idprojecte").val() != "")
                {   
                    $(".2npas").html("");
                    $("#idprojecte").val("");
                    $("#idx").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmprojectes").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmprojectes").removeAttr("checked").removeAttr("selected");
                    $("div.errorprojectes span").html("");
                    $(".panelldadesprojectes").show();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultprojectes").html("");
                	$("#spancodi").html("Serà assignat al crear el projecte");
                	$(".linus").html("Nou projecte");
                }else{
                    $(".panelldadesprojectes").toggle();
                }

            });

			
			

			setTimeout(function () {
	';
				
				// Per iniciar el popup automàticament 500milisegons despres d'iniciar
				if (isset($_GET['id']))
				{
					$idproj = intval($_GET['id']);

					$js .= ' 
						
						$("#divcamps").html("<div  style=\"text-align: center;\"><img src=\"../images/loading.gif\" /></div>");
			        	$(".amagamissatges").html("");
			        	$(".panelldades").show();
			        	$("#divcamps").show();
			    		$("#divcamps").load("../load", {id: '.$idproj.', o: 3, t: 1});
				
					';

				}


	$js .='			
					

             }, 1500);
           


	';

			//$(".llistat_1").html("<img src=\''.$url.'/images/loading.gif\'/>");
			//$(".llistat_1").load("'.$url.'/load",{o:2,id:1,t:"1",c:{"f":"'.$_GET['f'].'"}}, function(){

			if (isset($_GET['n']))
			{
	$js .='		$(".nou").click(); ';
			}

			if (isset($_GET['a']))
			{

				$VinculacioAssignat = $dbb->FreeSql("SELECT t.titol_ca as titol, t.id as id, t.clau_pam
											  FROM pfx_projectes_vinculacio t
											  WHERE nivell = 4 AND t.id = :id ",array("id"=>intval($_GET[a])));

				if (!empty($VinculacioAssignat)){
					$nomassignat = $VinculacioAssignat[1][titol];
					$idassignat = $VinculacioAssignat[1][clau_pam]."_".$VinculacioAssignat[1][id];
				}


			}


	$js .= '

		$(document).off("click",".mostrarconsulta").on("click",".mostrarconsulta",function(event){

				event.preventDefault();

	            var id = $(this).data( "id" );	

	            var t = $(this).data( "t" );
                if (t == ""){ t = 1; }
	              
	          	$(".mostrarmodal2").html("<img src=\'../images/loadingg.gif\'/>");
	          	$(".mostrarmodal2").load("../load",{o:3, id:id, t: t, c:{co:1  } }, function(){

	                    
	                $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmplens" ).attr(\'disabled\', \'disabled\');
	               
	                $("#myModalLabel").html("Detalls");
	                
	                $(\'.mostrarmodal\').find(\'.btn\').hide();

	                setTimeout(function () {
	                    $(\'.mostrarmodal\').find(\'.btn\').hide();
	                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
	                }, 1500);
	                
	                setTimeout(function () {
	                    $(\'.mostrarmodal\').find(\'.btn\').hide();
	                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
	                }, 2000);

	                setTimeout(function () {
	                    $(\'.mostrarmodal\').find(\'.btn\').hide();
	                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
	                }, 4000);

	                setTimeout(function () {
	                    $(\'.mostrarmodal\').find(\'.btn\').hide();
	                    $(\'.mostrarmodal\').find(\'.btn-success\').show();
	                }, 5000);


	          }); 
	            
	            

	     });

	';

	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		'RegistreProjecte' => $RegistreProjecte,
		'Usuaris' => $Usuaris,
		'Dimensions' => $Dimensions,
		'Origens' => $Origens,
		'Vinculacio' => $Vinculacio,
		'Districtes' => $Districtes,
		'Organse' => $Organse,

		'nomassignat' => $nomassignat,
		'idassignat' => $idassignat,

		'LlistatProjectes' => $LlistatProjectes,

		// Filtre.
		'Vinculacio' => $Vinculacio,
		'Districtes' => $Districtes,
		'Organse' => $Organse,
		'UsuarisFiltre' => $UsuarisFiltre, // Només admins.
		'UsuarisFiltre2' => $UsuarisFiltre2, // Només admins.
		'vinculacionsget' => $vinculacionsget,
		'eixosget' => $eixosget,
		'liniesget' => $liniesget,
		'liniesgetjson' => json_encode($liniesget),
		'actuacionsget' => $actuacionsget,
		'actuacionsgetjson' => json_encode($actuacionsget),
		'estatsget' => $estatsget,
		'sensibleget' => $sensibleget,
		'teprojectesget' => $teprojectesget,
		'responsablesget' => $responsablesget,
		'membresget' => $membresget,
		'districtesget' => $districtesget,
		'estrategicget' => $estrategicget,
		'paraulesget' => $paraulesget,
		'percent1get' => $percent1get,
		'percent2get' => $percent2get,
		'percent3get' => $percent3get,
		'percent4get' => $percent4get,
		'hihafiltre' => $hihafiltre,
		'datamenor' => $datamenor,
		'datamajor' => $datamajor,
		'iniciget' => $iniciget,
		'finalget' => $finalget, 
		'altresprojectes' => $altresprojectes,
		'nomesprojectesget' => $nomesprojectesget,
		'ambitsget' => $ambitsget, 
		'ambits2get' => $ambits2get, 
		'ambits2getjson' => json_encode($ambits2get),
		'serveisget' => $serveisget,
		'serveisgetjson' => json_encode($serveisget),
		
		'pressu1get' => $pressu1get, 
		'pressu2get' => $pressu2get, 

		'botodesc' => $botodesc,

		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

