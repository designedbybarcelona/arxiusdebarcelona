<?php 

	$idpagina = 3;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());


	// Permisos
	$claupermisos = $app['session']->get(constant('General::nomsesiouser')."-permisos");
	$Permisos = $dbb->Llistats("permisos"," AND estat = true  AND id >= :id",array("id"=>$claupermisos),"nom_ca");

	$Organse = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");

	//$Vinculacio = $dbb->Llistats("projectes_vinculacio"," ", array(), "1*SUBSTRING_INDEX(titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(titol_ca, '.', -1) ASC");

	
	// JS.

	$js = '

			$(".nou").click(function(){

                if ($("#idusuari").val() != "")
                {   
                    $("#idusuari").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmusuaris").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmusuaris").removeAttr("checked").removeAttr("selected");
                    $("div.errorusuaris span").html("");
                    $(".panelldadesusuaris").show();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultusuaris").html("");
                }else{
                    $(".panelldadesusuaris").toggle();
                }

            });

			
			$(".llistat_4").html("<img src=\''.$url.'/images/loading.gif\'/>");
			$(".llistat_4").load("'.$url.'/load",{o:2,id:1,t:"4"}, function(){});


	';
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		'Permisos' => $Permisos,
		'Organse' => $Organse,
		'Vinculacio' => $Vinculacio,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}

	$dadesplantilla[Plans] = $Plans;

	return $dadesplantilla;

