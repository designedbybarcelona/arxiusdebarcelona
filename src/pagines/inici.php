<?php 

	$idpagina = 999;

	//$time_start = microtime(true);
	//$startMemory = memory_get_usage();
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 3 )
	{
		//header("Location: ./19/consultes.html");
		//exit();
	}

	$db = new Db();

	// Filtres
	require_once("consultesfiltres_inc.php");

	$arrayCalendari = array();

	//$projectes_clau_cero = 1; // nomes carregem la query de projectes 0. Els altres venen per ajax.
	require_once("inici_inc.php");


	// Resum projectes.
	$arrayresum = array("1"=>array(),"2"=>array(),"3"=>array(),"4"=>array());
	

	/* Versió inner join.
	$condiciovinc = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

	$Vinculacio = $dbb->FreeSql("SELECT pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.clau_cero, pv.clau_pam, pv.id as id,
									GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
									GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
									GROUP_CONCAT( DISTINCT IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents
									
								  FROM pfx_projectes_vinculacio pv
								  INNER JOIN pfx_projectes p ON $condiciovinc 
								  WHERE nivell = 4 $condicio_permis $condicio_permis2
								  GROUP By pv.id
								  ORDER BY 1*SUBSTRING_INDEX(pv.titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(pv.titol_ca, '.', -1) ASC",array());
	*/
	
	
	require_once("consultesprojectes_inc.php");

	// càlculs on no afecta el filtre per mostrar dades a les actuacions.
	include("consultaprojectes_inccalcact.php");

	
	/*
	$Vinculacio4 = $db->query("SELECT pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.clau_cero, pv.clau_pam, pv.id as id,
									GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
									GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
									GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents, 
									GROUP_CONCAT( IFNULL(p.pesrelatiu,'') SEPARATOR ';') as pesosrelatius,
									(Select LEFT(pv2.titol_ca, 2) FROM pfx_projectes_vinculacio pv2 WHERE pv2.id = pv.clau_pam) as codipam

								  FROM pfx_projectes_vinculacio pv
								  LEFT JOIN pfx_projectes_relacionsvinc rp ON rp.id_act = pv.id
								  LEFT JOIN pfx_projectes p ON rp.id_proj = p.id 
								  WHERE nivell = 4 $condicio_permis $condicio_permis2 $condicioprojectes
								  GROUP By pv.id
								  ORDER BY SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 1), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 2), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 3), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 4), '.', -1) + 0
								  ",array());


	if (!empty($Vinculacio4)){
		foreach ($Vinculacio4 as $key => $value) {
			$Vinculacio4[$key][titol] = str_replace(".", "", $value[codipam]).'.'.$value[titol];
			if (!empty($value[datesinici])){
				$datesinici = explode(",", $value[datesinici]);
				$datesinici = array_filter($datesinici, function($value2){return !empty($value2) && $value2 != "0000-00-00";});
				sort($datesinici);
				$Vinculacio4[$key][datesinici] = $datesinici;
			}
			if (!empty($value[datesfinal])){
				$datesfinal = explode(",", $value[datesfinal]);
				$datesfinal = array_filter($datesfinal, function($value){return !empty($value) && $value != "0000-00-00";});
				rsort($datesfinal);
				$Vinculacio4[$key][datesfinal]= $datesfinal;
			}
			
			$mitjaexec = 0;
			if (($value[tantspercents])!=""){ // no posar !empty o es salta els % = 0i dona resultats erronis.
				$tantspercents = explode(";", $value[tantspercents]);
				$pesosrelatius = explode(";", $value[pesosrelatius]);

				// Verificar els pesos relatius, en cas de tenir pes 0.00, es reparteix el % restant en aquests que tenen 0.
				$totalpesrelaiu = 0;
				$totalpesosindicats = 0;
				$contapesos = count($pesosrelatius);
				foreach ($pesosrelatius as $key_pes => $value_pes) {
					if ($value_pes != "0.00"){
						$totalpesrelaiu += $value_pes;
						$totalpesosindicats++;
					}
				}
				if ($totalpesrelaiu != "100" && $contapesos>$totalpesosindicats){
					$calcpes = 100-$totalpesrelaiu;
					$pescalculat = $calcpes/($contapesos-$totalpesosindicats);
				}

				$contatants = count($tantspercents);
				$sumatants = 0;
				foreach ($tantspercents as $key_t => $value_t) {
					if ($pesosrelatius[$key_t]!="0.00"){
						$sumatants += ($value_t)*($pesosrelatius[$key_t]/100);
					}else{
						$sumatants += $value_t*($pescalculat/100);
					}
					
				}
				
				$mitjaexec = $sumatants;

				$Vinculacio4[$key][tantspercents]= number_format($mitjaexec,2);

			}
		}
	}
	*/



	
	
   	$Dates = $dbb->FreeSql("SELECT *
						    FROM (
						    		SELECT  observacionsfita as titol, t.datai, t.datai as datafi, '6/projectes.html?id=' as pagina, t.id, p.titol_ca as nomprojecte, ti.titol_ca as tipologiafita,
						    		'' as color, t.clau_projecte, UNIX_TIMESTAMP(t.datai) as iniciunix, UNIX_TIMESTAMP(t.datai) as finalunix
						    	    FROM pfx_projectes_fites t
						    	    INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
						    	    INNER JOIN pfx_projectes_relacionsvinc rp ON rp.id_proj = p.id
						    	    LEFT JOIN pfx_projectes_fites_tipologies ti ON ti.id = t.tipologia
						    	    WHERE 1 = 1 $condicio_permis_uvinc $condicio_permis_projectes
							    
							    			
							) a
							ORDER BY datai",
						  array());	

   	if (!empty($Dates)){

   		foreach ($Dates as $key_d => $value_d) 
		{
			array_push($arrayCalendari, array("id"=> "Cp_".$value_d[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[titol])))), 
											  "start" =>date("Y-m-d",strtotime($value_d[datai])), 
											  "end" =>date("Y-m-d",strtotime($value_d[datafi])), 
											  "pagina"=> $value_d[pagina].$value_d[clau_projecte], 
											  "inici" =>date("d/m/Y",strtotime($value_d[datai])), 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[nomprojecte])))), 
											  "color" => $value_d[color],
											  'tipus' => "fita",
											  'tipologia' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[tipologiafita])))), 
											  'className' => "calfites",
											  'color' => "#5681de",
											  'icon' => "calendar",
											  ));

			if (in_array($value_d["clau_projecte"], $arrayprojectesmeus )){
					
				array_push($arrayCalendari, array("id"=> "Cp_".$value_d[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[titol])))), 
											  "start" =>date("Y-m-d",strtotime($value_d[datai])), 
											  "end" =>date("Y-m-d",strtotime($value_d[datafi])), 
											  "pagina"=> $value_d[pagina].$value_d[clau_projecte], 
											  "inici" =>date("d/m/Y",strtotime($value_d[datai])), 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[nomprojecte])))), 
											  "color" => $value_d[color],
											  'tipus' => "fita",
											  'tipologia' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_d[tipologiafita])))), 
											  'className' => "calmeus",
											  'color' => "#5681de",
											  'icon' => "calendar",
											  ));

			}
		}

   	}



   	// Filtre.
   	//if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 1 )
	//{

		// Filtres
		require_once("consultesfiltres_inc.php");

	   	$Vinculacio = $dbb->Llistats("projectes_vinculacio"," AND (nivell = 1 OR nivell = 2 OR nivell = 3) AND estat = true  ", array(), " SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 1), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 2), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 3), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(titol_ca, '.'), '.', 4), '.', -1) + 0");

		$Districtes = $dbb->Llistats("projectes_districtes"," ", array(), "titol_ca");

	//}

	//$time_end = microtime(true);
	//$execution_time = ($time_end - $time_start);
	//echo '<b>Total Execution Time:</b> '.$execution_time.' Sec<br><br>';
	//echo memory_get_usage() - $startMemory, ' bytes <br>';

	// D'entrada el filtre te de 0 a 100 % però no ho mostrem.
	if ($percent1get == 0 && $getenc['percent1'] == "") $percent1get = "";
	if ($percent2get == 100 && $getenc['percent2'] == "") $percent2get = "";
	if ($percent3get == 0 && $getenc['percent3'] == "") $percent3get = "";
	if ($percent4get == 100 && $getenc['percent4'] == "") $percent4get = "";

	if (!empty($iniciget)){
		$iniciget = explode("-",$iniciget);
		$iniciget = date("$iniciget[2]/$iniciget[1]/$iniciget[0]");
	}
	
	if (!empty($finalget)){
		$finalget = explode("-",$finalget);
		$finalget = date("$finalget[2]/$finalget[1]/$finalget[0]");
	}


	if (!empty($responsablesget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Responsables</b>: ";
		foreach ($UsuarisFiltre as $key_di => $value_di) {
			
			if(in_array($value_di[id], $responsablesget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	if (!empty($membresget)){
		$nusfiltre .= (!empty($nusfiltre)?" ":"")."<div style='display:inline-block;border:1px solid red; padding:5px; margin-bottom:2px;'><b style='color:#cd0d2d;'>Membres</b>: ";
		foreach ($UsuarisFiltre2 as $key_di => $value_di) {
			
			if(in_array($value_di[id], $membresget)){
				$nusfiltre .= $value_di[nom]." ".$value_di[cognoms].", ";
			}
			
		}
		$nusfiltre .= "</div>";
		$filtredef=1;
	}

	// Generar Export.
	if (!empty($exportget)) include("consulta_export.php");

	// Export individual
	if (!empty($exportget_ind)) include("consulta_export_ind.php");



	// Mapa.
	if ($_GET['t'] == 'm'){

		$DadesCentres = $dbb->FreeSql("SELECT t.*
								  
									  FROM pfx_projectes_organs_e t
									  WHERE t.nivell = 1
									  
									  ORDER BY titol_ca ", array());
		


		$Dadesmapa .= '
			
			<a name="mapes"></a>

  
            <style type="text/css">
                #map_canvas2 {height:600px;width:100%;}
            </style>

	        <div class="row">
				<div class="col-md-12">
		            <h3 style="color:#cc032f;">
		                Mapa de centres
		            </h3>
		            <hr style="border-top: 1px solid #cc032f;">
		        </div>
	        </div>

			<div class="row">
	        	<div class="col-lg-12">
                  <div id="map_canvas2"></div>
                </div>
            </div>

            <script type="text/javascript">

				$(document).ready(function(){

					jQuery.loadScript = function (url, callback) {
					    jQuery.ajax({
					        url: url,
					        dataType: \'script\',
					        success: callback,
					        async: true
					    });
					}

					if (typeof someObject == \'undefined\') $.loadScript(\'https://maps.google.com/maps/api/js?key=AIzaSyAveCJBu7A_jXf0-EHbdBKzv8dU14oGHMU\', function(){
					    
						
						function initMap() {
			              var map = new google.maps.Map(document.getElementById(\'map_canvas2\'), {
			                zoom: 13,
			                center: {lat: 41.3896803, lng: 2.1801652}
			              });

			              setMarkers(map);
			            }

			            // Data for the markers consisting of a name, a LatLng and a zIndex for the
			            // order in which these markers should display on top of each other.
			            var beaches = [
			     ';
			     			if (!empty($DadesCentres))
	                        {
				     			foreach ($DadesCentres as $key_p => $value_p) {
				     				
				     				if ($value_p[lat] != "" && $value_p[lng]!= "")
				     				{
					                    
					                    $Dadesmapa .= '
						                     [\''.strip_tags($value_p[titol_ca]).'\', '.$value_p[lat].', '.$value_p[lng].', 4,

						                      \'<div id="content">\'+
						                      \'<div id="siteNotice">\'+
						                      \'</div>\'+
						                      \'<h5 id="firstHeading" class="firstHeading">\'+
						                      \'<b>'.str_replace("'", "`", $value_p[titol_ca]).'</b>\'+
						                      \'</h5>\'+
						                      \'<h5 id="firstHeading" class="firstHeading">\'+
						                      \'('.str_replace("'", "`", $value_p[acronim]).')\'+
						                      \'<br>'.str_replace("'", "`",$value_p[adreca]).'\'+
						                      \'<br>'.str_replace("'", "`",$value_p[telefon]).'\'+
						                      \'<br>'.str_replace("'", "`",$value_p[email]).'\'+
						                      \'<br><a href="'.str_replace("'", "`",$value_p[web]).'" target="_blank">'.str_replace("'", "`",$value_p[web]).'</a>\'+
						                      \'<br><br>'.preg_replace( "/\r|\n/", "", str_replace("'", "`",trim(stripslashes(htmlspecialchars($value_p[descripcio_ca]))))).'\'+
						                      \'</h5>\'+
						                      \'<div id="bodyContent">\'+
						                      \'</p>\'+
						                      \'</div>\'+
						                      \'</div>\',
											  \''.$estatasca.'\',
						                      '.$value_p[estat].'

					                     	],
					                    ';
				                  	}
				              	}
				            }

			$Dadesmapa .= '
			            ];

			            var markerGroups = {
			              "1": [],
			              "2": []
			            };

			            function setMarkers(map) {
			             

			              for (var i = 0; i < beaches.length; i++) {
			                var beach = beaches[i];
			                var marker = new google.maps.Marker({
			                  position: {lat: beach[1], lng: beach[2]},
			                  map: map,
			                  icon: \'images/marker_1.png\',
			                  title: beach[0],
			                  zIndex: beach[3],
			                  type: beach[5],
			                });

			                 attachSecretMessage(marker, beach[4]);
			                 
			                 if (!markerGroups[beach[5]]) markerGroups[beach[5]] = [];
			                 markerGroups[beach[5]].push(marker);


			                 
			              }
			            }


			            function attachSecretMessage(marker, secretMessage) {
			              var infowindow = new google.maps.InfoWindow({
			                content: secretMessage
			              });

			               marker.addListener(\'click\', function() {
			                  infowindow.open(marker.get(\'map\'), marker);
			                });
			            }

			         

			            initMap ();


					});

					
		        });

			</script>

	    ';

	}

	
	$dadesplantilla = array(
		
		'Vinculacio4' => $Vinculacio4,
		'Vinculacio4dates' => $Vinculacio4dates,
		'Vinculacio4tantspercents' => $Vinculacio4tantspercents,
		'arrayresum' => $arrayresum,
		'DadesProjectes' => $DadesProjectes, // només per altres projectes
		'Actuacions' => $Actuacions, // només per altres projectes
		'Tasques' => $Tasques, // només per altres projectes

		'f' => $_GET['f'],
		'getc' => $_GET['t'],
		'Dadesmapa' => $Dadesmapa,
		

		'arrayCalendari' => json_encode($arrayCalendari),

		// Filtre.
		'Vinculacio' => $Vinculacio,
		'Districtes' => $Districtes,
		'Organse' => $Organse,
		'UsuarisFiltre' => $UsuarisFiltre, // Només admins.
		'UsuarisFiltre2' => $UsuarisFiltre2, // Només admins.
		'vinculacionsget' => $vinculacionsget,
		'eixosget' => $eixosget,
		'liniesget' => $liniesget,
		'liniesgetjson' => json_encode($liniesget),
		'actuacionsget' => $actuacionsget,
		'actuacionsgetjson' => json_encode($actuacionsget),
		'estatsget' => $estatsget,
		'sensibleget' => $sensibleget,
		'teprojectesget' => $teprojectesget,
		'responsablesget' => $responsablesget,
		'membresget' => $membresget,
		'districtesget' => $districtesget,
		'estrategicget' => $estrategicget,
		'paraulesget' => $paraulesget,
		'percent1get' => $percent1get,
		'percent2get' => $percent2get,
		'percent3get' => $percent3get,
		'percent4get' => $percent4get,
		'hihafiltre' => $hihafiltre,
		'datamenor' => $datamenor,
		'datamajor' => $datamajor,
		'iniciget' => $iniciget,
		'finalget' => $finalget, 
		'altresprojectes' => $altresprojectes,
		'nomesprojectesget' => $nomesprojectesget,
		'ambitsget' => $ambitsget, 
		'ambits2get' => $ambits2get, 
		'ambits2getjson' => json_encode($ambits2get),
		'serveisget' => $serveisget,
		'serveisgetjson' => json_encode($serveisget),
		'pressu1get' => $pressu1get, 
		'pressu2get' => $pressu2get, 

		'botodesc' => $botodesc,

		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

