<?php 

	$idpagina = 29;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	
	// JS.

	$js = '

			$(".nou").click(function(){

                if ($("#idd").val() != "")
                {   
                    $("#idd").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmdistrictes").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmdistrictes").removeAttr("checked").removeAttr("selected");
                    $("div.errordistrictes span").html("");
                    $("#resultdistrictes").html("");
                    $(".panelldadesdistrictes").toggle();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultdistrictes").html("");
                	$(".vinculacio2").html("");
                }else{
                    $(".panelldadesdistrictes").toggle();
                }

            });


			$(".llistat_15").load("'.$url.'/load",{o:2,id:1,t:"15"}, function(){
                    
            });


	';
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

