<?php
	
	exit();

	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Crear noves relacions a la taula relacionada.

	$Projectes = $dbb->Llistats("projectes"," " ,array(), "id", false );

	foreach ($Projectes as $key_p => $value_p) 
	{
		if (!empty($value_p[vinculacio]))
		{
			// Eliminar relacions previes.
			$db = new Db();
			$db->query("DELETE FROM pfx_projectes_relacionsvinc
				     	WHERE id_proj = '".$value_p[id]."' ");

			$vinculacio = json_decode($value_p[vinculacio]);

			$arrayvinc=array();
			foreach ($vinculacio as $key_v => $value_v) {

				$valors = explode("_", $value_v);

				foreach ($valors as $key_v2 => $valor) 
				{	
					if (!in_array($valor, $arrayvinc))
					{
						$NivellVinc = $dbb->Llistats("projectes_vinculacio"," AND id = :id" ,array("id"=>$valor), "id", false );
						$nivell = $NivellVinc[1][nivell];
						if ($nivell==1)
							$camp = "id_pam";
						elseif ($nivell==2)
							$camp = "id_eix";
						elseif ($nivell==3)
							$camp = "id_linia";
						elseif ($nivell==4)
							$camp = "id_act";
						
						$dadesr[$camp] = $valor;
						array_push($arrayvinc, $valor);
					}
				}
				
			}

			$dadesr[id_proj] = $value_p[id];
				
			$arrayguardarr = array('taula' =>  'projectes_relacionsvinc', 
								  'dades' =>  $dadesr,
								  'tipus' =>  a,);
			$dbb->GuardarRegistre2($arrayguardarr);
		}
		
	}

	