<?php	
	
	$queryVinculacio4 = "
		
		SELECT pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.clau_cero, pv.clau_pam, pv.id as id,
		GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
		GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
		GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents, 
		GROUP_CONCAT( IFNULL(p.pesrelatiu,'') SEPARATOR ';') as pesosrelatius, 
		GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents2, 
		GROUP_CONCAT( DISTINCT IFNULL(p.estat,'') SEPARATOR ',') as estatssproj,
		pv.clau_eix, pv.parent_id as clau_linia,
		(Select LEFT(pv2.titol_ca, 2) FROM pfx_projectes_vinculacio pv2 WHERE pv2.id = pv.clau_pam) as codipam,
		count( DISTINCT IFNULL(p.id,'') ) as contaprojectes,
		pv3.titol_ca as titolambit, pv2.titol_ca as titoleix, pv.estat as estatvinculacio,
		(Select GROUP_CONCAT( DISTINCT IFNULL(p2.estat,'') SEPARATOR ',') FROM pfx_projectes p2 
			INNER JOIN pfx_projectes_relacionsvinc rp2 ON rp2.id_proj = p2.id
		 WHERE rp2.id_act = pv.id
		) as estatssproj2
		

	  FROM pfx_projectes_vinculacio pv
	  LEFT JOIN pfx_projectes_relacionsvinc rp ON rp.id_act = pv.id
	  LEFT JOIN pfx_projectes p ON rp.id_proj = p.id $condicio_psensible $condicio_permis_centres $condicioprojectes_centres
	  LEFT JOIN pfx_projectes_vinculacio pv3 ON pv3.id = pv.parent_id AND pv3.nivell = 3
	  LEFT JOIN pfx_projectes_vinculacio pv2 ON pv2.id = pv.clau_eix AND pv2.nivell = 2 
	  $innerresponsables
	  $leftoperadors
	  WHERE pv.nivell = 4 $condicio_permis $condicio_permis2 $condicioprojectes  $condiciollistatprojectes $condicioprojectesfiltrats $condicioactuacionsfiltrades $condicioprojectesinici $condicio_permis_idvinc $condicioresponsables 
	  GROUP By pv.id
	  ORDER BY  SUBSTR(pv.titol_ca FROM 1 FOR 1),
    			SUBSTR(pv.titol_ca FROM 2)

	
	"; // LENGTH(concat(codipam)), concat(codipam,pv.titol_ca)
	   // 1*SUBSTRING_INDEX(concat(codipam,pv.titol_ca), '.', 1) ASC, 1*SUBSTRING_INDEX(concat(codipam,pv.titol_ca), '.', -1) ASC
	   // SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 1), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 2), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 3), '.', -1) + 0, SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(concat(codipam,pv.titol_ca), '.'), '.', 4), '.', -1) + 0
	$queryVinculacio4orig = $queryVinculacio4;

	/*
		GROUP_CONCAT( DISTINCT IFNULL(p.estat,'') SEPARATOR ';') as estats,
		GROUP_CONCAT( DISTINCT IFNULL(p.titol_ca,'') SEPARATOR '¬') as titolsproj,
		GROUP_CONCAT( DISTINCT IFNULL(p.districtes,'') SEPARATOR ';') as districtesproj,
		GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici2,
		GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal2
	*/

	//echo $queryVinculacio4;Exit();

