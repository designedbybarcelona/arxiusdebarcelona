<?php
	
	$tantsxcentseixos =  $tantsxcentslinies =  $tantsxcentspads = $eixosamostrar = $liniesamostrar = $actuacionsamostrar = array();
	
	// Versió inner join.
	/*
	$condiciovinc = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

	$Vinculacio4 = $dbb->FreeSql("SELECT pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.clau_cero, pv.clau_pam, pv.id as id,
									GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
									GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
									GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents, 
									GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents2, pv.clau_eix

								  FROM pfx_projectes_vinculacio pv
								  INNER JOIN pfx_projectes p ON $condiciovinc 
								  WHERE nivell = 4 $condicio_permis $condicio_permis2 $condicioprojectes
								  GROUP By pv.id
								  ORDER BY 1*SUBSTRING_INDEX(pv.titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(pv.titol_ca, '.', -1) ASC",array());
	*/

	if ($aplicarfiltrevinculacio==1){
		$condiciollistatprojectes = " AND pv.id = :idvinc";
		$arraycondiciollistat = array("idvinc" => $id);
	}else{
		$arraycondiciollistat = array();
	}
	
	if (!isset($saltaquery)) include("consultaprojectes_query.php");

	$Vinculacio4 = $db->query("$queryVinculacio4",$arraycondiciollistat);


	$arrayVinculacions = array();
	if (!empty($Vinculacio4)){
		$condicioprojectesfiltrats = "";
		foreach ($Vinculacio4 as $key => $value) {

			if (!empty($value[idsproj]))
			{
				if (!empty($value[datesinici])){
					$datesinici = explode(",", $value[datesinici]);
					$datesinici = array_filter($datesinici, function($value){return !empty($value) && $value != "0000-00-00";});
					sort($datesinici);
					$Vinculacio4[$key][datesinici] = $datesinici;
					$Vinculacio4[$key][iniciunix] = strtotime($datesinici[0]);
				}
				if (!empty($value[datesfinal])){
					$datesfinal = explode(",", $value[datesfinal]);
					$datesfinal = array_filter($datesfinal, function($value){return !empty($value) && $value != "0000-00-00";});
					rsort($datesfinal);
					$Vinculacio4[$key][datesfinal]= $datesfinal;
				}
				$mitjaexec = 0;
				if (($value[tantspercents])!=""){ // no posar !empty o es salta els % = 0i dona resultats erronis.
					$tantspercents = explode(";", $value[tantspercents]);
					$pesosrelatius = explode(";", $value[pesosrelatius]);

					// Verificar els pesos relatius, en cas de tenir pes 0.00, es reparteix el % restant en aquests que tenen 0.
					$totalpesrelaiu = 0;
					$totalpesosindicats = 0;
					$contapesos = count($pesosrelatius);
					foreach ($pesosrelatius as $key_pes => $value_pes) {
						if ($value_pes != "0.00" && !empty($value_pes)){
							$totalpesrelaiu += $value_pes;
							$totalpesosindicats++;
						}
					}
					if ($totalpesrelaiu != "100" && $contapesos>$totalpesosindicats){
						$calcpes = 100-$totalpesrelaiu;
						$pescalculat = $calcpes/($contapesos-$totalpesosindicats);
					}

					$contatants = count($tantspercents);
					$sumatants = 0;
					foreach ($tantspercents as $key_t => $value_t) {
						if ($pesosrelatius[$key_t]!="0.00" && !empty($pesosrelatius[$key_t])){
							$sumatants += ($value_t)*($pesosrelatius[$key_t]/100);
						}else{
							if (!empty($value_t))
							$sumatants += $value_t*($pescalculat/100);
						}
						
					}
					
					$mitjaexec = $sumatants;
					/*
					if ($contatants>0){
						$mitjaexec = $sumatants/$contatants;
					}else{
						$mitjaexec = 0;
					}
					*/
					

					$Vinculacio4[$key][tantspercents]= number_format($mitjaexec,2);

				}

				/* Versió context.
					// Si els filtres son buits. Afetim tots.
					if (empty($estatsafiltrar)&& empty($paraulesget) && $getenc['percent1'] == "" && $getenc['percent2'] == "" && $getenc['percent3'] == "" && $getenc['percent4'] == "" 
						&& empty($districteafiltrar) && empty($iniciget)  && empty($finalget)  ){
						$afegir = true;
					}
					

					// Per el filtre d'estats, mirem si algun dels projectes el compleix, en cas afirmatiu, fiquem eix, linea i acutació. a l'array de lines a mostrar.
					if (($value[estats])!="")
					{ 
						if (!empty($estatsafiltrar)){
							$estats = explode(";", $value[estats]);
							$estastrobats = array_intersect($estatsafiltrar, $estats);
							if (!empty($estastrobats)){
								$afegir = true;
							}
						}
					}
					// Per el filtre de paraules, mirem si algun dels projectes el compleix, en cas afirmatiu, fiquem eix, linea i acutació. a l'array de lines a mostrar.
					if (!empty($paraulesget))
					{ 
						// Títol actuació.
						foreach ($paraulesget as $key_pa => $value_pa) {
							if (strpos(mb_strtolower($value[titol],'UTF-8'), mb_strtolower($value_pa,'UTF-8')) !== false) {
							    $afegir = true;
							}
							// Títols projecte.
							if (($value[titolsproj])!=""){
								if (strpos(mb_strtolower($value[titolsproj],'UTF-8'), mb_strtolower($value_pa,'UTF-8')) !== false) {
								    $afegir = true;
								}
							}
						}
					}
					// Per el filtre de % d'actuació, mirem si algun de les actuacions el compleix, en cas afirmatiu, fiquem eix, linea i acutació. a l'array de lines a mostrar.
					if ($getenc['percent3'] != "" && $getenc['percent4'] != ""){
						if ($Vinculacio4[$key][tantspercents]>=$percent3get && $Vinculacio4[$key][tantspercents]<=$percent4get){
							$afegir = true;
						}else{
							if (!empty($value[idsproj])){
								$idsproj = explode(",", $value[idsproj]);
								foreach ($idsproj as $key_proj => $value_idproj) {
									if ($condicioprojectesfiltrats == "") { $condicioprojectesfiltrats .= " AND ( ";}else{$condicioprojectesfiltrats .= " AND ";}
									$condicioprojectesfiltrats .= " p.id <> '$value_idproj'";
								}
							}
						}
					}
					// Per el filtre de % de projectes, mirem si algun dels projectes el compleix, en cas afirmatiu, fiquem eix, linea i acutació. a l'array de lines a mostrar.
					if (($value[tantspercents2])!="")
					{ 
						if ($getenc['percent1'] != "" && $getenc['percent2'] != ""){
							$tantspercents = explode(";", $value[tantspercents2]);
							foreach ($tantspercents as $key_tant => $value_tant) {
								if ($value_tant>=$percent1get && $value_tant<=$percent2get){
									$afegir = true;
								}
							}
						}
					}
					// Per el filtre de districtes.
					if (($value[districtesproj])!="")
					{
						if (!empty($districteafiltrar)){
							$districtesproj = explode(";", $value[districtesproj]);
							foreach ($districtesproj as $key_dis => $value_dis) {
								if (!empty($value_dis)){
									$value_dis = json_decode($value_dis,true);
									$districtestrobats = array_intersect($districteafiltrar, $value_dis);
									if (!empty($districtestrobats)){
										$afegir = true;
										break;
									}
								}
							}
							
						}
					}
					// Per el filtre de dates.
					if (!empty($iniciget) && empty($finalget) && !empty($value[datesinici2])){
						$datesinici = explode(",", $value[datesinici2]);
						foreach ($datesinici as $key_inici => $value_inici) {
							if (!empty($value_inici) ){
								if (strtotime($value_inici) >= strtotime($iniciget)){
									$afegir = true;
								}
							}
						}
					}
					if (!empty($finalget) && empty($iniciget) && !empty($value[datesfinal2])){
						$datesfinal = explode(",", $value[datesfinal2]);
						foreach ($datesfinal as $key_final => $value_final) {
							if (!empty($value_final) ){
								if (strtotime($value_final) <= strtotime($finalget)){
									$afegir = true;
								}
							}
						}
					}
					if (!empty($iniciget) && !empty($finalget) && !empty($value[datesinici2]) && !empty($value[datesfinal2])){
						$datesinici = explode(",", $value[datesinici2]);
						$datesfinal = explode(",", $value[datesfinal2]);
						foreach ($datesinici as $key_inici => $value_inici) {
							if (!empty($value_inici) && $datesfinal[$key_inici]){
								if (strtotime($value_inici) >= strtotime($iniciget) && strtotime($datesfinal[$key_inici]) <= strtotime($finalget)){
									$afegir = true;
								}
							}
						}
					}



					// Afegir les dades a mostrar,
					if ($afegir == true){
						if (!in_array($value[clau_eix], $eixosamostrar)) array_push($eixosamostrar, $value[clau_eix]);
						if (!in_array($value[clau_linia], $liniesamostrar)) array_push($liniesamostrar, $value[clau_linia]);
						if (!in_array($value[id], $actuacionsamostrar)) array_push($actuacionsamostrar, $value[id]);

						// per el gantt
						if ($condicioactuacionsfiltrades == "") { $condicioactuacionsfiltrades .= " AND ( ";}else{$condicioactuacionsfiltrades .= " OR ";}
						$condicioactuacionsfiltrades .= " pv.id = '".$value[id]."'";

						
						
						$afegir = false;
					}
				*/


				
				// Per el filtre de % d'actuació. Sinó compleix esborrem de l'array.
				if ($Vinculacio4[$key][tantspercents]>=$percent3get && $Vinculacio4[$key][tantspercents]<=$percent4get){
					$arrayVinculacions[$value[id]] = $Vinculacio4[$key];

					// Càlcul gràfics.
					if (isset($tantsxcentspads[$value[clau_pam]])){
		      			$contanoupads = $tantsxcentspads[$value[clau_pam]][conta]+1;
		      			$tantsxcentspads[$value[clau_pam]] = array("conta" => $contanoupads,
		      														  "tantpercent" => ($Vinculacio4[$key][tantspercents]+$tantsxcentspads[$value[clau_pam]][tantpercent]));
		      		}else{
		      			$tantsxcentspads[$value[clau_pam]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4[$key][tantspercents]));
		      		}

		      		if (isset($tantsxcentseixos[$value[clau_eix]])){
		      			$contanoueix = $tantsxcentseixos[$value[clau_eix]][conta]+1;
		      			$tantsxcentseixos[$value[clau_eix]] = array("conta" => $contanoueix,
		      														  "tantpercent" => ($Vinculacio4[$key][tantspercents]+$tantsxcentseixos[$value[clau_eix]][tantpercent]));
		      		}else{
		      			$tantsxcentseixos[$value[clau_eix]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4[$key][tantspercents]));
		      		}

		      		if (isset($tantsxcentslinies[$value[clau_linia]])){
		      			$contanoulinea = $tantsxcentslinies[$value[clau_linia]][conta]+1;
		      			$tantsxcentslinies[$value[clau_linia]] = array("conta" => $contanoulinea,
		      														     "tantpercent" => ($Vinculacio4[$key][tantspercents]+$tantsxcentslinies[$value[clau_linia]][tantpercent]));
		      		}else{
		      			$tantsxcentslinies[$value[clau_linia]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4[$key][tantspercents]));
		      		}

		      		// Dades per pads triats desde el cercador.

		      		// Eixos.
		      		if (isset($tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]])){
		      			$contanoueix = $tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]][conta]+1;
		      			$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]] = array("conta" => $contanoueix,
		      														  						 "tantpercent" => ($Vinculacio4[$key][tantspercents]+$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]][tantpercent]));
		      		}else{
		      			$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4[$key][tantspercents]));
		      		}


		      		// Linies
		      		if (isset($tantsxcentsliniespad[$value[clau_pam]][$value[clau_linia]])){
		      			$contanoulinea = $tantsxcentsliniespad[$value[clau_pam]][$value[clau_linia]][conta]+1;
		      			$tantsxcentsliniespad[$value[clau_pam]][$value[clau_linia]] = array("conta" => $contanoulinea,
		      														  						    "tantpercent" => ($Vinculacio4[$key][tantspercents]+$tantsxcentsliniespad[$value[clau_pam]][$value[clau_linia]][tantpercent]));
		      		}else{
		      			$tantsxcentsliniespad[$value[clau_pam]][$value[clau_linia]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4[$key][tantspercents]));
		      		}

		      		// Detalls nus.
		      		if (!empty($actuacionsget)){
			    		if(in_array($value[id], $actuacionsget)){
			    			$nusfiltreactuacions .= $value[titol].", ";
			    		}
			    	}


			    	// Dades Resum.
			    	//if ($idpagina == 999){
			    		$id = $value[id];
						include "inici_inc.php";	
						$primeraentrada = true;
				    	if (!empty($DadesProjectes_llistat) && isset($arrayresum[1])){
							foreach ($DadesProjectes_llistat as $key_llistat => $value_llistat) {
								
								if ($value_llistat["diesfinsinici"]<0 && ($value_llistat["estat"] == 2) ){
									array_push($arrayresum[1], $DadesProjectes_llistat[$key_llistat]);
								}
								if ( ($value_llistat["diesfinsfinal"]<0 && $value_llistat["estat"] == 1 )  || $value_llistat["estat"] == 3){
									array_push($arrayresum[2], $DadesProjectes_llistat[$key_llistat]);
								}
								if ($value_llistat["diesfinsfinal"]>=0 && $value_llistat["diesfinsinici"]<0 && $value_llistat["estat"] == 1 ){
									array_push($arrayresum[3], $DadesProjectes_llistat[$key_llistat]);
								}
								if ($value_llistat["estat"] == 4 ){
									array_push($arrayresum[4], $DadesProjectes_llistat[$key_llistat]);
								}

							}
						}
					
			    	//}
					
			    	


				}else{
					unset($Vinculacio4[$key]);

					if (!empty($value[idsproj])){
						$idsproj = explode(",", $value[idsproj]);
						foreach ($idsproj as $key_proj => $value_idproj) {
							if ($condicioprojectesfiltrats == "") { $condicioprojectesfiltrats .= " AND ( ";}else{$condicioprojectesfiltrats .= " AND ";}
							$condicioprojectesfiltrats .= " p.id <> '$value_idproj'";
						}
					}
					
					
				}

			}


		}

		if ($condicioprojectesfiltrats != "") $condicioprojectesfiltrats .= " ) ";
		//if ($condicioactuacionsfiltrades != "") $condicioactuacionsfiltrades .= " ) ";
	}


	// Per poder mostrar els % de tots els pad en cas de ser $nomespam = 1
	if ($nomespam == 1){
		$strelim = "$condicioaeliminar";
		$queryVinculacio4 = str_replace($strelim, "", $queryVinculacio4);

		$Vinculacio4AUXILIAR = $db->query("$queryVinculacio4",$arraycondiciollistat);

		if (!empty($Vinculacio4AUXILIAR)){
			foreach ($Vinculacio4AUXILIAR as $key => $value) {

				if (!empty($value[idsproj]))
				{

					
					$mitjaexec = 0;
					if (($value[tantspercents])!=""){ // no posar !empty o es salta els % = 0i dona resultats erronis.
						$tantspercents = explode(";", $value[tantspercents]);
						$pesosrelatius = explode(";", $value[pesosrelatius]);

						// Verificar els pesos relatius, en cas de tenir pes 0.00, es reparteix el % restant en aquests que tenen 0.
						$totalpesrelaiu = 0;
						$totalpesosindicats = 0;
						$contapesos = count($pesosrelatius);
						foreach ($pesosrelatius as $key_pes => $value_pes) {
							if ($value_pes != "0.00" && !empty($value_pes)){
								$totalpesrelaiu += $value_pes;
								$totalpesosindicats++;
							}
						}
						if ($totalpesrelaiu != "100" && $contapesos>$totalpesosindicats){
							$calcpes = 100-$totalpesrelaiu;
							$pescalculat = $calcpes/($contapesos-$totalpesosindicats);
						}

						$sumatants = 0;
						foreach ($tantspercents as $key_t => $value_t) {
							if ($pesosrelatius[$key_t]!="0.00" && !empty($pesosrelatius[$key_t])){
								$sumatants += ($value_t)*($pesosrelatius[$key_t]/100);
							}else{
								if (!empty($value_t)){
									$sumatants += $value_t*($pescalculat/100);
								}
							}
							
						}
						
						$mitjaexec = $sumatants;
						
						$Vinculacio4AUXILIAR[$key][tantspercents]= number_format($mitjaexec,2);

					}

					
					
					// Per el filtre de % d'actuació. Sinó compleix esborrem de l'array.
					if ($Vinculacio4AUXILIAR[$key][tantspercents]>=$percent3get && $Vinculacio4AUXILIAR[$key][tantspercents]<=$percent4get){
						
						// Càlcul gràfics.

						if (isset($tantsxcentspads[$value[clau_pam]])){
			      			$contanoupads = $tantsxcentspads[$value[clau_pam]][conta]+1;
			      			$tantsxcentspads[$value[clau_pam]] = array("conta" => $contanoupads,
			      														  "tantpercent" => (floatval($Vinculacio4AUXILIAR[$key][tantspercents])+$tantsxcentspads[$value[clau_pam]][tantpercent]));
			      		}else{
			      			$tantsxcentspads[$value[clau_pam]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4AUXILIAR[$key][tantspercents]));
			      		}

			      	
			      		// Dades per pads triats desde el cercador.

			      		// Eixos.
			      		if (isset($tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]])){
			      			$contanoueix = $tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]][conta]+1;
			      			$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]] = array("conta" => $contanoueix,
			      														  						 "tantpercent" => (floatval($Vinculacio4AUXILIAR[$key][tantspercents])+$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]][tantpercent]));
			      		}else{
			      			$tantsxcentseixospad[$value[clau_pam]][$value[clau_eix]] = array("conta"=>1,"tantpercent"=>floatval($Vinculacio4AUXILIAR[$key][tantspercents]));
			      		}

			      		
				
						
						
					}

				}


			}

		}

	}
	
	// Versió inner join.
	/*
		$condiciovincusuari = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

		$DadesProjectes =  $dbb->FreeSql("SELECT p.titol_ca as nom, p.color1 as color1,  IFNULL(pa.text,'----') as estatprojecte, p.vinculacio,
										  (select count(a2.id) from pfx_projectes_fites a2 where a2.clau_projecte = p.id  ) as contafites, 
										  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id, DATE_FORMAT(p.inici, '%d/%m/%Y') as inici,
										  p.color1 as color1, p.codi as codi, p.tantpercent, p.estat as estat, DATE_FORMAT(p.final, '%Y/%m/%d') as final2,
										  p.lat, p.lng, pv.nivell, pv.clau_pam, pv.clau_cero, UNIX_TIMESTAMP(p.inici) as iniciunix, UNIX_TIMESTAMP(p.final) as finalunix,
										  pv.clau_eix, pv.parent_id as clau_linia, pv.titol_ca as nomactuacio
										  
										  FROM pfx_projectes p
										  INNER JOIN pfx_projectes_vinculacio pv ON $condiciovincusuari
										  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau 
										  WHERE 1=1 $condicio_permis $condicioprojectes  $condicio_permis2 
										  AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
										  GROUP By p.id
										  ORDER BY p.titol_ca ",array());
	*/
	$arrayCalendari = array();
	$DadesProjectes =  $db->query("SELECT p.titol_ca as nom, p.color1 as color1, p.vinculacio, IFNULL(pa.text,'----') as estatprojecte,
									  (select count(a2.id) from pfx_projectes_fites a2 where a2.clau_projecte = p.id  ) as contafites, 
									  DATE_FORMAT(p.final, '%d/%m/%Y') as final, p.id as id, DATE_FORMAT(p.inici, '%d/%m/%Y') as inici,
									  p.color1 as color1, p.codi as codi, p.tantpercent, p.estat as estat, DATE_FORMAT(p.final, '%Y/%m/%d') as final2,
									  p.lat, p.lng, pv.nivell, pv.clau_pam, pv.clau_cero, UNIX_TIMESTAMP(p.inici) as iniciunix, UNIX_TIMESTAMP(p.final) as finalunix, 
									  p.inici as inicibd, p.final as finalbd,
									  pv.clau_eix, pv.parent_id as clau_linia, pv.titol_ca as nomactuacio, p.usuaris,
									  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(poe.acronim),'') SEPARATOR ' <br> ') as acronim,
									  u_r.nom as nomresp, u_r.cognoms as cognomsresp, p.cap_projecte,
									  GROUP_CONCAT( DISTINCT IFNULL(p.usuaris,'') SEPARATOR ',') as equip,
									  GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u_2.nom,' ',u_2.cognoms),'') SEPARATOR ' | ') as nomsequip
									  FROM pfx_projectes p
									  INNER JOIN pfx_projectes_relacionsvinc rp ON rp.id_proj = p.id
									  INNER JOIN pfx_projectes_vinculacio pv ON pv.id = rp.id_act
									  LEFT JOIN pfx_projectes_organs_e poe ON FIND_IN_SET(poe.id ,REPLACE( REPLACE( REPLACE( p.ambits,'[', ''),']' ,'') ,'\"','')) > 0
									  LEFT JOIN pfx_usuaris u_r ON p.cap_projecte = u_r.id
									  LEFT JOIN pfx_usuaris u_2 ON FIND_IN_SET(u_2.id ,REPLACE( REPLACE( REPLACE( p.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
									  $innerresponsables
									  $leftoperadors
									  LEFT JOIN pfx_paraules pa ON p.estat = pa.clau 
									  WHERE 1=1 $condicio_permis $condicioprojectes $condicioprojectes2  $condicio_permis2 $condicioprojectesfiltrats $condiciollistatprojectes $condicio_permis_uvinc $condicio_permis_projectes $condicio_psensible $condicioresponsables $condicio_permis_centres
									  AND pa.codi = 'estatprojecte' AND pa.idioma = '$idioma'
									  GROUP By p.id
									  ORDER BY p.titol_ca ",$arraycondiciollistat);  


	$arrayequip = array();
	$arrayprojectesmeus = array();

	// Color semàfor i data inici menor i major.
	$arraycolors = array(
		"1" => "#7bd148", // verd.
		"2" => "#ffb878", // ambar.
		"3" => "#ff887c", // vermell.
		"4" => "#e1e1e1", // gris.
		"5" => "#5681de", // blau.
	);
	$datamenor =9999999999;
	$datamajor =0000000000;
	$contaprojecte = 0;
	$primer = false;
	if (!empty($DadesProjectes)){
		$lastElement = end($DadesProjectes);
		foreach ($DadesProjectes as $key_p => $value_p) 
		{	
			$DadesProjectes[$key_p][colorsemafor] = 0;
			$diesfinsfinal = (strtotime($value_p["final2"]) - time()) / 60 / 60 / 24;
			$DadesProjectes[$key_p][diesfinsfinal] = $diesfinsfinal;

			/*
			if ($diesfinsfinal > 0){;
				if ( $diesfinsfinal >= $ConfigAlertes[1][diesi]   ) {
					$DadesProjectes[$key_p][colorsemafor] = 1; // Verd.
				}elseif ($diesfinsfinal > $ConfigAlertes[2][diesi] && $diesfinsfinal < $ConfigAlertes[2][diesf] ) {
					$DadesProjectes[$key_p][colorsemafor] = 2; // Àmbar.
				}elseif ($diesfinsfinal <= $ConfigAlertes[3][diesi] ) {
					$DadesProjectes[$key_p][colorsemafor] = 3; // Vermell.
				}
			}else{
				$DadesProjectes[$key_p][colorsemafor] = 3; // Vermell.
			}*/

			if ($value_p[estat] == 1) { $DadesProjectes[$key_p][colorsemafor] = 2; } // Iniciat, Àmbar.
			if ($value_p[estat] == 2) { $DadesProjectes[$key_p][colorsemafor] = 5; } // No iniciat, Blau.
			if ($value_p[estat] == 3) { $DadesProjectes[$key_p][colorsemafor] = 3; } // Amb dificultats, Vermell.
			if ($value_p[estat] == 4) { $DadesProjectes[$key_p][colorsemafor] = 1; } // Finalitzat, Verd.
			if ($value_p[estat] == 5) { $DadesProjectes[$key_p][colorsemafor] = 4; } // Descartat, Gris.
			if ($value_p[estat] == ""){ $DadesProjectes[$key_p][colorsemafor] = 4; } // Gris.

			/*
			if ($value_p[estat] == 4) { // Finalitzat
				$DadesProjectes[$key_p][colorsemafor] = 5; // Blau.
			}
			if ($value_p[estat] == 5) {
				$DadesProjectes[$key_p][colorsemafor] = 4; // Gris.
			}
			*/

			// Data menor i major.
			if ($value_p[iniciunix]<$datamenor && !empty($value_p[iniciunix])) $datamenor = $value_p[iniciunix];
			if ($value_p[finalunix]>$datamajor && !empty($value_p[finalunix])) $datamajor = $value_p[finalunix];

			$sumatoritant = $sumatoritant + $value_p[tantpercent];
			if ($value_p[estat]!=0) $estatsprojecte[$value_p[estat]]++;


			// Usuaris membres.
			if (!empty($value_p[usuaris])){
				foreach (json_decode($value_p[usuaris]) as $key_u => $value_u) {


					if (!in_array($value_u, $arrayequip)){
						$arrayequip[] = $value_u;
					}
				}
			}
      		
      		// Càlculs gràfics. Mogut al bucle de vinvulació per calcular el pes relatiu.
      		/*
      		if (isset($tantsxcentseixos[$value_p[clau_eix]])){
      			$contanoueix = $tantsxcentseixos[$value_p[clau_eix]][conta]+1;
      			$tantsxcentseixos[$value_p[clau_eix]] = array("conta" => $contanoueix,
      														  "tantpercent" => ($value_p[tantpercent]+$tantsxcentseixos[$value_p[clau_eix]][tantpercent]));
      		}else{
      			$tantsxcentseixos[$value_p[clau_eix]] = array("conta"=>1,"tantpercent"=>floatval($value_p[tantpercent]));
      		}

      		if (isset($tantsxcentslinies[$value_p[clau_linia]])){
      			$contanoulinea = $tantsxcentslinies[$value_p[clau_linia]][conta]+1;
      			$tantsxcentslinies[$value_p[clau_linia]] = array("conta" => $contanoulinea,
      														     "tantpercent" => ($value_p[tantpercent]+$tantsxcentslinies[$value_p[clau_linia]][tantpercent]));
      		}else{
      			$tantsxcentslinies[$value_p[clau_linia]] = array("conta"=>1,"tantpercent"=>floatval($value_p[tantpercent]));
      		}

      		
      		if (isset($tantsxcentspads[$value_p[clau_pam]])){
      			$contanoupads = $tantsxcentspads[$value_p[clau_pam]][conta]+1;
      			$tantsxcentspads[$value_p[clau_pam]] = array("conta" => $contanoupads,
      														  "tantpercent" => ($value_p[tantpercent]+$tantsxcentspads[$value_p[clau_pam]][tantpercent]));
      		}else{
      			$tantsxcentspads[$value_p[clau_pam]] = array("conta"=>1,"tantpercent"=>floatval($value_p[tantpercent]));
      		}
      		*/

      		// Dades per pads triats desde el cercador.
      		// Estats.
      		if ($value_p[estat]!=0){
      			if (isset($estatsprojectepad[$value_p[clau_pam]][$value_p[estat]])){
	      			$estatsprojectepad[$value_p[clau_pam]][$value_p[estat]]++;
	      		}else{
	      			$estatsprojectepad[$value_p[clau_pam]][$value_p[estat]] = 1;
	      		}
      		}
      		if (isset($contaprojectepad[$value_p[clau_pam]])){
      			$contaprojectepad[$value_p[clau_pam]]++;
      		}else{
      			$contaprojectepad[$value_p[clau_pam]]= 1;
      		}

      		/* Mogut al bucle de vinvulació per calcular el pes relatiu.
      		// Eixos.
      		if (isset($tantsxcentseixospad[$value_p[clau_pam]][$value_p[clau_eix]])){
      			$contanoueix = $tantsxcentseixospad[$value_p[clau_pam]][$value_p[clau_eix]][conta]+1;
      			$tantsxcentseixospad[$value_p[clau_pam]][$value_p[clau_eix]] = array("conta" => $contanoueix,
      														  						 "tantpercent" => ($value_p[tantpercent]+$tantsxcentseixospad[$value_p[clau_pam]][$value_p[clau_eix]][tantpercent]));
      		}else{
      			$tantsxcentseixospad[$value_p[clau_pam]][$value_p[clau_eix]] = array("conta"=>1,"tantpercent"=>floatval($value_p[tantpercent]));
      		}

      		// Linies
      		if (isset($tantsxcentsliniespad[$value_p[clau_pam]][$value_p[clau_linia]])){
      			$contanoulinea = $tantsxcentsliniespad[$value_p[clau_pam]][$value_p[clau_linia]][conta]+1;
      			$tantsxcentsliniespad[$value_p[clau_pam]][$value_p[clau_linia]] = array("conta" => $contanoulinea,
      														  						    "tantpercent" => ($value_p[tantpercent]+$tantsxcentsliniespad[$value_p[clau_pam]][$value_p[clau_linia]][tantpercent]));
      		}else{
      			$tantsxcentsliniespad[$value_p[clau_pam]][$value_p[clau_linia]] = array("conta"=>1,"tantpercent"=>floatval($value_p[tantpercent]));
      		}
      		*/

      		// Final dades per pads.
      		
      		// Condicions per les actuacions i les tasques del mapa.
      		if ($primer == false) { $condicioacttasc .= " AND ( "; $primer = true; }
			$condicioacttasc .=  " a.clau_projecte = '".intval($value_p[id])."'";
			($value_p == $lastElement) ?  $condicioacttasc .= " ) " : $condicioacttasc .= " OR "; 

			// Class per filtrar els meus projeces a calendari home.
			$classmeu = "";
			if (!empty($value_p['usuaris'])){
				$arrayequip2 = json_decode($value_p['usuaris']);
				if (is_array($arrayequip2)){
					if (in_array($app['session']->get(constant('General::nomsesiouser')), $arrayequip2)){
						$classmeu = "calmeus";
						if (!in_array($value_p[id], $arrayprojectesmeus )){
							array_push($arrayprojectesmeus, $value_p[id]);
						}
					}
				}
			}
			if ($app['session']->get(constant('General::nomsesiouser')) == $value_p["cap_projecte"]){
				$classmeu = "calmeus";
				if (!in_array($value_p[id], $arrayprojectesmeus )){
					array_push($arrayprojectesmeus, $value_p[id]);
				}
			}

			array_push($arrayCalendari, array("id"=> "p_".$value_p[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
											  "start" =>date('Y-m-d',$value_p[finalunix]), 
											  "end" =>date('Y-m-d',$value_p[finalunix]), 
											  "pagina"=> "6/projectes.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
											  "tipus" => 'projecte', // final
											  'icon' => "flag-checkered",
											  'className' => "calprojectes",
											  "color" => $arraycolors[$DadesProjectes[$key_p][colorsemafor]],
											  ));

			array_push($arrayCalendari, array("id"=> "p_".$value_p[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
											  "start" =>date('Y-m-d',$value_p[iniciunix]), 
											  "end" =>date('Y-m-d',$value_p[iniciunix]), 
											  "pagina"=> "6/projectes.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
											  "tipus" => 'projecteinici',
											  'icon' => "play-circle",
											  'className' => "calprojectes",
											  "color" => $arraycolors[$DadesProjectes[$key_p][colorsemafor]],
											  ));
			
			// Inici i final dels meus projectes.
			if (!empty($classmeu)){

				array_push($arrayCalendari, array("id"=> "p_".$value_p[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
												  "start" =>date('Y-m-d',$value_p[finalunix]), 
												  "end" =>date('Y-m-d',$value_p[finalunix]), 
												  "pagina"=> "6/projectes.html?id=".$value_p[id], 
												  "inici" =>$value_p["inici"], 
												  "final" =>$value_p["final"], 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
												  "tipus" => 'projecte', // final
												  'icon' => "flag-checkered",
												  'className' => "calmeus",
												  "color" => $arraycolors[$DadesProjectes[$key_p][colorsemafor]],
												  ));

				array_push($arrayCalendari, array("id"=> "p_".$value_p[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
												  "start" =>date('Y-m-d',$value_p[iniciunix]), 
												  "end" =>date('Y-m-d',$value_p[iniciunix]), 
												  "pagina"=> "6/projectes.html?id=".$value_p[id], 
												  "inici" =>$value_p["inici"], 
												  "final" =>$value_p["final"], 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[nom])))), 
												  "tipus" => 'projecteinici',
												  'icon' => "play-circle",
												  'className' => "calmeus",
												  "color" => $arraycolors[$DadesProjectes[$key_p][colorsemafor]],
												  ));

			}

      		$contaprojecte++;
		}
	}


	if (empty($condicioacttasc) && ($pressu1get != "" || $pressu3get != "") ) $condicioacttasc = " AND 1=2";

	//var_dump($condicioacttasc);exit();


	$Actuacions = $db->query("SELECT a.*, IFNULL(pa.text,'----') as estatactuacio, DATE_FORMAT(a.inici, '%d/%m/%Y') as inici,  DATE_FORMAT(a.final, '%d/%m/%Y') as final, UNIX_TIMESTAMP(a.inici) as iniciunix, UNIX_TIMESTAMP(a.final) as finalunix,
								p.titol_ca as nomprojecte, p.estat as estatprojecte
								  FROM pfx_actuacions a
								  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
								  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
								  WHERE 1=1 $condicioactuacions $condicioacttasc
								  GROUP By a.id ",array());
	if (!empty($Actuacions)){
		foreach ($Actuacions as $key_p => $value_p) 
		{	
			
			// Inciat en verd / Amb dificulats en vermell / No iniciat, tancat, descartat en gris.
			if ($value_p[estat] == 1) { $Actuacions[$key_p][colorsemafor] = 1; } // Verd.
			if ($value_p[estat] == 3) { $Actuacions[$key_p][colorsemafor] = 3; } // Vermell.
			if ($value_p[estat] == 2 || $value_p[estat] == 4 || $value_p[estat] == 5 || $value_p[estat] == "" ) { $Actuacions[$key_p][colorsemafor] = 4; } // Gris.
			

			$colorsemafor = ""; // Color calendari
			if ($value_p[estatprojecte] == 1) { $colorsemafor = 2; } // Iniciat, Àmbar.
			if ($value_p[estatprojecte] == 2) { $colorsemafor = 5; } // No iniciat, Blau.
			if ($value_p[estatprojecte] == 3) { $colorsemafor = 3; } // Amb dificultats, Vermell.
			if ($value_p[estatprojecte] == 4) { $colorsemafor = 1; } // Finalitzat, Verd.
			if ($value_p[estatprojecte] == 5) { $colorsemafor = 4; } // Descartat, Gris.
			if ($value_p[estatprojecte] == ""){ $colorsemafor = 4; } // Gris.

			

			array_push($arrayCalendari, array("id"=> "a_".$value_p[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[titol_ca])))), 
											  "start" =>date('Y-m-d',$value_p["finalunix"]), 
											  "end" =>date('Y-m-d',$value_p["finalunix"]), 
											  "pagina"=> "7/fases.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
											  "tipus" => 'actuacio', // final
											  'icon' => "flag-checkered",
											  'className' => "calactuacions",
											  "color" => $arraycolors[$colorsemafor],
											  ));

			array_push($arrayCalendari, array("id"=> "a_".$value_p[id] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[titol_ca])))), 
											  "start" =>date('Y-m-d',$value_p["iniciunix"]), 
											  "end" =>date('Y-m-d',$value_p["iniciunix"]), 
											  "pagina"=> "7/dases.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
											  "tipus" => 'actuacioinici',
											  'icon' => "play-circle",
											  'className' => "calactuacions",
											  "color" => $arraycolors[$colorsemafor],
											  ));

			if (in_array($value_p["clau_projecte"], $arrayprojectesmeus )){
					
				array_push($arrayCalendari, array("id"=> "a_".$value_p[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[titol_ca])))), 
												  "start" =>date('Y-m-d',$value_p["iniciunix"]), 
												  "end" =>date('Y-m-d',$value_p["iniciunix"]), 
												  "pagina"=> "7/dases.html?id=".$value_p[id], 
												  "inici" =>$value_p["inici"], 
												  "final" =>$value_p["final"], 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
												  "tipus" => 'actuacioinici',
												  'icon' => "play-circle",
												  'className' => "calmeus",
												  "color" => $arraycolors[$colorsemafor],
												  ));

				array_push($arrayCalendari, array("id"=> "a_".$value_p[id] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[titol_ca])))), 
												  "start" =>date('Y-m-d',$value_p["finalunix"]), 
												  "end" =>date('Y-m-d',$value_p["finalunix"]), 
												  "pagina"=> "7/fases.html?id=".$value_p[id], 
												  "inici" =>$value_p["inici"], 
												  "final" =>$value_p["final"], 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
												  "tipus" => 'actuacio', // final
												  'icon' => "flag-checkered",
												  'className' => "calmeus",
												  "color" => $arraycolors[$colorsemafor],
												  ));

			}

		}
	}

	$Tasques = $db->query("SELECT t.*, IFNULL(pa.text,'----') as estattasca, DATE_FORMAT(t.inici, '%d/%m/%Y') as inici,  DATE_FORMAT(t.final, '%d/%m/%Y') as final, UNIX_TIMESTAMP(t.inici) as iniciunix, UNIX_TIMESTAMP(t.final) as finalunix,
								p.titol_ca as nomprojecte, p.estat as estatprojecte
							  FROM pfx_tasques t
							  INNER JOIN pfx_actuacions a ON a.id = t.clau_actuacio
							  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
							  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
							  WHERE 1=1 $condiciotasques $condicioacttasc
							  GROUP By t.id ",array());

	if (!empty($Tasques)){
		foreach ($Tasques as $key_p => $value_p) 
		{
			// Inciat en verd / Amb dificulats en vermell / No iniciat, tancat, descartat en gris.
			if ($value_p[estat] == 1) { $Tasques[$key_p][colorsemafor] = 1; } // Verd.
			if ($value_p[estat] == 3) { $Tasques[$key_p][colorsemafor] = 3; } // Vermell.
			if ($value_p[estat] == 2 || $value_p[estat] == 4 || $value_p[estat] == 5 || $value_p[estat] == "" ) { $Tasques[$key_p][colorsemafor] = 4; } // Gris.

			$colorsemafor = ""; // Color calendari
			if ($value_p[estatprojecte] == 1) { $colorsemafor = 2; } // Iniciat, Àmbar.
			if ($value_p[estatprojecte] == 2) { $colorsemafor = 5; } // No iniciat, Blau.
			if ($value_p[estatprojecte] == 3) { $colorsemafor = 3; } // Amb dificultats, Vermell.
			if ($value_p[estatprojecte] == 4) { $colorsemafor = 1; } // Finalitzat, Verd.
			if ($value_p[estatprojecte] == 5) { $colorsemafor = 4; } // Descartat, Gris.
			if ($value_p[estatprojecte] == ""){ $colorsemafor = 4; } // Gris.

			array_push($arrayCalendari, array("id"=> "t_".$value_p["id"] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["titol_ca"])))), 
											  "start" =>date('Y-m-d',$value_p["finalunix"]), 
											  "end" =>date('Y-m-d',$value_p["finalunix"]), 
											  "pagina"=> "8/accions.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
											  "tipus" => 'tasca', // final
											  'icon' => "flag-checkered",
											  'className' => "caltasques",
											  "color" => $arraycolors[$colorsemafor],
											  ));

			array_push($arrayCalendari, array("id"=> "t_".$value_p["id"] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["titol_ca"])))), 
											  "start" =>date('Y-m-d',$value_p["iniciunix"]), 
											  "end" =>date('Y-m-d',$value_p["iniciunix"]), 
											  "pagina"=> "8/accions.html?id=".$value_p["id"], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
											  "tipus" => 'tascainici',
											  'icon' => "play-circle",
											  'className' => "caltasques",
											  "color" => $arraycolors[$colorsemafor],
											  ));

			if (in_array($value_p["clau_projecte"], $arrayprojectesmeus )){
					
				array_push($arrayCalendari, array("id"=> "t_".$value_p["id"] , 
												  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p[titol_ca])))), 
												  "start" =>date('Y-m-d',$value_p["iniciunix"]), 
												  "end" =>date('Y-m-d',$value_p["iniciunix"]), 
												  "pagina"=> "8/accions.html?id=".$value_p[id], 
												  "inici" =>$value_p["inici"], 
												  "final" =>$value_p["final"], 
												  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
												  "tipus" => 'tascainici',
												  'icon' => "play-circle",
												  'className' => "calmeus",
												  "color" => $arraycolors[$colorsemafor],
												  ));

				array_push($arrayCalendari, array("id"=> "t_".$value_p["id"] , 
											  "title"=> strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["titol_ca"])))), 
											  "start" =>date('Y-m-d',$value_p["finalunix"]), 
											  "end" =>date('Y-m-d',$value_p["finalunix"]), 
											  "pagina"=> "8/accions.html?id=".$value_p[id], 
											  "inici" =>$value_p["inici"], 
											  "final" =>$value_p["final"], 
											  'nomprojecte' => strip_tags(_convert(str_replace("&nbsp;"," ",str_replace("&#39;", "`", $value_p["nomprojecte"])))), 
											  "tipus" => 'tasca', // final
											  'icon' => "flag-checkered",
											  'className' => "calmeus",
											  "color" => $arraycolors[$colorsemafor],
											  ));

			}

		}
	}

	$array_cap = array("II"=>0, "IV"=>0, "VI"=>0);
	$array_cap_anys = array("II"=>array("2015"=>0,"2016"=>0,"2017"=>0,"2018"=>0,"2019"=>0), 
							"IV"=>array("2015"=>0,"2016"=>0,"2017"=>0,"2018"=>0,"2019"=>0),  
							"VI"=>array("2015"=>0,"2016"=>0,"2017"=>0,"2018"=>0,"2019"=>0)
						);
	$RegistreAnys = $db->query("SELECT a.pressupost1, a.pressupost2, a.pressupost3, a.any
								  FROM pfx_projectes_conceptes a
								  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
								  WHERE 1=1 $condicioacttasc $condicioconceptes
								  GROUP By a.id ",array());
	if (!empty($RegistreAnys)){
		foreach ($RegistreAnys as $key_p => $value_p) 
		{
			$array_cap["II"] += $value_p[pressupost1];
			$array_cap["IV"] += $value_p[pressupost2];
			$array_cap["VI"] += $value_p[pressupost3];

			$array_cap_anys["II"][$value_p[any]] += $value_p[pressupost1];
			$array_cap_anys["IV"][$value_p[any]] += $value_p[pressupost2];
			$array_cap_anys["VI"][$value_p[any]] += $value_p[pressupost3];
		}
	}



	$Organse = $dbb->Llistats("projectes_organs_e"," ", array(), "titol_ca");


	$UsuarisFiltre =  $dbb->FreeSql("SELECT u.nom, u.cognoms, u.id
									    FROM pfx_usuaris u
									    ORDER BY cognoms ",
									  array());

	$UsuarisFiltre2 = array();
	foreach ($UsuarisFiltre as $key_u => $value_u) {
		if (in_array($value_u['id'], $arrayequip)){
			array_push($UsuarisFiltre2, $UsuarisFiltre[$key_u]);
		}
	}

