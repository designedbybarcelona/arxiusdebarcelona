<?php 

	$idpagina = 24;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Procés de verificació que totes les linies tinguin ACTUACIÓ 0.
	/*

	$RelacionsEixos = $dbb->Llistats("projectes_vinculacio"," AND nivell = 2 " ,array(""), "id", false );
	if (!empty($RelacionsEixos)){
		foreach ($RelacionsEixos as $key => $value) {
			if (!empty($value[parents_ids])){
				$parents_ids = json_decode($value[parents_ids]);
				foreach ($parents_ids as $key_p => $value_p) 
				{
					// linies de l'eix.
					$RelacionsLinia = $dbb->Llistats("projectes_vinculacio"," AND nivell = 3 AND parent_id = :id" ,array("id"=>$value[id]), "id", false );
					if (!empty($RelacionsLinia)){
						foreach ($RelacionsLinia as $key_l => $value_l) {


							// Existeix la 0?
							$Busca0 = $dbb->Llistats("projectes_vinculacio"," AND nivell = 4 AND clau_cero = 1 AND clau_pam = :clau_pam AND clau_eix =:clau_eix AND parent_id = :id" ,
													   array("id"=>$value_l[id], "clau_pam" =>$value_p, "clau_eix" => $value[id] ), "id", false );
							if (empty($Busca0))
							{
								$dades = array();
								$dades[nivell] = 4;
								$dades[clau_eix] = $value[id];
								$dades[parent_id] = $value_l[id];
								$dades[titol_ca] = "0.Sense vinculació directa.";
								$dades[clau_cero] = 1;
								$dades[clau_pam] = $value_p;
								$dades[estat] = 1;
								$dades[clau_pla] = 1;

								$arrayguardar = array('taula' =>  'projectes_vinculacio', 
													  'dades' =>  $dades,
													  'tipus' =>  a,);
								$dbb->GuardarRegistre2($arrayguardar); 
							}
							
						}
					}
				}
			}
		}
	}
	*/

	
	// JS.

	$js = '

			$(document).off("click",".nou").on("click",".nou",function(event){

                if ($("#idv").val() != "")
                {   
                    $("#idv").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmprojectesvinc").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmprojectesvinc").removeAttr("checked").removeAttr("selected");
                    $("div.errorprojectesvinc span").html("");
                    $("#resultprojectesvinc").html("");
                    $(".panelldadesprojectesvinc").toggle();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultprojectesvinc").html("");
                	$(".vinculacio2").html("");
                	$(".vinculaciopam").html("");
                	$(".vinculacioeix").html("");
                }else{
                    $(".panelldadesprojectesvinc").toggle();
                }

            });


			$(".llistat_10").load("'.$url.'/load",{o:2,id:1,t:"10"}, function(){
                    
            });


	';
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

