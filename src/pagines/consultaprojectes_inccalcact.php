<?php
	
	// En el llistat de projectes i actuacions i en el gannt, ha de mostrar el % global independentment del filtre.
	// Tornem a calcular el % per les actuacions.
	$queryVinculacio4perllistat = "

		SELECT pv.titol_ca as titol, GROUP_CONCAT( DISTINCT IFNULL(p.id,'') SEPARATOR ',') as idsproj, pv.id as id,
		GROUP_CONCAT( DISTINCT IFNULL(p.inici,'') SEPARATOR ',') as datesinici,
		GROUP_CONCAT( DISTINCT IFNULL(p.final,'') SEPARATOR ',') as datesfinal,
		GROUP_CONCAT( IFNULL(p.tantpercent,'') SEPARATOR ';') as tantspercents, 
		GROUP_CONCAT( IFNULL(p.pesrelatiu,'') SEPARATOR ';') as pesosrelatius

		  FROM pfx_projectes_vinculacio pv
		  LEFT JOIN pfx_projectes_relacionsvinc rp ON rp.id_act = pv.id
		  LEFT JOIN pfx_projectes p ON rp.id_proj = p.id 
		  WHERE nivell = 4 
		  GROUP By pv.id
		  ORDER BY 1*SUBSTRING_INDEX(pv.titol_ca, '.', 1) ASC, 1*SUBSTRING_INDEX(pv.titol_ca, '.', -1) ASC

	";

	$Vinculacio4perllistat = $db->query("$queryVinculacio4perllistat",$arraycondiciollistat);

	if (!empty($Vinculacio4perllistat)){
		foreach ($Vinculacio4perllistat as $key => $value) {

			if (!empty($value[idsproj]))
			{
				if (!empty($value[datesinici])){
					$datesinici = explode(",", $value[datesinici]);
					$datesinici = array_filter($datesinici, function($value){return !empty($value) && $value != "0000-00-00";});
					sort($datesinici);
					$Vinculacio4dates[$value[id]][datesinici] = $datesinici;
					$Vinculacio4dates[$value[id]][iniciunix] = strtotime($datesinici[0]);
				}
				if (!empty($value[datesfinal])){
					$datesfinal = explode(",", $value[datesfinal]);
					$datesfinal = array_filter($datesfinal, function($value){return !empty($value) && $value != "0000-00-00";});
					rsort($datesfinal);
					$Vinculacio4dates[$value[id]][datesfinal]= $datesfinal;
				}
				$mitjaexec = 0;
				if (($value[tantspercents])!=""){ // no posar !empty o es salta els % = 0i dona resultats erronis.
					$tantspercents = explode(";", $value[tantspercents]);
					$pesosrelatius = explode(";", $value[pesosrelatius]);

					// Verificar els pesos relatius, en cas de tenir pes 0.00, es reparteix el % restant en aquests que tenen 0.
					$totalpesrelaiu = 0;
					$totalpesosindicats = 0;
					$contapesos = count($pesosrelatius);
					foreach ($pesosrelatius as $key_pes => $value_pes) {
						if ($value_pes != "0.00" && !empty($value_pes)){
							$totalpesrelaiu += $value_pes;
							$totalpesosindicats++;
						}
					}
					if ($totalpesrelaiu != "100" && $contapesos>$totalpesosindicats){
						$calcpes = 100-$totalpesrelaiu;
						$pescalculat = $calcpes/($contapesos-$totalpesosindicats);
					}

					$contatants = count($tantspercents);
					$sumatants = 0;
					foreach ($tantspercents as $key_t => $value_t) {
						if ($pesosrelatius[$key_t]!="0.00" && !empty($pesosrelatius[$key_t])){
							$sumatants += ($value_t)*($pesosrelatius[$key_t]/100);
						}else{
							if (!empty($value_t)){
								$sumatants += $value_t*($pescalculat/100);
							}
							
						}
						
					}
					
					$mitjaexec = $sumatants;
				

					$Vinculacio4tantspercents[$value[id]][tantspercents]= number_format($mitjaexec,2);

				}

				
			}


		}

	}