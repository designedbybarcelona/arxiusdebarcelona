<?php
	
	$permisos = $app['session']->get(constant('General::nomsesiouser')."-permisos");

	$Dades = '';

	//  RESUM

		$Dades = '
	    <div class="row">

	        <div class="col-lg-3 col-md-3">        
	                            
	            <div class="panel panel-primary">

	                <div class="panel-heading">Projectes no iniciats i amb retard</div>
	                <div class="panel-body">
	                    <div class="form-group">

	                        <div class="row tile_count text-center">
	                            <div class="col-lg-12 mostrarllistat" data-llistat="llistat1">  
	                                <div class="count" style="font-size:40px;">'.count($arrayresum[1]).' <span class="signe"><i class="fa fa-angle-down"></i></span></div>
	                            </div>
	                        </div>

	                    </div>
	                </div>

	            </div>

	        </div>

	        <div class="col-lg-3 col-md-3">        
	                            
	            <div class="panel panel-danger">

	                <div class="panel-heading">Proj. vius amb retard o dificultats</div>
	                <div class="panel-body">
	                    <div class="form-group">

	                        <div class="row tile_count text-center">
	                            <div class="col-lg-12 mostrarllistat" data-llistat="llistat2">  
	                                <div class="count" style="font-size:40px;">'.count($arrayresum[2]).' <span class="signe"><i class="fa fa-angle-down"></i></span></div>
	                            </div>
	                        </div>

	                    </div>
	                </div>

	            </div>

	        </div>

	        <div class="col-lg-3 col-md-3">        
	                            
	            <div class="panel panel-warning">

	                <div class="panel-heading">Projectes vius en termini</div>
	                <div class="panel-body">
	                    <div class="form-group">

	                        <div class="row tile_count text-center">
	                            <div class="col-lg-12 mostrarllistat" data-llistat="llistat3">  
	                                <div class="count" style="font-size:40px;">'.count($arrayresum[3]).' <span class="signe"><i class="fa fa-angle-down"></i></span></div>
	                            </div>
	                        </div>


	                    </div>
	                </div>

	            </div>

	        </div>

	        <div class="col-lg-3 col-md-3">        
	                            
	            <div class="panel panel-success">

	                <div class="panel-heading">Projectes finalitzats</div>
	                <div class="panel-body">
	                    <div class="form-group">

	                        <div class="row tile_count text-center">
	                            <div class="col-lg-12 mostrarllistat" data-llistat="llistat4">  
	                                <div class="count" style="font-size:40px;">'.count($arrayresum[4]).' <span class="signe"><i class="fa fa-angle-down"></i></span></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>

	            </div>

	        </div>

	    </div>
	    ';

	    if (!empty($arrayresum[1])){

	    	$Dades .= '
	    		
	    		<div class="row llistat1 llistats" style="margin:1px; display: none;">

		            <table class="table table-hover dataTablesresum">
		                <thead class="backgroundblack">
		                    <tr>
		                    	<th>Objectiu PDA</th>
		                        <th>Centre o servei</th>
                                <th>Nom de projecte</th>
                                <th>Cap de projecte</th>
                                <th>Inici</th>
                                <th>Final</th>
                                <th>Estat</th>
                                <th>% execució</th>
                                <th>Actualitzat</th>
                                <th></th>
		                    </tr>
		                </thead>
		                <tbody>';

		                	foreach ($arrayresum[1] as $key_r => $value_r) {
		                		$codnomvinc = explode(".", $value_r[nomvinc]);
            					$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
		                		$Dades .= '
	                			 	<tr>
	                			 		<td>'.$nomvinc.'</td>
	                			 		<td>'.$value_r['acronim'].'</td>
	                                    <td width="40%">'.$value_r['nom'].'</td>
	                                    <td>'.$value_r['nomresp'].' '.$value_r['cognomsresp'].'</td>
	                                    <td><span style="display:none">'.$value_r[iniciunix].'</span>'.$value_r['inici'].'</td>
	                                    <td><span style="display:none">'.$value_r[finalunix].'</span>'.$value_r['final'].'</td>
	                                    <td>'.$value_r['estatprojecte'].'</td>
	                                    <td>'.$value_r['tantpercent'].'</td>
	                                    <td>'.$value_r['updated'].'</td>
			                            <td width="7%"><a href="../6/projectes.html?id='.$value_r['id'].'"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
			                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_r['id'].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			                        </tr>
		                		';
		                	}
		                   
		                $Dades .= '
		                </tbody>
		            </table>

		        </div>

	    	';
	    }

	    if (!empty($arrayresum[2])){

	    	$Dades .= '

	    		<div class="row llistat2 llistats" style="margin:1px; display: none;">

		            <table class="table table-hover dataTablesresum">
		                <thead class="backgroundblack">
		                    <tr>
		                    	<th>Objectiu PDA</th>
		                        <th>Centre o servei</th>
                                <th>Nom de projecte</th>
                                <th>Cap de projecte</th>
                                <th>Inici</th>
                                <th>Final</th>
                                <th>Estat</th>
                                <th>% execució</th>
                                <th>Actualitzat</th>
                                <th></th>
		                    </tr>
		                </thead>
		                <tbody>';

		                	foreach ($arrayresum[2] as $key_r => $value_r) {
		                		$codnomvinc = explode(".", $value_r[nomvinc]);
            					$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
		                		$Dades .= '
	                			 	<tr>
	                			 		<td>'.$nomvinc.'</td>
			                            <td>'.$value_r['acronim'].'</td>
	                                    <td width="40%">'.$value_r['nom'].'</td>
	                                    <td>'.$value_r['nomresp'].' '.$value_r['cognomsresp'].'</td>
	                                    <td><span style="display:none">'.$value_r[iniciunix].'</span>'.$value_r['inici'].'</td>
	                                    <td><span style="display:none">'.$value_r[finalunix].'</span>'.$value_r['final'].'</td>
	                                    <td>'.$value_r['estatprojecte'].'</td>
	                                    <td>'.$value_r['tantpercent'].'</td>
	                                    <td>'.$value_r['updated'].'</td>
			                            <td width="7%"><a href="../6/projectes.html?id='.$value_r['id'].'"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
			                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_r['id'].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			                        </tr>
		                		';
		                	}
		                   
		                $Dades .= '
		                </tbody>
		            </table>

		        </div>

	    	';
	    }

     	if (!empty($arrayresum[3])){

	    	$Dades .= '

	    		<div class="row llistat3 llistats" style="margin:1px; display: none;">

		            <table class="table table-hover dataTablesresum">
		                <thead class="backgroundblack">
		                    <tr>
		                    	<th>Objectiu PDA</th>
		                        <th>Centre o servei</th>
                                <th>Nom de projecte</th>
                                <th>Cap de projecte</th>
                                <th>Inici</th>
                                <th>Final</th>
                                <th>Estat</th>
                                <th>% execució</th>
                                <th>Actualitzat</th>
                                <th></th>
		                    </tr>
		                </thead>
		                <tbody>';

		                	foreach ($arrayresum[3] as $key_r => $value_r) {
		                		$codnomvinc = explode(".", $value_r[nomvinc]);
            					$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
		                		$Dades .= '
	                			 	<tr>
	                			 		<td>'.$nomvinc.'</td>
			                            <td>'.$value_r['acronim'].'</td>
	                                    <td width="40%">'.$value_r['nom'].'</td>
	                                    <td>'.$value_r['nomresp'].' '.$value_r['cognomsresp'].'</td>
	                                    <td><span style="display:none">'.$value_r[iniciunix].'</span>'.$value_r['inici'].'</td>
	                                    <td><span style="display:none">'.$value_r[finalunix].'</span>'.$value_r['final'].'</td>
	                                    <td>'.$value_r['estatprojecte'].'</td>
	                                    <td>'.$value_r['tantpercent'].'</td>
	                                    <td>'.$value_r['updated'].'</td>
			                            <td width="7%"><a href="../6/projectes.html?id='.$value_r['id'].'"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
			                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_r['id'].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			                        </tr>
		                		';
		                	}
		                   
		                $Dades .= '
		                </tbody>
		            </table>

		        </div>

	    	';
	    }

     	if (!empty($arrayresum[4])){

	    	$Dades .= '

	    		<div class="row llistat4 llistats" style="margin:1px; display: none;">

		            <table class="table table-hover dataTablesresum">
		                <thead class="backgroundblack">
		                    <tr>
		                    	<th>Objectiu PDA</th>
		                        <th>Centre o servei</th>
                                <th>Nom de projecte</th>
                                <th>Cap de projecte</th>
                                <th>Inici</th>
                                <th>Final</th>
                                <th>Estat</th>
                                <th>% execució</th>
                                <th>Actualitzat</th>
                                <th></th>
		                    </tr>
		                </thead>
		                <tbody>';

		                	foreach ($arrayresum[4] as $key_r => $value_r) {
		                		$codnomvinc = explode(".", $value_r[nomvinc]);
            					$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
		                		$Dades .= '
	                			 	<tr>
	                			 		<td>'.$nomvinc.'</td>
			                            <td>'.$value_r['acronim'].'</td>
	                                    <td width="40%">'.$value_r['nom'].'</td>
	                                    <td>'.$value_r['nomresp'].' '.$value_r['cognomsresp'].'</td>
	                                    <td><span style="display:none">'.$value_r[iniciunix].'</span>'.$value_r['inici'].'</td>
	                                    <td><span style="display:none">'.$value_r[finalunix].'</span>'.$value_r['final'].'</td>
	                                    <td>'.$value_r['estatprojecte'].'</td>
	                                    <td>'.$value_r['tantpercent'].'</td>
	                                    <td>'.$value_r['updated'].'</td>
			                            <td width="7%"><a href="../6/projectes.html?id='.$value_r['id'].'"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
			                            <button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_r['id'].'" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button></td>
			                        </tr>
		                		';
		                	}
		                   
		                $Dades .= '
		                </tbody>
		            </table>

		        </div>

	    	';
	    }

        
    // Llistat.

	if (!empty($DadesProjectes_llistat))
	{
		 
		$Dades .= '

			  <div class="row">
    
			    <div class="col-lg-12">
			    	<p>
			        <span class="export" data-t="5" style="cursor:pointer;"><img src="../images/icon-pdf.png" width="35"/></span> <span class="export" data-t="3" style="cursor:pointer;"> <img src="../images/icon-xlsx.png" width="35" /> </span> <span class="resultexport"></span> '.$botodesc.'
			        </p>
			    </div>

			  </div>


		';
    	
        $captaula = false;
        foreach ($DadesProjectes_llistat as $key_p => $value_p) 
		{

			if ($captaula == false){
		    	$captaula = true;

		    	$Dades .= '
		            <table class="table table-hover taulaprojectes">
		                <thead class="backgroundred">
		                    <tr>
		                        <th>#</th>
		                        <th>Objectiu PDA</th>
	                            <th>Centre o servei</th>
	                            <th>Ind.</th>
	                            <th width="50%">Nom de projecte</th>
	                            <th>Cap de projecte</th>
	                            <th width="10%" style="text-align:center;">Inici</th>
	                            <th width="10%" style="text-align:center;">Final</th>
	                            <th width="7%">Estat</th>
	                            <th width="5% style="text-align:center;"">% exec.</th>
	                            <th width="5%" style="text-align:center;">Actualizat</th>
	                            '.($permisos == 2?'<th width="5%" style="text-align:center;">Accions</th>':'').'
	                            <th width="10%"> Detalls </th>
		                    </tr>
		                </thead>
		                <tbody>
		            ';
		    }

	            $indicadorsok = 0;
                $indicadorsko = 0;
                $contaidicadors = 0;
            
            	$IndicadorsProj = $db->query("SELECT assolit
											  FROM pfx_projectes_indicadors_arxius
											  WHERE clau_projecte = :id
											  ",array("id"=>$value_p[id]));
                foreach ($IndicadorsProj as $key_i => $value_i) {
                	if ($value_i['assolit'] == 0) $indicadorsko++;
                	if ($value_i['assolit'] == 1) $indicadorsok++;
                	$contaidicadors++;
				}

				$simbolindicador = "";
				// UP: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que SI han estat assolits tots ells.
				// DOWN: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit 1 o més indicadors, i s’hagi dit a la fitxa que NO han estats assolits cap d’ells.
				// WORK: En cas que el projecte estigui en estat “Iniciat”, “Amb dificultats” o “Finalitzat”, tingui definit + d’1 indicador, i s’hagi dit a la fitxa que algun SI ha estat assolit, i algun altre NO.
				if ($value_p['estat'] == 1 || $value_p['estat'] == 3 || $value_p['estat'] == 4 ){
					if ($indicadorsok > 0 && $indicadorsko == 0) $simbolindicador = '<i class="fa fa-thumbs-up"></i>';
					if ($indicadorsok == 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-thumbs-down"></i>';
					if ($indicadorsok > 0 && $indicadorsko > 0) $simbolindicador = '<i class="fa fa-exclamation-triangle"></i>';
				}
            	// '.($value_p[idsactuacions]!=""?'class="mostarprojecte" data-id="'.$value_p[id].'" style="cursor:pointer;"':"").' 
            	$codnomvinc = explode(".", $value_p[nomvinc]);
            	$nomvinc = $codnomvinc[0].".".$codnomvinc[1].".".$codnomvinc[2];
              	$Dades .= '
                  <tr class="mostrarelacionat details-control"  data-child-id="'.$value_p[id].'" >
                    <td>'.($value_p[idsactuacions]!=""?'<i class="fa fa-plus"></i>':"").'</td>
                    <td>'.$nomvinc.'</td>
                    <td>'.$value_p[acronim].'</td>
                    <td>'.$simbolindicador.'</td>
                    <td>'.$value_p[nom].'</td>
                    <td>'.$value_p[nomresp].' '.$value_p[cognomsresp].'</td>
                    <td style="text-align:center;"><span style="display:none">'.$value_p[iniciunix].'</span><b>'.($value_p[inici]!= "00/00/0000"?$value_p[inici]:"").'</b></td>
                    <td style="text-align:center;"><span style="display:none">'.$value_p[finalunix].'</span><b>';

                    	if ($value_p["final"] != "00/00/0000") {
                    		if ($value_p[colorsemafor]){
                    			if ($value_p[colorsemafor]== "1") 
                    				$colorsemafor="#7bd148"; //verd
                    			elseif ($value_p[colorsemafor]== "2") 
                    				$colorsemafor="#ffb878"; //ambar
                    			elseif ($value_p[colorsemafor]== "3") 
                    				$colorsemafor="#ff887c"; //vermell
                    			elseif ($value_p[colorsemafor]== "4") 
                    				$colorsemafor="#e1e1e1"; //gris
                    			elseif ($value_p[colorsemafor]== "5") 
                    				$colorsemafor="#5681de"; //blau
                    		}
                    		$Dades .= $value_p["final"].' '.$value_p["simbol"].'<span class="badge" style="background-color:'.$colorsemafor.';">&nbsp;&nbsp;</span>';
                    	}

                    $Dades .= '
                            </b></td>
                    <td>'.$value_p[estatprojecte].'</td>
                    <td style="text-align:center;">'.$value_p[tantpercent].'</td>
                    
                    <td style="text-align:center;"><span style="display:none;">'.$value_p['updatedunix'].'</span>'.$value_p[updated].'</td>';
                    if ($permisos == 2){
                    	if ($value_p[estrategic] == 1){
                    		$Dades .= '
								<td>
                                    <a href="../7/subprojectes.html?n&idp='.$value_p[id].'">
                                    <button type="button" class="btn btn-success btn-xs edicio" style="margin-bottom: 2px;">+ Nova fase</button>
                                    </a>
                                    <a href="../8/accions.html?n&idp='.$value_p[id].'">
                                    <button type="button" class="btn btn-success btn-xs edicio">+ Nova accio</button>
                                    </a>
                          		</td>
                        	';
                    	}else{
                    		$Dades .= '
							<td>
                                <button type="button" class="btn btn-success btn-xs" style="margin-bottom: 2px;" onClick="alert(\'Per crear fases, cal marcar el projecte com estratègic\')">+ Nova fase</button>
                                <button type="button" class="btn btn-success btn-xs" onClick="alert(\'Per crear accions, cal marcar el projecte com estratègic\')">+ Nova accio</button>
                                
                      		</td>
                    	';
                    	}
                    	
                    	
                    }
                   	$Dades .= '
                    <td>';
                    if ($permisos != 3){
                    	$Dades .= '
                            <a href="../6/projectes.html?id='.$value_p[id].'">
                            <button type="button" class="btn btn-primary btn-xs edicio" style="padding: 1px 5px;"><i class="fa fa-edit"></i></button>
                            </a>
                            <button type="button" class="btn btn-danger btn-xs eliminarp" data-id="'.$value_p[id].'" data-idx="'.md5(constant('General::tockenid').$value_p[id]).'" data-t="1" style="padding: 1px 5px;"><i class="fa fa-trash"></i></button>
                       
                    	';
                    }
                    $Dades .= '

                    	<button type="button" class="btn btn-primary btn-xs mostrarconsulta" data-id="'.$value_p["id"].'" data-t="1"  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalconsulta" style="padding: 1px 5px;"><i class="fa fa-search" aria-hidden="true"></i></button>
                    	<span class="export_ind" data-id="'.$value_p["id"].'" style="cursor:pointer;"><img src="../images/icon-pdf.png" width="25"/></span>
                    </td>
                  </tr>
                  
                ';


		}

		if ($captaula == true){
      		$Dades .= '
	          </tbody>
	        </table> 
	        ';
        }

        $Dades .= '
        <script type="text/javascript">
          $(document).ready(function($) {

          		/*
					 * Natural Sort algorithm for Javascript - Version 0.7 - Released under MIT license
					 * Author: Jim Palmer (based on chunking idea from Dave Koelle)
					 * Contributors: Mike Grier (mgrier.com), Clint Priest, Kyle Adams, guillermo
					 * See: http://js-naturalsort.googlecode.com/svn/trunk/naturalSort.js
					 */
					function naturalSort (a, b, html) {
					    var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?%?$|^0x[0-9a-f]+$|[0-9]+)/gi,
					        sre = /(^[ ]*|[ ]*$)/g,
					        dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
					        hre = /^0x[0-9a-f]+$/i,
					        ore = /^0/,
					        htmre = /(<([^>]+)>)/ig,
					        // convert all to strings and trim()
					        x = a.toString().replace(sre, \'\') || \'\',
					        y = b.toString().replace(sre, \'\') || \'\';
					        // remove html from strings if desired
					        if (!html) {
					            x = x.replace(htmre, \'\');
					            y = y.replace(htmre, \'\');
					        }
					        // chunk/tokenize
					    var xN = x.replace(re, \'\0$1\0\').replace(/\0$/,\'\').replace(/^\0/,\'\').split(\'\0\'),
					        yN = y.replace(re, \'\0$1\0\').replace(/\0$/,\'\').replace(/^\0/,\'\').split(\'\0\'),
					        // numeric, hex or date detection
					        xD = parseInt(x.match(hre), 10) || (xN.length !== 1 && x.match(dre) && Date.parse(x)),
					        yD = parseInt(y.match(hre), 10) || xD && y.match(dre) && Date.parse(y) || null;
					 
					    // first try and sort Hex codes or Dates
					    if (yD) {
					        if ( xD < yD ) {
					            return -1;
					        }
					        else if ( xD > yD ) {
					            return 1;
					        }
					    }
					 
					    // natural sorting through split numeric strings and default strings
					    for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
					        // find floats not starting with \'0\', string or 0 if not defined (Clint Priest)
					        var oFxNcL = !(xN[cLoc] || \'\').match(ore) && parseFloat(xN[cLoc], 10) || xN[cLoc] || 0;
					        var oFyNcL = !(yN[cLoc] || \'\').match(ore) && parseFloat(yN[cLoc], 10) || yN[cLoc] || 0;
					        // handle numeric vs string comparison - number < string - (Kyle Adams)
					        if (isNaN(oFxNcL) !== isNaN(oFyNcL)) {
					            return (isNaN(oFxNcL)) ? 1 : -1;
					        }
					        // rely on string comparison if different types - i.e. \'02\' < 2 != \'02\' < \'2\'
					        else if (typeof oFxNcL !== typeof oFyNcL) {
					            oFxNcL += \'\';
					            oFyNcL += \'\';
					        }
					        if (oFxNcL < oFyNcL) {
					            return -1;
					        }
					        if (oFxNcL > oFyNcL) {
					            return 1;
					        }
					    }
					    return 0;
					}
					 
					jQuery.extend( jQuery.fn.dataTableExt.oSort, {
					    "natural-asc": function ( a, b ) {
					        return naturalSort(a,b,true);
					    },
					 
					    "natural-desc": function ( a, b ) {
					        return naturalSort(a,b,true) * -1;
					    },
					 
					    "natural-nohtml-asc": function( a, b ) {
					        return naturalSort(a,b,false);
					    },
					 
					    "natural-nohtml-desc": function( a, b ) {
					        return naturalSort(a,b,false) * -1;
					    },
					 
					    "natural-ci-asc": function( a, b ) {
					        a = a.toString().toLowerCase();
					        b = b.toString().toLowerCase();
					 
					        return naturalSort(a,b,true);
					    },
					 
					    "natural-ci-desc": function( a, b ) {
					        a = a.toString().toLowerCase();
					        b = b.toString().toLowerCase();
					 
					        return naturalSort(a,b,true) * -1;
					    }
					} );


          		function format(id){

          			return "<div id=\"rowv_"+id+"\" width=\"100%\"><img src=\'../images/loading.gif\'/></div>";

          		}
      			
      			 var table = $(".dataTablesresum").DataTable({
	                "language": {
	                    "url": "../js/plugins/dataTables/dataTables.catala.lang"
	                },
	                "columnDefs": [
	                { "type": "natural", targets: 0 }
	            ],
	              });
              
              var table = $(".taulaprojectes").DataTable({
                "language": {
                    "url": "../js/plugins/dataTables/dataTables.catala.lang"
                },
            	"columnDefs": [
	                { "type": "natural", targets: 1 }
	            ],
              });

              // Add event listener for opening and closing details
              $(".taulaprojectes").on("click", "tr.details-control", function () {
                  var tr = $(this).closest("tr");
                  var row = table.row(tr);

                  if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass("shown");
                  } else {
                      // Open this row
                      row.child(format(tr.data("child-id"))).show();
                      tr.addClass("shown");
                      var idtr= tr.data("child-id");
                      $.ajax({
	                    type: "POST",
	                    url: "../load",
	                    data: "o=45&id="+tr.data("child-id"), // +"&f='.$_GET['f'].'"
	                    beforeSend:function(){
	                        
	                    },
	                    error: function(jqXHR, textStatus, errorThrown){
	                        // Error.
	                    },
	                    success: function(data){
	                    	 $("#rowv_"+idtr).html(data);
	                    }
	                });

                  }
              });

              $(document).off("click",".mostartasques").on("click",".mostartasques",function(event){

                    var id = $(this).data( "id" );
                    
                    $(".tasc_"+id).toggle("fast");

                    $(this).find(".fa-plus").toggleClass("fa-minus");
                    

              });


          });
        </script>';

	}


	$LlistatProjectes = $Dades;

