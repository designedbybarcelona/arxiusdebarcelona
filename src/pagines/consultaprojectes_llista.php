<?php
	
	$Dades .= '
		
		<a name="listaproj"></a>	
		
        <div class="row">
			<div class="col-md-12">
	            <h3 style="color:#cc032f;">
	                Objectius i projectes <span class="export" data-t="1" style="cursor:pointer;"><img src="../images/icon-pdf.png" width="50"/></span> <span class="export" data-t="2" style="cursor:pointer;"> <img src="../images/icon-xlsx.png" width="50" /> </span> <span class="resultexport"></span> '.$botodesc.'
	            </h3>
	            <hr style="border-top: 1px solid #cc032f;">
	        </div>
        </div>

		<div class="row">
			<div class="col-md-12">

	';
						// càlculs on no afecta el filtre per mostrar dades a les actuacions.
						include("consultaprojectes_inccalcact.php");


						if (!empty($Vinculacio4))
						{
							 
				        	// Vinculats a PAM
					        /*
					        $Dades .= '

					        <div class="row">
					            <div class="col-lg-12">
					             <h4><i class="fa fa-tasks" style="color:#cd0d2d;"></i> Els <span><b><a href="../6/projectes.html" style="color:#cd0d2d;">PROJECTES</a></b></span> vinculats als PAM que gestiones:  </h4>
					            </div>

					        </div>

					        ';*/

					        $captaula = false;
					        foreach ($Vinculacio4 as $key_v => $value_v) 
							{	 
								if ($value_v[clau_cero] == $condicioclau0  &&(($nomespam == 1 && $value_v[clau_pam] == 1) || $nomespam == 0) ) // && $value_v[clau_pam] == 9 // && in_array($value_v[id], $actuacionsamostrar) && !empty($value_v[idsproj])
								{
									$textdatesv = "<span style='display:none;'>".$Vinculacio4dates[$value_v[id]][iniciunix]."</span><b>";
									if ($Vinculacio4dates[$value_v[id]][datesinici][0]!= "0000-00-00") $textdatesv .= date_format(date_create($Vinculacio4dates[$value_v[id]][datesinici][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_v[id]][datesfinal][0]!= "0000-00-00") $textdatesv .= " - ".date_format(date_create($Vinculacio4dates[$value_v[id]][datesfinal][0]),"d/m/Y");
									if ($Vinculacio4dates[$value_v[id]][datesinici][0] == "0000-00-00" && $Vinculacio4dates[$value_v[id]][datesfinal][0]== "0000-00-00") $textdatesv .= "------";
									$textdatesv .= "</b>";//" <b>".$value_v[tantspercents]." %</b> d'execució.";
									
									/*
									$Dades .= '
							        <div class="row"><br>
							          <div class="col-lg-12">
							              <h4 class="mostrarelacionat" data-id="'.$value_v[id].'" style="cursor:pointer; font-size: 16px !important;"><i class="fa fa-plus"></i> '.$value_v[titol].' '.$textdatesv.'</b></h4>
							          </div>
							        </div>';
							        */

							        if ($captaula == false){
							        	$captaula = true;

							        	$Dades .= '
								            <table class="table table-hover dataTables2">
								                <thead class="backgroundred">
								                    <tr>
								                        <th width="2%">#</th>
								                        <th width="50%">Objectius</th>
								                        <th width="20%">Calendari</th>
								                        <th width="10%">% d\'execució</th>
								                    </tr>
								                </thead>
								                <tbody>
								            ';
							        }

							        // str_replace(".", "", $value_v[codipam]).'.'

							        $Dades .= '

					                <tr class="mostrarelacionat details-control"  data-child-id="'.$value_v[id].'" style="cursor:pointer;font-size: 16px !important;">
					                    <td><i class="fa fa-plus"></i></td>
					                    <td> '.($value_v[estatvinculacio]==0?'<span style="color:#ff0000;"><b>CANCEL·LAT</b></span> ':"").''.$value_v[titol].'</td>
					                    <td> '.$textdatesv.'</td>
					                    <td> <b>'.$Vinculacio4tantspercents[$value_v[id]][tantspercents].''.($Vinculacio4tantspercents[$value_v[id]][tantspercents]==""?"0.00":"").' %</b></td>
					                </tr>

					               '; // '.$value_v[tantspercents].''.($value_v[tantspercents]==""?"0.00":"").' %
											
								}
					      	}

					      	if ($captaula == true){
					      		$Dades .= '
						          </tbody>
						        </table> 
						        ';
					        }

					      	
				      		// Vinculats a PAD
					      	/*
					      	$Dades .= '

					      	<hr>

					        <div class="row">
					            <div class="col-lg-12">
					             <h4><i class="fa fa-tasks" style="color:#cd0d2d;"></i> Els <span><b><a href="../6/projectes.html" style="color:#cd0d2d;">PROJECTES</a></b></span> vinculats als PAD que gestiones:  </h4>
					            </div>

					        </div>

					        ';
					        

					        foreach ($Vinculacio4 as $key_v => $value_v) 
							{
								if ($value_v[clau_cero] == 0 && $value_v[clau_pam] != 9)
								{

									$textdatesv = "(<b>";
									if ($value_v[datesinici][0]!= "0000-00-00") $textdatesv .= date_format(date_create($value_v[datesinici][0]),"d/m/Y");
									if ($value_v[datesfinal][0]!= "0000-00-00") $textdatesv .= " - ".date_format(date_create($value_v[datesfinal][0]),"d/m/Y");
									$textdatesv .= "</b>)";// " <b>".$value_v[tantspercents]." %</b> d'execució.";
									
									
									$Dades .= '
							        <div class="row"><br>
							          <div class="col-lg-12">
							              <h4 class="mostrarelacionat" data-id="'.$value_v[id].'" style="cursor:pointer; font-size: 16px !important;"><i class="fa fa-plus"></i> '.$value_v[titol].' '.$textdatesv.'</h4>
							          </div>
							        </div>
							        ';
							        

							        if ($captaula == false){
							        	$captaula = true;

							        	$Dades .= '
								            <table class="table table-hover ">
								                <thead class="backgroundred">
								                    <tr>
								                        <th width="2%">#</th>
								                        <th width="50%">Actuacions</th>
								                        <th width="20%">Calendari</th>
								                        <th width="10%">% d\'execució</th>
								                    </tr>
								                </thead>
								                <tbody>
								            ';
							        }

							        $Dades .= '

					                <tr class="mostrarelacionat" data-id="'.$value_v[id].'" style="cursor:pointer;font-size: 16px !important;">
					                    <td><i class="fa fa-plus"></i></td>
					                    <td> '.$value_v[titol].'</td>
					                    <td> '.$textdatesv.'</td>
					                    <td> <b>'.$value_v[tantspercents].' %</b></td>
					                </tr>

					                <tr style="display:none;" class="act_'.$value_v[id].'">
					                    <td colspan="7">
					                      <div class="row">
					                          <div class="col-lg-12">

										     
										          <table class="table table-striped table-hover ">
										            <thead>
										              <tr>
										                <th>#</th>
										                <th width="55%">Nom projecte</th>
					                                    <th width="7%">Estat</th>
					                                    <th width="8% style="text-align:center;"">% execució</th>
					                                    <th width="10%" style="text-align:center;">Inici</th>
					                                    <th width="15%" style="text-align:center;">Finalització</th>
					                                    <th width="5%" style="text-align:center;">Fites</th>
					                                    <th width="10%">Detalls</th>
										              </tr>
										            </thead>
										            <tbody>
										        ';
									              	$idsproj = array();
										              if (!empty($value_v[idsproj])){
										              	$idsproj = explode(",", $value_v[idsproj]);
									              	}

									              	foreach ($DadesProjectes as $key_p => $value_p) 
													{
														if (in_array($value_p[id], $idsproj) && $value_v[clau_pam] != 9)
														{

															$Dades .= '
												                  <tr class="mostarprojecte" data-id="'.$value_p[id].'" style="cursor:pointer;">
												                    <td><i class="fa fa-plus"></i></td>
												                    <td>'.$value_p[nom].'</td>
												                    <td>'.$value_p[estatprojecte].'</td>
												                    <td style="text-align:center;">'.$value_p[tantpercent].'</td>
												                    <td style="text-align:center;"><b>'.($value_p["inici"]!="00/00/0000"?$value_p["inici"]:"").'</b></td>
												                    <td style="text-align:center;"><b>'.($value_p["final"]!="00/00/0000"?$value_p["final"]:"").'</b></td>
												                    <td style="text-align:center;">'.$value_p[contafites].'</td>
												                    <td><button type="button" class="btn btn-primary btn-xs novetats" data-id="'.$value_p[id].'"
												                       data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalprojectes"
												                     style="padding: 0px 16px;"><i class="fa fa-search" aria-hidden="true"></i></button></td>
												                  </tr>
												                  <tr style="display:none;" class="proj_'.$value_p[id].'">
												                    <td colspan="8">
												                      <div class="row">
												                          <div class="col-lg-12">

												                              <div class="panel panel-info">
												                               
												                                  <div class="panel-body">
												                                      <table class="table table-striped table-hover ">
												                                        <thead>
												                                          <tr>
												                                            <th>#</th>
												                                            <th>Subprojecte</th>
												                                            <th>Accions</th>
												                                            <th>Estat</th>
												                                          </tr>
												                                        </thead>
												                                        <tbody>
												            ';

												                                        $tedades = false;

												                                        if (!empty($Actuacions))
												                                        {
												                                        	foreach ($Actuacions as $key_a => $value_a) 
																							{
																								if ($value_a[clau_projecte] == $value_p[id])
																								{

													                                          	$Dades .= '
													                                              <tr class="mostartasques" data-id="'.$value_a[id].'" style="cursor:pointer;">
													                                                <td><i class="fa fa-plus"></i></td>
													                                                <td>'.$value_a[titol].'</td>
													                                                <td>
													                                            ';
													                                                    $contatasques = $contatasquestancades = $contatasquespendents = 0;
													                                                    foreach ($Tasques as $key_t => $value_t) {
													                                                    	if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
													                                                    		if ($value_t[estat]==1 or $value_t[clau_projecte]==3) $contatasquex++;
													                                                    		if ($value_t[estat]==2 or $value_t[clau_projecte]==6) $contatasquestancades++;
													                                                    		if ($value_t[estat]==4 or $value_t[clau_projecte]==5 or $value_t[estat]==0) $contatasquespendents++;
													                                                    	}
													                                                    }
													                                                       
													                                                    $contatasques = $contatasques + $contatasquestancades + $contatasquespendents;
													                                            $Dades .= $contatasques.' 
													                                                </td>
													                                                <td>'.$value_a[estatactuacio].'</td>
													                                              </tr>
													                                              <tr style="display:none;" class="tasc_'.$value_a[id].'">
													                                                <td colspan="4">
													                                                    <div class="row">
													                                                        <div class="col-lg-12">

													                                                            <div class="panel panel-primary">
													                                                                <div class="panel-body">
													                                                                    <table class="table table-striped table-hover ">
													                                                                      <thead>
													                                                                        <tr>
													                                                                          <th>Acció</th>
													                                                                          <th>Edició</th>
													                                                                        </tr>
													                                                                      </thead>
													                                                                      <tbody>
																								';
													                                                                        $tedades2 = false;
													                                                                        foreach ($Tasques as $key_t => $value_t) {
													                                                    						if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
														                                                                            $Dades .= '
														                                                                            <tr>
														                                                                                <td>'.$value_a[titol].'</td>
														                                                                            </tr>
														                                                                            ';
														                                                                            $tedades2 = trues;
													                                                                        	}
													                                                                        }
													                                                                        if ($tedades2 == false){
																					                                        	$Dades .= '
																					                                        		<tr>
																						                                                <td colspan="2">Sense accions.</td>
																						                                              </tr>
																					                                        	';
																					                                        }
													                                                                        
													                                            $Dades .= '
													                                                                      </tbody>
													                                                                    </table> 
													                                                                </div>
													                                                            </div>

													                                                        </div>
													                                                    </div>
													                                                </td>
													                                              </tr>
													                                              ';
													                                              $tedades = true;
													                                          	}
													                                        }
												                                        }
												                                        

												                                        if ($tedades == false){
												                                        	$Dades .= '
												                                        		<tr>
													                                                <td colspan="2">Sense propostes.</td>
													                                              </tr>
												                                        	';
												                                        }
												                                         
												                                        
												            $Dades .= '
												                                        </tbody>
												                                      </table> 
												                                  </div>
												                              </div>

												                          </div>
												                          
												                      </div>
												                    </td>
												                  </tr>
												                  <tr style="display:none;">
												                    <td colspan="5">
												                    </td>
												                  </tr>
												            ';
										              
										              	}

										            }
										            
										        $Dades .= '      
										            </tbody>
										          </table> 
										          
										        ';
									$Dades .= '
											 </div>
					                      </div>
					                    </td>
					                </tr>
					                ';
								
								}
					      	}

					      	if ($captaula == true){
					      		$Dades .= '
						          </tbody>
						        </table> 
						        ';
					        }*/


					      	// Altres. (Vinculació 0)
					      	/*

					      	$Dades .= '

					      	<hr>

					        <div class="row">
					            <div class="col-lg-12">
					             <h4><i class="fa fa-tasks" style="color:#cd0d2d;"></i> Altres <span><b><a href="../6/projectes.html" style="color:#cd0d2d;">PROJECTES</a></b></span> que gestiones:  </h4>
					            </div>

					        </div>

					        ';

					        $captaula = false;

					        foreach ($Vinculacio4 as $key_v => $value_v) 
							{
								if ($value_v[clau_cero] == 1)
								{
									if ($captaula == false){
									$Dades .= '
							        
							        <div class="row act_'.$value_v[id].'">
							          <div class="col-lg-12">
							          <table class="table table-striped table-hover ">
							            <thead>
							              <tr>
							                <th>#</th>
							                <th>Codi</th>
							                <th>Nom projecte</th>
							                <th>Estat</th>
							                <th>% execució</th>
							                <th>Finalització</th>
							                <th>Subprojectes</th>
							                <th>Accions</th>
							              </tr>
							            </thead>
							            <tbody>
							        ';
							        $captaula = true;
							    	}
						              	$idsproj = array();
							              if (!empty($value_v[idsproj])){
							              	$idsproj = explode(",", $value_v[idsproj]);
						              	}

						              	foreach ($DadesProjectes as $key_p => $value_p) 
										{
											if (in_array($value_p[id], $idsproj))
											{

												$Dades .= '
									                  <tr class="mostarprojecte" data-id="'.$value_p[id].'" style="cursor:pointer;">
									                    <td><i class="fa fa-plus"></i></td>
									                    <td>'.$value_p[codi].'</td>
									                    <td>'.$value_p[nom].'</td>
									                    <td>'.$value_p[estatprojecte].'</td>
									                    <td>'.$value_p[tantpercent].'</td>
									                    <td><b>'.($value_p["final"]!="00/00/0000"?$value_p["final"]:"").'</b></td>
									                    <td>'.$value_p[contaactuacions].'</td>
									                    <td>'.$value_p[contatasques].'</td>
									                  </tr>
									                  <tr style="display:none;" class="proj_'.$value_p[id].'">
									                    <td colspan="7">
									                      <div class="row">
									                          <div class="col-lg-12">

									                              <div class="panel panel-info">
									                               
									                                  <div class="panel-body">
									                                      <table class="table table-striped table-hover ">
									                                        <thead>
									                                          <tr>
									                                            <th>#</th>
									                                            <th>Subprojecte</th>
									                                            <th>Accions</th>
									                                            <th>Estat</th>
									                                          </tr>
									                                        </thead>
									                                        <tbody>
									            ';

									                                        $tedades = false;

									                                        foreach ($Actuacions as $key_a => $value_a) 
																			{
																				if ($value_a[clau_projecte] == $value_p[id])
																				{

									                                          	$Dades .= '
									                                              <tr class="mostartasques" data-id="'.$value_a[id].'" style="cursor:pointer;">
									                                                <td><i class="fa fa-plus"></i></td>
									                                                <td>'.$value_a[titol].'</td>
									                                                <td>
									                                            ';
									                                                    $contatasques = $contatasquestancades = $contatasquespendents = 0;
									                                                    foreach ($Tasques as $key_t => $value_t) {
									                                                    	if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
									                                                    		if ($value_t[estat]==1 or $value_t[clau_projecte]==3) $contatasquex++;
									                                                    		if ($value_t[estat]==2 or $value_t[clau_projecte]==6) $contatasquestancades++;
									                                                    		if ($value_t[estat]==4 or $value_t[clau_projecte]==5 or $value_t[estat]==0) $contatasquespendents++;
									                                                    	}
									                                                    }
									                                                       
									                                                    $contatasques = $contatasques + $contatasquestancades + $contatasquespendents;
									                                            $Dades .= $contatasques.' 
									                                                </td>
									                                                <td>'.$value_a[estatactuacio].'</td>
									                                              </tr>
									                                              <tr style="display:none;" class="tasc_'.$value_a[id].'">
									                                                <td colspan="4">
									                                                    <div class="row">
									                                                        <div class="col-lg-12">

									                                                            <div class="panel panel-primary">
									                                                                <div class="panel-body">
									                                                                    <table class="table table-striped table-hover ">
									                                                                      <thead>
									                                                                        <tr>
									                                                                          <th>Acció</th>
									                                                                          <th>Edició</th>
									                                                                        </tr>
									                                                                      </thead>
									                                                                      <tbody>
																				';
									                                                                        $tedades2 = false;
									                                                                        foreach ($Tasques as $key_t => $value_t) {
									                                                    						if ($value_t[clau_projecte] == $value_p[id] && $value_t[clau_actuacio] == $value_a[id]){
										                                                                            $Dades .= '
										                                                                            <tr>
										                                                                                <td>'.$value_a[titol].'</td>
										                                                                            </tr>
										                                                                            ';
										                                                                            $tedades2 = trues;
									                                                                        	}
									                                                                        }
									                                                                        if ($tedades2 == false){
																	                                        	$Dades .= '
																	                                        		<tr>
																		                                                <td colspan="2">Sense accions.</td>
																		                                              </tr>
																	                                        	';
																	                                        }
									                                                                        
									                                            $Dades .= '
									                                                                      </tbody>
									                                                                    </table> 
									                                                                </div>
									                                                            </div>

									                                                        </div>
									                                                    </div>
									                                                </td>
									                                              </tr>
									                                              ';
									                                              $tedades = true;
									                                          	}
									                                        }

									                                        if ($tedades == false){
									                                        	$Dades .= '
									                                        		<tr>
										                                                <td colspan="2">Sense propostes.</td>
										                                              </tr>
									                                        	';
									                                        }
									                                         
									                                        
									            $Dades .= '
									                                        </tbody>
									                                      </table> 
									                                  </div>
									                              </div>

									                          </div>
									                          
									                      </div>
									                    </td>
									                  </tr>
									                  <tr style="display:none;">
									                    <td colspan="5">
									                    </td>
									                  </tr>
									            ';
							              
							              	}

							            }
							            
								
								}
					      	}

					      	if ($captaula == true){
					      		 $Dades .= '      
						            </tbody>
						          </table> 
						          </div>
						        </div>
						        ';
					      	}
					      	*/



					       $Dades .= '  
					         <script type="text/javascript">
					          $(document).ready(function($) {

					                  
					                  $(document).off("click",".mostrarelacionat").on("click",".mostrarelacionat",function(event){

					                        var id = $(this).data( "id" );
					                        
					                        $(".act_"+id).toggle("fast");

					                        $(this).find(".fa-plus").toggleClass("fa-minus");
											
											/*
					                        var color = $(this).css("background-color");
					                        if (color == "rgb(238, 4, 44)"){
					                            $(this).css("background-color", "#ffffff");
					                            $(this).css("color", "#3e3f3a");
					                            $(this).find(".edicio").css("background-color","#55ad45");

					                        }else{
					                            $(this).css("background-color", "#ee042c");
					                            $(this).css("color", "#ffffff");
					                            $(this).find(".edicio").css("background-color","#000000");
					                            $(this).css("border","#3e3f3a !important");
					                        }
					                        */
					                        
					                        

					                  });

					               
					                  $(document).off("click",".mostarprojecte").on("click",".mostarprojecte",function(event){

					                        var id = $(this).data( "id" );
					                        
					                        $(".proj_"+id).toggle("fast");

					                        $(this).find(".fa-plus").toggleClass("fa-minus");
					                        

					                  });

					                  $(document).off("click",".mostartasques").on("click",".mostartasques",function(event){

					                        var id = $(this).data( "id" );
					                        
					                        $(".tasc_"+id).toggle("fast");

					                        $(this).find(".fa-plus").toggleClass("fa-minus");
					                        

					                  });


									$(document).off("click",".novetats").on("click",".novetats",function(event){

											event.preventDefault();
				
					                        var id = $(this).data( "id" );
					                        /*
					                        $(".mostrarmodal").html("<img src=\'../images/loadingg.gif\'/>");
		                                  	$(".mostrarmodal").load("../load",{o:35, id:id }, function(){

			                                    $("#myModalLabel").html("Novetat/Observació més recent");
			                                    

			                                }); */

											
			                                  
		                                  	$(".mostrarmodal").html("<img src=\'../images/loadingg.gif\'/>");
		                                  	$(".mostrarmodal").load("../load",{o:3, id:id, t: 1, c:{co:1  } }, function(){

			                                        
		                                        $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmplens" ).attr(\'disabled\', \'disabled\');
		                                       
		                                        $("#myModalLabel").html("Detalls");
		                                        
		                                        $(\'.mostrarmodal\').find(\'.btn\').hide();

		                                        setTimeout(function () {
		                                            $(\'.mostrarmodal\').find(\'.btn\').hide();
		                                            $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                                        }, 1500);
		                                        
		                                        setTimeout(function () {
		                                            $(\'.mostrarmodal\').find(\'.btn\').hide();
		                                            $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                                        }, 2000);

		                                        setTimeout(function () {
		                                            $(\'.mostrarmodal\').find(\'.btn\').hide();
		                                            $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                                        }, 4000);

		                                        setTimeout(function () {
		                                            $(\'.mostrarmodal\').find(\'.btn\').hide();
		                                            $(\'.mostrarmodal\').find(\'.btn-success\').show();
		                                        }, 5000);


		                                  }); 
					                        
					                        

					                 });


									$(document).off("click",".detallsfites").on("click",".detallsfites",function(event){

											event.preventDefault();

					                        var id = $(this).data( "id" );
					                        
					                        $(".mostrarmodal").html("<img src=\'../images/loadingg.gif\'/>");
		                                  	$(".mostrarmodal").load("../load",{o:36, id:id }, function(){

			                                    $("#myModalLabel").html("Detalls de les fites del projecte");
			                                    

			                                }); 
					                        
					                        

				                 	});


				                 	$(document).off("click",".export").on("click",".export",function(event){ 

				                 		$("#modalprojectes").modal();
				                 		$(".mostrarmodal").html("No tanqui la finestra, aquest procés pot trigar uns minuts.<br>Si finalitzat el procés, la descàrrega no comença automàticament faci click al botó \"DESCARREGAR\"<br><img src=\'../images/loadingdoc.gif\'/>");

				                 		$("#myModalLabel").html("Generant document");

		                            	var t = $(this).data("t");
		                            	$("#export").val(t);
		                            	$("#frmfiltrar").submit();

		                            
		                            });


					                $(document).off("click",".export_ind").on("click",".export_ind",function(event){ 

		                            	var t = $(this).data("id");
		                            	$("#export_ind").val(t);
		                            	$("#frmfiltrar").submit();

		                            
		                            });
					                


					          });
					        </script>

					    	';
					    }

	   $Dades .= '       		 	
			        
	        </div>
        </div>

      	<script type="text/javascript">
          $(document).ready(function($) {

          		function format(id){

          			return "<div id=\"rowv_"+id+"\" width=\"100%\"><img src=\'../images/loading.gif\'/></div>";

          		}
        ';			
        			
         $Dades .= '
						
        
              

		 
              var table = $(".dataTables2").DataTable({
                "language": {
                    "url": "../js/plugins/dataTables/dataTables.catala.lang"
                }
              });

              // Add event listener for opening and closing details
              $(".dataTables2").on("click", "tr.details-control", function () {
                  var tr = $(this).closest("tr");
                  var row = table.row(tr);

                  if (row.child.isShown()) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass("shown");
                  } else {
                      // Open this row
                      row.child(format(tr.data("child-id"))).show();
                      tr.addClass("shown");
                      var idtr= tr.data("child-id");
                      $.ajax({
	                    type: "POST",
	                    url: "../load",
	                    data: "o=37&id="+tr.data("child-id")+"&f='.$_GET['f'].'",
	                    beforeSend:function(){
	                        
	                    },
	                    error: function(jqXHR, textStatus, errorThrown){
	                        // Error.
	                    },
	                    success: function(data){
	                    	 $("#rowv_"+idtr).html(data);
	                    }
	                });

                  }
              });


				$(".dataTables2").on("page.dt", function(){
				    var info = table.page.info();
				    $("#paginagant").val(info.page + 1);
					$( "#frmgant" ).submit();
				});
                 


          });
        </script>

     ';