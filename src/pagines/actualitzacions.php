<?php 

	$idpagina = 84;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	if ( $app['session']->get(constant('General::nomsesiouser')."-permisos") == 1 )
	{
		
		$condiciovinc = " FIND_IN_SET(concat(pv.clau_pam,'_',pv.id) ,REPLACE( REPLACE( REPLACE( p.vinculacio,'[', ''),']' ,'') ,'\"','')) > 0 ";

		
		$DadesProjectes =  $dbb->FreeSql("SELECT p.titol_ca as nom, UNIX_TIMESTAMP(p.inserted) as inserted, UNIX_TIMESTAMP(p.updated) as updated,
										  IFNULL(pv.titol_ca,'') as actuacio, IFNULL(pv2.titol_ca,'') as pampad, p.user_inserted, p.user_updated
										  FROM pfx_projectes p
										  LEFT JOIN pfx_projectes_vinculacio pv ON pv.nivell = 4 AND $condiciovinc 
										  LEFT JOIN pfx_projectes_vinculacio pv2 ON pv.clau_pam = pv2.id
										  ",array());
	

		$Dades = '
			
			<div class="table-responsive"  style="overflow: hidden !important;">
                <table class="table table-striped table-bordered table-hover dataTables1 dataTables">
                    <thead>
                        <tr>
                            <th>Nom projecte</th>
                            <th>Objectiu</th>
                            <th>PDA al qual pertanyen</th>
                            <th>Última actualització</th>
                            <th>Usuari</th>
                        </tr>
                    </thead>   
                    <tbody>
        ';

                        foreach ($DadesProjectes as $key => $value) 
                        {	
                        	//$dies90 = 90*24*60*60;
                        	//$Modi90 = "No";
                        	//if (time()-$value[inserted] <=$dies90) $Modi90 = "Si";
                        	//if (time()-$value[updated] <=$dies90) $Modi90 = "Si";

                        	$Modi90 = ($value[updated]>$value[inserted] && !empty($value[updated]) ?$value[updated]:$value[inserted]);

                        	$idusuari = (!empty($value[user_updated])? $value[user_updated] : $value[user_inserted]);

                        	$LlistatUsuaris = $dbb->Llistats("usuaris"," AND t.id = :id ", array("id" => $idusuari), "id");

                        	$nomusuari = (!empty($LlistatUsuaris)? $LlistatUsuaris[1][nom]." ".$LlistatUsuaris[1][cognoms] : "");

                        	$Dades .= ' 
								
							 	<tr class="odd">
	                                <td>
	                                    '.$value[nom].'
	                                </td>
	                                <td>
	                                    '.$value[actuacio].'
	                                </td>
	                                <td>
	                                    '.$value[pampad].'
	                                </td>
	                                <td>
	                                   '.date("d/m/Y",$Modi90).' 
	                                </td>
	                                <td>
	                                   '.$nomusuari.' 
	                                </td>
	                            </tr> 

                        	';
                        }

        $Dades .= '                  
                    </tbody>
                </table>
            </div>
			
					
		';

		$js = '
			
			$(".dataTables1").dataTable( {
                "language": {
                    "url": "'.$url.'/js/plugins/dataTables/dataTables.catala.lang"
                },
                "pageLength": 200
            } );

		';
	}
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'Dades' => $Dades,
		'js' => $js,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

