<?php
exit();
	$idpagina = 6;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';
	
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Clases Php
	require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel.php");
	require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel/Reader/Excel2007.php");

	error_reporting(E_ALL ^ E_NOTICE);
	ini_set("display_errors", 1);


		
	//Directori de fitxers de càrrega.
	$max_allowed_file_size = 100000; // size in KB 
	$allowed_extensions = array("xls");
	$upload_folder = "./carregues/fitxers/";
	$fitxers = 1;

	$errors ='';

	$db = new Db();

	//Recorrem carpeta per llegir els fitxers.
	if (is_dir($upload_folder)) 
	{
		if ($gd = opendir($upload_folder)) 
		{
			while ($archivo = readdir($gd)) 
			{ 

				$extencio = substr($archivo, strrpos($archivo,".") + 1);
				$nomfile = basename($archivo, ".xls");


				if ( ($extencio == "xls"))
				{
					
					// Carregar document.
					$objReader = new PHPExcel_Reader_Excel5(); // PER xlsx: PHPExcel_Reader_Excel2007
					//cargamos el archivo excel(extensi�n *.xlsx)
					$objPHPExcel = $objReader->load($upload_folder.$archivo);
					// Asignamos el excel activo
					$objPHPExcel->setActiveSheetIndex(0);

					



					$i=3; //Si existiera una fila con los t�tulos inicial $i=2
					//Recorremos las filas del excel


					echo "<u>$archivo</u><br>";

					$comptadorlinia = 0;
					$comptadorproductes = 0;
					$coleccioerrors = "";
					$mostrarerrors = false;
					$error = false;	

					while(trim($objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue()) != '' )
					{

						$tipuserror = "";
						$error = false;	

						$P_ID         = trim($objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue());
						if ($P_ID > "1034") exit(); 
						$nomscentres  = trim($objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue());
						$nomsvinc     = $objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue();
						$NOM          = $objPHPExcel->getActiveSheet()->getCell("D".$i)->getValue();
						$DESC         = $objPHPExcel->getActiveSheet()->getCell("E".$i)->getValue();
						$resp         = $objPHPExcel->getActiveSheet()->getCell("F".$i)->getValue();
						//$equip        = $objPHPExcel->getActiveSheet()->getCell("G".$i)->getValue();
						$rec          = $objPHPExcel->getActiveSheet()->getCell("H".$i)->getValue();
						$inici        = $objPHPExcel->getActiveSheet()->getCell("I".$i)->getValue();
						$final        = $objPHPExcel->getActiveSheet()->getCell("J".$i)->getValue();
						$TANTPERCENT  = $objPHPExcel->getActiveSheet()->getCell("L".$i)->getValue();
						$estat        = $objPHPExcel->getActiveSheet()->getCell("M".$i)->getValue();
						$NOVETAT      = $objPHPExcel->getActiveSheet()->getCell("N".$i)->getValue();

						// Centres.
						//$centres = explode("-", $nomscentres);
						$ambits = array();
						//if (!empty($centres)){
							//foreach ($centres as $key => $value) {
								$value = $nomscentres;
								$value = str_replace("'", "&#39;", $value);
								$centreassociat = $dbb->Llistats("projectes_organs_e"," AND t.titol_ca = \"$value\" ", array(), "titol_ca");
								if (!empty($centreassociat)) array_push($ambits, $centreassociat['1']['id']);
							//}
						//}
						if (empty($ambits)) echo 'Centre buit: '.$P_ID."<br><br>";

						// Vinculació.
						preg_match('/(\d+(\.\d+)*)/', $nomsvinc, $codivinc);
						$vinculacions = array();
						$dadesr = array();
						if (!empty($codivinc)){
							$vinculacionsassoc = $dbb->Llistats("projectes_vinculacio"," AND t.titol_ca like '%".$codivinc[0]."%' ", array(), "titol_ca");
							if (!empty($vinculacionsassoc)) {
								array_push($vinculacions, $vinculacionsassoc['1']['clau_pam']);
								array_push($vinculacions, $vinculacionsassoc['1']['clau_eix']);
								array_push($vinculacions, $vinculacionsassoc['1']['parent_id']);
								array_push($vinculacions, $vinculacionsassoc['1']['id']);
								$dadesr['id_pam'] = $vinculacionsassoc['1']['clau_pam'];
				                $dadesr['id_eix'] = $vinculacionsassoc['1']['clau_eix'];
				                $dadesr['id_linia'] = $vinculacionsassoc['1']['parent_id'];
				                $dadesr['id_act'] = $vinculacionsassoc['1']['id'];
							}
							
						}
						if (empty($vinculacions)) echo 'Vinculacio buit: '.$P_ID."<br><br>";

						// Responsable.
						$RESPONSABLE = "";
						if (!empty($resp)){
							$responsableassoc = $dbb->Llistats("usuaris"," AND concat(nom,' ',cognoms) = \"$resp\" ", array(), "nom");
							if (!empty($responsableassoc)) $RESPONSABLE = $responsableassoc[1]['id'];
							
						}
						if (empty($RESPONSABLE)) echo 'Responsable buit: '.$P_ID."<br><br>";

						// Recursos.
						$RECURSOS = "";
						if (!empty($rec)){
							$idresp = $dbb->Llistats("paraules"," AND codi='recursosprojecte' AND text = \"$rec\" ", array(), "codi");
							if (!empty($idresp)) $RECURSOS = $idresp[1]['clau'];
							
						}
						if (empty($RECURSOS)) echo 'Recursos buit: '.$P_ID."<br><br>";

						// Inici.
						$inici = explode("/",$inici);
						$DATAINICI = date("$inici[2]-$inici[1]-$inici[0]");
						
						// FINAL
						$final = explode("/",$final);
						$DATAFINAL = date("$final[2]-$final[1]-$final[0]");

						// Estat.
						$IDESTAT = "";
						if (!empty($estat)){
							$idresp = $dbb->Llistats("paraules"," AND codi='estatprojecte' AND text = \"$estat\" ", array(), "codi");
							if (!empty($idresp)) $IDESTAT = $idresp[1]['clau'];
							
						}
						if (empty($IDESTAT)) echo 'Estat buit: '.$P_ID."<br><br>";

							echo "Guardant projecte: $P_ID - $NOM<br>";
						if ($error == false)
						{
							// Alta.
							echo "Guardant projecte: $P_ID - $NOM<br>";

							$data = array();
							$data['id'] = $P_ID;
							$data['ambits'] = json_encode($ambits);
							$data['vinculacio'] = json_encode($vinculacions);
							$data['titol_ca'] = str_replace("'", "`",str_replace('"', '`',$NOM));
							$data['descripcio_ca'] = $DESC;
							$data['cap_projecte'] = $RESPONSABLE;
							$data['recursos'] = $RECURSOS;
							$data['inici'] = $DATAINICI;
							$data['final'] = $DATAFINAL;
							$data['tantpercent'] = trim(str_replace("-", "",str_replace("%", "", $TANTPERCENT)));
							$data['estat'] = $IDESTAT;
							$data['estat'] = $IDESTAT;


							$fields     =  array_keys($data);
							$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
							$sql 		= "INSERT INTO pfx_projectes (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")";
							
			                $db->query("$sql",$data); 
			                $ultimid = $db->lastInsertId();

			                // Relacions.
			                $dadesr['id_proj'] = $ultimid;
			                /* definit on busca la vinculació
			                $dadesr[id_pam] = 
			                $dadesr[id_eix] = 
			                $dadesr[id_linia] = 
			                $dadesr[id_act] = 
			                */
							$arrayguardarr = array('taula' =>  'projectes_relacionsvinc', 
												  'dades' =>  $dadesr,
												  'tipus' =>  'a',);
							$dbb->GuardarRegistre2($arrayguardarr);

							// Novetat.
							$dadesn = array();
							$dadesn["datai"] = date("Y-m-d 00:00:00");
							$dadesn["descripcio_ca"] = $NOVETAT;
							$dadesn["clau_projecte"] = $ultimid;
							$dadesn["clau_pla"] = 1;
							$dadesn["estat"] = 1;

							$arrayguardarr = array('taula' =>  'projectes_novetats', 
												  'dades' =>  $dadesn,
												  'tipus' =>  'a',);
							$dbb->GuardarRegistre2($arrayguardarr);

						
							$comptadorproductes++;

							echo '
					        <script>
								x = 0;  //horizontal coord
								y = document.height; //vertical coord
								window.scroll(x,y);
					        </script>';
						}

						if ($error==true)
						{
							$mostrarerrors = true;
							$coleccioerrors .= $tipuserror."";
						}

		                flush();

						$i++;
					}

					

				
					echo "Total productes: $comptadorproductes <br>";

					echo $coleccioerrors;

					
			
				}


			
			}
			closedir($gd);
			echo "Final càrrega.";
			$final = true;
		}
	}
	

	
?>