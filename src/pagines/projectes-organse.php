<?php 

	$idpagina = 30;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	
	// JS.

	$js = '

			$(".nou").click(function(){

                if ($("#ido").val() != "")
                {   
                    $("#ido").val("");
                    $("INPUT:text, INPUT:password, INPUT:file, SELECT, TEXTAREA", "#frmprojectesorig").val("");  
                	$("INPUT:checkbox, INPUT:radio", "#frmprojectesorig").removeAttr("checked").removeAttr("selected");
                    $("div.errorprojectesorig span").html("");
                    $("#resultprojectesorig").html("");
                    $(".panelldadesprojectesorig").toggle();
                	$("input[name=estat]").prop("checked", true);
                	$("#resultprojectesorig").html("");
                }else{
                    $(".panelldadesprojectesorig").toggle();
                }

            });


			$(".llistat_16").load("'.$url.'/load",{o:2,id:1,t:"16"}, function(){
                    
            });


	';
	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'js' => $js,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

