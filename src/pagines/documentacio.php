<?php 

	$idpagina = 88;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';

	// Validar accès a la pàgina.
	$dbb->AreaPrivada($idpagina);

	// Dades Pàgina.
	$dbb->Pagines($idpagina);

	// Tocken de seguretat.
	$app['session']->set('tockenseguretat', makeToken());

	
	// JS.

	$js = ' 

		

	';

	$Documentacio =  $dbb->FreeSql("SELECT u.nomfile, u.id as id, d.titol_ca, d.descripcio_ca
									  
								  FROM pfx_documentacio d
								  LEFT JOIN pfx_upfile u ON u.clau_id = d.id AND u.taula = 'documentacio'
								  WHERE d.estat = 1 
								  GROUP BY u.id
								  ");

	if (!empty($Documentacio)){
		foreach ($Documentacio as $key => $value) {
			

			$ranid = base64_encode($value["id"]);

			$tocken = md5('TockenSecretViewDBB!'.$value["id"]);

			$Documentacio[$key]['ranid'] = $ranid;
			$Documentacio[$key]['tocken'] = $tocken;

		}
	}


	
	
	$dadesplantilla = array(
		
		'Pagines' => $Pagines,
		'Documentacio' => $Documentacio,
		'js' => $js,
		
	);

	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

