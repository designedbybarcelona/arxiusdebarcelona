<?php
	
	//ini_set('display_errors', 0);

	ini_set('max_execution_time', 30); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Path secret.
    $imagePATHsecret=pathfiles."/../../secretfiles/tmp/";
	
	function myFlush() {
	    echo(str_repeat(' ', 256));
	    if (@ob_get_contents()) {
	        @ob_end_flush();
	    }
	    flush();
	}

	if (ob_get_level() == 0) ob_start();


	// Generar PDF.
 	//if ($exportget_ind == 1){


 		//echo "Generant PDF...<br>";
		//myFlush();

		

			// Inici creació PDF.

			// Include the main TCPDF library (search for installation path).
		    require_once( __DIR__.'/../../js/tcpdf/tcpdf.php');

		 	class MYPDF extends TCPDF {

			    //Page header
			    public function Header() {

			    	if ($this->tocpage) {
			            // *** replace the following parent::Header() with your code for TOC page
			            //parent::Header();

			        } else { 
			        	// *** replace the following parent::Header() with your code for normal pages
			        }
			            
		            // Logo
			        $image_file = pathfiles."/../images/".'logopdf.png';
			        $this->Image($image_file, 10, 5, 30, '');
			        // $this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

			        $dbb = new General($idioma, $app);
			        $DadesGenerals = $dbb->Llistats("general","",array(),'id');

			        $this->SetFont('helvetica', 'B', 7);
			        $this->SetTextColor(255, 0, 0);
			        $this->SetXY(158, 10);
			        $this->Cell(60, 0, $DadesGenerals[1][descripcio_ca], 0, false, 'C', 0, '', 0, false, 'M', 'M');
			        $this->SetTextColor(0, 0, 0);
			        $this->SetXY(107, 15);
			        $this->Cell(100, 0, 'Projecte', 0, false, 'C', 0, '', 0, false, 'M', 'M');

					$this->SetXY(7, 20);
					$this->setCellHeightRatio(0.2);
					$this->SetDrawColor(0,0,0);
					$this->Cell(195, 0,'', '', 1, 'C', 1, '', 0, false, 'T', 'C');
		       
			        
			    }

			    // Page footer
			    
			    public function Footer() {
			    	
			        if ($this->tocpage) {
			            // *** replace the following parent::Footer() with your code for TOC page
			            //parent::Footer();
			        } else {
			            // *** replace the following parent::Footer() with your code for normal pages
			            // Position at 15 mm from bottom
		        		$this->SetY(-15);
				        // Set font
				        $this->SetFont('helvetica', 'I', 8);
				        // Page number
				        $this->Cell(0, 20, 'Pàgina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		        	
				        
			        }
			    }
			    
			}
		  

		    // create new PDF document
		    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		    // Path imatges.
		    $imagePATH=pathfiles."/../../images/";

		    // set document information
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor($app['translator']->trans('Projectes'));
		    $pdf->SetTitle($app['translator']->trans('Projectes'));
		    $pdf->SetSubject($app['translator']->trans('Projectes'));
		    $pdf->SetKeywords($app['translator']->trans('Projectes'));

		    //$pdf->startPage( $orientation = '',$format = '', true );
		   
		    //$pdf->SetHeaderData("../../images/logo.jpg", '50', '', ""); 
		    //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, "Seguiment de projectes");
		    $pdf->SetHeaderData('','','','');

		    $pdf->setPrintHeader(false);
		    //$pdf->setPrintHeader();
		    // set header and footer fonts
		    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
		    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		    $pdf->setPrintFooter(false);

		    // set default monospaced font
		    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		    // set margins
		    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetMargins(7, 7, 7);
		    $pdf->SetHeaderMargin(0);
		    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		    $pdf->SetFooterMargin(0); // PDF_MARGIN_FOOTER

		    // set auto page breaks
		    $pdf->SetAutoPageBreak(TRUE, 5);

		    // set image scale factor
		    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		    // set some language-dependent strings (optional)
		    //if (@file_exists(dirname(__FILE__).'/lang/esp.php')) {
		    //  require_once(dirname(__FILE__).'/lang/esp.php');
		    //  $pdf->setLanguageArray($l);
		    //}

		    $tbl = "";


				 // ---------------------------------------------------------

		    // -- Portada.

		    $pdf->AddPage();
			$pdf->SetFont('helvetica', 'B', 12);
			//$pdf->SetFontSize(20);

			$image_file = pathfiles."/../images/".'logopdf.png';
		    $pdf->Image($image_file, 4, 4, 30, '');

			

		

		
		$caps = false;



		$condicioprojectes_ind .= " AND p.id = '".intval($exportget_ind)."'";
		

		$DadesProjectes_ind =  $db->query("SELECT p.id, p.titol_ca as nom, p.descripcio_ca, p.ambits, p.arees, p.estrategic,
										   p.director, p.cap_projecte, p.usuaris, p.recursos, p.operadors, p.motivacio, p.impactes,
										   DATE_FORMAT(p.inici, '%d/%m/%Y') as inici, DATE_FORMAT(p.final, '%d/%m/%Y') as final,
										   p.tantpercent, p.estat
									  
										  FROM pfx_projectes p
										 
										  WHERE 1=1 $condicio_permis $condicioprojectes_ind 
										  ",array());  


		if (!empty($DadesProjectes_ind))
		{
			

			
			
			$taula .= '
					    
					';

			foreach ($DadesProjectes_ind as $key_exp => $value_exp) 
			{
				

				// Capçaleres.
				if ($caps == false)
				{
					
					$pdf->SetXY(0, 3);
					$pdf->Write(0, 'Informe projecte', '', 0, 'C', true, 0, false, false, 0);
					$pdf->SetXY(60, 2);
					$pdf->SetFont('helvetica', '', 10);
					$pdf->Write(0, 'Data informe: '.date('d/m/Y'), '', 0, 'R', true, 0, false, false, 0);
					$pdf->SetFont('helvetica', 'B', 12);
					$pdf->SetXY(0, 7);
					$pdf->Write(0, $value_exp['nom'], '', 0, 'C', true, 0, false, false, 0);

					$pdf->SetFont('helvetica', '', 7);

					$pdf->Ln(6);
					/*
					if (!empty($ConfigPla)){

						$pdf->SetXY(7, 20);
						$pdf->Write(0, str_replace("<p>", "", str_replace("</p>", "\n", $ConfigPla[1][descripcio_ca])), '', 0, 'L', true, 0, false, false, 0);

					}

					$pdf->Ln(2);*/

					$pdf->SetFillColor(255, 255, 255);
				    $pdf->SetTextColor(0,0,0);
				    $pdf->SetLineWidth(1.2);
				    $pdf->SetDrawColor(0,0,0);
					$pdf->setCellHeightRatio(2);

					$taula .= '<table cellspacing="0" cellpadding="3" border="0">';

					$caps = true;
				}

				// Vinculació.
				$Vinculacio4_ind = $db->query("SELECT rp.id_act,
												(select pv4.titol_ca from pfx_projectes_vinculacio pv4 where pv4.id = rp.id_act) as nomact
											   FROM pfx_projectes_relacionsvinc rp 
											   WHERE rp.id_proj = :id", array("id" => $value_exp['id']));

				if (!empty($Vinculacio4_ind[0]['id_act'])){
					$Vinculacio4_detall = $dbb->Llistats("projectes_vinculacio"," AND id = :id",array("id" => $Vinculacio4_ind[0]['id_act']),'id');
					$Vinculacio3_ind = $dbb->Llistats("projectes_vinculacio"," AND id = :id",array("id" => $Vinculacio4_detall[1]['parent_id']),'id');
					$Vinculacio2_ind = $dbb->Llistats("projectes_vinculacio"," AND id = :id",array("id" => $Vinculacio3_ind[1]['parent_id']),'id');
				}

				// Centre o Organ
				$Centre_ind = $db->query("SELECT po.titol_ca
										  FROM pfx_projectes_organs_e po 
										  WHERE FIND_IN_SET(po.id ,REPLACE( REPLACE( REPLACE( '".$value_exp['ambits']."' ,'[', ''),']' ,'') ,'\"','')) > 0", 
										  array());
				if (!empty($Centre_ind)){
					foreach ($Centre_ind as $key_a => $value_a) {
						$nomcentre.= (!empty($nomcentre)?", ":"").$value_a['titol_ca'];
					}
				}

				// Àrea
				$Area_ind = $db->query("SELECT pa.titol_ca
										FROM pfx_projectes_area pa 
										WHERE FIND_IN_SET(pa.id ,REPLACE( REPLACE( REPLACE( '".$value_exp['arees']."' ,'[', ''),']' ,'') ,'\"','')) > 0", 
										array());

				if (!empty($Area_ind)){
					foreach ($Area_ind as $key_a => $value_a) {
						$nomarea.= $value_a['titol_ca']." ";
					}
				}

				// Usuaris.
				$Gestor_ind = $dbb->Llistats("usuaris"," AND id = :id",array("id" => $value_exp['cap_projecte']),'id');
				$Equip_ind = $db->query("SELECT u.nom, u.cognoms
										  FROM pfx_usuaris u
										  WHERE FIND_IN_SET(u.id ,REPLACE( REPLACE( REPLACE( '".$value_exp['usuaris']."' ,'[', ''),']' ,'') ,'\"','')) > 0", 
										  array());
				if (!empty($Equip_ind)){
					foreach ($Equip_ind as $key_a => $value_a) {
						$nomequip.= (!empty($nomequip)?", ":"").$value_a['nom']." ".$value_a['cognoms'];
					}
				}
				$Equipext_ind = $db->query("SELECT u.titol_ca
										  FROM pfx_projectes_operadors u
										  WHERE FIND_IN_SET(u.id ,REPLACE( REPLACE( REPLACE( '".$value_exp['operadors']."' ,'[', ''),']' ,'') ,'\"','')) > 0", 
										  array());
				if (!empty($Equipext_ind)){
					foreach ($Equipext_ind as $key_a => $value_a) {
						$nomequipext.=  (!empty($nomequipext)?", ":"").$value_a['titol_ca'];
					}
				}

				// Recursos.
				$Recursos_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'recursosprojecte' ",array("id" => $value_exp['recursos']),'id');
				
				// Pressupostos.
				$Pressupostos_ind = $dbb->Llistats("projectes_conceptes"," AND clau_projecte = :id ",array("id" => $value_exp['id']),'any');
				$totalpressu1 = $totalpressu2 = $totalpressu3 = 0;
				if (!empty($Pressupostos_ind)){
					foreach ($Pressupostos_ind as $key_a => $value_a) {
						$liniespressupost.= '<tr><td>'.$value_a['any'].'</td><td>'.$value_a['titol_ca'].'</td><td>'.number_format($value_a['pressupost1'],2,',','.').' €</td><td>'.number_format($value_a['pressupost2'],2,',','.').' €</td><td>'.number_format($value_a['pressupost3'],2,',','.').' €</td><td>'.number_format(($value_a['pressupost1']+$value_a['pressupost2']+$value_a['pressupost3']),2,',','.').' €</td></tr>';
						$totalpressu1 += $value_a['pressupost1'];
						$totalpressu2 += $value_a['pressupost2'];
						$totalpressu3 += $value_a['pressupost3'];

					}
				}

											  

				// Gantt
				$ActuacionsProjecte_ind = $dbb->FreeSql(
					"SELECT a.id, DATE_FORMAT(a.inici, '%d/%m/%Y') as inici, DATE_FORMAT(a.final, '%d/%m/%Y') as final, a.titol_ca as titol ,GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as operadors, IFNULL(pa.text,'----') as estat
				  FROM pfx_actuacions a
				  INNER JOIN pfx_projectes p ON p.id = a.clau_projecte
				  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( a.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
				  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( a.operadors,'[', ''),']' ,'') ,'\"','')) > 0
				  LEFT JOIN pfx_paraules pa ON a.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
				  WHERE a.clau_projecte = '".$value_exp['id']."'
				  GROUP By a.id 
				  ORDER BY a.id asc",array());

				if (!empty($ActuacionsProjecte_ind)){

					
					foreach ($ActuacionsProjecte_ind as $key_a => $value_a) {
						
						$gantt_ind .= "<tr><td>".$value_a['titol']."</td><td>".$value_a['inici']."</td><td>".$value_a['final']."</td><td>".$value_a['equip']."</td><td>".$value_a['operadors']."</td><td>".$value_a['estat']."</td></tr>";

						$TasquesProjecte_ind = $dbb->FreeSql("SELECT t.id, DATE_FORMAT(t.inici, '%d/%m/%Y') as inici, DATE_FORMAT(t.final, '%d/%m/%Y') as final, t.titol_ca as titol, t.clau_actuacio, t.estat, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u2.nom,' ',u2.cognoms),'') SEPARATOR ' | ') as equip, GROUP_CONCAT( DISTINCT IFNULL(CONCAT(u3.titol_ca),'') SEPARATOR ' | ') as operadors,
							IFNULL(pa.text,'----') as estat
						  FROM pfx_tasques t
						  INNER JOIN pfx_projectes p ON p.id = t.clau_projecte
						  LEFT JOIN pfx_usuaris u2 ON FIND_IN_SET(u2.id ,REPLACE( REPLACE( REPLACE( t.usuaris,'[', ''),']' ,'') ,'\"','')) > 0
						  LEFT JOIN pfx_projectes_operadors u3 ON FIND_IN_SET(u3.id ,REPLACE( REPLACE( REPLACE( t.operadors,'[', ''),']' ,'') ,'\"','')) > 0
						  LEFT JOIN pfx_paraules pa ON t.estat = pa.clau AND pa.codi = 'estatactuacio' AND pa.idioma = '$idioma'
						  WHERE t.clau_projecte = '".$value_exp['id']."' AND t.clau_actuacio = '".$value_a["id"]."'
						  GROUP BY t.id
						  ORDER BY t.id asc, t.titol_ca ", array());

						if (!empty($TasquesProjecte_ind))
						foreach ($TasquesProjecte_ind as $key_t => $value_t) {
							$gantt_ind .= "<tr><td>&nbsp;&nbsp;".$value_t['titol']."</td><td>".$value_t['inici']."</td><td>".$value_t['final']."</td><td>".$value_t['equip']."</td><td>".$value_t['operadors']."</td><td>".$value_t['estat']."</td></tr>";
						}
					}

					$gantt_ind = '

						<table cellspacing="0" cellpadding="3" border="0">
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td><b>Nom</b></td>
									<td><b>Data inici</b></td>
									<td><b>Data final</b></td>
									<td><b>Equip de treball intern</b></td>
									<td><b>Equip de treball extern</b></td>
									<td><b>Estat execució</b></td>
								</tr>
								'.$gantt_ind.'
								</table>
							</td>
						</tr>
						</table>

					';
				}

				// Fites.
				$FitesProjecte_ind = $dbb->Llistats("projectes_fites"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$value_exp['id']), "datai asc", false);
				if (!empty($FitesProjecte_ind)){
					foreach ($FitesProjecte_ind as $key_a => $value_a) {
						$tipufita = $dbb->Llistats("projectes_fites_tipologies"," AND id = :tipologia ",array("tipologia" => $value_a['tipologia']),'id');
			
						$liniesfites .= '<tr><td>'.date('d/m/Y',strtotime($value_a['datai'])).'</td><td>'.$tipufita['1']['titol_ca'].'</td><td>'.$value_a['observacionsfita'].'</td></tr>';
					}
				}
				

				// Camps propis estratgic.
				if ($value_exp['estrategic']==1){
					$Justificacio = '

						<table cellspacing="0" cellpadding="3" border="1">
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td><b>Justificació amb dades:</b></td>
									<td><b>Principals beneficiaris del projecte:</b></td>
								</tr>
								<tr>
									<td>'.$value_exp['motivacio'].'</td>
									<td>'.$value_exp['impactes'].'</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						
					';

					// Objectius específics.

					$Objectius_especifics_ind = $dbb->Llistats("projectes_objectius"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$value_exp['id']), "titol_ca", false);
					if (!empty($Objectius_especifics_ind)){
						foreach ($Objectius_especifics_ind as $key_a => $value_a) {
							$especfic_ind .= "<tr><td>".strip_tags($value_a['descripcio_ca'],"<br>")."</td></tr>";
						}

						$taula_especifics = '

						<table cellspacing="0" cellpadding="3" border="1">
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td><b>Objectius específics:</b></td>
								</tr>
								'.$especfic_ind.'
								</table>
							</td>
						</tr>
						</table>

						';

					}

					// Matriu de responsabilitats.
					$RegistreMatriu = $dbb->Llistats("projectes_responsabilitats"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$value_exp['id']), "observacionsfita", false);
					if (!empty($RegistreMatriu)){
						foreach ($RegistreMatriu as $key_a => $value_a) {
							$matriu_resp .= "<tr><td>".strip_tags($value_a['responsable'],"<br>")."</td><td>".strip_tags($value_a['titol_ca'],"<br>")."</td></tr>";
						}

						$matriu_responsabilitats = '

						<table cellspacing="0" cellpadding="3" border="1">
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr><td colspan="2"><b>Matriu de responsabilitats</b></td></tr>
								<tr>
									<td><b>Responsable:</b></td><td><b>Rol:</b></td>
								</tr>
								'.$matriu_resp.'
								</table>
							</td>
						</tr>
						</table>

						';

					}


					// Pla de riscos.
					$RegistreRiscos = $dbb->Llistats("projectes_riscos"," AND t.clau_projecte = :clau_projecte ",array("clau_projecte"=>$value_exp['id']), "datai", false);	
					if (!empty($RegistreRiscos)){
						foreach ($RegistreRiscos as $key_a => $value_a) {
							$riscos_ind .= "<tr><td>".date("d/m/Y",strtotime($value_a['datai']))."</td><td>".strip_tags($value_a['descripcio_ca'],"<br>")."</td></tr>";
						}

						$taula_riscos = '

						<table cellspacing="0" cellpadding="3" border="1">
						<tr>
							<td>
								<table cellspacing="0" cellpadding="0" border="0">
								<tr><td colspan="2"><b>Pla de riscos</b></td></tr>
								<tr>
									<td><b>Data:</b></td><td><b>Nom/Descripció:</b></td>
								</tr>
								'.$riscos_ind.'
								</table>
							</td>
						</tr>
						</table>

						';

					}

				
				}

				// Estat.
				$Estat_ind = $dbb->Llistats("paraules"," AND clau = :id AND codi = 'estatprojecte' ",array("id" => $value_exp['estat']),'id');

				// Indicadors.
				$IndicadorsProjecte_ind = $dbb->Llistats("projectes_indicadors_arxius"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$value_exp['id']), "id", false);
				if (!empty($IndicadorsProjecte_ind)){
					foreach ($IndicadorsProjecte_ind as $key_a => $value_a) {
					
			
						$liniesindicadors .= '<tr><td>'.$value_a['titol'].'</td><td>'.date('Y',strtotime($value_a['datai'])).'</td><td>'.$value_a['objectiu'].'</td><td>'.($value_a['assolit'] == 1 ? "Si":"No").'</td></tr>';
					}
				}

				// Observacions.
				$ObservacionsProjecte_ind = $dbb->Llistats("projectes_novetats"," AND t.clau_projecte = :clau_projecte ", array("clau_projecte"=>$value_exp['id']), "datai", false);
				if (!empty($ObservacionsProjecte_ind)){
					foreach ($ObservacionsProjecte_ind as $key_a => $value_a) {
					
						$liniesobservacions .= '<tr><td>'.date('d/m/Y',strtotime($value_a['datai'])).'</td><td>'.$value_a['descripcio'].'</td></tr>';
					}
				}

				
				
				

				$taula .= '<tr>';

				$taula .= '<td width="100%">

								<table cellspacing="0" cellpadding="3" border="1">
								<tr style="background-color:#ECECEC;font-weight: bold;" >
									<td>DADES GENERALS</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td><b>Nom del projecte:</b> <br>'.$value_exp['nom'].'</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td><b>Descripció del projecte:</b></td></tr>
											<tr><td>'.$value_exp['descripcio_ca'].'</td></tr>
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td><b>Objectiu vinculat al Pla Director:</b></td></tr>
											<tr><td>'.$Vinculacio2_ind['1']['titol'].'</td></tr>
											<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Vinculacio3_ind['1']['titol'].'</td></tr>
											<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$Vinculacio4_ind['0']['nomact'].'</td></tr>
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="3" border="0">
										<tr>
											<td><b>Centre o òrgan:</b></td>
											<td><b>Àrea impulsora:</b></td>
											'.($value_exp['estrategic']==1 ? '<td><b>Director de projecte:</b></td>':'').'
										</tr>
										<tr>
											<td>'.$nomcentre.'</td>
											<td>'.$nomarea.'</td>
											'.($value_exp['estrategic']==1 ? '<td>'.$value_exp['director'].'</td>':'').'
										</tr>
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td><b>Gestor/a del projecte:</b></td>
											<td><b>Equip de projecte intern:</b></td>
										</tr>
										<tr>
											<td>'.$Gestor_ind[1]['nom'].' '.$Gestor_ind[1]['cognoms'].'</td>
											<td>'.$nomequip.'</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td><b>Recursos:</b></td>
											<td><b>Equip de projecte extern:</b></td>
										</tr>
										<tr>
											<td>'.$Recursos_ind[1]['text'].'</td>
											<td>'.$nomequipext.'</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td colspan="6"><b>Pressupost:</b></td></tr>
											<tr><td width="10%"><b>Any:</b></td><td width="40%"><b>Concepte:</b></td><td width="10%"><b>Cap II:</b></td><td width="10%"><b>CAP IV:</b></td><td width="10%"><b>CAP VI:</b></td><td width="10%"><b>Total</b></td></tr>
											'.$liniespressupost.'
											<tr><td><b>Totals:</b></td><td></td><td><b>'.number_format($totalpressu1,2,',','.').' €</b></td><td><b>'.number_format($totalpressu2,2,',','.').' €</b></td><td><b>'.number_format($totalpressu3,2,',','.').' €</b></td><td>'.number_format(($totalpressu1+$totalpressu2+$totalpressu3),2,',','.').' €</td></tr>
										</table>
									</td>
								</tr>
								</table>

								'.$Justificacio.'

							</td>';
				


				
				$taula .= '</tr>';


				$taula .= '<tr>';

				$taula .= '<td width="100%">

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr style="background-color:#ECECEC;font-weight: bold;" >
									<td>PLANIFICACIÓ</td>
								</tr>
								</table>


								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td><b>Inici:</b> '.$value_exp['inici'].'</td>
									<td><b>Final:</b> '.$value_exp['final'].'</td>
								</tr>
								</table>

								'.$matriu_responsabilitats.'

								'.$taula_riscos.'

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td><b>Diagrama de Gantt:</b></td>
										</tr>
										<tr>
											<td>'.$gantt_ind.'</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>

								

								'.$taula_especifics.'

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td colspan="3"><b>Fites:</b></td></tr>
											<tr><td><b>Data:</b></td><td><b>Tipologia:</b></td><td><b>Nom/Descripció:</b></td></tr>
											'.$liniesfites.'
										</table>
									</td>
								</tr>
								</table>


							</td>';


				$taula .= '</tr>';



				$taula .= '<tr>';

				$taula .= '<td width="100%">

								<table cellspacing="0" cellpadding="3" border="1">
								<tr style="background-color:#ECECEC;font-weight: bold;" >
									<td>SEGUIMENT EXECUCIÓ I INDICADORS</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td><b>Estat d\'execució:</b> '.$Estat_ind[1]['text'].'</td>
									<td><b>Grau d\'execució del projecte:</b> '.$value_exp['tantpercent'].' %</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td colspan="4"><b>Indicadors de resultat:</b></td></tr>
											<tr><td><b>Definició de l\'indicador de referència:</b></td><td><b>Any:</b></td><td><b>Objectiu:</b></td><td><b>Assolit:</b></td></tr>
											'.$liniesindicadors.'
										</table>
									</td>
								</tr>
								</table>

								<table cellspacing="0" cellpadding="3" border="1" nobr="true">
								<tr>
									<td>
										<table cellspacing="0" cellpadding="0" border="0">
											<tr><td colspan="2"><b>Observacions i altres comentaris d\'interès:</b></td></tr>
											<tr><td width="20%"><b>Data:</b></td><td width="80%"><b>Nom/Descripció:</b></td></tr>
											'.$liniesobservacions.'
										</table>
									</td>
								</tr>
								</table>


								


							</td>';


				$taula .= '</tr>';

	
				
			}

			

			$taula .= '

					</table>
					';

			$pdf->writeHTMLCell(0, 0, $pdf->getX(), $pdf->getY(), $taula, 0, 0, 1, true, 'L', true);



			
		}

		

		

		// -----------------------------------------------------------------------------

			

			    //Close and output PDF document
			    //$pdf->Output('example_048.pdf', 'I');

			    //orientació L(horitzonal), P(vertical)

			    //Close and output PDF document
			    // D: download, F: Save file
			    $nomfile = 'Consulta_Individual_'.date("Y-m-d_H-i-s").'.pdf';
			    $rutafile = __DIR__."/../../../secretfiles/informes/".$nomfile;
			    $pdf->Output($rutafile,'F');

			    $ranid = base64_encode($nomfile);

				$tocken = md5('TockenSecretViewDBB!'.$nomfile);

				
				$botodesc = '
						
						<a href="'.constant("DBB::url").'/docs/'.$ranid.'/?t='.$tocken.'&i=1" target="_blank">
							<button class="btn btn-success btn-xs desc" type="button" >
								<i class="fa fa-download"></i> DESCARREGAR
							</button>
						</a><br><br><br>
						<script>
							//$(".caixablanca").hide();
							$(".desc").click();
						</script>
				';
				

				/*
			    $pdf->Output($nomfile,'D');
			    
			    $tmp = ini_get('upload_tmp_dir');

			    $fileatt = "./".$rutafile;
			    $fileatttype = "application/pdf";
			    $fileattname = $nomfile;

			    $file = fopen($fileatt, 'rb');
			    $data = fread($file, filesize($fileatt));
			    fclose($file);
			    
			    ob_end_clean();*/

			    

			    //============================================================+
			    // END OF FILE
			    //============================================================+

			

		

	

 	//}

 	// Eliminar temporals.
    $pathdel = __DIR__."/../../../secretfiles/tmp/" ;


	if ($handle = opendir($pathdel)) {

     	while (false !== ($file = readdir($handle))) {

            if (preg_match('/\.png$/i', $file)) {
			      unlink($pathdel.$file);
			}
				
	     }
   	}




	

	