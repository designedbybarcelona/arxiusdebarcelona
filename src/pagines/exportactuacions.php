<?php

	$idpagina = 6;
	
	// Variables Generals.
	require_once __DIR__.'/../app/general.php';
	
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
	ini_set('output_buffering','on');
	//ini_set('zlib.output_compression', 0);

	// Clases Php.
	require_once(__DIR__."/../app/include/class/excel/Classes/PHPExcel.php");

	// Nom fitxer.
	$nomfile = 'projectmonitor_act_'.date('YmdHis').'.csv';
	
	// Inicialitza Excel.
    $objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	$rowCount = 1;
	$colCount = 0;

	$DadesProjectes = $dbb->Llistats("projectes_vinculacio"," AND nivell = 4 AND clau_cero = 0 " ,array(""), "id", false );

	


	if (!empty($DadesProjectes)){
			
			// Capçaleres.
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "ID");
			$colCount++;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, "Titulo");
			$colCount++;

			$colCount = 0;
			$rowCount = 2;
	
			foreach ($DadesProjectes as $key => $value) 
			{
				$colCount = 0;

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[id]));
				$colCount++;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colCount, $rowCount, neteja_e($value[titol_ca]));
				$colCount++;
				
				$rowCount++;


			}

			// We'll be outputting an excel file
			header('Content-Encoding: UTF-8');
			header('Content-type: text/csv; charset=UTF-8');
			header('Content-Disposition: attachment; filename="'.$nomfile.'"');
			header("Pragma: no-cache");
			header("Expires: 0");
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
			$objWriter->save('php://output');
	}

	$dadesplantilla = array();
	foreach ($arraygeneral as $key => $value) 
	{
		$dadesplantilla[$key] = $value;
	}
	return $dadesplantilla;

	exit();